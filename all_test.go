// Copyright 2023 The libexpat-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libexpat // import "modernc.org/libexpat"

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"testing"
	"time"

	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/fileutil/ccgo"
	_ "modernc.org/libc"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
	target = fmt.Sprintf("%s/%s", goos, goarch)
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func Test(t *testing.T) {
	switch target {
	case "freebsd/amd64":
		t.Skip("TODO")
	}

	tempDir := t.TempDir()
	bin := filepath.Join(tempDir, "runtests")
	if goos == "windows" {
		bin += ".exe"
	}
	out, err := run(5*time.Minute, "", "go", "build", "-o", bin, "./"+filepath.Join("internal", goos, goarch, "tests", "runtests.go"))
	if err != nil {
		t.Fatalf("out=%s FAIL err=%v", out, err)
	}

	if out, err = run(5*time.Minute, "", bin, "-v"); err != nil {
		t.Fatalf("out=%s FAIL err=%v", out, err)
	}

	t.Logf("out=%s", out)
}

func run(limit time.Duration, inDir, bin string, args ...string) (out []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), limit)

	defer cancel()

	cmd := exec.CommandContext(ctx, bin, args...)
	cmd.Dir = inDir
	cmd.WaitDelay = 10 * time.Second
	return cmd.CombinedOutput()
}
