// Code generated for linux/s390x by 'generator --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -I /home/jnml/src/modernc.org/builder/.exclude/modernc.org/libbsd/include/linux/s390x -extended-errors -ignore-unsupported-alignment -ignore-link-errors -lbsd -o libexpat.go --package-name libexpat lib/.libs/libexpat.a', DO NOT EDIT.

//go:build linux && s390x

package libexpat

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_ARG_MAX = 131072
const m_ASCII_0 = 48
const m_ASCII_1 = 49
const m_ASCII_2 = 50
const m_ASCII_3 = 51
const m_ASCII_4 = 0x34
const m_ASCII_5 = 0x35
const m_ASCII_6 = 0x36
const m_ASCII_7 = 0x37
const m_ASCII_8 = 56
const m_ASCII_9 = 57
const m_ASCII_A = 65
const m_ASCII_AMP = 0x26
const m_ASCII_APOS = 0x27
const m_ASCII_B = 0x42
const m_ASCII_C = 67
const m_ASCII_COLON = 58
const m_ASCII_COMMA = 44
const m_ASCII_D = 68
const m_ASCII_E = 69
const m_ASCII_EQUALS = 61
const m_ASCII_EXCL = 33
const m_ASCII_F = 70
const m_ASCII_FF = 12
const m_ASCII_G = 0x47
const m_ASCII_GT = 0x3E
const m_ASCII_H = 0x48
const m_ASCII_HASH = 35
const m_ASCII_I = 73
const m_ASCII_J = 0x4A
const m_ASCII_K = 75
const m_ASCII_L = 76
const m_ASCII_LPAREN = 40
const m_ASCII_LSQB = 0x5B
const m_ASCII_LT = 0x3C
const m_ASCII_M = 77
const m_ASCII_MINUS = 0x2D
const m_ASCII_N = 78
const m_ASCII_O = 79
const m_ASCII_P = 0x50
const m_ASCII_PERIOD = 46
const m_ASCII_PIPE = 124
const m_ASCII_Q = 0x51
const m_ASCII_QUOT = 0x22
const m_ASCII_R = 82
const m_ASCII_RPAREN = 41
const m_ASCII_RSQB = 0x5D
const m_ASCII_S = 83
const m_ASCII_SEMI = 0x3B
const m_ASCII_SLASH = 47
const m_ASCII_SPACE = 0x20
const m_ASCII_T = 84
const m_ASCII_TAB = 0x09
const m_ASCII_U = 0x55
const m_ASCII_UNDERSCORE = 0x5F
const m_ASCII_V = 0x56
const m_ASCII_W = 0x57
const m_ASCII_X = 88
const m_ASCII_Y = 89
const m_ASCII_Z = 0x5A
const m_ASCII_a = 97
const m_ASCII_b = 0x62
const m_ASCII_c = 99
const m_ASCII_d = 0x64
const m_ASCII_e = 101
const m_ASCII_f = 0x66
const m_ASCII_g = 103
const m_ASCII_h = 104
const m_ASCII_i = 0x69
const m_ASCII_j = 0x6A
const m_ASCII_k = 0x6B
const m_ASCII_l = 108
const m_ASCII_m = 109
const m_ASCII_n = 110
const m_ASCII_o = 111
const m_ASCII_p = 112
const m_ASCII_q = 0x71
const m_ASCII_r = 114
const m_ASCII_s = 115
const m_ASCII_t = 116
const m_ASCII_u = 0x75
const m_ASCII_v = 0x76
const m_ASCII_w = 119
const m_ASCII_x = 120
const m_ASCII_y = 0x79
const m_ASCII_z = 0x7A
const m_AT_EACCESS = 0x200
const m_AT_EMPTY_PATH = 0x1000
const m_AT_NO_AUTOMOUNT = 0x800
const m_AT_RECURSIVE = 0x8000
const m_AT_REMOVEDIR = 0x200
const m_AT_STATX_DONT_SYNC = 0x4000
const m_AT_STATX_FORCE_SYNC = 0x2000
const m_AT_STATX_SYNC_AS_STAT = 0x0000
const m_AT_STATX_SYNC_TYPE = 0x6000
const m_AT_SYMLINK_FOLLOW = 0x400
const m_AT_SYMLINK_NOFOLLOW = 0x100
const m_BC_BASE_MAX = 99
const m_BC_DIM_MAX = 2048
const m_BC_SCALE_MAX = 99
const m_BC_STRING_MAX = 1000
const m_BIG_ENDIAN = "__BIG_ENDIAN"
const m_BUFSIZ = 1024
const m_BYTEORDER = 4321
const m_BYTE_ORDER = "__BYTE_ORDER"
const m_CHARCLASS_NAME_MAX = 14
const m_CHAR_BIT = 8
const m_CHAR_MAX = 255
const m_CHAR_MIN = 0
const m_COLL_WEIGHTS_MAX = 2
const m_DELAYTIMER_MAX = 0x7fffffff
const m_DN_ACCESS = 0x00000001
const m_DN_ATTRIB = 0x00000020
const m_DN_CREATE = 0x00000004
const m_DN_DELETE = 0x00000008
const m_DN_MODIFY = 0x00000002
const m_DN_MULTISHOT = 0x80000000
const m_DN_RENAME = 0x00000010
const m_E2BIG = 7
const m_EACCES = 13
const m_EADDRINUSE = 98
const m_EADDRNOTAVAIL = 99
const m_EADV = 68
const m_EAFNOSUPPORT = 97
const m_EAGAIN = 11
const m_EALREADY = 114
const m_EBADE = 52
const m_EBADF = 9
const m_EBADFD = 77
const m_EBADMSG = 74
const m_EBADR = 53
const m_EBADRQC = 56
const m_EBADSLT = 57
const m_EBFONT = 59
const m_EBUSY = 16
const m_ECANCELED = 125
const m_ECHILD = 10
const m_ECHRNG = 44
const m_ECOMM = 70
const m_ECONNABORTED = 103
const m_ECONNREFUSED = 111
const m_ECONNRESET = 104
const m_EDEADLK = 35
const m_EDEADLOCK = "EDEADLK"
const m_EDESTADDRREQ = 89
const m_EDOM = 33
const m_EDOTDOT = 73
const m_EDQUOT = 122
const m_EEXIST = 17
const m_EFAULT = 14
const m_EFBIG = 27
const m_EHOSTDOWN = 112
const m_EHOSTUNREACH = 113
const m_EHWPOISON = 133
const m_EIDRM = 43
const m_EILSEQ = 84
const m_EINPROGRESS = 115
const m_EINTR = 4
const m_EINVAL = 22
const m_EIO = 5
const m_EISCONN = 106
const m_EISDIR = 21
const m_EISNAM = 120
const m_EKEYEXPIRED = 127
const m_EKEYREJECTED = 129
const m_EKEYREVOKED = 128
const m_EL2HLT = 51
const m_EL2NSYNC = 45
const m_EL3HLT = 46
const m_EL3RST = 47
const m_ELIBACC = 79
const m_ELIBBAD = 80
const m_ELIBEXEC = 83
const m_ELIBMAX = 82
const m_ELIBSCN = 81
const m_ELNRNG = 48
const m_ELOOP = 40
const m_EMEDIUMTYPE = 124
const m_EMFILE = 24
const m_EMLINK = 31
const m_EMSGSIZE = 90
const m_EMULTIHOP = 72
const m_ENAMETOOLONG = 36
const m_ENAVAIL = 119
const m_ENETDOWN = 100
const m_ENETRESET = 102
const m_ENETUNREACH = 101
const m_ENFILE = 23
const m_ENOANO = 55
const m_ENOBUFS = 105
const m_ENOCSI = 50
const m_ENODATA = 61
const m_ENODEV = 19
const m_ENOENT = 2
const m_ENOEXEC = 8
const m_ENOKEY = 126
const m_ENOLCK = 37
const m_ENOLINK = 67
const m_ENOMEDIUM = 123
const m_ENOMEM = 12
const m_ENOMSG = 42
const m_ENONET = 64
const m_ENOPKG = 65
const m_ENOPROTOOPT = 92
const m_ENOSPC = 28
const m_ENOSR = 63
const m_ENOSTR = 60
const m_ENOSYS = 38
const m_ENOTBLK = 15
const m_ENOTCONN = 107
const m_ENOTDIR = 20
const m_ENOTEMPTY = 39
const m_ENOTNAM = 118
const m_ENOTRECOVERABLE = 131
const m_ENOTSOCK = 88
const m_ENOTSUP = "EOPNOTSUPP"
const m_ENOTTY = 25
const m_ENOTUNIQ = 76
const m_ENXIO = 6
const m_EOPNOTSUPP = 95
const m_EOVERFLOW = 75
const m_EOWNERDEAD = 130
const m_EPERM = 1
const m_EPFNOSUPPORT = 96
const m_EPIPE = 32
const m_EPROTO = 71
const m_EPROTONOSUPPORT = 93
const m_EPROTOTYPE = 91
const m_ERANGE = 34
const m_EREMCHG = 78
const m_EREMOTE = 66
const m_EREMOTEIO = 121
const m_ERESTART = 85
const m_ERFKILL = 132
const m_EROFS = 30
const m_ESHUTDOWN = 108
const m_ESOCKTNOSUPPORT = 94
const m_ESPIPE = 29
const m_ESRCH = 3
const m_ESRMNT = 69
const m_ESTALE = 116
const m_ESTRPIPE = 86
const m_ETIME = 62
const m_ETIMEDOUT = 110
const m_ETOOMANYREFS = 109
const m_ETXTBSY = 26
const m_EUCLEAN = 117
const m_EUNATCH = 49
const m_EUSERS = 87
const m_EWOULDBLOCK = "EAGAIN"
const m_EXDEV = 18
const m_EXFULL = 54
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_EXPAND_SPARE = 24
const m_EXPR_NEST_MAX = 32
const m_Expat_External_INCLUDED = 1
const m_Expat_INCLUDED = 1
const m_FALLOC_FL_KEEP_SIZE = 1
const m_FALLOC_FL_PUNCH_HOLE = 2
const m_FAPPEND = "O_APPEND"
const m_FASYNC = "O_ASYNC"
const m_FD_CLOEXEC = 1
const m_FD_SETSIZE = 1024
const m_FFSYNC = "O_SYNC"
const m_FILENAME_MAX = 4096
const m_FILESIZEBITS = 64
const m_FNDELAY = "O_NDELAY"
const m_FNONBLOCK = "O_NONBLOCK"
const m_FOPEN_MAX = 1000
const m_F_ADD_SEALS = 1033
const m_F_CANCELLK = 1029
const m_F_DUPFD = 0
const m_F_DUPFD_CLOEXEC = 1030
const m_F_GETFD = 1
const m_F_GETFL = 3
const m_F_GETLEASE = 1025
const m_F_GETLK = 5
const m_F_GETOWN = 9
const m_F_GETOWNER_UIDS = 17
const m_F_GETOWN_EX = 16
const m_F_GETPIPE_SZ = 1032
const m_F_GETSIG = 11
const m_F_GET_FILE_RW_HINT = 1037
const m_F_GET_RW_HINT = 1035
const m_F_GET_SEALS = 1034
const m_F_LOCK = 1
const m_F_NOTIFY = 1026
const m_F_OFD_GETLK = 36
const m_F_OFD_SETLK = 37
const m_F_OFD_SETLKW = 38
const m_F_OK = 0
const m_F_OWNER_GID = 2
const m_F_OWNER_PGRP = 2
const m_F_OWNER_PID = 1
const m_F_OWNER_TID = 0
const m_F_RDLCK = 0
const m_F_SEAL_FUTURE_WRITE = 0x0010
const m_F_SEAL_GROW = 0x0004
const m_F_SEAL_SEAL = 0x0001
const m_F_SEAL_SHRINK = 0x0002
const m_F_SEAL_WRITE = 0x0008
const m_F_SETFD = 2
const m_F_SETFL = 4
const m_F_SETLEASE = 1024
const m_F_SETLK = 6
const m_F_SETLKW = 7
const m_F_SETOWN = 8
const m_F_SETOWN_EX = 15
const m_F_SETPIPE_SZ = 1031
const m_F_SETSIG = 10
const m_F_SET_FILE_RW_HINT = 1038
const m_F_SET_RW_HINT = 1036
const m_F_TEST = 3
const m_F_TLOCK = 2
const m_F_ULOCK = 0
const m_F_UNLCK = 2
const m_F_WRLCK = 1
const m_GRND_INSECURE = 0x0004
const m_GRND_NONBLOCK = 1
const m_GRND_RANDOM = 0x0002
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_EXPAT_CONFIG_H = 1
const m_HAVE_FCNTL_H = 1
const m_HAVE_GETPAGESIZE = 1
const m_HAVE_GETRANDOM = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_MEMORY_H = 1
const m_HAVE_MMAP = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYSCALL_GETRANDOM = 1
const m_HAVE_SYS_PARAM_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HOST_NAME_MAX = 255
const m_INIT_ATTS_SIZE = 16
const m_INIT_ATTS_VERSION = 4294967295
const m_INIT_BLOCK_SIZE = 1024
const m_INIT_BUFFER_SIZE = 1024
const m_INIT_DATA_BUF_SIZE = 1024
const m_INIT_POWER = 6
const m_INIT_SCAFFOLD_ELEMENTS = 32
const m_INIT_TAG_BUF_SIZE = 32
const m_INT16_MAX = 0x7fff
const m_INT32_MAX = 0x7fffffff
const m_INT64_MAX = 0x7fffffffffffffff
const m_INT8_MAX = 0x7f
const m_INTMAX_MAX = "INT64_MAX"
const m_INTMAX_MIN = "INT64_MIN"
const m_INTPTR_MAX = "INT64_MAX"
const m_INTPTR_MIN = "INT64_MIN"
const m_INT_FAST16_MAX = "INT32_MAX"
const m_INT_FAST16_MIN = "INT32_MIN"
const m_INT_FAST32_MAX = "INT32_MAX"
const m_INT_FAST32_MIN = "INT32_MIN"
const m_INT_FAST64_MAX = "INT64_MAX"
const m_INT_FAST64_MIN = "INT64_MIN"
const m_INT_FAST8_MAX = "INT8_MAX"
const m_INT_FAST8_MIN = "INT8_MIN"
const m_INT_LEAST16_MAX = "INT16_MAX"
const m_INT_LEAST16_MIN = "INT16_MIN"
const m_INT_LEAST32_MAX = "INT32_MAX"
const m_INT_LEAST32_MIN = "INT32_MIN"
const m_INT_LEAST64_MAX = "INT64_MAX"
const m_INT_LEAST64_MIN = "INT64_MIN"
const m_INT_LEAST8_MAX = "INT8_MAX"
const m_INT_LEAST8_MIN = "INT8_MIN"
const m_INT_MAX = 2147483647
const m_IOV_MAX = 1024
const m_ITIMER_PROF = 2
const m_ITIMER_REAL = 0
const m_ITIMER_VIRTUAL = 1
const m_LINE_MAX = 4096
const m_LITTLE_ENDIAN = "__LITTLE_ENDIAN"
const m_LLONG_MAX = 0x7fffffffffffffff
const m_LOGIN_NAME_MAX = 256
const m_LONG_BIT = 64
const m_LONG_MAX = "__LONG_MAX"
const m_LT_OBJDIR = ".libs/"
const m_L_INCR = 1
const m_L_SET = 0
const m_L_XTND = 2
const m_L_ctermid = 20
const m_L_cuserid = 20
const m_L_tmpnam = 20
const m_MAX_HANDLE_SZ = 128
const m_MB_LEN_MAX = 4
const m_MQ_PRIO_MAX = 32768
const m_NAME_MAX = 255
const m_NDEBUG = 1
const m_NGROUPS_MAX = 32
const m_NL_ARGMAX = 9
const m_NL_LANGMAX = 32
const m_NL_MSGMAX = 32767
const m_NL_NMAX = 16
const m_NL_SETMAX = 255
const m_NL_TEXTMAX = 2048
const m_NZERO = 20
const m_O_APPEND = 02000
const m_O_ASYNC = 020000
const m_O_CLOEXEC = 02000000
const m_O_CREAT = 0100
const m_O_DIRECT = 040000
const m_O_DIRECTORY = 0200000
const m_O_DSYNC = 010000
const m_O_EXCL = 0200
const m_O_EXEC = "O_PATH"
const m_O_LARGEFILE = 0100000
const m_O_NDELAY = "O_NONBLOCK"
const m_O_NOATIME = 01000000
const m_O_NOCTTY = 0400
const m_O_NOFOLLOW = 0400000
const m_O_NONBLOCK = 04000
const m_O_PATH = 010000000
const m_O_RDONLY = 0
const m_O_RDWR = 02
const m_O_RSYNC = 04010000
const m_O_SEARCH = "O_PATH"
const m_O_SYNC = 04010000
const m_O_TMPFILE = 020200000
const m_O_TRUNC = 01000
const m_O_TTY_INIT = 0
const m_O_WRONLY = 01
const m_PACKAGE = "expat"
const m_PACKAGE_BUGREPORT = "expat-bugs@libexpat.org"
const m_PACKAGE_NAME = "expat"
const m_PACKAGE_STRING = "expat 2.2.10"
const m_PACKAGE_TARNAME = "expat"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "2.2.10"
const m_PAGESIZE = 4096
const m_PAGE_SIZE = "PAGESIZE"
const m_PATH_MAX = 4096
const m_PDP_ENDIAN = "__PDP_ENDIAN"
const m_PIPE_BUF = 4096
const m_POSIX_CLOSE_RESTART = 0
const m_POSIX_FADV_DONTNEED = 6
const m_POSIX_FADV_NOREUSE = 7
const m_POSIX_FADV_NORMAL = 0
const m_POSIX_FADV_RANDOM = 1
const m_POSIX_FADV_SEQUENTIAL = 2
const m_POSIX_FADV_WILLNEED = 3
const m_PTHREAD_DESTRUCTOR_ITERATIONS = 4
const m_PTHREAD_KEYS_MAX = 128
const m_PTHREAD_STACK_MIN = 2048
const m_PTRDIFF_MAX = "INT64_MAX"
const m_PTRDIFF_MIN = "INT64_MIN"
const m_P_tmpdir = "/tmp"
const m_RAND_MAX = 0x7fffffff
const m_RE_DUP_MAX = 255
const m_RWF_WRITE_LIFE_NOT_SET = 0
const m_RWH_WRITE_LIFE_EXTREME = 5
const m_RWH_WRITE_LIFE_LONG = 4
const m_RWH_WRITE_LIFE_MEDIUM = 3
const m_RWH_WRITE_LIFE_NONE = 1
const m_RWH_WRITE_LIFE_SHORT = 2
const m_R_OK = 4
const m_SCHAR_MAX = 127
const m_SEEK_DATA = 3
const m_SEEK_HOLE = 4
const m_SEM_NSEMS_MAX = 256
const m_SEM_VALUE_MAX = 0x7fffffff
const m_SHRT_MAX = 0x7fff
const m_SIG_ATOMIC_MAX = "INT32_MAX"
const m_SIG_ATOMIC_MIN = "INT32_MIN"
const m_SIP_KEYLEN = 16
const m_SIZE_MAX = "UINT64_MAX"
const m_SPLICE_F_GIFT = 8
const m_SPLICE_F_MORE = 4
const m_SPLICE_F_MOVE = 1
const m_SPLICE_F_NONBLOCK = 2
const m_SSIZE_MAX = "LONG_MAX"
const m_STDC_HEADERS = 1
const m_STDERR_FILENO = 2
const m_STDIN_FILENO = 0
const m_STDOUT_FILENO = 1
const m_SYMLOOP_MAX = 40
const m_SYNC_FILE_RANGE_WAIT_AFTER = 4
const m_SYNC_FILE_RANGE_WAIT_BEFORE = 1
const m_SYNC_FILE_RANGE_WRITE = 2
const m_S_IRGRP = 0040
const m_S_IROTH = 0004
const m_S_IRUSR = 0400
const m_S_IRWXG = 0070
const m_S_IRWXO = 0007
const m_S_IRWXU = 0700
const m_S_ISGID = 02000
const m_S_ISUID = 04000
const m_S_ISVTX = 01000
const m_S_IWGRP = 0020
const m_S_IWOTH = 0002
const m_S_IWUSR = 0200
const m_S_IXGRP = 0010
const m_S_IXOTH = 0001
const m_S_IXUSR = 0100
const m_TMP_MAX = 10000
const m_TTY_NAME_MAX = 32
const m_TZNAME_MAX = 6
const m_UCHAR_MAX = 255
const m_UINT16_MAX = 0xffff
const m_UINT32_MAX = "0xffffffffu"
const m_UINT64_MAX = "0xffffffffffffffffu"
const m_UINT8_MAX = 0xff
const m_UINTMAX_MAX = "UINT64_MAX"
const m_UINTPTR_MAX = "UINT64_MAX"
const m_UINT_FAST16_MAX = "UINT32_MAX"
const m_UINT_FAST32_MAX = "UINT32_MAX"
const m_UINT_FAST64_MAX = "UINT64_MAX"
const m_UINT_FAST8_MAX = "UINT8_MAX"
const m_UINT_LEAST16_MAX = "UINT16_MAX"
const m_UINT_LEAST32_MAX = "UINT32_MAX"
const m_UINT_LEAST64_MAX = "UINT64_MAX"
const m_UINT_LEAST8_MAX = "UINT8_MAX"
const m_UINT_MAX = 0xffffffff
const m_USHRT_MAX = 0xffff
const m_VERSION = "2.2.10"
const m_WINT_MAX = "UINT32_MAX"
const m_WINT_MIN = 0
const m_WNOHANG = 1
const m_WORDS_BIGENDIAN = 1
const m_WORD_BIT = 32
const m_WUNTRACED = 2
const m_W_OK = 2
const m_XML_ATTRIBUTE_VALUE_LITERAL = 0
const m_XML_BUILDING_EXPAT = 1
const m_XML_CDATA_SECTION_STATE = 2
const m_XML_CONTENT_STATE = 1
const m_XML_CONTEXT_BYTES = 1024
const m_XML_DEV_URANDOM = 1
const m_XML_DTD = 1
const m_XML_ENABLE_VISIBILITY = 1
const m_XML_ENCODE_MAX = "XML_UTF8_ENCODE_MAX"
const m_XML_ENTITY_VALUE_LITERAL = 1
const m_XML_GetErrorByteIndex = "XML_GetCurrentByteIndex"
const m_XML_GetErrorColumnNumber = "XML_GetCurrentColumnNumber"
const m_XML_GetErrorLineNumber = "XML_GetCurrentLineNumber"
const m_XML_IGNORE_SECTION_STATE = 3
const m_XML_MAJOR_VERSION = 2
const m_XML_MICRO_VERSION = 10
const m_XML_MINOR_VERSION = 2
const m_XML_NS = 1
const m_XML_N_LITERAL_TYPES = 2
const m_XML_N_STATES = 4
const m_XML_PROLOG_STATE = 0
const m_XML_TOK_ATTRIBUTE_VALUE_S = 39
const m_XML_TOK_BOM = 14
const m_XML_TOK_CDATA_SECT_CLOSE = 40
const m_XML_TOK_CDATA_SECT_OPEN = 8
const m_XML_TOK_CHAR_REF = 10
const m_XML_TOK_CLOSE_BRACKET = 26
const m_XML_TOK_CLOSE_PAREN = 24
const m_XML_TOK_CLOSE_PAREN_ASTERISK = 36
const m_XML_TOK_CLOSE_PAREN_PLUS = 37
const m_XML_TOK_CLOSE_PAREN_QUESTION = 35
const m_XML_TOK_COMMA = 38
const m_XML_TOK_COMMENT = 13
const m_XML_TOK_COND_SECT_CLOSE = 34
const m_XML_TOK_COND_SECT_OPEN = 33
const m_XML_TOK_DATA_CHARS = 6
const m_XML_TOK_DATA_NEWLINE = 7
const m_XML_TOK_DECL_CLOSE = 17
const m_XML_TOK_DECL_OPEN = 16
const m_XML_TOK_EMPTY_ELEMENT_NO_ATTS = 4
const m_XML_TOK_EMPTY_ELEMENT_WITH_ATTS = 3
const m_XML_TOK_END_TAG = 5
const m_XML_TOK_ENTITY_REF = 9
const m_XML_TOK_IGNORE_SECT = 42
const m_XML_TOK_INSTANCE_START = 29
const m_XML_TOK_INVALID = 0
const m_XML_TOK_LITERAL = 27
const m_XML_TOK_NAME = 18
const m_XML_TOK_NAME_ASTERISK = 31
const m_XML_TOK_NAME_PLUS = 32
const m_XML_TOK_NAME_QUESTION = 30
const m_XML_TOK_NMTOKEN = 19
const m_XML_TOK_OPEN_BRACKET = 25
const m_XML_TOK_OPEN_PAREN = 23
const m_XML_TOK_OR = 21
const m_XML_TOK_PARAM_ENTITY_REF = 28
const m_XML_TOK_PERCENT = 22
const m_XML_TOK_PI = 11
const m_XML_TOK_POUND_NAME = 20
const m_XML_TOK_PREFIXED_NAME = 41
const m_XML_TOK_PROLOG_S = 15
const m_XML_TOK_START_TAG_NO_ATTS = 2
const m_XML_TOK_START_TAG_WITH_ATTS = 1
const m_XML_TOK_XML_DECL = 12
const m_XML_UTF16_ENCODE_MAX = 2
const m_XML_UTF8_ENCODE_MAX = 4
const m_X_OK = 1
const m_XmlConvert = "XmlUtf8Convert"
const m_XmlEncode = "XmlUtf8Encode"
const m_XmlGetInternalEncoding = "XmlGetUtf8InternalEncoding"
const m_XmlGetInternalEncodingNS = "XmlGetUtf8InternalEncodingNS"
const m_XmlRole_INCLUDED = 1
const m_XmlTok_INCLUDED = 1
const m__CS_GNU_LIBC_VERSION = 2
const m__CS_GNU_LIBPTHREAD_VERSION = 3
const m__CS_PATH = 0
const m__CS_POSIX_V5_WIDTH_RESTRICTED_ENVS = 4
const m__CS_POSIX_V6_ILP32_OFF32_CFLAGS = 1116
const m__CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 1117
const m__CS_POSIX_V6_ILP32_OFF32_LIBS = 1118
const m__CS_POSIX_V6_ILP32_OFF32_LINTFLAGS = 1119
const m__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 1120
const m__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 1121
const m__CS_POSIX_V6_ILP32_OFFBIG_LIBS = 1122
const m__CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS = 1123
const m__CS_POSIX_V6_LP64_OFF64_CFLAGS = 1124
const m__CS_POSIX_V6_LP64_OFF64_LDFLAGS = 1125
const m__CS_POSIX_V6_LP64_OFF64_LIBS = 1126
const m__CS_POSIX_V6_LP64_OFF64_LINTFLAGS = 1127
const m__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 1128
const m__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 1129
const m__CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 1130
const m__CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS = 1131
const m__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 1
const m__CS_POSIX_V7_ILP32_OFF32_CFLAGS = 1132
const m__CS_POSIX_V7_ILP32_OFF32_LDFLAGS = 1133
const m__CS_POSIX_V7_ILP32_OFF32_LIBS = 1134
const m__CS_POSIX_V7_ILP32_OFF32_LINTFLAGS = 1135
const m__CS_POSIX_V7_ILP32_OFFBIG_CFLAGS = 1136
const m__CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS = 1137
const m__CS_POSIX_V7_ILP32_OFFBIG_LIBS = 1138
const m__CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS = 1139
const m__CS_POSIX_V7_LP64_OFF64_CFLAGS = 1140
const m__CS_POSIX_V7_LP64_OFF64_LDFLAGS = 1141
const m__CS_POSIX_V7_LP64_OFF64_LIBS = 1142
const m__CS_POSIX_V7_LP64_OFF64_LINTFLAGS = 1143
const m__CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS = 1144
const m__CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS = 1145
const m__CS_POSIX_V7_LPBIG_OFFBIG_LIBS = 1146
const m__CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS = 1147
const m__CS_POSIX_V7_THREADS_CFLAGS = 1150
const m__CS_POSIX_V7_THREADS_LDFLAGS = 1151
const m__CS_POSIX_V7_WIDTH_RESTRICTED_ENVS = 5
const m__CS_V6_ENV = 1148
const m__CS_V7_ENV = 1149
const m__GNU_SOURCE = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__LP64 = 1
const m__PC_2_SYMLINKS = 20
const m__PC_ALLOC_SIZE_MIN = 18
const m__PC_ASYNC_IO = 10
const m__PC_CHOWN_RESTRICTED = 6
const m__PC_FILESIZEBITS = 13
const m__PC_LINK_MAX = 0
const m__PC_MAX_CANON = 1
const m__PC_MAX_INPUT = 2
const m__PC_NAME_MAX = 3
const m__PC_NO_TRUNC = 7
const m__PC_PATH_MAX = 4
const m__PC_PIPE_BUF = 5
const m__PC_PRIO_IO = 11
const m__PC_REC_INCR_XFER_SIZE = 14
const m__PC_REC_MAX_XFER_SIZE = 15
const m__PC_REC_MIN_XFER_SIZE = 16
const m__PC_REC_XFER_ALIGN = 17
const m__PC_SOCK_MAXBUF = 12
const m__PC_SYMLINK_MAX = 19
const m__PC_SYNC_IO = 9
const m__PC_VDISABLE = 8
const m__POSIX2_BC_BASE_MAX = 99
const m__POSIX2_BC_DIM_MAX = 2048
const m__POSIX2_BC_SCALE_MAX = 99
const m__POSIX2_BC_STRING_MAX = 1000
const m__POSIX2_CHARCLASS_NAME_MAX = 14
const m__POSIX2_COLL_WEIGHTS_MAX = 2
const m__POSIX2_C_BIND = "_POSIX_VERSION"
const m__POSIX2_EXPR_NEST_MAX = 32
const m__POSIX2_LINE_MAX = 2048
const m__POSIX2_RE_DUP_MAX = 255
const m__POSIX2_VERSION = "_POSIX_VERSION"
const m__POSIX_ADVISORY_INFO = "_POSIX_VERSION"
const m__POSIX_AIO_LISTIO_MAX = 2
const m__POSIX_AIO_MAX = 1
const m__POSIX_ARG_MAX = 4096
const m__POSIX_ASYNCHRONOUS_IO = "_POSIX_VERSION"
const m__POSIX_BARRIERS = "_POSIX_VERSION"
const m__POSIX_CHILD_MAX = 25
const m__POSIX_CHOWN_RESTRICTED = 1
const m__POSIX_CLOCKRES_MIN = 20000000
const m__POSIX_CLOCK_SELECTION = "_POSIX_VERSION"
const m__POSIX_CPUTIME = "_POSIX_VERSION"
const m__POSIX_DELAYTIMER_MAX = 32
const m__POSIX_FSYNC = "_POSIX_VERSION"
const m__POSIX_HOST_NAME_MAX = 255
const m__POSIX_IPV6 = "_POSIX_VERSION"
const m__POSIX_JOB_CONTROL = 1
const m__POSIX_LINK_MAX = 8
const m__POSIX_LOGIN_NAME_MAX = 9
const m__POSIX_MAPPED_FILES = "_POSIX_VERSION"
const m__POSIX_MAX_CANON = 255
const m__POSIX_MAX_INPUT = 255
const m__POSIX_MEMLOCK = "_POSIX_VERSION"
const m__POSIX_MEMLOCK_RANGE = "_POSIX_VERSION"
const m__POSIX_MEMORY_PROTECTION = "_POSIX_VERSION"
const m__POSIX_MESSAGE_PASSING = "_POSIX_VERSION"
const m__POSIX_MONOTONIC_CLOCK = "_POSIX_VERSION"
const m__POSIX_MQ_OPEN_MAX = 8
const m__POSIX_MQ_PRIO_MAX = 32
const m__POSIX_NAME_MAX = 14
const m__POSIX_NGROUPS_MAX = 8
const m__POSIX_NO_TRUNC = 1
const m__POSIX_OPEN_MAX = 20
const m__POSIX_PATH_MAX = 256
const m__POSIX_PIPE_BUF = 512
const m__POSIX_RAW_SOCKETS = "_POSIX_VERSION"
const m__POSIX_READER_WRITER_LOCKS = "_POSIX_VERSION"
const m__POSIX_REALTIME_SIGNALS = "_POSIX_VERSION"
const m__POSIX_REGEXP = 1
const m__POSIX_RE_DUP_MAX = 255
const m__POSIX_RTSIG_MAX = 8
const m__POSIX_SAVED_IDS = 1
const m__POSIX_SEMAPHORES = "_POSIX_VERSION"
const m__POSIX_SEM_NSEMS_MAX = 256
const m__POSIX_SEM_VALUE_MAX = 32767
const m__POSIX_SHARED_MEMORY_OBJECTS = "_POSIX_VERSION"
const m__POSIX_SHELL = 1
const m__POSIX_SIGQUEUE_MAX = 32
const m__POSIX_SPAWN = "_POSIX_VERSION"
const m__POSIX_SPIN_LOCKS = "_POSIX_VERSION"
const m__POSIX_SSIZE_MAX = 32767
const m__POSIX_SS_REPL_MAX = 4
const m__POSIX_STREAM_MAX = 8
const m__POSIX_SYMLINK_MAX = 255
const m__POSIX_SYMLOOP_MAX = 8
const m__POSIX_THREADS = "_POSIX_VERSION"
const m__POSIX_THREAD_ATTR_STACKADDR = "_POSIX_VERSION"
const m__POSIX_THREAD_ATTR_STACKSIZE = "_POSIX_VERSION"
const m__POSIX_THREAD_CPUTIME = "_POSIX_VERSION"
const m__POSIX_THREAD_DESTRUCTOR_ITERATIONS = 4
const m__POSIX_THREAD_KEYS_MAX = 128
const m__POSIX_THREAD_PRIORITY_SCHEDULING = "_POSIX_VERSION"
const m__POSIX_THREAD_PROCESS_SHARED = "_POSIX_VERSION"
const m__POSIX_THREAD_SAFE_FUNCTIONS = "_POSIX_VERSION"
const m__POSIX_THREAD_THREADS_MAX = 64
const m__POSIX_TIMEOUTS = "_POSIX_VERSION"
const m__POSIX_TIMERS = "_POSIX_VERSION"
const m__POSIX_TIMER_MAX = 32
const m__POSIX_TRACE_EVENT_NAME_MAX = 30
const m__POSIX_TRACE_NAME_MAX = 8
const m__POSIX_TRACE_SYS_MAX = 8
const m__POSIX_TRACE_USER_EVENT_MAX = 32
const m__POSIX_TTY_NAME_MAX = 9
const m__POSIX_TZNAME_MAX = 6
const m__POSIX_V6_LP64_OFF64 = 1
const m__POSIX_V7_LP64_OFF64 = 1
const m__POSIX_VDISABLE = 0
const m__POSIX_VERSION = 200809
const m__SC_2_CHAR_TERM = 95
const m__SC_2_C_BIND = 47
const m__SC_2_C_DEV = 48
const m__SC_2_FORT_DEV = 49
const m__SC_2_FORT_RUN = 50
const m__SC_2_LOCALEDEF = 52
const m__SC_2_PBS = 168
const m__SC_2_PBS_ACCOUNTING = 169
const m__SC_2_PBS_CHECKPOINT = 175
const m__SC_2_PBS_LOCATE = 170
const m__SC_2_PBS_MESSAGE = 171
const m__SC_2_PBS_TRACK = 172
const m__SC_2_SW_DEV = 51
const m__SC_2_UPE = 97
const m__SC_2_VERSION = 46
const m__SC_ADVISORY_INFO = 132
const m__SC_AIO_LISTIO_MAX = 23
const m__SC_AIO_MAX = 24
const m__SC_AIO_PRIO_DELTA_MAX = 25
const m__SC_ARG_MAX = 0
const m__SC_ASYNCHRONOUS_IO = 12
const m__SC_ATEXIT_MAX = 87
const m__SC_AVPHYS_PAGES = 86
const m__SC_BARRIERS = 133
const m__SC_BC_BASE_MAX = 36
const m__SC_BC_DIM_MAX = 37
const m__SC_BC_SCALE_MAX = 38
const m__SC_BC_STRING_MAX = 39
const m__SC_CHILD_MAX = 1
const m__SC_CLK_TCK = 2
const m__SC_CLOCK_SELECTION = 137
const m__SC_COLL_WEIGHTS_MAX = 40
const m__SC_CPUTIME = 138
const m__SC_DELAYTIMER_MAX = 26
const m__SC_EXPR_NEST_MAX = 42
const m__SC_FSYNC = 15
const m__SC_GETGR_R_SIZE_MAX = 69
const m__SC_GETPW_R_SIZE_MAX = 70
const m__SC_HOST_NAME_MAX = 180
const m__SC_IOV_MAX = 60
const m__SC_IPV6 = 235
const m__SC_JOB_CONTROL = 7
const m__SC_LINE_MAX = 43
const m__SC_LOGIN_NAME_MAX = 71
const m__SC_MAPPED_FILES = 16
const m__SC_MEMLOCK = 17
const m__SC_MEMLOCK_RANGE = 18
const m__SC_MEMORY_PROTECTION = 19
const m__SC_MESSAGE_PASSING = 20
const m__SC_MINSIGSTKSZ = 249
const m__SC_MONOTONIC_CLOCK = 149
const m__SC_MQ_OPEN_MAX = 27
const m__SC_MQ_PRIO_MAX = 28
const m__SC_NGROUPS_MAX = 3
const m__SC_NPROCESSORS_CONF = 83
const m__SC_NPROCESSORS_ONLN = 84
const m__SC_NZERO = 109
const m__SC_OPEN_MAX = 4
const m__SC_PAGESIZE = 30
const m__SC_PAGE_SIZE = 30
const m__SC_PASS_MAX = 88
const m__SC_PHYS_PAGES = 85
const m__SC_PRIORITIZED_IO = 13
const m__SC_PRIORITY_SCHEDULING = 10
const m__SC_RAW_SOCKETS = 236
const m__SC_READER_WRITER_LOCKS = 153
const m__SC_REALTIME_SIGNALS = 9
const m__SC_REGEXP = 155
const m__SC_RE_DUP_MAX = 44
const m__SC_RTSIG_MAX = 31
const m__SC_SAVED_IDS = 8
const m__SC_SEMAPHORES = 21
const m__SC_SEM_NSEMS_MAX = 32
const m__SC_SEM_VALUE_MAX = 33
const m__SC_SHARED_MEMORY_OBJECTS = 22
const m__SC_SHELL = 157
const m__SC_SIGQUEUE_MAX = 34
const m__SC_SIGSTKSZ = 250
const m__SC_SPAWN = 159
const m__SC_SPIN_LOCKS = 154
const m__SC_SPORADIC_SERVER = 160
const m__SC_SS_REPL_MAX = 241
const m__SC_STREAMS = 174
const m__SC_STREAM_MAX = 5
const m__SC_SYMLOOP_MAX = 173
const m__SC_SYNCHRONIZED_IO = 14
const m__SC_THREADS = 67
const m__SC_THREAD_ATTR_STACKADDR = 77
const m__SC_THREAD_ATTR_STACKSIZE = 78
const m__SC_THREAD_CPUTIME = 139
const m__SC_THREAD_DESTRUCTOR_ITERATIONS = 73
const m__SC_THREAD_KEYS_MAX = 74
const m__SC_THREAD_PRIORITY_SCHEDULING = 79
const m__SC_THREAD_PRIO_INHERIT = 80
const m__SC_THREAD_PRIO_PROTECT = 81
const m__SC_THREAD_PROCESS_SHARED = 82
const m__SC_THREAD_ROBUST_PRIO_INHERIT = 247
const m__SC_THREAD_ROBUST_PRIO_PROTECT = 248
const m__SC_THREAD_SAFE_FUNCTIONS = 68
const m__SC_THREAD_SPORADIC_SERVER = 161
const m__SC_THREAD_STACK_MIN = 75
const m__SC_THREAD_THREADS_MAX = 76
const m__SC_TIMEOUTS = 164
const m__SC_TIMERS = 11
const m__SC_TIMER_MAX = 35
const m__SC_TRACE = 181
const m__SC_TRACE_EVENT_FILTER = 182
const m__SC_TRACE_EVENT_NAME_MAX = 242
const m__SC_TRACE_INHERIT = 183
const m__SC_TRACE_LOG = 184
const m__SC_TRACE_NAME_MAX = 243
const m__SC_TRACE_SYS_MAX = 244
const m__SC_TRACE_USER_EVENT_MAX = 245
const m__SC_TTY_NAME_MAX = 72
const m__SC_TYPED_MEMORY_OBJECTS = 165
const m__SC_TZNAME_MAX = 6
const m__SC_UIO_MAXIOV = 60
const m__SC_V6_ILP32_OFF32 = 176
const m__SC_V6_ILP32_OFFBIG = 177
const m__SC_V6_LP64_OFF64 = 178
const m__SC_V6_LPBIG_OFFBIG = 179
const m__SC_V7_ILP32_OFF32 = 237
const m__SC_V7_ILP32_OFFBIG = 238
const m__SC_V7_LP64_OFF64 = 239
const m__SC_V7_LPBIG_OFFBIG = 240
const m__SC_VERSION = 29
const m__SC_XBS5_ILP32_OFF32 = 125
const m__SC_XBS5_ILP32_OFFBIG = 126
const m__SC_XBS5_LP64_OFF64 = 127
const m__SC_XBS5_LPBIG_OFFBIG = 128
const m__SC_XOPEN_CRYPT = 92
const m__SC_XOPEN_ENH_I18N = 93
const m__SC_XOPEN_LEGACY = 129
const m__SC_XOPEN_REALTIME = 130
const m__SC_XOPEN_REALTIME_THREADS = 131
const m__SC_XOPEN_SHM = 94
const m__SC_XOPEN_STREAMS = 246
const m__SC_XOPEN_UNIX = 91
const m__SC_XOPEN_VERSION = 89
const m__SC_XOPEN_XCU_VERSION = 90
const m__SC_XOPEN_XPG2 = 98
const m__SC_XOPEN_XPG3 = 99
const m__SC_XOPEN_XPG4 = 100
const m__STDC_PREDEF_H = 1
const m__XOPEN_ENH_I18N = 1
const m__XOPEN_IOV_MAX = 16
const m__XOPEN_NAME_MAX = 255
const m__XOPEN_PATH_MAX = 1024
const m__XOPEN_UNIX = 1
const m__XOPEN_VERSION = 700
const m___ARCH__ = 9
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 8
const m___BIG_ENDIAN = 4321
const m___BYTE_ORDER = 4321
const m___BYTE_ORDER__ = "__ORDER_BIG_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___CHAR_UNSIGNED__ = 1
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DIG__ = 15
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DEC128_EPSILON__ = 1e-33
const m___DEC128_MANT_DIG__ = 34
const m___DEC128_MAX_EXP__ = 6145
const m___DEC128_MAX__ = "9.999999999999999999999999999999999E6144"
const m___DEC128_MIN__ = 1e-6143
const m___DEC128_SUBNORMAL_MIN__ = 0.000000000000000000000000000000001e-6143
const m___DEC32_EPSILON__ = 1e-6
const m___DEC32_MANT_DIG__ = 7
const m___DEC32_MAX_EXP__ = 97
const m___DEC32_MAX__ = 9.999999e96
const m___DEC32_MIN__ = 1e-95
const m___DEC32_SUBNORMAL_MIN__ = 0.000001e-95
const m___DEC64_EPSILON__ = 1e-15
const m___DEC64_MANT_DIG__ = 16
const m___DEC64_MAX_EXP__ = 385
const m___DEC64_MAX__ = "9.999999999999999E384"
const m___DEC64_MIN__ = 1e-383
const m___DEC64_SUBNORMAL_MIN__ = 0.000000000000001e-383
const m___DECIMAL_DIG__ = 17
const m___DEC_EVAL_METHOD__ = 2
const m___ELF__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLOAT_WORD_ORDER__ = "__ORDER_BIG_ENDIAN__"
const m___FLT32X_DECIMAL_DIG__ = 17
const m___FLT32X_DENORM_MIN__ = 4.9406564584124654e-324
const m___FLT32X_DIG__ = 15
const m___FLT32X_EPSILON__ = 2.2204460492503131e-16
const m___FLT32X_HAS_DENORM__ = 1
const m___FLT32X_HAS_INFINITY__ = 1
const m___FLT32X_HAS_QUIET_NAN__ = 1
const m___FLT32X_MANT_DIG__ = 53
const m___FLT32X_MAX_10_EXP__ = 308
const m___FLT32X_MAX_EXP__ = 1024
const m___FLT32X_MAX__ = 1.7976931348623157e+308
const m___FLT32X_MIN__ = 2.2250738585072014e-308
const m___FLT32X_NORM_MAX__ = 1.7976931348623157e+308
const m___FLT32_DECIMAL_DIG__ = 9
const m___FLT32_DENORM_MIN__ = 1.4012984643248171e-45
const m___FLT32_DIG__ = 6
const m___FLT32_EPSILON__ = 1.1920928955078125e-7
const m___FLT32_HAS_DENORM__ = 1
const m___FLT32_HAS_INFINITY__ = 1
const m___FLT32_HAS_QUIET_NAN__ = 1
const m___FLT32_MANT_DIG__ = 24
const m___FLT32_MAX_10_EXP__ = 38
const m___FLT32_MAX_EXP__ = 128
const m___FLT32_MAX__ = 3.4028234663852886e+38
const m___FLT32_MIN__ = 1.1754943508222875e-38
const m___FLT32_NORM_MAX__ = 3.4028234663852886e+38
const m___FLT64_DECIMAL_DIG__ = 17
const m___FLT64_DENORM_MIN__ = 4.9406564584124654e-324
const m___FLT64_DIG__ = 15
const m___FLT64_EPSILON__ = 2.2204460492503131e-16
const m___FLT64_HAS_DENORM__ = 1
const m___FLT64_HAS_INFINITY__ = 1
const m___FLT64_HAS_QUIET_NAN__ = 1
const m___FLT64_MANT_DIG__ = 53
const m___FLT64_MAX_10_EXP__ = 308
const m___FLT64_MAX_EXP__ = 1024
const m___FLT64_MAX__ = 1.7976931348623157e+308
const m___FLT64_MIN__ = 2.2250738585072014e-308
const m___FLT64_NORM_MAX__ = 1.7976931348623157e+308
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.4012984643248171e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.1920928955078125e-7
const m___FLT_EVAL_METHOD_TS_18661_3__ = 0
const m___FLT_EVAL_METHOD__ = 0
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.4028234663852886e+38
const m___FLT_MIN__ = 1.1754943508222875e-38
const m___FLT_NORM_MAX__ = 3.4028234663852886e+38
const m___FLT_RADIX__ = 2
const m___FP_FAST_FMA = 1
const m___FP_FAST_FMAF = 1
const m___FP_FAST_FMAF32 = 1
const m___FP_FAST_FMAF32x = 1
const m___FP_FAST_FMAF64 = 1
const m___FP_FAST_FMAL = 1
const m___FUNCTION__ = "__func__"
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GCC_IEC_559 = 2
const m___GCC_IEC_559_COMPLEX = 2
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 1
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC__ = 10
const m___GXX_ABI_VERSION = 1014
const m___HAVE_SPECULATION_SAFE_VALUE = 1
const m___INT16_MAX__ = 0x7fff
const m___INT32_MAX__ = 0x7fffffff
const m___INT32_TYPE__ = "int"
const m___INT64_MAX__ = 0x7fffffffffffffff
const m___INT8_MAX__ = 0x7f
const m___INTMAX_MAX__ = 0x7fffffffffffffff
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_MAX__ = 0x7fffffffffffffff
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_MAX__ = 0x7fffffffffffffff
const m___INT_FAST16_WIDTH__ = 64
const m___INT_FAST32_MAX__ = 0x7fffffffffffffff
const m___INT_FAST32_WIDTH__ = 64
const m___INT_FAST64_MAX__ = 0x7fffffffffffffff
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_MAX__ = 0x7f
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_MAX__ = 0x7fff
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_MAX__ = 0x7fffffff
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_MAX__ = 0x7fffffffffffffff
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_MAX__ = 0x7f
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 0x7fffffff
const m___INT_WIDTH__ = 32
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.2204460492503131e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.7976931348623157e+308
const m___LDBL_MIN__ = 2.2250738585072014e-308
const m___LDBL_NORM_MAX__ = 1.7976931348623157e+308
const m___LITTLE_ENDIAN = 1234
const m___LONG_LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_LONG_WIDTH__ = 64
const m___LONG_MAX = 0x7fffffffffffffff
const m___LONG_MAX__ = 0x7fffffffffffffff
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___NO_INLINE__ = 1
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PDP_ENDIAN = 3412
const m___PIC__ = 2
const m___PIE__ = 2
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTRDIFF_MAX__ = 0x7fffffffffffffff
const m___PTRDIFF_WIDTH__ = 64
const m___SCHAR_MAX__ = 0x7f
const m___SCHAR_WIDTH__ = 8
const m___SHRT_MAX__ = 0x7fff
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 0x7fffffff
const m___SIG_ATOMIC_TYPE__ = "int"
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_MAX__ = 0xffffffffffffffff
const m___SIZE_WIDTH__ = 64
const m___STDC_HOSTED__ = 1
const m___STDC_IEC_559_COMPLEX__ = 1
const m___STDC_IEC_559__ = 1
const m___STDC_ISO_10646__ = 201706
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC__ = 1
const m___UINT16_MAX__ = 0xffff
const m___UINT32_MAX__ = 0xffffffff
const m___UINT64_MAX__ = 0xffffffffffffffff
const m___UINT8_MAX__ = 0xff
const m___UINTMAX_MAX__ = 0xffffffffffffffff
const m___UINTPTR_MAX__ = 0xffffffffffffffff
const m___UINT_FAST16_MAX__ = 0xffffffffffffffff
const m___UINT_FAST32_MAX__ = 0xffffffffffffffff
const m___UINT_FAST64_MAX__ = 0xffffffffffffffff
const m___UINT_FAST8_MAX__ = 0xff
const m___UINT_LEAST16_MAX__ = 0xffff
const m___UINT_LEAST32_MAX__ = 0xffffffff
const m___UINT_LEAST64_MAX__ = 0xffffffffffffffff
const m___UINT_LEAST8_MAX__ = 0xff
const m___USE_TIME_BITS64 = 1
const m___VERSION__ = "10.2.1 20210110"
const m___WCHAR_MAX__ = 0x7fffffff
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 0xffffffff
const m___WINT_MIN__ = 0
const m___WINT_WIDTH__ = 32
const m___gnu_linux__ = 1
const m___inline = "inline"
const m___linux = 1
const m___linux__ = 1
const m___pic__ = 2
const m___pie__ = 2
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___s390__ = 1
const m___s390x__ = 1
const m___unix = 1
const m___unix__ = 1
const m___zarch__ = 1
const m_alloca = "__builtin_alloca"
const m_inline = "__inline"
const m_linux = 1
const m_loff_t = "off_t"
const m_static_assert = "_Static_assert"
const m_unix = 1

type t__builtin_va_list = uintptr

type t__predefined_size_t = uint64

type t__predefined_wchar_t = int32

type t__predefined_ptrdiff_t = int64

type Twchar_t = int32

type Tmax_align_t = struct {
	F__ll int64
	F__ld float64
}

type Tsize_t = uint64

type Tptrdiff_t = int64

type Tlocale_t = uintptr

type Tssize_t = int64

type Toff_t = int64

type Tva_list = uintptr

type t__isoc_va_list = uintptr

type Tfpos_t = struct {
	F__lldata [0]int64
	F__align  [0]float64
	F__opaque [16]uint8
}

type T_G_fpos64_t = Tfpos_t

type Tcookie_io_functions_t = struct {
	Fread   uintptr
	Fwrite  uintptr
	Fseek   uintptr
	Fclose1 uintptr
}

type T_IO_cookie_io_functions_t = Tcookie_io_functions_t

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tuintptr_t = uint64

type Tintptr_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tintmax_t = int64

type Tuint8_t = uint8

type Tuint16_t = uint16

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tuintmax_t = uint64

type Tint_fast8_t = int8

type Tint_fast64_t = int64

type Tint_least8_t = int8

type Tint_least16_t = int16

type Tint_least32_t = int32

type Tint_least64_t = int64

type Tuint_fast8_t = uint8

type Tuint_fast64_t = uint64

type Tuint_least8_t = uint8

type Tuint_least16_t = uint16

type Tuint_least32_t = uint32

type Tuint_least64_t = uint64

type Tint_fast16_t = int32

type Tint_fast32_t = int32

type Tuint_fast16_t = uint32

type Tuint_fast32_t = uint32

type Ttime_t = int64

type Tsuseconds_t = int64

type Ttimeval = struct {
	Ftv_sec  Ttime_t
	Ftv_usec Tsuseconds_t
}

type Ttimespec = struct {
	Ftv_sec  Ttime_t
	Ftv_nsec int64
}

type Tsigset_t = struct {
	F__bits [16]uint64
}

type t__sigset_t = Tsigset_t

type Tfd_mask = uint64

type Tfd_set = struct {
	Ffds_bits [16]uint64
}

type Titimerval = struct {
	Fit_interval Ttimeval
	Fit_value    Ttimeval
}

type Ttimezone = struct {
	Ftz_minuteswest int32
	Ftz_dsttime     int32
}

type Tregister_t = int64

type Tu_int64_t = uint64

type Tmode_t = uint32

type Tnlink_t = uint64

type Tino_t = uint64

type Tdev_t = uint64

type Tblksize_t = int64

type Tblkcnt_t = int64

type Tfsblkcnt_t = uint64

type Tfsfilcnt_t = uint64

type Ttimer_t = uintptr

type Tclockid_t = int32

type Tclock_t = int64

type Tpid_t = int32

type Tid_t = uint32

type Tuid_t = uint32

type Tgid_t = uint32

type Tkey_t = int32

type Tuseconds_t = uint32

type Tpthread_t = uintptr

type Tpthread_once_t = int32

type Tpthread_key_t = uint32

type Tpthread_spinlock_t = int32

type Tpthread_mutexattr_t = struct {
	F__attr uint32
}

type Tpthread_condattr_t = struct {
	F__attr uint32
}

type Tpthread_barrierattr_t = struct {
	F__attr uint32
}

type Tpthread_rwlockattr_t = struct {
	F__attr [2]uint32
}

type Tpthread_attr_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__s  [0][7]uint64
		F__i  [14]int32
	}
}

type Tpthread_mutex_t = struct {
	F__u struct {
		F__vi [0][10]int32
		F__p  [0][5]uintptr
		F__i  [10]int32
	}
}

type Tpthread_cond_t = struct {
	F__u struct {
		F__vi [0][12]int32
		F__p  [0][6]uintptr
		F__i  [12]int32
	}
}

type Tpthread_rwlock_t = struct {
	F__u struct {
		F__vi [0][14]int32
		F__p  [0][7]uintptr
		F__i  [14]int32
	}
}

type Tpthread_barrier_t = struct {
	F__u struct {
		F__vi [0][8]int32
		F__p  [0][4]uintptr
		F__i  [8]int32
	}
}

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tcaddr_t = uintptr

type Tu_char = uint8

type Tu_short = uint16

type Tushort = uint16

type Tu_int = uint32

type Tuint = uint32

type Tu_long = uint64

type Tulong = uint64

type Tquad_t = int64

type Tu_quad_t = uint64

type Tiovec = struct {
	Fiov_base uintptr
	Fiov_len  Tsize_t
}

type Tflock = struct {
	Fl_type   int16
	Fl_whence int16
	Fl_start  Toff_t
	Fl_len    Toff_t
	Fl_pid    Tpid_t
}

type Tfile_handle = struct {
	Fhandle_bytes uint32
	Fhandle_type  int32
}

type Tf_owner_ex = struct {
	Ftype1 int32
	Fpid   Tpid_t
}

type TXML_Char = uint8

type TXML_LChar = uint8

type TXML_Index = int64

type TXML_Size = uint64

type TXML_ParserStruct = struct {
	Fm_userData                      uintptr
	Fm_handlerArg                    uintptr
	Fm_buffer                        uintptr
	Fm_mem                           TXML_Memory_Handling_Suite
	Fm_bufferPtr                     uintptr
	Fm_bufferEnd                     uintptr
	Fm_bufferLim                     uintptr
	Fm_parseEndByteIndex             TXML_Index
	Fm_parseEndPtr                   uintptr
	Fm_dataBuf                       uintptr
	Fm_dataBufEnd                    uintptr
	Fm_startElementHandler           TXML_StartElementHandler
	Fm_endElementHandler             TXML_EndElementHandler
	Fm_characterDataHandler          TXML_CharacterDataHandler
	Fm_processingInstructionHandler  TXML_ProcessingInstructionHandler
	Fm_commentHandler                TXML_CommentHandler
	Fm_startCdataSectionHandler      TXML_StartCdataSectionHandler
	Fm_endCdataSectionHandler        TXML_EndCdataSectionHandler
	Fm_defaultHandler                TXML_DefaultHandler
	Fm_startDoctypeDeclHandler       TXML_StartDoctypeDeclHandler
	Fm_endDoctypeDeclHandler         TXML_EndDoctypeDeclHandler
	Fm_unparsedEntityDeclHandler     TXML_UnparsedEntityDeclHandler
	Fm_notationDeclHandler           TXML_NotationDeclHandler
	Fm_startNamespaceDeclHandler     TXML_StartNamespaceDeclHandler
	Fm_endNamespaceDeclHandler       TXML_EndNamespaceDeclHandler
	Fm_notStandaloneHandler          TXML_NotStandaloneHandler
	Fm_externalEntityRefHandler      TXML_ExternalEntityRefHandler
	Fm_externalEntityRefHandlerArg   TXML_Parser
	Fm_skippedEntityHandler          TXML_SkippedEntityHandler
	Fm_unknownEncodingHandler        TXML_UnknownEncodingHandler
	Fm_elementDeclHandler            TXML_ElementDeclHandler
	Fm_attlistDeclHandler            TXML_AttlistDeclHandler
	Fm_entityDeclHandler             TXML_EntityDeclHandler
	Fm_xmlDeclHandler                TXML_XmlDeclHandler
	Fm_encoding                      uintptr
	Fm_initEncoding                  TINIT_ENCODING
	Fm_internalEncoding              uintptr
	Fm_protocolEncodingName          uintptr
	Fm_ns                            TXML_Bool
	Fm_ns_triplets                   TXML_Bool
	Fm_unknownEncodingMem            uintptr
	Fm_unknownEncodingData           uintptr
	Fm_unknownEncodingHandlerData    uintptr
	Fm_unknownEncodingRelease        uintptr
	Fm_prologState                   TPROLOG_STATE
	Fm_processor                     uintptr
	Fm_errorCode                     _XML_Error
	Fm_eventPtr                      uintptr
	Fm_eventEndPtr                   uintptr
	Fm_positionPtr                   uintptr
	Fm_openInternalEntities          uintptr
	Fm_freeInternalEntities          uintptr
	Fm_defaultExpandInternalEntities TXML_Bool
	Fm_tagLevel                      int32
	Fm_declEntity                    uintptr
	Fm_doctypeName                   uintptr
	Fm_doctypeSysid                  uintptr
	Fm_doctypePubid                  uintptr
	Fm_declAttributeType             uintptr
	Fm_declNotationName              uintptr
	Fm_declNotationPublicId          uintptr
	Fm_declElementType               uintptr
	Fm_declAttributeId               uintptr
	Fm_declAttributeIsCdata          TXML_Bool
	Fm_declAttributeIsId             TXML_Bool
	Fm_dtd                           uintptr
	Fm_curBase                       uintptr
	Fm_tagStack                      uintptr
	Fm_freeTagList                   uintptr
	Fm_inheritedBindings             uintptr
	Fm_freeBindingList               uintptr
	Fm_attsSize                      int32
	Fm_nSpecifiedAtts                int32
	Fm_idAttIndex                    int32
	Fm_atts                          uintptr
	Fm_nsAtts                        uintptr
	Fm_nsAttsVersion                 uint64
	Fm_nsAttsPower                   uint8
	Fm_position                      TPOSITION
	Fm_tempPool                      TSTRING_POOL
	Fm_temp2Pool                     TSTRING_POOL
	Fm_groupConnector                uintptr
	Fm_groupSize                     uint32
	Fm_namespaceSeparator            TXML_Char
	Fm_parentParser                  TXML_Parser
	Fm_parsingStatus                 TXML_ParsingStatus
	Fm_isParamEntity                 TXML_Bool
	Fm_useForeignDTD                 TXML_Bool
	Fm_paramEntityParsing            _XML_ParamEntityParsing
	Fm_hash_secret_salt              uint64
}

type _XML_Error = int32

const _XML_ERROR_NONE = 0
const _XML_ERROR_NO_MEMORY = 1
const _XML_ERROR_SYNTAX = 2
const _XML_ERROR_NO_ELEMENTS = 3
const _XML_ERROR_INVALID_TOKEN = 4
const _XML_ERROR_UNCLOSED_TOKEN = 5
const _XML_ERROR_PARTIAL_CHAR = 6
const _XML_ERROR_TAG_MISMATCH = 7
const _XML_ERROR_DUPLICATE_ATTRIBUTE = 8
const _XML_ERROR_JUNK_AFTER_DOC_ELEMENT = 9
const _XML_ERROR_PARAM_ENTITY_REF = 10
const _XML_ERROR_UNDEFINED_ENTITY = 11
const _XML_ERROR_RECURSIVE_ENTITY_REF = 12
const _XML_ERROR_ASYNC_ENTITY = 13
const _XML_ERROR_BAD_CHAR_REF = 14
const _XML_ERROR_BINARY_ENTITY_REF = 15
const _XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF = 16
const _XML_ERROR_MISPLACED_XML_PI = 17
const _XML_ERROR_UNKNOWN_ENCODING = 18
const _XML_ERROR_INCORRECT_ENCODING = 19
const _XML_ERROR_UNCLOSED_CDATA_SECTION = 20
const _XML_ERROR_EXTERNAL_ENTITY_HANDLING = 21
const _XML_ERROR_NOT_STANDALONE = 22
const _XML_ERROR_UNEXPECTED_STATE = 23
const _XML_ERROR_ENTITY_DECLARED_IN_PE = 24
const _XML_ERROR_FEATURE_REQUIRES_XML_DTD = 25
const _XML_ERROR_CANT_CHANGE_FEATURE_ONCE_PARSING = 26
const _XML_ERROR_UNBOUND_PREFIX = 27
const _XML_ERROR_UNDECLARING_PREFIX = 28
const _XML_ERROR_INCOMPLETE_PE = 29
const _XML_ERROR_XML_DECL = 30
const _XML_ERROR_TEXT_DECL = 31
const _XML_ERROR_PUBLICID = 32
const _XML_ERROR_SUSPENDED = 33
const _XML_ERROR_NOT_SUSPENDED = 34
const _XML_ERROR_ABORTED = 35
const _XML_ERROR_FINISHED = 36
const _XML_ERROR_SUSPEND_PE = 37
const _XML_ERROR_RESERVED_PREFIX_XML = 38
const _XML_ERROR_RESERVED_PREFIX_XMLNS = 39
const _XML_ERROR_RESERVED_NAMESPACE_URI = 40
const _XML_ERROR_INVALID_ARGUMENT = 41

type _XML_ParamEntityParsing = int32

const _XML_PARAM_ENTITY_PARSING_NEVER = 0
const _XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE = 1
const _XML_PARAM_ENTITY_PARSING_ALWAYS = 2

type TXML_Parser = uintptr

type TXML_Bool = uint8

type _XML_Status = int32

const _XML_STATUS_ERROR = 0
const _XML_STATUS_OK = 1
const _XML_STATUS_SUSPENDED = 2

type _XML_Content_Type = int32

const _XML_CTYPE_EMPTY = 1
const _XML_CTYPE_ANY = 2
const _XML_CTYPE_MIXED = 3
const _XML_CTYPE_NAME = 4
const _XML_CTYPE_CHOICE = 5
const _XML_CTYPE_SEQ = 6

type _XML_Content_Quant = int32

const _XML_CQUANT_NONE = 0
const _XML_CQUANT_OPT = 1
const _XML_CQUANT_REP = 2
const _XML_CQUANT_PLUS = 3

type TXML_Content = struct {
	Ftype1       _XML_Content_Type
	Fquant       _XML_Content_Quant
	Fname        uintptr
	Fnumchildren uint32
	Fchildren    uintptr
}

type TXML_cp = TXML_Content

type TXML_ElementDeclHandler = uintptr

type TXML_AttlistDeclHandler = uintptr

type TXML_XmlDeclHandler = uintptr

type TXML_Memory_Handling_Suite = struct {
	Fmalloc_fcn  uintptr
	Frealloc_fcn uintptr
	Ffree_fcn    uintptr
}

type TXML_StartElementHandler = uintptr

type TXML_EndElementHandler = uintptr

type TXML_CharacterDataHandler = uintptr

type TXML_ProcessingInstructionHandler = uintptr

type TXML_CommentHandler = uintptr

type TXML_StartCdataSectionHandler = uintptr

type TXML_EndCdataSectionHandler = uintptr

type TXML_DefaultHandler = uintptr

type TXML_StartDoctypeDeclHandler = uintptr

type TXML_EndDoctypeDeclHandler = uintptr

type TXML_EntityDeclHandler = uintptr

type TXML_UnparsedEntityDeclHandler = uintptr

type TXML_NotationDeclHandler = uintptr

type TXML_StartNamespaceDeclHandler = uintptr

type TXML_EndNamespaceDeclHandler = uintptr

type TXML_NotStandaloneHandler = uintptr

type TXML_ExternalEntityRefHandler = uintptr

type TXML_SkippedEntityHandler = uintptr

type TXML_Encoding = struct {
	Fmap1    [256]int32
	Fdata    uintptr
	Fconvert uintptr
	Frelease uintptr
}

type TXML_UnknownEncodingHandler = uintptr

type _XML_Parsing = int32

const _XML_INITIALIZED = 0
const _XML_PARSING = 1
const _XML_FINISHED = 2
const _XML_SUSPENDED = 3

type TXML_ParsingStatus = struct {
	Fparsing     _XML_Parsing
	FfinalBuffer TXML_Bool
}

type TXML_Expat_Version = struct {
	Fmajor int32
	Fminor int32
	Fmicro int32
}

type _XML_FeatureEnum = int32

const _XML_FEATURE_END = 0
const _XML_FEATURE_UNICODE = 1
const _XML_FEATURE_UNICODE_WCHAR_T = 2
const _XML_FEATURE_DTD = 3
const _XML_FEATURE_CONTEXT_BYTES = 4
const _XML_FEATURE_MIN_SIZE = 5
const _XML_FEATURE_SIZEOF_XML_CHAR = 6
const _XML_FEATURE_SIZEOF_XML_LCHAR = 7
const _XML_FEATURE_NS = 8
const _XML_FEATURE_LARGE_SIZE = 9
const _XML_FEATURE_ATTR_INFO = 10

type TXML_Feature = struct {
	Ffeature _XML_FeatureEnum
	Fname    uintptr
	Fvalue   int64
}

type Tsiphash = struct {
	Fv0  Tuint64_t
	Fv1  Tuint64_t
	Fv2  Tuint64_t
	Fv3  Tuint64_t
	Fbuf [8]uint8
	Fp   uintptr
	Fc   Tuint64_t
}

type Tsipkey = struct {
	Fk [2]Tuint64_t
}

func _sip_tokey(tls *libc.TLS, key uintptr, src uintptr) (r uintptr) {
	*(*Tuint64_t)(unsafe.Pointer(key)) = uint64(*(*uint8)(unsafe.Pointer(src)))<<libc.Int32FromInt32(0) | uint64(*(*uint8)(unsafe.Pointer(src + 1)))<<libc.Int32FromInt32(8) | uint64(*(*uint8)(unsafe.Pointer(src + 2)))<<libc.Int32FromInt32(16) | uint64(*(*uint8)(unsafe.Pointer(src + 3)))<<libc.Int32FromInt32(24) | uint64(*(*uint8)(unsafe.Pointer(src + 4)))<<libc.Int32FromInt32(32) | uint64(*(*uint8)(unsafe.Pointer(src + 5)))<<libc.Int32FromInt32(40) | uint64(*(*uint8)(unsafe.Pointer(src + 6)))<<libc.Int32FromInt32(48) | uint64(*(*uint8)(unsafe.Pointer(src + 7)))<<libc.Int32FromInt32(56)
	*(*Tuint64_t)(unsafe.Pointer(key + 1*8)) = uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8))))<<libc.Int32FromInt32(0) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 1)))<<libc.Int32FromInt32(8) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 2)))<<libc.Int32FromInt32(16) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 3)))<<libc.Int32FromInt32(24) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 4)))<<libc.Int32FromInt32(32) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 5)))<<libc.Int32FromInt32(40) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 6)))<<libc.Int32FromInt32(48) | uint64(*(*uint8)(unsafe.Pointer(src + libc.UintptrFromInt32(8) + 7)))<<libc.Int32FromInt32(56)
	return key
}

func _sip_round(tls *libc.TLS, H uintptr, rounds int32) {
	var i int32
	_ = i
	i = 0
	for {
		if !(i < rounds) {
			break
		}
		*(*Tuint64_t)(unsafe.Pointer(H)) += (*Tsiphash)(unsafe.Pointer(H)).Fv1
		(*Tsiphash)(unsafe.Pointer(H)).Fv1 = (*Tsiphash)(unsafe.Pointer(H)).Fv1<<libc.Int32FromInt32(13) | (*Tsiphash)(unsafe.Pointer(H)).Fv1>>(libc.Int32FromInt32(64)-libc.Int32FromInt32(13))
		*(*Tuint64_t)(unsafe.Pointer(H + 8)) ^= (*Tsiphash)(unsafe.Pointer(H)).Fv0
		(*Tsiphash)(unsafe.Pointer(H)).Fv0 = (*Tsiphash)(unsafe.Pointer(H)).Fv0<<libc.Int32FromInt32(32) | (*Tsiphash)(unsafe.Pointer(H)).Fv0>>(libc.Int32FromInt32(64)-libc.Int32FromInt32(32))
		*(*Tuint64_t)(unsafe.Pointer(H + 16)) += (*Tsiphash)(unsafe.Pointer(H)).Fv3
		(*Tsiphash)(unsafe.Pointer(H)).Fv3 = (*Tsiphash)(unsafe.Pointer(H)).Fv3<<libc.Int32FromInt32(16) | (*Tsiphash)(unsafe.Pointer(H)).Fv3>>(libc.Int32FromInt32(64)-libc.Int32FromInt32(16))
		*(*Tuint64_t)(unsafe.Pointer(H + 24)) ^= (*Tsiphash)(unsafe.Pointer(H)).Fv2
		*(*Tuint64_t)(unsafe.Pointer(H)) += (*Tsiphash)(unsafe.Pointer(H)).Fv3
		(*Tsiphash)(unsafe.Pointer(H)).Fv3 = (*Tsiphash)(unsafe.Pointer(H)).Fv3<<libc.Int32FromInt32(21) | (*Tsiphash)(unsafe.Pointer(H)).Fv3>>(libc.Int32FromInt32(64)-libc.Int32FromInt32(21))
		*(*Tuint64_t)(unsafe.Pointer(H + 24)) ^= (*Tsiphash)(unsafe.Pointer(H)).Fv0
		*(*Tuint64_t)(unsafe.Pointer(H + 16)) += (*Tsiphash)(unsafe.Pointer(H)).Fv1
		(*Tsiphash)(unsafe.Pointer(H)).Fv1 = (*Tsiphash)(unsafe.Pointer(H)).Fv1<<libc.Int32FromInt32(17) | (*Tsiphash)(unsafe.Pointer(H)).Fv1>>(libc.Int32FromInt32(64)-libc.Int32FromInt32(17))
		*(*Tuint64_t)(unsafe.Pointer(H + 8)) ^= (*Tsiphash)(unsafe.Pointer(H)).Fv2
		(*Tsiphash)(unsafe.Pointer(H)).Fv2 = (*Tsiphash)(unsafe.Pointer(H)).Fv2<<libc.Int32FromInt32(32) | (*Tsiphash)(unsafe.Pointer(H)).Fv2>>(libc.Int32FromInt32(64)-libc.Int32FromInt32(32))
		goto _1
	_1:
		;
		i++
	}
}

func _sip24_init(tls *libc.TLS, H uintptr, key uintptr) (r uintptr) {
	(*Tsiphash)(unsafe.Pointer(H)).Fv0 = libc.Uint64FromUint32(0x736f6d65)<<libc.Int32FromInt32(32) | libc.Uint64FromUint32(0x70736575) ^ *(*Tuint64_t)(unsafe.Pointer(key))
	(*Tsiphash)(unsafe.Pointer(H)).Fv1 = libc.Uint64FromUint32(0x646f7261)<<libc.Int32FromInt32(32) | libc.Uint64FromUint32(0x6e646f6d) ^ *(*Tuint64_t)(unsafe.Pointer(key + 1*8))
	(*Tsiphash)(unsafe.Pointer(H)).Fv2 = libc.Uint64FromUint32(0x6c796765)<<libc.Int32FromInt32(32) | libc.Uint64FromUint32(0x6e657261) ^ *(*Tuint64_t)(unsafe.Pointer(key))
	(*Tsiphash)(unsafe.Pointer(H)).Fv3 = libc.Uint64FromUint32(0x74656462)<<libc.Int32FromInt32(32) | libc.Uint64FromUint32(0x79746573) ^ *(*Tuint64_t)(unsafe.Pointer(key + 1*8))
	(*Tsiphash)(unsafe.Pointer(H)).Fp = H + 32
	(*Tsiphash)(unsafe.Pointer(H)).Fc = uint64(0)
	return H
}

func _sip24_update(tls *libc.TLS, H uintptr, src uintptr, len1 Tsize_t) (r uintptr) {
	var m Tuint64_t
	var p, pe, v1, v2, v3 uintptr
	_, _, _, _, _, _ = m, p, pe, v1, v2, v3
	p = src
	pe = p + uintptr(len1)
	for cond := true; cond; cond = p < pe {
		for p < pe && (*Tsiphash)(unsafe.Pointer(H)).Fp < H+32+uintptr(libc.Uint64FromInt64(8)/libc.Uint64FromInt64(1)) {
			v2 = H + 40
			v1 = *(*uintptr)(unsafe.Pointer(v2))
			*(*uintptr)(unsafe.Pointer(v2))++
			v3 = p
			p++
			*(*uint8)(unsafe.Pointer(v1)) = *(*uint8)(unsafe.Pointer(v3))
		}
		if (*Tsiphash)(unsafe.Pointer(H)).Fp < H+32+uintptr(libc.Uint64FromInt64(8)/libc.Uint64FromInt64(1)) {
			break
		}
		m = uint64(*(*uint8)(unsafe.Pointer(H + 32)))<<libc.Int32FromInt32(0) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 1)))<<libc.Int32FromInt32(8) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 2)))<<libc.Int32FromInt32(16) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 3)))<<libc.Int32FromInt32(24) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 4)))<<libc.Int32FromInt32(32) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 5)))<<libc.Int32FromInt32(40) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 6)))<<libc.Int32FromInt32(48) | uint64(*(*uint8)(unsafe.Pointer(H + 32 + 7)))<<libc.Int32FromInt32(56)
		*(*Tuint64_t)(unsafe.Pointer(H + 24)) ^= m
		_sip_round(tls, H, int32(2))
		*(*Tuint64_t)(unsafe.Pointer(H)) ^= m
		(*Tsiphash)(unsafe.Pointer(H)).Fp = H + 32
		*(*Tuint64_t)(unsafe.Pointer(H + 48)) += uint64(8)
	}
	return H
}

func _sip24_final(tls *libc.TLS, H uintptr) (r Tuint64_t) {
	var b Tuint64_t
	var left uint8
	_, _ = b, left
	left = libc.Uint8FromInt64(int64((*Tsiphash)(unsafe.Pointer(H)).Fp) - t__predefined_ptrdiff_t(H+32))
	b = ((*Tsiphash)(unsafe.Pointer(H)).Fc + uint64(left)) << int32(56)
	switch libc.Int32FromUint8(left) {
	case int32(7):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32 + 6))) << int32(48)
		fallthrough
	case int32(6):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32 + 5))) << int32(40)
		fallthrough
	case int32(5):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32 + 4))) << int32(32)
		fallthrough
	case int32(4):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32 + 3))) << int32(24)
		fallthrough
	case int32(3):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32 + 2))) << int32(16)
		fallthrough
	case int32(2):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32 + 1))) << int32(8)
		fallthrough
	case int32(1):
		b |= uint64(*(*uint8)(unsafe.Pointer(H + 32))) << 0
		fallthrough
	case 0:
		break
	}
	*(*Tuint64_t)(unsafe.Pointer(H + 24)) ^= b
	_sip_round(tls, H, int32(2))
	*(*Tuint64_t)(unsafe.Pointer(H)) ^= b
	*(*Tuint64_t)(unsafe.Pointer(H + 16)) ^= uint64(0xff)
	_sip_round(tls, H, int32(4))
	return (*Tsiphash)(unsafe.Pointer(H)).Fv0 ^ (*Tsiphash)(unsafe.Pointer(H)).Fv1 ^ (*Tsiphash)(unsafe.Pointer(H)).Fv2 ^ (*Tsiphash)(unsafe.Pointer(H)).Fv3
}

func _siphash24(tls *libc.TLS, src uintptr, len1 Tsize_t, key uintptr) (r Tuint64_t) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var _ /* state at bp+0 */ Tsiphash
	*(*Tsiphash)(unsafe.Pointer(bp)) = Tsiphash{}
	return _sip24_final(tls, _sip24_update(tls, _sip24_init(tls, bp, key), src, len1))
}

func _sip24_valid(tls *libc.TLS) (r int32) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var i Tsize_t
	var _ /* in at bp+0 */ [64]uint8
	var _ /* k at bp+64 */ Tsipkey
	_ = i
	_sip_tokey(tls, bp+64, __ccgo_ts)
	i = uint64(0)
	for {
		if !(i < uint64(64)) {
			break
		}
		(*(*[64]uint8)(unsafe.Pointer(bp)))[i] = uint8(i)
		if _siphash24(tls, bp, i, bp+64) != uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8)))<<libc.Int32FromInt32(0)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 1)))<<libc.Int32FromInt32(8)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 2)))<<libc.Int32FromInt32(16)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 3)))<<libc.Int32FromInt32(24)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 4)))<<libc.Int32FromInt32(32)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 5)))<<libc.Int32FromInt32(40)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 6)))<<libc.Int32FromInt32(48)|uint64(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_vectors)) + uintptr(i)*8 + 7)))<<libc.Int32FromInt32(56) {
			return 0
		}
		goto _1
	_1:
		;
		i++
	}
	return int32(1)
}

var _vectors = [64][8]uint8{
	0: {
		0: uint8(0x31),
		1: uint8(0x0e),
		2: uint8(0x0e),
		3: uint8(0xdd),
		4: uint8(0x47),
		5: uint8(0xdb),
		6: uint8(0x6f),
		7: uint8(0x72),
	},
	1: {
		0: uint8(0xfd),
		1: uint8(0x67),
		2: uint8(0xdc),
		3: uint8(0x93),
		4: uint8(0xc5),
		5: uint8(0x39),
		6: uint8(0xf8),
		7: uint8(0x74),
	},
	2: {
		0: uint8(0x5a),
		1: uint8(0x4f),
		2: uint8(0xa9),
		3: uint8(0xd9),
		4: uint8(0x09),
		5: uint8(0x80),
		6: uint8(0x6c),
		7: uint8(0x0d),
	},
	3: {
		0: uint8(0x2d),
		1: uint8(0x7e),
		2: uint8(0xfb),
		3: uint8(0xd7),
		4: uint8(0x96),
		5: uint8(0x66),
		6: uint8(0x67),
		7: uint8(0x85),
	},
	4: {
		0: uint8(0xb7),
		1: uint8(0x87),
		2: uint8(0x71),
		3: uint8(0x27),
		4: uint8(0xe0),
		5: uint8(0x94),
		6: uint8(0x27),
		7: uint8(0xcf),
	},
	5: {
		0: uint8(0x8d),
		1: uint8(0xa6),
		2: uint8(0x99),
		3: uint8(0xcd),
		4: uint8(0x64),
		5: uint8(0x55),
		6: uint8(0x76),
		7: uint8(0x18),
	},
	6: {
		0: uint8(0xce),
		1: uint8(0xe3),
		2: uint8(0xfe),
		3: uint8(0x58),
		4: uint8(0x6e),
		5: uint8(0x46),
		6: uint8(0xc9),
		7: uint8(0xcb),
	},
	7: {
		0: uint8(0x37),
		1: uint8(0xd1),
		2: uint8(0x01),
		3: uint8(0x8b),
		4: uint8(0xf5),
		6: uint8(0x02),
		7: uint8(0xab),
	},
	8: {
		0: uint8(0x62),
		1: uint8(0x24),
		2: uint8(0x93),
		3: uint8(0x9a),
		4: uint8(0x79),
		5: uint8(0xf5),
		6: uint8(0xf5),
		7: uint8(0x93),
	},
	9: {
		0: uint8(0xb0),
		1: uint8(0xe4),
		2: uint8(0xa9),
		3: uint8(0x0b),
		4: uint8(0xdf),
		5: uint8(0x82),
		7: uint8(0x9e),
	},
	10: {
		0: uint8(0xf3),
		1: uint8(0xb9),
		2: uint8(0xdd),
		3: uint8(0x94),
		4: uint8(0xc5),
		5: uint8(0xbb),
		6: uint8(0x5d),
		7: uint8(0x7a),
	},
	11: {
		0: uint8(0xa7),
		1: uint8(0xad),
		2: uint8(0x6b),
		3: uint8(0x22),
		4: uint8(0x46),
		5: uint8(0x2f),
		6: uint8(0xb3),
		7: uint8(0xf4),
	},
	12: {
		0: uint8(0xfb),
		1: uint8(0xe5),
		2: uint8(0x0e),
		3: uint8(0x86),
		4: uint8(0xbc),
		5: uint8(0x8f),
		6: uint8(0x1e),
		7: uint8(0x75),
	},
	13: {
		0: uint8(0x90),
		1: uint8(0x3d),
		2: uint8(0x84),
		3: uint8(0xc0),
		4: uint8(0x27),
		5: uint8(0x56),
		6: uint8(0xea),
		7: uint8(0x14),
	},
	14: {
		0: uint8(0xee),
		1: uint8(0xf2),
		2: uint8(0x7a),
		3: uint8(0x8e),
		4: uint8(0x90),
		5: uint8(0xca),
		6: uint8(0x23),
		7: uint8(0xf7),
	},
	15: {
		0: uint8(0xe5),
		1: uint8(0x45),
		2: uint8(0xbe),
		3: uint8(0x49),
		4: uint8(0x61),
		5: uint8(0xca),
		6: uint8(0x29),
		7: uint8(0xa1),
	},
	16: {
		0: uint8(0xdb),
		1: uint8(0x9b),
		2: uint8(0xc2),
		3: uint8(0x57),
		4: uint8(0x7f),
		5: uint8(0xcc),
		6: uint8(0x2a),
		7: uint8(0x3f),
	},
	17: {
		0: uint8(0x94),
		1: uint8(0x47),
		2: uint8(0xbe),
		3: uint8(0x2c),
		4: uint8(0xf5),
		5: uint8(0xe9),
		6: uint8(0x9a),
		7: uint8(0x69),
	},
	18: {
		0: uint8(0x9c),
		1: uint8(0xd3),
		2: uint8(0x8d),
		3: uint8(0x96),
		4: uint8(0xf0),
		5: uint8(0xb3),
		6: uint8(0xc1),
		7: uint8(0x4b),
	},
	19: {
		0: uint8(0xbd),
		1: uint8(0x61),
		2: uint8(0x79),
		3: uint8(0xa7),
		4: uint8(0x1d),
		5: uint8(0xc9),
		6: uint8(0x6d),
		7: uint8(0xbb),
	},
	20: {
		0: uint8(0x98),
		1: uint8(0xee),
		2: uint8(0xa2),
		3: uint8(0x1a),
		4: uint8(0xf2),
		5: uint8(0x5c),
		6: uint8(0xd6),
		7: uint8(0xbe),
	},
	21: {
		0: uint8(0xc7),
		1: uint8(0x67),
		2: uint8(0x3b),
		3: uint8(0x2e),
		4: uint8(0xb0),
		5: uint8(0xcb),
		6: uint8(0xf2),
		7: uint8(0xd0),
	},
	22: {
		0: uint8(0x88),
		1: uint8(0x3e),
		2: uint8(0xa3),
		3: uint8(0xe3),
		4: uint8(0x95),
		5: uint8(0x67),
		6: uint8(0x53),
		7: uint8(0x93),
	},
	23: {
		0: uint8(0xc8),
		1: uint8(0xce),
		2: uint8(0x5c),
		3: uint8(0xcd),
		4: uint8(0x8c),
		5: uint8(0x03),
		6: uint8(0x0c),
		7: uint8(0xa8),
	},
	24: {
		0: uint8(0x94),
		1: uint8(0xaf),
		2: uint8(0x49),
		3: uint8(0xf6),
		4: uint8(0xc6),
		5: uint8(0x50),
		6: uint8(0xad),
		7: uint8(0xb8),
	},
	25: {
		0: uint8(0xea),
		1: uint8(0xb8),
		2: uint8(0x85),
		3: uint8(0x8a),
		4: uint8(0xde),
		5: uint8(0x92),
		6: uint8(0xe1),
		7: uint8(0xbc),
	},
	26: {
		0: uint8(0xf3),
		1: uint8(0x15),
		2: uint8(0xbb),
		3: uint8(0x5b),
		4: uint8(0xb8),
		5: uint8(0x35),
		6: uint8(0xd8),
		7: uint8(0x17),
	},
	27: {
		0: uint8(0xad),
		1: uint8(0xcf),
		2: uint8(0x6b),
		3: uint8(0x07),
		4: uint8(0x63),
		5: uint8(0x61),
		6: uint8(0x2e),
		7: uint8(0x2f),
	},
	28: {
		0: uint8(0xa5),
		1: uint8(0xc9),
		2: uint8(0x1d),
		3: uint8(0xa7),
		4: uint8(0xac),
		5: uint8(0xaa),
		6: uint8(0x4d),
		7: uint8(0xde),
	},
	29: {
		0: uint8(0x71),
		1: uint8(0x65),
		2: uint8(0x95),
		3: uint8(0x87),
		4: uint8(0x66),
		5: uint8(0x50),
		6: uint8(0xa2),
		7: uint8(0xa6),
	},
	30: {
		0: uint8(0x28),
		1: uint8(0xef),
		2: uint8(0x49),
		3: uint8(0x5c),
		4: uint8(0x53),
		5: uint8(0xa3),
		6: uint8(0x87),
		7: uint8(0xad),
	},
	31: {
		0: uint8(0x42),
		1: uint8(0xc3),
		2: uint8(0x41),
		3: uint8(0xd8),
		4: uint8(0xfa),
		5: uint8(0x92),
		6: uint8(0xd8),
		7: uint8(0x32),
	},
	32: {
		0: uint8(0xce),
		1: uint8(0x7c),
		2: uint8(0xf2),
		3: uint8(0x72),
		4: uint8(0x2f),
		5: uint8(0x51),
		6: uint8(0x27),
		7: uint8(0x71),
	},
	33: {
		0: uint8(0xe3),
		1: uint8(0x78),
		2: uint8(0x59),
		3: uint8(0xf9),
		4: uint8(0x46),
		5: uint8(0x23),
		6: uint8(0xf3),
		7: uint8(0xa7),
	},
	34: {
		0: uint8(0x38),
		1: uint8(0x12),
		2: uint8(0x05),
		3: uint8(0xbb),
		4: uint8(0x1a),
		5: uint8(0xb0),
		6: uint8(0xe0),
		7: uint8(0x12),
	},
	35: {
		0: uint8(0xae),
		1: uint8(0x97),
		2: uint8(0xa1),
		3: uint8(0x0f),
		4: uint8(0xd4),
		5: uint8(0x34),
		6: uint8(0xe0),
		7: uint8(0x15),
	},
	36: {
		0: uint8(0xb4),
		1: uint8(0xa3),
		2: uint8(0x15),
		3: uint8(0x08),
		4: uint8(0xbe),
		5: uint8(0xff),
		6: uint8(0x4d),
		7: uint8(0x31),
	},
	37: {
		0: uint8(0x81),
		1: uint8(0x39),
		2: uint8(0x62),
		3: uint8(0x29),
		4: uint8(0xf0),
		5: uint8(0x90),
		6: uint8(0x79),
		7: uint8(0x02),
	},
	38: {
		0: uint8(0x4d),
		1: uint8(0x0c),
		2: uint8(0xf4),
		3: uint8(0x9e),
		4: uint8(0xe5),
		5: uint8(0xd4),
		6: uint8(0xdc),
		7: uint8(0xca),
	},
	39: {
		0: uint8(0x5c),
		1: uint8(0x73),
		2: uint8(0x33),
		3: uint8(0x6a),
		4: uint8(0x76),
		5: uint8(0xd8),
		6: uint8(0xbf),
		7: uint8(0x9a),
	},
	40: {
		0: uint8(0xd0),
		1: uint8(0xa7),
		2: uint8(0x04),
		3: uint8(0x53),
		4: uint8(0x6b),
		5: uint8(0xa9),
		6: uint8(0x3e),
		7: uint8(0x0e),
	},
	41: {
		0: uint8(0x92),
		1: uint8(0x59),
		2: uint8(0x58),
		3: uint8(0xfc),
		4: uint8(0xd6),
		5: uint8(0x42),
		6: uint8(0x0c),
		7: uint8(0xad),
	},
	42: {
		0: uint8(0xa9),
		1: uint8(0x15),
		2: uint8(0xc2),
		3: uint8(0x9b),
		4: uint8(0xc8),
		5: uint8(0x06),
		6: uint8(0x73),
		7: uint8(0x18),
	},
	43: {
		0: uint8(0x95),
		1: uint8(0x2b),
		2: uint8(0x79),
		3: uint8(0xf3),
		4: uint8(0xbc),
		5: uint8(0x0a),
		6: uint8(0xa6),
		7: uint8(0xd4),
	},
	44: {
		0: uint8(0xf2),
		1: uint8(0x1d),
		2: uint8(0xf2),
		3: uint8(0xe4),
		4: uint8(0x1d),
		5: uint8(0x45),
		6: uint8(0x35),
		7: uint8(0xf9),
	},
	45: {
		0: uint8(0x87),
		1: uint8(0x57),
		2: uint8(0x75),
		3: uint8(0x19),
		4: uint8(0x04),
		5: uint8(0x8f),
		6: uint8(0x53),
		7: uint8(0xa9),
	},
	46: {
		0: uint8(0x10),
		1: uint8(0xa5),
		2: uint8(0x6c),
		3: uint8(0xf5),
		4: uint8(0xdf),
		5: uint8(0xcd),
		6: uint8(0x9a),
		7: uint8(0xdb),
	},
	47: {
		0: uint8(0xeb),
		1: uint8(0x75),
		2: uint8(0x09),
		3: uint8(0x5c),
		4: uint8(0xcd),
		5: uint8(0x98),
		6: uint8(0x6c),
		7: uint8(0xd0),
	},
	48: {
		0: uint8(0x51),
		1: uint8(0xa9),
		2: uint8(0xcb),
		3: uint8(0x9e),
		4: uint8(0xcb),
		5: uint8(0xa3),
		6: uint8(0x12),
		7: uint8(0xe6),
	},
	49: {
		0: uint8(0x96),
		1: uint8(0xaf),
		2: uint8(0xad),
		3: uint8(0xfc),
		4: uint8(0x2c),
		5: uint8(0xe6),
		6: uint8(0x66),
		7: uint8(0xc7),
	},
	50: {
		0: uint8(0x72),
		1: uint8(0xfe),
		2: uint8(0x52),
		3: uint8(0x97),
		4: uint8(0x5a),
		5: uint8(0x43),
		6: uint8(0x64),
		7: uint8(0xee),
	},
	51: {
		0: uint8(0x5a),
		1: uint8(0x16),
		2: uint8(0x45),
		3: uint8(0xb2),
		4: uint8(0x76),
		5: uint8(0xd5),
		6: uint8(0x92),
		7: uint8(0xa1),
	},
	52: {
		0: uint8(0xb2),
		1: uint8(0x74),
		2: uint8(0xcb),
		3: uint8(0x8e),
		4: uint8(0xbf),
		5: uint8(0x87),
		6: uint8(0x87),
		7: uint8(0x0a),
	},
	53: {
		0: uint8(0x6f),
		1: uint8(0x9b),
		2: uint8(0xb4),
		3: uint8(0x20),
		4: uint8(0x3d),
		5: uint8(0xe7),
		6: uint8(0xb3),
		7: uint8(0x81),
	},
	54: {
		0: uint8(0xea),
		1: uint8(0xec),
		2: uint8(0xb2),
		3: uint8(0xa3),
		4: uint8(0x0b),
		5: uint8(0x22),
		6: uint8(0xa8),
		7: uint8(0x7f),
	},
	55: {
		0: uint8(0x99),
		1: uint8(0x24),
		2: uint8(0xa4),
		3: uint8(0x3c),
		4: uint8(0xc1),
		5: uint8(0x31),
		6: uint8(0x57),
		7: uint8(0x24),
	},
	56: {
		0: uint8(0xbd),
		1: uint8(0x83),
		2: uint8(0x8d),
		3: uint8(0x3a),
		4: uint8(0xaf),
		5: uint8(0xbf),
		6: uint8(0x8d),
		7: uint8(0xb7),
	},
	57: {
		0: uint8(0x0b),
		1: uint8(0x1a),
		2: uint8(0x2a),
		3: uint8(0x32),
		4: uint8(0x65),
		5: uint8(0xd5),
		6: uint8(0x1a),
		7: uint8(0xea),
	},
	58: {
		0: uint8(0x13),
		1: uint8(0x50),
		2: uint8(0x79),
		3: uint8(0xa3),
		4: uint8(0x23),
		5: uint8(0x1c),
		6: uint8(0xe6),
		7: uint8(0x60),
	},
	59: {
		0: uint8(0x93),
		1: uint8(0x2b),
		2: uint8(0x28),
		3: uint8(0x46),
		4: uint8(0xe4),
		5: uint8(0xd7),
		6: uint8(0x06),
		7: uint8(0x66),
	},
	60: {
		0: uint8(0xe1),
		1: uint8(0x91),
		2: uint8(0x5f),
		3: uint8(0x5c),
		4: uint8(0xb1),
		5: uint8(0xec),
		6: uint8(0xa4),
		7: uint8(0x6c),
	},
	61: {
		0: uint8(0xf3),
		1: uint8(0x25),
		2: uint8(0x96),
		3: uint8(0x5c),
		4: uint8(0xa1),
		5: uint8(0x6d),
		6: uint8(0x62),
		7: uint8(0x9f),
	},
	62: {
		0: uint8(0x57),
		1: uint8(0x5f),
		2: uint8(0xf2),
		3: uint8(0x8e),
		4: uint8(0x60),
		5: uint8(0x38),
		6: uint8(0x1b),
		7: uint8(0xe5),
	},
	63: {
		0: uint8(0x72),
		1: uint8(0x45),
		2: uint8(0x06),
		3: uint8(0xeb),
		4: uint8(0x4c),
		5: uint8(0x32),
		6: uint8(0x8a),
		7: uint8(0x95),
	},
}

type TICHAR = uint8

type TPOSITION = struct {
	FlineNumber   TXML_Size
	FcolumnNumber TXML_Size
}

type Tposition = TPOSITION

type TATTRIBUTE = struct {
	Fname       uintptr
	FvaluePtr   uintptr
	FvalueEnd   uintptr
	Fnormalized uint8
}

type Tencoding = struct {
	Fscanners             [4]TSCANNER
	FliteralScanners      [2]TSCANNER
	FnameMatchesAscii     uintptr
	FnameLength           uintptr
	FskipS                uintptr
	FgetAtts              uintptr
	FcharRefNumber        uintptr
	FpredefinedEntityName uintptr
	FupdatePosition       uintptr
	FisPublicId           uintptr
	Futf8Convert          uintptr
	Futf16Convert         uintptr
	FminBytesPerChar      int32
	FisUtf8               uint8
	FisUtf16              uint8
}

type TENCODING = struct {
	Fscanners             [4]TSCANNER
	FliteralScanners      [2]TSCANNER
	FnameMatchesAscii     uintptr
	FnameLength           uintptr
	FskipS                uintptr
	FgetAtts              uintptr
	FcharRefNumber        uintptr
	FpredefinedEntityName uintptr
	FupdatePosition       uintptr
	FisPublicId           uintptr
	Futf8Convert          uintptr
	Futf16Convert         uintptr
	FminBytesPerChar      int32
	FisUtf8               uint8
	FisUtf16              uint8
}

type TSCANNER = uintptr

type _XML_Convert_Result = int32

const _XML_CONVERT_COMPLETED = 0
const _XML_CONVERT_INPUT_INCOMPLETE = 1
const _XML_CONVERT_OUTPUT_EXHAUSTED = 2

type TINIT_ENCODING = struct {
	FinitEnc TENCODING
	FencPtr  uintptr
}

type TCONVERTER = uintptr

const _XML_ROLE_ERROR = -1
const _XML_ROLE_NONE = 0
const _XML_ROLE_XML_DECL = 1
const _XML_ROLE_INSTANCE_START = 2
const _XML_ROLE_DOCTYPE_NONE = 3
const _XML_ROLE_DOCTYPE_NAME = 4
const _XML_ROLE_DOCTYPE_SYSTEM_ID = 5
const _XML_ROLE_DOCTYPE_PUBLIC_ID = 6
const _XML_ROLE_DOCTYPE_INTERNAL_SUBSET = 7
const _XML_ROLE_DOCTYPE_CLOSE = 8
const _XML_ROLE_GENERAL_ENTITY_NAME = 9
const _XML_ROLE_PARAM_ENTITY_NAME = 10
const _XML_ROLE_ENTITY_NONE = 11
const _XML_ROLE_ENTITY_VALUE = 12
const _XML_ROLE_ENTITY_SYSTEM_ID = 13
const _XML_ROLE_ENTITY_PUBLIC_ID = 14
const _XML_ROLE_ENTITY_COMPLETE = 15
const _XML_ROLE_ENTITY_NOTATION_NAME = 16
const _XML_ROLE_NOTATION_NONE = 17
const _XML_ROLE_NOTATION_NAME = 18
const _XML_ROLE_NOTATION_SYSTEM_ID = 19
const _XML_ROLE_NOTATION_NO_SYSTEM_ID = 20
const _XML_ROLE_NOTATION_PUBLIC_ID = 21
const _XML_ROLE_ATTRIBUTE_NAME = 22
const _XML_ROLE_ATTRIBUTE_TYPE_CDATA = 23
const _XML_ROLE_ATTRIBUTE_TYPE_ID = 24
const _XML_ROLE_ATTRIBUTE_TYPE_IDREF = 25
const _XML_ROLE_ATTRIBUTE_TYPE_IDREFS = 26
const _XML_ROLE_ATTRIBUTE_TYPE_ENTITY = 27
const _XML_ROLE_ATTRIBUTE_TYPE_ENTITIES = 28
const _XML_ROLE_ATTRIBUTE_TYPE_NMTOKEN = 29
const _XML_ROLE_ATTRIBUTE_TYPE_NMTOKENS = 30
const _XML_ROLE_ATTRIBUTE_ENUM_VALUE = 31
const _XML_ROLE_ATTRIBUTE_NOTATION_VALUE = 32
const _XML_ROLE_ATTLIST_NONE = 33
const _XML_ROLE_ATTLIST_ELEMENT_NAME = 34
const _XML_ROLE_IMPLIED_ATTRIBUTE_VALUE = 35
const _XML_ROLE_REQUIRED_ATTRIBUTE_VALUE = 36
const _XML_ROLE_DEFAULT_ATTRIBUTE_VALUE = 37
const _XML_ROLE_FIXED_ATTRIBUTE_VALUE = 38
const _XML_ROLE_ELEMENT_NONE = 39
const _XML_ROLE_ELEMENT_NAME = 40
const _XML_ROLE_CONTENT_ANY = 41
const _XML_ROLE_CONTENT_EMPTY = 42
const _XML_ROLE_CONTENT_PCDATA = 43
const _XML_ROLE_GROUP_OPEN = 44
const _XML_ROLE_GROUP_CLOSE = 45
const _XML_ROLE_GROUP_CLOSE_REP = 46
const _XML_ROLE_GROUP_CLOSE_OPT = 47
const _XML_ROLE_GROUP_CLOSE_PLUS = 48
const _XML_ROLE_GROUP_CHOICE = 49
const _XML_ROLE_GROUP_SEQUENCE = 50
const _XML_ROLE_CONTENT_ELEMENT = 51
const _XML_ROLE_CONTENT_ELEMENT_REP = 52
const _XML_ROLE_CONTENT_ELEMENT_OPT = 53
const _XML_ROLE_CONTENT_ELEMENT_PLUS = 54
const _XML_ROLE_PI = 55
const _XML_ROLE_COMMENT = 56
const _XML_ROLE_TEXT_DECL = 57
const _XML_ROLE_IGNORE_SECT = 58
const _XML_ROLE_INNER_PARAM_ENTITY_REF = 59
const _XML_ROLE_PARAM_ENTITY_REF = 60

type TPROLOG_STATE = struct {
	Fhandler        uintptr
	Flevel          uint32
	Frole_none      int32
	FincludeLevel   uint32
	FdocumentEntity int32
	FinEntityValue  int32
}

type Tprolog_state = TPROLOG_STATE

type TKEY = uintptr

type TNAMED = struct {
	Fname TKEY
}

type THASH_TABLE = struct {
	Fv     uintptr
	Fpower uint8
	Fsize  Tsize_t
	Fused  Tsize_t
	Fmem   uintptr
}

/* For probing (after a collision) we need a step size relative prime
   to the hash table size, which is a power of 2. We use double-hashing,
   since we can calculate a second hash value cheaply by taking those bits
   of the first hash value that were discarded (masked out) when the table
   index was calculated: index = hash & mask, where mask = table->size - 1.
   We limit the maximum step size to table->size / 4 (mask >> 2) and make
   it odd, since odd numbers are always relative prime to a power of 2.
*/

type THASH_TABLE_ITER = struct {
	Fp   uintptr
	Fend uintptr
}

type TBINDING = struct {
	Fprefix            uintptr
	FnextTagBinding    uintptr
	FprevPrefixBinding uintptr
	FattId             uintptr
	Furi               uintptr
	FuriLen            int32
	FuriAlloc          int32
}

type Tbinding = TBINDING

type TPREFIX = struct {
	Fname    uintptr
	Fbinding uintptr
}

type Tprefix = TPREFIX

type TTAG_NAME = struct {
	Fstr       uintptr
	FlocalPart uintptr
	Fprefix    uintptr
	FstrLen    int32
	FuriLen    int32
	FprefixLen int32
}

// C documentation
//
//	/* TAG represents an open element.
//	   The name of the element is stored in both the document and API
//	   encodings.  The memory buffer 'buf' is a separately-allocated
//	   memory area which stores the name.  During the XML_Parse()/
//	   XMLParseBuffer() when the element is open, the memory for the 'raw'
//	   version of the name (in the document encoding) is shared with the
//	   document buffer.  If the element is open across calls to
//	   XML_Parse()/XML_ParseBuffer(), the buffer is re-allocated to
//	   contain the 'raw' name as well.
//
//	   A parser re-uses these structures, maintaining a list of allocated
//	   TAG objects in a free list.
//	*/
type TTAG = struct {
	Fparent        uintptr
	FrawName       uintptr
	FrawNameLength int32
	Fname          TTAG_NAME
	Fbuf           uintptr
	FbufEnd        uintptr
	Fbindings      uintptr
}

// C documentation
//
//	/* TAG represents an open element.
//	   The name of the element is stored in both the document and API
//	   encodings.  The memory buffer 'buf' is a separately-allocated
//	   memory area which stores the name.  During the XML_Parse()/
//	   XMLParseBuffer() when the element is open, the memory for the 'raw'
//	   version of the name (in the document encoding) is shared with the
//	   document buffer.  If the element is open across calls to
//	   XML_Parse()/XML_ParseBuffer(), the buffer is re-allocated to
//	   contain the 'raw' name as well.
//
//	   A parser re-uses these structures, maintaining a list of allocated
//	   TAG objects in a free list.
//	*/
type Ttag = TTAG

type TENTITY = struct {
	Fname        uintptr
	FtextPtr     uintptr
	FtextLen     int32
	Fprocessed   int32
	FsystemId    uintptr
	Fbase        uintptr
	FpublicId    uintptr
	Fnotation    uintptr
	Fopen        TXML_Bool
	Fis_param    TXML_Bool
	Fis_internal TXML_Bool
}

type TCONTENT_SCAFFOLD = struct {
	Ftype1      _XML_Content_Type
	Fquant      _XML_Content_Quant
	Fname       uintptr
	Ffirstchild int32
	Flastchild  int32
	Fchildcnt   int32
	Fnextsib    int32
}

type TBLOCK = struct {
	Fnext uintptr
	Fsize int32
	Fs    [1]TXML_Char
}

type Tblock = TBLOCK

type TSTRING_POOL = struct {
	Fblocks     uintptr
	FfreeBlocks uintptr
	Fend        uintptr
	Fptr        uintptr
	Fstart      uintptr
	Fmem        uintptr
}

// C documentation
//
//	/* The XML_Char before the name is used to determine whether
//	   an attribute has been specified. */
type TATTRIBUTE_ID = struct {
	Fname           uintptr
	Fprefix         uintptr
	FmaybeTokenized TXML_Bool
	Fxmlns          TXML_Bool
}

// C documentation
//
//	/* The XML_Char before the name is used to determine whether
//	   an attribute has been specified. */
type Tattribute_id = TATTRIBUTE_ID

type TDEFAULT_ATTRIBUTE = struct {
	Fid      uintptr
	FisCdata TXML_Bool
	Fvalue   uintptr
}

type TNS_ATT = struct {
	Fversion uint64
	Fhash    uint64
	FuriName uintptr
}

type TELEMENT_TYPE = struct {
	Fname             uintptr
	Fprefix           uintptr
	FidAtt            uintptr
	FnDefaultAtts     int32
	FallocDefaultAtts int32
	FdefaultAtts      uintptr
}

type TDTD = struct {
	FgeneralEntities    THASH_TABLE
	FelementTypes       THASH_TABLE
	FattributeIds       THASH_TABLE
	Fprefixes           THASH_TABLE
	Fpool               TSTRING_POOL
	FentityValuePool    TSTRING_POOL
	FkeepProcessing     TXML_Bool
	FhasParamEntityRefs TXML_Bool
	Fstandalone         TXML_Bool
	FparamEntityRead    TXML_Bool
	FparamEntities      THASH_TABLE
	FdefaultPrefix      TPREFIX
	Fin_eldecl          TXML_Bool
	Fscaffold           uintptr
	FcontentStringLen   uint32
	FscaffSize          uint32
	FscaffCount         uint32
	FscaffLevel         int32
	FscaffIndex         uintptr
}

type TOPEN_INTERNAL_ENTITY = struct {
	FinternalEventPtr    uintptr
	FinternalEventEndPtr uintptr
	Fnext                uintptr
	Fentity              uintptr
	FstartTagLevel       int32
	FbetweenDecl         TXML_Bool
}

type Topen_internal_entity = TOPEN_INTERNAL_ENTITY

func XXML_ParserCreate(tls *libc.TLS, encodingName uintptr) (r TXML_Parser) {
	return XXML_ParserCreate_MM(tls, encodingName, libc.UintptrFromInt32(0), libc.UintptrFromInt32(0))
}

func XXML_ParserCreateNS(tls *libc.TLS, encodingName uintptr, nsSep TXML_Char) (r TXML_Parser) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* tmp at bp+0 */ [2]TXML_Char
	*(*TXML_Char)(unsafe.Pointer(bp)) = nsSep
	return XXML_ParserCreate_MM(tls, encodingName, libc.UintptrFromInt32(0), bp)
}

var _implicitContext = [41]TXML_Char{
	0:  uint8(m_ASCII_x),
	1:  uint8(m_ASCII_m),
	2:  uint8(m_ASCII_l),
	3:  uint8(m_ASCII_EQUALS),
	4:  uint8(m_ASCII_h),
	5:  uint8(m_ASCII_t),
	6:  uint8(m_ASCII_t),
	7:  uint8(m_ASCII_p),
	8:  uint8(m_ASCII_COLON),
	9:  uint8(m_ASCII_SLASH),
	10: uint8(m_ASCII_SLASH),
	11: uint8(m_ASCII_w),
	12: uint8(m_ASCII_w),
	13: uint8(m_ASCII_w),
	14: uint8(m_ASCII_PERIOD),
	15: uint8(m_ASCII_w),
	16: uint8(m_ASCII_3),
	17: uint8(m_ASCII_PERIOD),
	18: uint8(m_ASCII_o),
	19: uint8(m_ASCII_r),
	20: uint8(m_ASCII_g),
	21: uint8(m_ASCII_SLASH),
	22: uint8(m_ASCII_X),
	23: uint8(m_ASCII_M),
	24: uint8(m_ASCII_L),
	25: uint8(m_ASCII_SLASH),
	26: uint8(m_ASCII_1),
	27: uint8(m_ASCII_9),
	28: uint8(m_ASCII_9),
	29: uint8(m_ASCII_8),
	30: uint8(m_ASCII_SLASH),
	31: uint8(m_ASCII_n),
	32: uint8(m_ASCII_a),
	33: uint8(m_ASCII_m),
	34: uint8(m_ASCII_e),
	35: uint8(m_ASCII_s),
	36: uint8(m_ASCII_p),
	37: uint8(m_ASCII_a),
	38: uint8(m_ASCII_c),
	39: uint8(m_ASCII_e),
}

/* To avoid warnings about unused functions: */

// C documentation
//
//	/* Obtain entropy on Linux 3.17+ */
func _writeRandomBytes_getrandom_nonblock(tls *libc.TLS, target uintptr, count Tsize_t) (r int32) {
	var bytesToWrite, bytesWrittenTotal Tsize_t
	var bytesWrittenMore, success int32
	var currentTarget uintptr
	var getrandomFlags uint32
	_, _, _, _, _, _ = bytesToWrite, bytesWrittenMore, bytesWrittenTotal, currentTarget, getrandomFlags, success
	success = 0 /* full count bytes written? */
	bytesWrittenTotal = uint64(0)
	getrandomFlags = uint32(m_GRND_NONBLOCK)
	for cond := true; cond; cond = !(success != 0) && *(*int32)(unsafe.Pointer(libc.X__errno_location(tls))) == int32(m_EINTR) {
		currentTarget = target + uintptr(bytesWrittenTotal)
		bytesToWrite = count - bytesWrittenTotal
		bytesWrittenMore = int32(libc.Xgetrandom(tls, currentTarget, bytesToWrite, getrandomFlags))
		if bytesWrittenMore > 0 {
			bytesWrittenTotal += libc.Uint64FromInt32(bytesWrittenMore)
			if bytesWrittenTotal >= count {
				success = int32(1)
			}
		}
	}
	return success
}

// C documentation
//
//	/* Extract entropy from /dev/urandom */
func _writeRandomBytes_dev_urandom(tls *libc.TLS, target uintptr, count Tsize_t) (r int32) {
	var bytesToWrite, bytesWrittenTotal Tsize_t
	var bytesWrittenMore Tssize_t
	var currentTarget uintptr
	var fd, success int32
	_, _, _, _, _, _ = bytesToWrite, bytesWrittenMore, bytesWrittenTotal, currentTarget, fd, success
	success = 0 /* full count bytes written? */
	bytesWrittenTotal = uint64(0)
	fd = libc.Xopen(tls, __ccgo_ts+17, m_O_RDONLY, 0)
	if fd < 0 {
		return 0
	}
	for cond := true; cond; cond = !(success != 0) && *(*int32)(unsafe.Pointer(libc.X__errno_location(tls))) == int32(m_EINTR) {
		currentTarget = target + uintptr(bytesWrittenTotal)
		bytesToWrite = count - bytesWrittenTotal
		bytesWrittenMore = libc.Xread(tls, fd, currentTarget, bytesToWrite)
		if bytesWrittenMore > 0 {
			bytesWrittenTotal += libc.Uint64FromInt64(bytesWrittenMore)
			if bytesWrittenTotal >= count {
				success = int32(1)
			}
		}
	}
	libc.Xclose(tls, fd)
	return success
}

func _gather_time_entropy(tls *libc.TLS) (r uint64) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var gettimeofday_res int32
	var _ /* tv at bp+0 */ Ttimeval
	_ = gettimeofday_res
	gettimeofday_res = libc.Xgettimeofday(tls, bp, libc.UintptrFromInt32(0))
	_ = gettimeofday_res
	/* Microseconds time is <20 bits entropy */
	return libc.Uint64FromInt64((*(*Ttimeval)(unsafe.Pointer(bp))).Ftv_usec)
}

func _ENTROPY_DEBUG(tls *libc.TLS, label uintptr, entropy uint64) (r uint64) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var EXPAT_ENTROPY_DEBUG uintptr
	_ = EXPAT_ENTROPY_DEBUG
	EXPAT_ENTROPY_DEBUG = libc.Xgetenv(tls, __ccgo_ts+30)
	if EXPAT_ENTROPY_DEBUG != 0 && !(libc.Xstrcmp(tls, EXPAT_ENTROPY_DEBUG, __ccgo_ts+50) != 0) {
		libc.Xfprintf(tls, libc.Xstderr, __ccgo_ts+52, libc.VaList(bp+8, label, libc.Int32FromInt64(8)*libc.Int32FromInt32(2), entropy, libc.Uint64FromInt64(8)))
	}
	return entropy
}

func _generate_hash_secret_salt(tls *libc.TLS, parser TXML_Parser) (r uint64) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* entropy at bp+0 */ uint64
	_ = parser
	/* "Failproof" high quality providers: */
	/* Try high quality providers first .. */
	if _writeRandomBytes_getrandom_nonblock(tls, bp, uint64(8)) != 0 {
		return _ENTROPY_DEBUG(tls, __ccgo_ts+89, *(*uint64)(unsafe.Pointer(bp)))
	}
	if _writeRandomBytes_dev_urandom(tls, bp, uint64(8)) != 0 {
		return _ENTROPY_DEBUG(tls, __ccgo_ts+17, *(*uint64)(unsafe.Pointer(bp)))
	}
	/* .. and self-made low quality for backup: */
	/* Process ID is 0 bits entropy if attacker has local access */
	*(*uint64)(unsafe.Pointer(bp)) = _gather_time_entropy(tls) ^ libc.Uint64FromInt32(libc.Xgetpid(tls))
	/* Factors are 2^31-1 and 2^61-1 (Mersenne primes M31 and M61) */
	if uint64(8) == uint64(4) {
		return _ENTROPY_DEBUG(tls, __ccgo_ts+99, *(*uint64)(unsafe.Pointer(bp))*uint64(2147483647))
	} else {
		return _ENTROPY_DEBUG(tls, __ccgo_ts+111, *(*uint64)(unsafe.Pointer(bp))*libc.Uint64FromUint64(2305843009213693951))
	}
	return r
}

func _get_hash_secret_salt(tls *libc.TLS, parser TXML_Parser) (r uint64) {
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != libc.UintptrFromInt32(0) {
		return _get_hash_secret_salt(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt
}

func _startParsing(tls *libc.TLS, parser TXML_Parser) (r TXML_Bool) {
	/* hash functions must be initialized before setContext() is called */
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt == uint64(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt = _generate_hash_secret_salt(tls, parser)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 {
		/* implicit context only set for root parser, since child
		   parsers (i.e. external entity parsers) will inherit it
		*/
		return _setContext(tls, parser, uintptr(unsafe.Pointer(&_implicitContext)))
	}
	return libc.Uint8FromInt32(1)
}

func XXML_ParserCreate_MM(tls *libc.TLS, encodingName uintptr, memsuite uintptr, nameSep uintptr) (r TXML_Parser) {
	return _parserCreate(tls, encodingName, memsuite, nameSep, libc.UintptrFromInt32(0))
}

func _parserCreate(tls *libc.TLS, encodingName uintptr, memsuite uintptr, nameSep uintptr, dtd uintptr) (r TXML_Parser) {
	var mtemp, mtemp1 uintptr
	var parser TXML_Parser
	_, _, _ = mtemp, mtemp1, parser
	if memsuite != 0 {
		parser = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(memsuite)).Fmalloc_fcn})))(tls, uint64(928))
		if parser != libc.UintptrFromInt32(0) {
			mtemp = parser + 24
			(*TXML_Memory_Handling_Suite)(unsafe.Pointer(mtemp)).Fmalloc_fcn = (*TXML_Memory_Handling_Suite)(unsafe.Pointer(memsuite)).Fmalloc_fcn
			(*TXML_Memory_Handling_Suite)(unsafe.Pointer(mtemp)).Frealloc_fcn = (*TXML_Memory_Handling_Suite)(unsafe.Pointer(memsuite)).Frealloc_fcn
			(*TXML_Memory_Handling_Suite)(unsafe.Pointer(mtemp)).Ffree_fcn = (*TXML_Memory_Handling_Suite)(unsafe.Pointer(memsuite)).Ffree_fcn
		}
	} else {
		parser = libc.Xmalloc(tls, uint64(928))
		if parser != libc.UintptrFromInt32(0) {
			mtemp1 = parser + 24
			(*TXML_Memory_Handling_Suite)(unsafe.Pointer(mtemp1)).Fmalloc_fcn = __ccgo_fp(libc.Xmalloc)
			(*TXML_Memory_Handling_Suite)(unsafe.Pointer(mtemp1)).Frealloc_fcn = __ccgo_fp(libc.Xrealloc)
			(*TXML_Memory_Handling_Suite)(unsafe.Pointer(mtemp1)).Ffree_fcn = __ccgo_fp(libc.Xfree)
		}
	}
	if !(parser != 0) {
		return parser
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize = int32(m_INIT_ATTS_SIZE)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize)*libc.Uint64FromInt64(32))
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts == libc.UintptrFromInt32(0) {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, parser)
		return libc.UintptrFromInt32(0)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(m_INIT_DATA_BUF_SIZE)*libc.Uint64FromInt64(1))
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf == libc.UintptrFromInt32(0) {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, parser)
		return libc.UintptrFromInt32(0)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBufEnd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf + uintptr(m_INIT_DATA_BUF_SIZE)
	if dtd != 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd = dtd
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd = _dtdCreate(tls, parser+24)
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd == libc.UintptrFromInt32(0) {
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf)
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, parser)
			return libc.UintptrFromInt32(0)
		}
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize = uint32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandlerData = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator = uint8(m_ASCII_EXCL)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns = libc.Uint8FromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets = libc.Uint8FromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsVersion = uint64(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower = uint8(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName = libc.UintptrFromInt32(0)
	_poolInit(tls, parser+784, parser+24)
	_poolInit(tls, parser+832, parser+24)
	_parserInit(tls, parser, encodingName)
	if encodingName != 0 && !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName != 0) {
		XXML_ParserFree(tls, parser)
		return libc.UintptrFromInt32(0)
	}
	if nameSep != 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns = libc.Uint8FromInt32(1)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding = XXmlGetUtf8InternalEncodingNS(tls)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator = *(*TXML_Char)(unsafe.Pointer(nameSep))
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding = XXmlGetUtf8InternalEncoding(tls)
	}
	return parser
}

func _parserInit(tls *libc.TLS, parser TXML_Parser, encodingName uintptr) {
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_prologInitProcessor)
	XXmlPrologStateInit(tls, parser+496)
	if encodingName != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName = _copyString(tls, encodingName, parser+24)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase = libc.UintptrFromInt32(0)
	XXmlInitEncoding(tls, parser+296, parser+288, uintptr(0))
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processingInstructionHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_commentHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endDoctypeDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unparsedEntityDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg = parser
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_xmlDeclHandler = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndByteIndex = 0
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypePubid = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationPublicId = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsCdata = libc.Uint8FromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsId = libc.Uint8FromInt32(0)
	libc.Xmemset(tls, parser+768, 0, uint64(16))
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NONE)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultExpandInternalEntities = libc.Uint8FromInt32(1)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel = 0
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_inheritedBindings = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nSpecifiedAtts = 0
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingMem = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingRelease = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingData = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser = libc.UintptrFromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_INITIALIZED)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_isParamEntity = libc.Uint8FromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD = libc.Uint8FromInt32(0)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing = int32(_XML_PARAM_ENTITY_PARSING_NEVER)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt = uint64(0)
}

// C documentation
//
//	/* moves list of bindings to m_freeBindingList */
func _moveToFreeBindingList(tls *libc.TLS, parser TXML_Parser, bindings uintptr) {
	var b uintptr
	_ = b
	for bindings != 0 {
		b = bindings
		bindings = (*TBINDING)(unsafe.Pointer(bindings)).FnextTagBinding
		(*TBINDING)(unsafe.Pointer(b)).FnextTagBinding = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList = b
	}
}

func XXML_ParserReset(tls *libc.TLS, parser TXML_Parser, encodingName uintptr) (r TXML_Bool) {
	var openEntity, openEntityList, tStk, tag uintptr
	_, _, _, _ = openEntity, openEntityList, tStk, tag
	if parser == libc.UintptrFromInt32(0) {
		return libc.Uint8FromInt32(0)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0 {
		return libc.Uint8FromInt32(0)
	}
	/* move m_tagStack to m_freeTagList */
	tStk = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack
	for tStk != 0 {
		tag = tStk
		tStk = (*TTAG)(unsafe.Pointer(tStk)).Fparent
		(*TTAG)(unsafe.Pointer(tag)).Fparent = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList
		_moveToFreeBindingList(tls, parser, (*TTAG)(unsafe.Pointer(tag)).Fbindings)
		(*TTAG)(unsafe.Pointer(tag)).Fbindings = libc.UintptrFromInt32(0)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList = tag
	}
	/* move m_openInternalEntities to m_freeInternalEntities */
	openEntityList = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
	for openEntityList != 0 {
		openEntity = openEntityList
		openEntityList = (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext
		(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities = openEntity
	}
	_moveToFreeBindingList(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_inheritedBindings)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingMem)
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingRelease != 0 {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingRelease})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingData)
	}
	_poolClear(tls, parser+784)
	_poolClear(tls, parser+832)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName = libc.UintptrFromInt32(0)
	_parserInit(tls, parser, encodingName)
	_dtdReset(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd, parser+24)
	return libc.Uint8FromInt32(1)
}

func XXML_SetEncoding(tls *libc.TLS, parser TXML_Parser, encodingName uintptr) (r _XML_Status) {
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_STATUS_ERROR)
	}
	/* Block after XML_Parse()/XML_ParseBuffer() has been called.
	   XXX There's no way for the caller to determine which of the
	   XXX possible error cases caused the XML_STATUS_ERROR return.
	*/
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_PARSING) || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
		return int32(_XML_STATUS_ERROR)
	}
	/* Get rid of any previous encoding name */
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName)
	if encodingName == libc.UintptrFromInt32(0) {
		/* No new encoding name */
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName = libc.UintptrFromInt32(0)
	} else {
		/* Copy the new encoding name into allocated memory */
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName = _copyString(tls, encodingName, parser+24)
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName != 0) {
			return int32(_XML_STATUS_ERROR)
		}
	}
	return int32(_XML_STATUS_OK)
}

func XXML_ExternalEntityParserCreate(tls *libc.TLS, oldParser TXML_Parser, context uintptr, encodingName uintptr) (r TXML_Parser) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var newDtd, oldDeclElementType, oldDtd, oldHandlerArg, oldUserData uintptr
	var oldAttlistDeclHandler TXML_AttlistDeclHandler
	var oldCharacterDataHandler TXML_CharacterDataHandler
	var oldCommentHandler TXML_CommentHandler
	var oldDefaultExpandInternalEntities, oldns_triplets TXML_Bool
	var oldDefaultHandler TXML_DefaultHandler
	var oldElementDeclHandler TXML_ElementDeclHandler
	var oldEndCdataSectionHandler TXML_EndCdataSectionHandler
	var oldEndElementHandler TXML_EndElementHandler
	var oldEndNamespaceDeclHandler TXML_EndNamespaceDeclHandler
	var oldEntityDeclHandler TXML_EntityDeclHandler
	var oldExternalEntityRefHandler TXML_ExternalEntityRefHandler
	var oldExternalEntityRefHandlerArg, parser TXML_Parser
	var oldInEntityValue int32
	var oldNotStandaloneHandler TXML_NotStandaloneHandler
	var oldNotationDeclHandler TXML_NotationDeclHandler
	var oldParamEntityParsing _XML_ParamEntityParsing
	var oldProcessingInstructionHandler TXML_ProcessingInstructionHandler
	var oldSkippedEntityHandler TXML_SkippedEntityHandler
	var oldStartCdataSectionHandler TXML_StartCdataSectionHandler
	var oldStartElementHandler TXML_StartElementHandler
	var oldStartNamespaceDeclHandler TXML_StartNamespaceDeclHandler
	var oldUnknownEncodingHandler TXML_UnknownEncodingHandler
	var oldUnparsedEntityDeclHandler TXML_UnparsedEntityDeclHandler
	var oldXmlDeclHandler TXML_XmlDeclHandler
	var oldhash_secret_salt uint64
	var _ /* tmp at bp+0 */ [2]TXML_Char
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = newDtd, oldAttlistDeclHandler, oldCharacterDataHandler, oldCommentHandler, oldDeclElementType, oldDefaultExpandInternalEntities, oldDefaultHandler, oldDtd, oldElementDeclHandler, oldEndCdataSectionHandler, oldEndElementHandler, oldEndNamespaceDeclHandler, oldEntityDeclHandler, oldExternalEntityRefHandler, oldExternalEntityRefHandlerArg, oldHandlerArg, oldInEntityValue, oldNotStandaloneHandler, oldNotationDeclHandler, oldParamEntityParsing, oldProcessingInstructionHandler, oldSkippedEntityHandler, oldStartCdataSectionHandler, oldStartElementHandler, oldStartNamespaceDeclHandler, oldUnknownEncodingHandler, oldUnparsedEntityDeclHandler, oldUserData, oldXmlDeclHandler, oldhash_secret_salt, oldns_triplets, parser
	parser = oldParser
	newDtd = libc.UintptrFromInt32(0)
	/* Validate the oldParser parameter before we pull everything out of it */
	if oldParser == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	/* Stash the original parser contents on the stack */
	oldDtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	oldStartElementHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler
	oldEndElementHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler
	oldCharacterDataHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler
	oldProcessingInstructionHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processingInstructionHandler
	oldCommentHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_commentHandler
	oldStartCdataSectionHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler
	oldEndCdataSectionHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler
	oldDefaultHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler
	oldUnparsedEntityDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unparsedEntityDeclHandler
	oldNotationDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler
	oldStartNamespaceDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler
	oldEndNamespaceDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler
	oldNotStandaloneHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler
	oldExternalEntityRefHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler
	oldSkippedEntityHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler
	oldUnknownEncodingHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandler
	oldElementDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler
	oldAttlistDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler
	oldEntityDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler
	oldXmlDeclHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_xmlDeclHandler
	oldDeclElementType = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType
	oldUserData = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData
	oldHandlerArg = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg
	oldDefaultExpandInternalEntities = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultExpandInternalEntities
	oldExternalEntityRefHandlerArg = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg
	oldParamEntityParsing = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing
	oldInEntityValue = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FinEntityValue
	oldns_triplets = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets
	/* Note that the new parser shares the same hash secret as the old
	   parser, so that dtdCopy and copyEntityTable can lookup values
	   from hash tables associated with either parser without us having
	   to worry which hash secrets each table has.
	*/
	oldhash_secret_salt = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt
	if !(context != 0) {
		newDtd = oldDtd
	}
	/* Note that the magical uses of the pre-processor to make field
	   access look more like C++ require that `parser' be overwritten
	   here.  This makes this function more painful to follow than it
	   would be otherwise.
	*/
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 {
		*(*TXML_Char)(unsafe.Pointer(bp)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator
		parser = _parserCreate(tls, encodingName, parser+24, bp, newDtd)
	} else {
		parser = _parserCreate(tls, encodingName, parser+24, libc.UintptrFromInt32(0), newDtd)
	}
	if !(parser != 0) {
		return libc.UintptrFromInt32(0)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler = oldStartElementHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler = oldEndElementHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler = oldCharacterDataHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processingInstructionHandler = oldProcessingInstructionHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_commentHandler = oldCommentHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler = oldStartCdataSectionHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler = oldEndCdataSectionHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler = oldDefaultHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unparsedEntityDeclHandler = oldUnparsedEntityDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler = oldNotationDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler = oldStartNamespaceDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler = oldEndNamespaceDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler = oldNotStandaloneHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler = oldExternalEntityRefHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler = oldSkippedEntityHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandler = oldUnknownEncodingHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler = oldElementDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler = oldAttlistDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler = oldEntityDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_xmlDeclHandler = oldXmlDeclHandler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType = oldDeclElementType
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData = oldUserData
	if oldUserData == oldHandlerArg {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg = parser
	}
	if oldExternalEntityRefHandlerArg != oldParser {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg = oldExternalEntityRefHandlerArg
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultExpandInternalEntities = oldDefaultExpandInternalEntities
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets = oldns_triplets
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt = oldhash_secret_salt
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser = oldParser
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing = oldParamEntityParsing
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FinEntityValue = oldInEntityValue
	if context != 0 {
		if !(_dtdCopy(tls, oldParser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd, oldDtd, parser+24) != 0) || !(_setContext(tls, parser, context) != 0) {
			XXML_ParserFree(tls, parser)
			return libc.UintptrFromInt32(0)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalEntityInitProcessor)
	} else {
		/* The DTD instance referenced by parser->m_dtd is shared between the
		   document's root parser and external PE parsers, therefore one does not
		   need to call setContext. In addition, one also *must* not call
		   setContext, because this would overwrite existing prefix->binding
		   pointers in parser->m_dtd with ones that get destroyed with the external
		   PE parser. This would leave those prefixes with dangling pointers.
		*/
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_isParamEntity = libc.Uint8FromInt32(1)
		XXmlPrologStateInitExternalEntity(tls, parser+496)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalParEntInitProcessor)
	}
	return parser
}

func _destroyBindings(tls *libc.TLS, bindings uintptr, parser TXML_Parser) {
	var b uintptr
	_ = b
	for {
		b = bindings
		if !(b != 0) {
			break
		}
		bindings = (*TBINDING)(unsafe.Pointer(b)).FnextTagBinding
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TBINDING)(unsafe.Pointer(b)).Furi)
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, b)
		goto _1
	_1:
	}
}

func XXML_ParserFree(tls *libc.TLS, parser TXML_Parser) {
	var entityList, openEntity, p, tagList uintptr
	_, _, _, _ = entityList, openEntity, p, tagList
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	/* free m_tagStack and m_freeTagList */
	tagList = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack
	for {
		if tagList == libc.UintptrFromInt32(0) {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList == libc.UintptrFromInt32(0) {
				break
			}
			tagList = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList = libc.UintptrFromInt32(0)
		}
		p = tagList
		tagList = (*TTAG)(unsafe.Pointer(tagList)).Fparent
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TTAG)(unsafe.Pointer(p)).Fbuf)
		_destroyBindings(tls, (*TTAG)(unsafe.Pointer(p)).Fbindings, parser)
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, p)
		goto _1
	_1:
	}
	/* free m_openInternalEntities and m_freeInternalEntities */
	entityList = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
	for {
		if entityList == libc.UintptrFromInt32(0) {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities == libc.UintptrFromInt32(0) {
				break
			}
			entityList = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities = libc.UintptrFromInt32(0)
		}
		openEntity = entityList
		entityList = (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(entityList)).Fnext
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, openEntity)
		goto _2
	_2:
	}
	_destroyBindings(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList, parser)
	_destroyBindings(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_inheritedBindings, parser)
	_poolDestroy(tls, parser+784)
	_poolDestroy(tls, parser+832)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName)
	/* external parameter entity parsers share the DTD structure
	   parser->m_dtd with the root parser, so we must not destroy it
	*/
	if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_isParamEntity != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd != 0 {
		_dtdDestroy(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0)), parser+24)
	}
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingMem)
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingRelease != 0 {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingRelease})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingData)
	}
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, parser)
}

func XXML_UseParserAsHandlerArg(tls *libc.TLS, parser TXML_Parser) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg = parser
	}
}

func XXML_UseForeignDTD(tls *libc.TLS, parser TXML_Parser, useDTD TXML_Bool) (r _XML_Error) {
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_ERROR_INVALID_ARGUMENT)
	}
	/* block after XML_Parse()/XML_ParseBuffer() has been called */
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_PARSING) || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
		return int32(_XML_ERROR_CANT_CHANGE_FEATURE_ONCE_PARSING)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD = useDTD
	return int32(_XML_ERROR_NONE)
}

func XXML_SetReturnNSTriplet(tls *libc.TLS, parser TXML_Parser, do_nst int32) {
	var v1 int32
	_ = v1
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	/* block after XML_Parse()/XML_ParseBuffer() has been called */
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_PARSING) || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
		return
	}
	if do_nst != 0 {
		v1 = libc.Int32FromUint8(libc.Uint8FromInt32(1))
	} else {
		v1 = libc.Int32FromUint8(libc.Uint8FromInt32(0))
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets = libc.Uint8FromInt32(v1)
}

func XXML_SetUserData(tls *libc.TLS, parser TXML_Parser, p uintptr) {
	var v1 uintptr
	_ = v1
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData {
		v1 = p
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData = v1
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg = v1
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_userData = p
	}
}

func XXML_SetBase(tls *libc.TLS, parser TXML_Parser, p uintptr) (r _XML_Status) {
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_STATUS_ERROR)
	}
	if p != 0 {
		p = _poolCopyString(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd+160, p)
		if !(p != 0) {
			return int32(_XML_STATUS_ERROR)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase = p
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase = libc.UintptrFromInt32(0)
	}
	return int32(_XML_STATUS_OK)
}

func XXML_GetBase(tls *libc.TLS, parser TXML_Parser) (r uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase
}

func XXML_GetSpecifiedAttributeCount(tls *libc.TLS, parser TXML_Parser) (r int32) {
	if parser == libc.UintptrFromInt32(0) {
		return -int32(1)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nSpecifiedAtts
}

func XXML_GetIdAttributeIndex(tls *libc.TLS, parser TXML_Parser) (r int32) {
	if parser == libc.UintptrFromInt32(0) {
		return -int32(1)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_idAttIndex
}

func XXML_SetElementHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartElementHandler, end TXML_EndElementHandler) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler = start
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler = end
}

func XXML_SetStartElementHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartElementHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler = start
	}
}

func XXML_SetEndElementHandler(tls *libc.TLS, parser TXML_Parser, end TXML_EndElementHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler = end
	}
}

func XXML_SetCharacterDataHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_CharacterDataHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler = handler
	}
}

func XXML_SetProcessingInstructionHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_ProcessingInstructionHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processingInstructionHandler = handler
	}
}

func XXML_SetCommentHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_CommentHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_commentHandler = handler
	}
}

func XXML_SetCdataSectionHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartCdataSectionHandler, end TXML_EndCdataSectionHandler) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler = start
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler = end
}

func XXML_SetStartCdataSectionHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartCdataSectionHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler = start
	}
}

func XXML_SetEndCdataSectionHandler(tls *libc.TLS, parser TXML_Parser, end TXML_EndCdataSectionHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler = end
	}
}

func XXML_SetDefaultHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_DefaultHandler) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler = handler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultExpandInternalEntities = libc.Uint8FromInt32(0)
}

func XXML_SetDefaultHandlerExpand(tls *libc.TLS, parser TXML_Parser, handler TXML_DefaultHandler) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler = handler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultExpandInternalEntities = libc.Uint8FromInt32(1)
}

func XXML_SetDoctypeDeclHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartDoctypeDeclHandler, end TXML_EndDoctypeDeclHandler) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler = start
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endDoctypeDeclHandler = end
}

func XXML_SetStartDoctypeDeclHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartDoctypeDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler = start
	}
}

func XXML_SetEndDoctypeDeclHandler(tls *libc.TLS, parser TXML_Parser, end TXML_EndDoctypeDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endDoctypeDeclHandler = end
	}
}

func XXML_SetUnparsedEntityDeclHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_UnparsedEntityDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unparsedEntityDeclHandler = handler
	}
}

func XXML_SetNotationDeclHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_NotationDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler = handler
	}
}

func XXML_SetNamespaceDeclHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartNamespaceDeclHandler, end TXML_EndNamespaceDeclHandler) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler = start
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler = end
}

func XXML_SetStartNamespaceDeclHandler(tls *libc.TLS, parser TXML_Parser, start TXML_StartNamespaceDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler = start
	}
}

func XXML_SetEndNamespaceDeclHandler(tls *libc.TLS, parser TXML_Parser, end TXML_EndNamespaceDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler = end
	}
}

func XXML_SetNotStandaloneHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_NotStandaloneHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler = handler
	}
}

func XXML_SetExternalEntityRefHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_ExternalEntityRefHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler = handler
	}
}

func XXML_SetExternalEntityRefHandlerArg(tls *libc.TLS, parser TXML_Parser, arg uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	if arg != 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg = arg
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg = parser
	}
}

func XXML_SetSkippedEntityHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_SkippedEntityHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler = handler
	}
}

func XXML_SetUnknownEncodingHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_UnknownEncodingHandler, data uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandler = handler
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandlerData = data
}

func XXML_SetElementDeclHandler(tls *libc.TLS, parser TXML_Parser, eldecl TXML_ElementDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler = eldecl
	}
}

func XXML_SetAttlistDeclHandler(tls *libc.TLS, parser TXML_Parser, attdecl TXML_AttlistDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler = attdecl
	}
}

func XXML_SetEntityDeclHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_EntityDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler = handler
	}
}

func XXML_SetXmlDeclHandler(tls *libc.TLS, parser TXML_Parser, handler TXML_XmlDeclHandler) {
	if parser != libc.UintptrFromInt32(0) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_xmlDeclHandler = handler
	}
}

func XXML_SetParamEntityParsing(tls *libc.TLS, parser TXML_Parser, peParsing _XML_ParamEntityParsing) (r int32) {
	if parser == libc.UintptrFromInt32(0) {
		return 0
	}
	/* block after XML_Parse()/XML_ParseBuffer() has been called */
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_PARSING) || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
		return 0
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing = peParsing
	return int32(1)
}

func XXML_SetHashSalt(tls *libc.TLS, parser TXML_Parser, hash_salt uint64) (r int32) {
	if parser == libc.UintptrFromInt32(0) {
		return 0
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0 {
		return XXML_SetHashSalt(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser, hash_salt)
	}
	/* block after XML_Parse()/XML_ParseBuffer() has been called */
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_PARSING) || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
		return 0
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_hash_secret_salt = hash_salt
	return int32(1)
}

func XXML_Parse(tls *libc.TLS, parser TXML_Parser, s uintptr, len1 int32, isFinal int32) (r _XML_Status) {
	var buff uintptr
	_ = buff
	if parser == libc.UintptrFromInt32(0) || len1 < 0 || s == libc.UintptrFromInt32(0) && len1 != 0 {
		if parser != libc.UintptrFromInt32(0) {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_INVALID_ARGUMENT)
		}
		return int32(_XML_STATUS_ERROR)
	}
	switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
	case int32(_XML_SUSPENDED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_SUSPENDED)
		return int32(_XML_STATUS_ERROR)
	case int32(_XML_FINISHED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_FINISHED)
		return int32(_XML_STATUS_ERROR)
	case int32(_XML_INITIALIZED):
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser == libc.UintptrFromInt32(0) && !(_startParsing(tls, parser) != 0) {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NO_MEMORY)
			return int32(_XML_STATUS_ERROR)
		}
		/* fall through */
		fallthrough
	default:
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_PARSING)
	}
	if len1 == 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer = libc.Uint8FromInt32(isFinal)
		if !(isFinal != 0) {
			return int32(_XML_STATUS_OK)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd
		/* If data are left over from last buffer, and we now know that these
		   data are the final chunk of input, then we have to check them again
		   to detect errors based on that fact.
		*/
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = (*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr) _XML_Error)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor})))(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr, parser+48)
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode == int32(_XML_ERROR_NONE) {
			switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
			case int32(_XML_SUSPENDED):
				/* It is hard to be certain, but it seems that this case
				 * cannot occur.  This code is cleaning up a previous parse
				 * with no new data (since len == 0).  Changing the parsing
				 * state requires getting to execute a handler function, and
				 * there doesn't seem to be an opportunity for that while in
				 * this circumstance.
				 *
				 * Given the uncertainty, we retain the code but exclude it
				 * from coverage tests.
				 *
				 * LCOV_EXCL_START
				 */
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FupdatePosition})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr, parser+768)
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr
				return int32(_XML_STATUS_SUSPENDED)
				/* LCOV_EXCL_STOP */
				fallthrough
			case int32(_XML_INITIALIZED):
				fallthrough
			case int32(_XML_PARSING):
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_FINISHED)
				/* fall through */
				fallthrough
			default:
				return int32(_XML_STATUS_OK)
			}
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_errorProcessor)
		return int32(_XML_STATUS_ERROR)
	} else {
		buff = XXML_GetBuffer(tls, parser, len1)
		if buff == libc.UintptrFromInt32(0) {
			return int32(_XML_STATUS_ERROR)
		} else {
			libc.Xmemcpy(tls, buff, s, libc.Uint64FromInt32(len1))
			return XXML_ParseBuffer(tls, parser, len1, isFinal)
		}
	}
	return r
}

func XXML_ParseBuffer(tls *libc.TLS, parser TXML_Parser, len1 int32, isFinal int32) (r _XML_Status) {
	var result _XML_Status
	var start uintptr
	_, _ = result, start
	result = int32(_XML_STATUS_OK)
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_STATUS_ERROR)
	}
	switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
	case int32(_XML_SUSPENDED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_SUSPENDED)
		return int32(_XML_STATUS_ERROR)
	case int32(_XML_FINISHED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_FINISHED)
		return int32(_XML_STATUS_ERROR)
	case int32(_XML_INITIALIZED):
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser == libc.UintptrFromInt32(0) && !(_startParsing(tls, parser) != 0) {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NO_MEMORY)
			return int32(_XML_STATUS_ERROR)
		}
		/* fall through */
		fallthrough
	default:
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_PARSING)
	}
	start = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = start
	*(*uintptr)(unsafe.Pointer(parser + 56)) += uintptr(len1)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd
	*(*TXML_Index)(unsafe.Pointer(parser + 72)) += int64(len1)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer = libc.Uint8FromInt32(isFinal)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = (*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr) _XML_Error)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor})))(tls, parser, start, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr, parser+48)
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode != int32(_XML_ERROR_NONE) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_errorProcessor)
		return int32(_XML_STATUS_ERROR)
	} else {
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			result = int32(_XML_STATUS_SUSPENDED)
		case int32(_XML_INITIALIZED):
			fallthrough
		case int32(_XML_PARSING):
			if isFinal != 0 {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_FINISHED)
				return result
			}
			fallthrough
		default: /* should not happen */
		}
	}
	(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FupdatePosition})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr, parser+768)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr
	return result
}

func XXML_GetBuffer(tls *libc.TLS, parser TXML_Parser, len1 int32) (r uintptr) {
	var bufferSize, keep, neededSize, offset int32
	var newBuf, v10, v11 uintptr
	var v1, v2, v3, v4, v5, v6, v7, v8, v9 int64
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = bufferSize, keep, neededSize, newBuf, offset, v1, v10, v11, v2, v3, v4, v5, v6, v7, v8, v9
	if parser == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	if len1 < 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NO_MEMORY)
		return libc.UintptrFromInt32(0)
	}
	switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
	case int32(_XML_SUSPENDED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_SUSPENDED)
		return libc.UintptrFromInt32(0)
	case int32(_XML_FINISHED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_FINISHED)
		return libc.UintptrFromInt32(0)
	default:
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd != 0 {
		v1 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd)
	} else {
		v1 = 0
	}
	if int64(len1) > v1 {
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 {
			v2 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr)
		} else {
			v2 = 0
		}
		/* Do not invoke signed arithmetic overflow: */
		neededSize = libc.Int32FromUint32(libc.Uint32FromInt32(len1) + libc.Uint32FromInt64(v2))
		if neededSize < 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NO_MEMORY)
			return libc.UintptrFromInt32(0)
		}
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer != 0 {
			v3 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer)
		} else {
			v3 = 0
		}
		keep = int32(v3)
		if keep > int32(m_XML_CONTEXT_BYTES) {
			keep = int32(m_XML_CONTEXT_BYTES)
		}
		neededSize += keep
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer != 0 {
			v4 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer)
		} else {
			v4 = 0
		}
		if int64(neededSize) <= v4 {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer != 0 {
				v5 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer)
			} else {
				v5 = 0
			}
			if int64(keep) < v5 {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer != 0 {
					v6 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer)
				} else {
					v6 = 0
				}
				offset = int32(v6) - keep
				/* The buffer pointers cannot be NULL here; we have at least some bytes
				 * in the buffer */
				libc.Xmemmove(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer+uintptr(offset), libc.Uint64FromInt64(int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd)-int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr)+int64(keep)))
				*(*uintptr)(unsafe.Pointer(parser + 56)) -= uintptr(offset)
				*(*uintptr)(unsafe.Pointer(parser + 48)) -= uintptr(offset)
			}
		} else {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 {
				v7 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr)
			} else {
				v7 = 0
			}
			bufferSize = int32(v7)
			if bufferSize == 0 {
				bufferSize = int32(m_INIT_BUFFER_SIZE)
			}
			for cond := true; cond; cond = bufferSize < neededSize && bufferSize > 0 {
				/* Do not invoke signed arithmetic overflow: */
				bufferSize = libc.Int32FromUint32(libc.Uint32FromUint32(2) * libc.Uint32FromInt32(bufferSize))
			}
			if bufferSize <= 0 {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NO_MEMORY)
				return libc.UintptrFromInt32(0)
			}
			newBuf = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(bufferSize))
			if newBuf == uintptr(0) {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NO_MEMORY)
				return libc.UintptrFromInt32(0)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferLim = newBuf + uintptr(bufferSize)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 {
					v8 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr)
				} else {
					v8 = 0
				}
				libc.Xmemcpy(tls, newBuf, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr+uintptr(-keep), libc.Uint64FromInt64(v8+int64(keep)))
				(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer)
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer = newBuf
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr != 0 {
					v9 = int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr)
				} else {
					v9 = 0
				}
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer + uintptr(v9) + uintptr(keep)
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer + uintptr(keep)
			} else {
				/* This must be a brand new buffer with no data in it yet */
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd = newBuf
				v10 = newBuf
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer = v10
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr = v10
			}
		}
		v11 = libc.UintptrFromInt32(0)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = v11
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = v11
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = libc.UintptrFromInt32(0)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd
}

func XXML_StopParser(tls *libc.TLS, parser TXML_Parser, resumable TXML_Bool) (r _XML_Status) {
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_STATUS_ERROR)
	}
	switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
	case int32(_XML_SUSPENDED):
		if resumable != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_SUSPENDED)
			return int32(_XML_STATUS_ERROR)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_FINISHED)
	case int32(_XML_FINISHED):
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_FINISHED)
		return int32(_XML_STATUS_ERROR)
	default:
		if resumable != 0 {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_isParamEntity != 0 {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_SUSPEND_PE)
				return int32(_XML_STATUS_ERROR)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_SUSPENDED)
		} else {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_FINISHED)
		}
	}
	return int32(_XML_STATUS_OK)
}

func XXML_ResumeParser(tls *libc.TLS, parser TXML_Parser) (r _XML_Status) {
	var result _XML_Status
	_ = result
	result = int32(_XML_STATUS_OK)
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_STATUS_ERROR)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing != int32(_XML_SUSPENDED) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = int32(_XML_ERROR_NOT_SUSPENDED)
		return int32(_XML_STATUS_ERROR)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_PARSING)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode = (*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr) _XML_Error)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor})))(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr, parser+48)
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode != int32(_XML_ERROR_NONE) {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_errorProcessor)
		return int32(_XML_STATUS_ERROR)
	} else {
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			result = int32(_XML_STATUS_SUSPENDED)
		case int32(_XML_INITIALIZED):
			fallthrough
		case int32(_XML_PARSING):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0 {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing = int32(_XML_FINISHED)
				return result
			}
			fallthrough
		default:
		}
	}
	(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FupdatePosition})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr, parser+768)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferPtr
	return result
}

func XXML_GetParsingStatus(tls *libc.TLS, parser TXML_Parser, status uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	*(*TXML_ParsingStatus)(unsafe.Pointer(status)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus
}

func XXML_GetErrorCode(tls *libc.TLS, parser TXML_Parser) (r _XML_Error) {
	if parser == libc.UintptrFromInt32(0) {
		return int32(_XML_ERROR_INVALID_ARGUMENT)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode
}

func XXML_GetCurrentByteIndex(tls *libc.TLS, parser TXML_Parser) (r TXML_Index) {
	if parser == libc.UintptrFromInt32(0) {
		return int64(-int32(1))
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr != 0 {
		return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndByteIndex - (int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parseEndPtr) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr))
	}
	return int64(-int32(1))
}

func XXML_GetCurrentByteCount(tls *libc.TLS, parser TXML_Parser) (r int32) {
	if parser == libc.UintptrFromInt32(0) {
		return 0
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr != 0 {
		return int32(int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr))
	}
	return 0
}

func XXML_GetInputContext(tls *libc.TLS, parser TXML_Parser, offset uintptr, size uintptr) (r uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer != 0 {
		if offset != libc.UintptrFromInt32(0) {
			*(*int32)(unsafe.Pointer(offset)) = int32(int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer))
		}
		if size != libc.UintptrFromInt32(0) {
			*(*int32)(unsafe.Pointer(size)) = int32(int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_bufferEnd) - int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer))
		}
		return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_buffer
	}
	return libc.UintptrFromInt32(0)
}

func XXML_GetCurrentLineNumber(tls *libc.TLS, parser TXML_Parser) (r TXML_Size) {
	if parser == libc.UintptrFromInt32(0) {
		return uint64(0)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr >= (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr {
		(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FupdatePosition})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr, parser+768)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_position.FlineNumber + uint64(1)
}

func XXML_GetCurrentColumnNumber(tls *libc.TLS, parser TXML_Parser) (r TXML_Size) {
	if parser == libc.UintptrFromInt32(0) {
		return uint64(0)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr >= (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr {
		(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FupdatePosition})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr, parser+768)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_positionPtr = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_position.FcolumnNumber
}

func XXML_FreeContentModel(tls *libc.TLS, parser TXML_Parser, model uintptr) {
	if parser != libc.UintptrFromInt32(0) {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, model)
	}
}

func XXML_MemMalloc(tls *libc.TLS, parser TXML_Parser, size Tsize_t) (r uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	return (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, size)
}

func XXML_MemRealloc(tls *libc.TLS, parser TXML_Parser, ptr uintptr, size Tsize_t) (r uintptr) {
	if parser == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	return (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, ptr, size)
}

func XXML_MemFree(tls *libc.TLS, parser TXML_Parser, ptr uintptr) {
	if parser != libc.UintptrFromInt32(0) {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, ptr)
	}
}

func XXML_DefaultCurrent(tls *libc.TLS, parser TXML_Parser) {
	if parser == libc.UintptrFromInt32(0) {
		return
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities != 0 {
			_reportDefault(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities)).FinternalEventPtr, (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities)).FinternalEventEndPtr)
		} else {
			_reportDefault(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr)
		}
	}
}

func XXML_ErrorString(tls *libc.TLS, code _XML_Error) (r uintptr) {
	switch code {
	case int32(_XML_ERROR_NONE):
		return libc.UintptrFromInt32(0)
	case int32(_XML_ERROR_NO_MEMORY):
		return __ccgo_ts + 123
	case int32(_XML_ERROR_SYNTAX):
		return __ccgo_ts + 137
	case int32(_XML_ERROR_NO_ELEMENTS):
		return __ccgo_ts + 150
	case int32(_XML_ERROR_INVALID_TOKEN):
		return __ccgo_ts + 167
	case int32(_XML_ERROR_UNCLOSED_TOKEN):
		return __ccgo_ts + 199
	case int32(_XML_ERROR_PARTIAL_CHAR):
		return __ccgo_ts + 214
	case int32(_XML_ERROR_TAG_MISMATCH):
		return __ccgo_ts + 232
	case int32(_XML_ERROR_DUPLICATE_ATTRIBUTE):
		return __ccgo_ts + 247
	case int32(_XML_ERROR_JUNK_AFTER_DOC_ELEMENT):
		return __ccgo_ts + 267
	case int32(_XML_ERROR_PARAM_ENTITY_REF):
		return __ccgo_ts + 295
	case int32(_XML_ERROR_UNDEFINED_ENTITY):
		return __ccgo_ts + 330
	case int32(_XML_ERROR_RECURSIVE_ENTITY_REF):
		return __ccgo_ts + 347
	case int32(_XML_ERROR_ASYNC_ENTITY):
		return __ccgo_ts + 374
	case int32(_XML_ERROR_BAD_CHAR_REF):
		return __ccgo_ts + 394
	case int32(_XML_ERROR_BINARY_ENTITY_REF):
		return __ccgo_ts + 432
	case int32(_XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF):
		return __ccgo_ts + 459
	case int32(_XML_ERROR_MISPLACED_XML_PI):
		return __ccgo_ts + 501
	case int32(_XML_ERROR_UNKNOWN_ENCODING):
		return __ccgo_ts + 548
	case int32(_XML_ERROR_INCORRECT_ENCODING):
		return __ccgo_ts + 565
	case int32(_XML_ERROR_UNCLOSED_CDATA_SECTION):
		return __ccgo_ts + 616
	case int32(_XML_ERROR_EXTERNAL_ENTITY_HANDLING):
		return __ccgo_ts + 639
	case int32(_XML_ERROR_NOT_STANDALONE):
		return __ccgo_ts + 685
	case int32(_XML_ERROR_UNEXPECTED_STATE):
		return __ccgo_ts + 712
	case int32(_XML_ERROR_ENTITY_DECLARED_IN_PE):
		return __ccgo_ts + 763
	case int32(_XML_ERROR_FEATURE_REQUIRES_XML_DTD):
		return __ccgo_ts + 799
	case int32(_XML_ERROR_CANT_CHANGE_FEATURE_ONCE_PARSING):
		return __ccgo_ts + 851
		/* Added in 1.95.7. */
		fallthrough
	case int32(_XML_ERROR_UNBOUND_PREFIX):
		return __ccgo_ts + 896
		/* Added in 1.95.8. */
		fallthrough
	case int32(_XML_ERROR_UNDECLARING_PREFIX):
		return __ccgo_ts + 911
	case int32(_XML_ERROR_INCOMPLETE_PE):
		return __ccgo_ts + 937
	case int32(_XML_ERROR_XML_DECL):
		return __ccgo_ts + 975
	case int32(_XML_ERROR_TEXT_DECL):
		return __ccgo_ts + 1007
	case int32(_XML_ERROR_PUBLICID):
		return __ccgo_ts + 1040
	case int32(_XML_ERROR_SUSPENDED):
		return __ccgo_ts + 1074
	case int32(_XML_ERROR_NOT_SUSPENDED):
		return __ccgo_ts + 1091
	case int32(_XML_ERROR_ABORTED):
		return __ccgo_ts + 1112
	case int32(_XML_ERROR_FINISHED):
		return __ccgo_ts + 1128
	case int32(_XML_ERROR_SUSPEND_PE):
		return __ccgo_ts + 1145
		/* Added in 2.0.0. */
		fallthrough
	case int32(_XML_ERROR_RESERVED_PREFIX_XML):
		return __ccgo_ts + 1189
	case int32(_XML_ERROR_RESERVED_PREFIX_XMLNS):
		return __ccgo_ts + 1269
	case int32(_XML_ERROR_RESERVED_NAMESPACE_URI):
		return __ccgo_ts + 1328
		/* Added in 2.2.5. */
		fallthrough
	case int32(_XML_ERROR_INVALID_ARGUMENT): /* Constant added in 2.2.1, already */
		return __ccgo_ts + 1392
	}
	return libc.UintptrFromInt32(0)
}

func XXML_ExpatVersion(tls *libc.TLS) (r uintptr) {
	/* V1 is used to string-ize the version number. However, it would
	   string-ize the actual version macro *names* unless we get them
	   substituted before being passed to V1. CPP is defined to expand
	   a macro, then rescan for more expansions. Thus, we use V2 to expand
	   the version macros, then CPP will expand the resulting V1() macro
	   with the correct numerals. */
	/* ### I'm assuming cpp is portable in this respect... */
	return __ccgo_ts + 1409
}

func XXML_ExpatVersionInfo(tls *libc.TLS) (r TXML_Expat_Version) {
	var version TXML_Expat_Version
	_ = version
	version.Fmajor = int32(m_XML_MAJOR_VERSION)
	version.Fminor = int32(m_XML_MINOR_VERSION)
	version.Fmicro = int32(m_XML_MICRO_VERSION)
	return version
}

func XXML_GetFeatureList(tls *libc.TLS) (r uintptr) {
	return uintptr(unsafe.Pointer(&_features))
}

var _features = [6]TXML_Feature{
	0: {
		Ffeature: int32(_XML_FEATURE_SIZEOF_XML_CHAR),
		Fname:    __ccgo_ts + 1422,
		Fvalue:   int64(1),
	},
	1: {
		Ffeature: int32(_XML_FEATURE_SIZEOF_XML_LCHAR),
		Fname:    __ccgo_ts + 1439,
		Fvalue:   int64(1),
	},
	2: {
		Ffeature: int32(_XML_FEATURE_DTD),
		Fname:    __ccgo_ts + 1457,
	},
	3: {
		Ffeature: int32(_XML_FEATURE_CONTEXT_BYTES),
		Fname:    __ccgo_ts + 1465,
		Fvalue:   int64(m_XML_CONTEXT_BYTES),
	},
	4: {
		Ffeature: int32(_XML_FEATURE_NS),
		Fname:    __ccgo_ts + 1483,
	},
	5: {},
}

// C documentation
//
//	/* Initially tag->rawName always points into the parse buffer;
//	   for those TAG instances opened while the current parse buffer was
//	   processed, and not yet closed, we need to store tag->rawName in a more
//	   permanent location, since the parse buffer is about to be discarded.
//	*/
func _storeRawNames(tls *libc.TLS, parser TXML_Parser) (r TXML_Bool) {
	var bufSize, nameLen int32
	var rawNameBuf, tag, temp uintptr
	_, _, _, _, _ = bufSize, nameLen, rawNameBuf, tag, temp
	tag = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack
	for tag != 0 {
		nameLen = libc.Int32FromUint64(uint64(1) * libc.Uint64FromInt32((*TTAG)(unsafe.Pointer(tag)).Fname.FstrLen+libc.Int32FromInt32(1)))
		rawNameBuf = (*TTAG)(unsafe.Pointer(tag)).Fbuf + uintptr(nameLen)
		/* Stop if already stored.  Since m_tagStack is a stack, we can stop
		   at the first entry that has already been copied; everything
		   below it in the stack is already been accounted for in a
		   previous call to this function.
		*/
		if (*TTAG)(unsafe.Pointer(tag)).FrawName == rawNameBuf {
			break
		}
		/* For re-use purposes we need to ensure that the
		   size of tag->buf is a multiple of sizeof(XML_Char).
		*/
		bufSize = libc.Int32FromUint64(libc.Uint64FromInt32(nameLen) + (libc.Uint64FromInt32((*TTAG)(unsafe.Pointer(tag)).FrawNameLength)+(libc.Uint64FromInt64(1)-libc.Uint64FromInt32(1))) & ^(libc.Uint64FromInt64(1)-libc.Uint64FromInt32(1)))
		if int64(bufSize) > int64((*TTAG)(unsafe.Pointer(tag)).FbufEnd)-int64((*TTAG)(unsafe.Pointer(tag)).Fbuf) {
			temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TTAG)(unsafe.Pointer(tag)).Fbuf, libc.Uint64FromInt32(bufSize))
			if temp == libc.UintptrFromInt32(0) {
				return libc.Uint8FromInt32(0)
			}
			/* if tag->name.str points to tag->buf (only when namespace
			   processing is off) then we have to update it
			*/
			if (*TTAG)(unsafe.Pointer(tag)).Fname.Fstr == (*TTAG)(unsafe.Pointer(tag)).Fbuf {
				(*TTAG)(unsafe.Pointer(tag)).Fname.Fstr = temp
			}
			/* if tag->name.localPart is set (when namespace processing is on)
			   then update it as well, since it will always point into tag->buf
			*/
			if (*TTAG)(unsafe.Pointer(tag)).Fname.FlocalPart != 0 {
				(*TTAG)(unsafe.Pointer(tag)).Fname.FlocalPart = temp + uintptr(int64((*TTAG)(unsafe.Pointer(tag)).Fname.FlocalPart)-int64((*TTAG)(unsafe.Pointer(tag)).Fbuf))
			}
			(*TTAG)(unsafe.Pointer(tag)).Fbuf = temp
			(*TTAG)(unsafe.Pointer(tag)).FbufEnd = temp + uintptr(bufSize)
			rawNameBuf = temp + uintptr(nameLen)
		}
		libc.Xmemcpy(tls, rawNameBuf, (*TTAG)(unsafe.Pointer(tag)).FrawName, libc.Uint64FromInt32((*TTAG)(unsafe.Pointer(tag)).FrawNameLength))
		(*TTAG)(unsafe.Pointer(tag)).FrawName = rawNameBuf
		tag = (*TTAG)(unsafe.Pointer(tag)).Fparent
	}
	return libc.Uint8FromInt32(1)
}

func _contentProcessor(tls *libc.TLS, parser TXML_Parser, start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	var result _XML_Error
	_ = result
	result = _doContent(tls, parser, 0, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, start, end, endPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)))
	if result == int32(_XML_ERROR_NONE) {
		if !(_storeRawNames(tls, parser) != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
	}
	return result
}

func _externalEntityInitProcessor(tls *libc.TLS, parser TXML_Parser, start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	var result _XML_Error
	_ = result
	result = _initializeEncoding(tls, parser)
	if result != int32(_XML_ERROR_NONE) {
		return result
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalEntityInitProcessor2)
	return _externalEntityInitProcessor2(tls, parser, start, end, endPtr)
}

func _externalEntityInitProcessor2(tls *libc.TLS, parser TXML_Parser, start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_ = tok
	*(*uintptr)(unsafe.Pointer(bp)) = start /* XmlContentTok doesn't always set the last arg */
	tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding + 1*8))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, start, end, bp)
	switch tok {
	case int32(m_XML_TOK_BOM):
		/* If we are at the end of the buffer, this would cause the next stage,
		   i.e. externalEntityInitProcessor3, to pass control directly to
		   doContent (by detecting XML_TOK_NONE) without processing any xml text
		   declaration - causing the error XML_ERROR_MISPLACED_XML_PI in doContent.
		*/
		if *(*uintptr)(unsafe.Pointer(bp)) == end && !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
			*(*uintptr)(unsafe.Pointer(endPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return int32(_XML_ERROR_NONE)
		}
		start = *(*uintptr)(unsafe.Pointer(bp))
	case -int32(1):
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
			*(*uintptr)(unsafe.Pointer(endPtr)) = start
			return int32(_XML_ERROR_NONE)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = start
		return int32(_XML_ERROR_UNCLOSED_TOKEN)
	case -int32(2):
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
			*(*uintptr)(unsafe.Pointer(endPtr)) = start
			return int32(_XML_ERROR_NONE)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = start
		return int32(_XML_ERROR_PARTIAL_CHAR)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalEntityInitProcessor3)
	return _externalEntityInitProcessor3(tls, parser, start, end, endPtr)
}

func _externalEntityInitProcessor3(tls *libc.TLS, parser TXML_Parser, start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var result _XML_Error
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_, _ = result, tok
	*(*uintptr)(unsafe.Pointer(bp)) = start /* XmlContentTok doesn't always set the last arg */
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = start
	tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding + 1*8))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, start, end, bp)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = *(*uintptr)(unsafe.Pointer(bp))
	switch tok {
	case int32(m_XML_TOK_XML_DECL):
		result = _processXmlDecl(tls, parser, int32(1), start, *(*uintptr)(unsafe.Pointer(bp)))
		if result != int32(_XML_ERROR_NONE) {
			return result
		}
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			*(*uintptr)(unsafe.Pointer(endPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return int32(_XML_ERROR_NONE)
		case int32(_XML_FINISHED):
			return int32(_XML_ERROR_ABORTED)
		default:
			start = *(*uintptr)(unsafe.Pointer(bp))
		}
	case -int32(1):
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
			*(*uintptr)(unsafe.Pointer(endPtr)) = start
			return int32(_XML_ERROR_NONE)
		}
		return int32(_XML_ERROR_UNCLOSED_TOKEN)
	case -int32(2):
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
			*(*uintptr)(unsafe.Pointer(endPtr)) = start
			return int32(_XML_ERROR_NONE)
		}
		return int32(_XML_ERROR_PARTIAL_CHAR)
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalEntityContentProcessor)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel = int32(1)
	return _externalEntityContentProcessor(tls, parser, start, end, endPtr)
}

func _externalEntityContentProcessor(tls *libc.TLS, parser TXML_Parser, start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	var result _XML_Error
	_ = result
	result = _doContent(tls, parser, int32(1), (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, start, end, endPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)))
	if result == int32(_XML_ERROR_NONE) {
		if !(_storeRawNames(tls, parser) != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
	}
	return result
}

func _doContent(tls *libc.TLS, parser TXML_Parser, startTagLevel int32, enc uintptr, _s uintptr, end uintptr, nextPtr uintptr, haveMore TXML_Bool) (r _XML_Error) {
	bp := tls.Alloc(112)
	defer tls.Free(112)
	*(*uintptr)(unsafe.Pointer(bp)) = _s
	var b, context, dtd, entity, eventEndPP, eventPP, localPart, name, prefix, rawName, rawName1, rawNameEnd, tag, tag1, temp, uri, v3, v4, v5, v6, v7, v9 uintptr
	var bufSize, convLen, len1, n, tok int32
	var charDataHandler TXML_CharacterDataHandler
	var convert_res, convert_res1 _XML_Convert_Result
	var noElmHandlers TXML_Bool
	var result, result1, result2, result3 _XML_Error
	var _ /* bindings at bp+40 */ uintptr
	var _ /* buf at bp+88 */ [4]TXML_Char
	var _ /* c at bp+16 */ TXML_Char
	var _ /* c at bp+92 */ TXML_Char
	var _ /* ch at bp+17 */ TXML_Char
	var _ /* dataPtr at bp+104 */ uintptr
	var _ /* dataPtr at bp+96 */ uintptr
	var _ /* fromPtr at bp+32 */ uintptr
	var _ /* name at bp+48 */ TTAG_NAME
	var _ /* next at bp+8 */ uintptr
	var _ /* toPtr at bp+24 */ uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = b, bufSize, charDataHandler, context, convLen, convert_res, convert_res1, dtd, entity, eventEndPP, eventPP, len1, localPart, n, name, noElmHandlers, prefix, rawName, rawName1, rawNameEnd, result, result1, result2, result3, tag, tag1, temp, tok, uri, v3, v4, v5, v6, v7, v9
	/* save one level of indirection */
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
		eventPP = parser + 544
		eventEndPP = parser + 552
	} else {
		eventPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
		eventEndPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities + 8
	}
	*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
	for {
		*(*uintptr)(unsafe.Pointer(bp + 8)) = *(*uintptr)(unsafe.Pointer(bp)) /* XmlContentTok doesn't always set the last arg */
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc + 1*8))})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp)), end, bp+8)
		*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp + 8))
		switch tok {
		case -int32(3):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			*(*uintptr)(unsafe.Pointer(eventEndPP)) = end
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
				*(*TXML_Char)(unsafe.Pointer(bp + 16)) = uint8(0xA)
				(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, bp+16, int32(1))
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), end)
				}
			}
			/* We are at the end of the final buffer, should we check for
			   XML_SUSPENDED, XML_FINISHED?
			*/
			if startTagLevel == 0 {
				return int32(_XML_ERROR_NO_ELEMENTS)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel != startTagLevel {
				return int32(_XML_ERROR_ASYNC_ENTITY)
			}
			*(*uintptr)(unsafe.Pointer(nextPtr)) = end
			return int32(_XML_ERROR_NONE)
		case -int32(4):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			if startTagLevel > 0 {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel != startTagLevel {
					return int32(_XML_ERROR_ASYNC_ENTITY)
				}
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_NO_ELEMENTS)
		case m_XML_TOK_INVALID:
			*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return int32(_XML_ERROR_INVALID_TOKEN)
		case -int32(1):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_UNCLOSED_TOKEN)
		case -int32(2):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_PARTIAL_CHAR)
		case int32(m_XML_TOK_ENTITY_REF):
			*(*TXML_Char)(unsafe.Pointer(bp + 17)) = libc.Uint8FromInt32((*(*func(*libc.TLS, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FpredefinedEntityName})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp + 8))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)))
			if *(*TXML_Char)(unsafe.Pointer(bp + 17)) != 0 {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, bp+17, int32(1))
				} else {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
						_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
					}
				}
				break
			}
			name = _poolStoreString(tls, dtd+160, enc, *(*uintptr)(unsafe.Pointer(bp))+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp + 8))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(name != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			entity = _lookup(tls, parser, dtd, name, uint64(0))
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
			/* First, determine if a check for an existing declaration is needed;
			   if yes, check that the entity exists, and that it is internal,
			   otherwise call the skipped entity or default handler.
			*/
			if !((*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs != 0) || (*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0 {
				if !(entity != 0) {
					return int32(_XML_ERROR_UNDEFINED_ENTITY)
				} else {
					if !((*TENTITY)(unsafe.Pointer(entity)).Fis_internal != 0) {
						return int32(_XML_ERROR_ENTITY_DECLARED_IN_PE)
					}
				}
			} else {
				if !(entity != 0) {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler != 0 {
						(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, name, 0)
					} else {
						if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
							_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
						}
					}
					break
				}
			}
			if (*TENTITY)(unsafe.Pointer(entity)).Fopen != 0 {
				return int32(_XML_ERROR_RECURSIVE_ENTITY_REF)
			}
			if (*TENTITY)(unsafe.Pointer(entity)).Fnotation != 0 {
				return int32(_XML_ERROR_BINARY_ENTITY_REF)
			}
			if (*TENTITY)(unsafe.Pointer(entity)).FtextPtr != 0 {
				if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultExpandInternalEntities != 0) {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler != 0 {
						(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TENTITY)(unsafe.Pointer(entity)).Fname, 0)
					} else {
						if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
							_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
						}
					}
					break
				}
				result = _processInternalEntity(tls, parser, entity, libc.Uint8FromInt32(0))
				if result != int32(_XML_ERROR_NONE) {
					return result
				}
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler != 0 {
					(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(1)
					context = _getContext(tls, parser)
					(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
					if !(context != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					if !((*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg, context, (*TENTITY)(unsafe.Pointer(entity)).Fbase, (*TENTITY)(unsafe.Pointer(entity)).FsystemId, (*TENTITY)(unsafe.Pointer(entity)).FpublicId) != 0) {
						return int32(_XML_ERROR_EXTERNAL_ENTITY_HANDLING)
					}
					(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
				} else {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
						_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
					}
				}
			}
		case int32(m_XML_TOK_START_TAG_NO_ATTS):
			/* fall through */
			fallthrough
		case int32(m_XML_TOK_START_TAG_WITH_ATTS):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList != 0 {
				tag = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList = (*TTAG)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList)).Fparent
			} else {
				tag = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt64(88))
				if !(tag != 0) {
					return int32(_XML_ERROR_NO_MEMORY)
				}
				(*TTAG)(unsafe.Pointer(tag)).Fbuf = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_INIT_TAG_BUF_SIZE)))
				if !((*TTAG)(unsafe.Pointer(tag)).Fbuf != 0) {
					(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, tag)
					return int32(_XML_ERROR_NO_MEMORY)
				}
				(*TTAG)(unsafe.Pointer(tag)).FbufEnd = (*TTAG)(unsafe.Pointer(tag)).Fbuf + uintptr(m_INIT_TAG_BUF_SIZE)
			}
			(*TTAG)(unsafe.Pointer(tag)).Fbindings = libc.UintptrFromInt32(0)
			(*TTAG)(unsafe.Pointer(tag)).Fparent = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack = tag
			(*TTAG)(unsafe.Pointer(tag)).Fname.FlocalPart = libc.UintptrFromInt32(0)
			(*TTAG)(unsafe.Pointer(tag)).Fname.Fprefix = libc.UintptrFromInt32(0)
			(*TTAG)(unsafe.Pointer(tag)).FrawName = *(*uintptr)(unsafe.Pointer(bp)) + uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
			(*TTAG)(unsafe.Pointer(tag)).FrawNameLength = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameLength})))(tls, enc, (*TTAG)(unsafe.Pointer(tag)).FrawName)
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel++
			rawNameEnd = (*TTAG)(unsafe.Pointer(tag)).FrawName + uintptr((*TTAG)(unsafe.Pointer(tag)).FrawNameLength)
			*(*uintptr)(unsafe.Pointer(bp + 32)) = (*TTAG)(unsafe.Pointer(tag)).FrawName
			*(*uintptr)(unsafe.Pointer(bp + 24)) = (*TTAG)(unsafe.Pointer(tag)).Fbuf
			for {
				convert_res = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp+32, rawNameEnd, bp+24, (*TTAG)(unsafe.Pointer(tag)).FbufEnd-uintptr(1))
				convLen = int32(int64(*(*uintptr)(unsafe.Pointer(bp + 24))) - int64((*TTAG)(unsafe.Pointer(tag)).Fbuf))
				if *(*uintptr)(unsafe.Pointer(bp + 32)) >= rawNameEnd || convert_res == int32(_XML_CONVERT_INPUT_INCOMPLETE) {
					(*TTAG)(unsafe.Pointer(tag)).Fname.FstrLen = convLen
					break
				}
				bufSize = int32(int64((*TTAG)(unsafe.Pointer(tag)).FbufEnd)-int64((*TTAG)(unsafe.Pointer(tag)).Fbuf)) << int32(1)
				temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TTAG)(unsafe.Pointer(tag)).Fbuf, libc.Uint64FromInt32(bufSize))
				if temp == libc.UintptrFromInt32(0) {
					return int32(_XML_ERROR_NO_MEMORY)
				}
				(*TTAG)(unsafe.Pointer(tag)).Fbuf = temp
				(*TTAG)(unsafe.Pointer(tag)).FbufEnd = temp + uintptr(bufSize)
				*(*uintptr)(unsafe.Pointer(bp + 24)) = temp + uintptr(convLen)
				goto _2
			_2:
			}
			(*TTAG)(unsafe.Pointer(tag)).Fname.Fstr = (*TTAG)(unsafe.Pointer(tag)).Fbuf
			*(*TXML_Char)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 24)))) = uint8('\000')
			result1 = _storeAtts(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), tag+24, tag+80)
			if result1 != 0 {
				return result1
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler != 0 {
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TTAG)(unsafe.Pointer(tag)).Fname.Fstr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
				}
			}
			_poolClear(tls, parser+784)
		case int32(m_XML_TOK_EMPTY_ELEMENT_NO_ATTS):
			/* fall through */
			fallthrough
		case int32(m_XML_TOK_EMPTY_ELEMENT_WITH_ATTS):
			rawName = *(*uintptr)(unsafe.Pointer(bp)) + uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
			*(*uintptr)(unsafe.Pointer(bp + 40)) = libc.UintptrFromInt32(0)
			noElmHandlers = libc.Uint8FromInt32(1)
			(*(*TTAG_NAME)(unsafe.Pointer(bp + 48))).Fstr = _poolStoreString(tls, parser+784, enc, rawName, rawName+uintptr((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameLength})))(tls, enc, rawName)))
			if !((*(*TTAG_NAME)(unsafe.Pointer(bp + 48))).Fstr != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			result2 = _storeAtts(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), bp+48, bp+40)
			if result2 != int32(_XML_ERROR_NONE) {
				_freeBindings(tls, parser, *(*uintptr)(unsafe.Pointer(bp + 40)))
				return result2
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler != 0 {
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*(*TTAG_NAME)(unsafe.Pointer(bp + 48))).Fstr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
				noElmHandlers = libc.Uint8FromInt32(0)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler != 0 {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startElementHandler != 0 {
					*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(eventEndPP))
				}
				(*(*func(*libc.TLS, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*(*TTAG_NAME)(unsafe.Pointer(bp + 48))).Fstr)
				noElmHandlers = libc.Uint8FromInt32(0)
			}
			if noElmHandlers != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
				_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
			}
			_poolClear(tls, parser+784)
			_freeBindings(tls, parser, *(*uintptr)(unsafe.Pointer(bp + 40)))
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel == 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing != int32(_XML_FINISHED) {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_epilogProcessor)
				} else {
					return _epilogProcessor(tls, parser, *(*uintptr)(unsafe.Pointer(bp + 8)), end, nextPtr)
				}
			}
		case int32(m_XML_TOK_END_TAG):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel == startTagLevel {
				return int32(_XML_ERROR_ASYNC_ENTITY)
			} else {
				tag1 = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack = (*TTAG)(unsafe.Pointer(tag1)).Fparent
				(*TTAG)(unsafe.Pointer(tag1)).Fparent = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeTagList = tag1
				rawName1 = *(*uintptr)(unsafe.Pointer(bp)) + uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar*int32(2))
				len1 = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameLength})))(tls, enc, rawName1)
				if len1 != (*TTAG)(unsafe.Pointer(tag1)).FrawNameLength || libc.Xmemcmp(tls, (*TTAG)(unsafe.Pointer(tag1)).FrawName, rawName1, libc.Uint64FromInt32(len1)) != 0 {
					*(*uintptr)(unsafe.Pointer(eventPP)) = rawName1
					return int32(_XML_ERROR_TAG_MISMATCH)
				}
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel--
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler != 0 {
					localPart = (*TTAG)(unsafe.Pointer(tag1)).Fname.FlocalPart
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 && localPart != 0 {
						/* localPart and prefix may have been overwritten in
						   tag->name.str, since this points to the binding->uri
						   buffer which gets re-used; so we have to add them again
						*/
						uri = (*TTAG)(unsafe.Pointer(tag1)).Fname.Fstr + uintptr((*TTAG)(unsafe.Pointer(tag1)).Fname.FuriLen)
						/* don't need to check for space - already done in storeAtts() */
						for *(*TXML_Char)(unsafe.Pointer(localPart)) != 0 {
							v3 = uri
							uri++
							v4 = localPart
							localPart++
							*(*TXML_Char)(unsafe.Pointer(v3)) = *(*TXML_Char)(unsafe.Pointer(v4))
						}
						prefix = (*TTAG)(unsafe.Pointer(tag1)).Fname.Fprefix
						if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets != 0 && prefix != 0 {
							v5 = uri
							uri++
							*(*TXML_Char)(unsafe.Pointer(v5)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator
							for *(*TXML_Char)(unsafe.Pointer(prefix)) != 0 {
								v6 = uri
								uri++
								v7 = prefix
								prefix++
								*(*TXML_Char)(unsafe.Pointer(v6)) = *(*TXML_Char)(unsafe.Pointer(v7))
							}
						}
						*(*TXML_Char)(unsafe.Pointer(uri)) = uint8('\000')
					}
					(*(*func(*libc.TLS, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endElementHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TTAG)(unsafe.Pointer(tag1)).Fname.Fstr)
				} else {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
						_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
					}
				}
				for (*TTAG)(unsafe.Pointer(tag1)).Fbindings != 0 {
					b = (*TTAG)(unsafe.Pointer(tag1)).Fbindings
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler != 0 {
						(*(*func(*libc.TLS, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Fprefix)).Fname)
					}
					(*TTAG)(unsafe.Pointer(tag1)).Fbindings = (*TBINDING)(unsafe.Pointer((*TTAG)(unsafe.Pointer(tag1)).Fbindings)).FnextTagBinding
					(*TBINDING)(unsafe.Pointer(b)).FnextTagBinding = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList = b
					(*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Fprefix)).Fbinding = (*TBINDING)(unsafe.Pointer(b)).FprevPrefixBinding
				}
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel == 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing != int32(_XML_FINISHED) {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
						(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_epilogProcessor)
					} else {
						return _epilogProcessor(tls, parser, *(*uintptr)(unsafe.Pointer(bp + 8)), end, nextPtr)
					}
				}
			}
		case int32(m_XML_TOK_CHAR_REF):
			n = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FcharRefNumber})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp)))
			if n < 0 {
				return int32(_XML_ERROR_BAD_CHAR_REF)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
				(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, bp+88, XXmlUtf8Encode(tls, n, bp+88))
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
				}
			}
		case int32(m_XML_TOK_XML_DECL):
			return int32(_XML_ERROR_MISPLACED_XML_PI)
		case int32(m_XML_TOK_DATA_NEWLINE):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
				*(*TXML_Char)(unsafe.Pointer(bp + 92)) = uint8(0xA)
				(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, bp+92, int32(1))
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
				}
			}
		case int32(m_XML_TOK_CDATA_SECT_OPEN):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler != 0 {
				(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startCdataSectionHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg)
			} else {
				if libc.Bool(0 != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf, 0)
				} else {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
						_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
					}
				}
			}
			result3 = _doCdataSection(tls, parser, enc, bp+8, end, nextPtr, haveMore)
			if result3 != int32(_XML_ERROR_NONE) {
				return result3
			} else {
				if !(*(*uintptr)(unsafe.Pointer(bp + 8)) != 0) {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_cdataSectionProcessor)
					return result3
				}
			}
		case -int32(5):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
				if !((*TENCODING)(unsafe.Pointer(enc)).FisUtf8 != 0) {
					*(*uintptr)(unsafe.Pointer(bp + 96)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf
					(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, end, bp+96, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBufEnd)
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf, int32(int64(*(*uintptr)(unsafe.Pointer(bp + 96)))-int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf)))
				} else {
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, *(*uintptr)(unsafe.Pointer(bp)), int32(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp)))))
				}
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), end)
				}
			}
			/* We are at the end of the final buffer, should we check for
			   XML_SUSPENDED, XML_FINISHED?
			*/
			if startTagLevel == 0 {
				*(*uintptr)(unsafe.Pointer(eventPP)) = end
				return int32(_XML_ERROR_NO_ELEMENTS)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel != startTagLevel {
				*(*uintptr)(unsafe.Pointer(eventPP)) = end
				return int32(_XML_ERROR_ASYNC_ENTITY)
			}
			*(*uintptr)(unsafe.Pointer(nextPtr)) = end
			return int32(_XML_ERROR_NONE)
		case int32(m_XML_TOK_DATA_CHARS):
			charDataHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler
			if charDataHandler != 0 {
				if !((*TENCODING)(unsafe.Pointer(enc)).FisUtf8 != 0) {
					for {
						*(*uintptr)(unsafe.Pointer(bp + 104)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf
						convert_res1 = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, *(*uintptr)(unsafe.Pointer(bp + 8)), bp+104, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBufEnd)
						*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp))
						(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{charDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf, int32(int64(*(*uintptr)(unsafe.Pointer(bp + 104)))-int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf)))
						if convert_res1 == int32(_XML_CONVERT_COMPLETED) || convert_res1 == int32(_XML_CONVERT_INPUT_INCOMPLETE) {
							break
						}
						*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
						goto _8
					_8:
					}
				} else {
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{charDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, *(*uintptr)(unsafe.Pointer(bp)), int32(int64(*(*uintptr)(unsafe.Pointer(bp + 8)))-int64(*(*uintptr)(unsafe.Pointer(bp)))))
				}
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
				}
			}
		case int32(m_XML_TOK_PI):
			if !(_reportProcessingInstruction(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8))) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		case int32(m_XML_TOK_COMMENT):
			if !(_reportComment(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8))) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		default:
			/* All of the tokens produced by XmlContentTok() have their own
			 * explicit cases, so this default is not strictly necessary.
			 * However it is a useful safety net, so we retain the code and
			 * simply exclude it from the coverage tests.
			 *
			 * LCOV_EXCL_START
			 */
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
				_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
			}
			break
			/* LCOV_EXCL_STOP */
		}
		v9 = *(*uintptr)(unsafe.Pointer(bp + 8))
		*(*uintptr)(unsafe.Pointer(bp)) = v9
		*(*uintptr)(unsafe.Pointer(eventPP)) = v9
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return int32(_XML_ERROR_NONE)
		case int32(_XML_FINISHED):
			return int32(_XML_ERROR_ABORTED)
		default:
		}
		goto _1
	_1:
	}
	/* not reached */
	return r
}

// C documentation
//
//	/* This function does not call free() on the allocated memory, merely
//	 * moving it to the parser's m_freeBindingList where it can be freed or
//	 * reused as appropriate.
//	 */
func _freeBindings(tls *libc.TLS, parser TXML_Parser, bindings uintptr) {
	var b uintptr
	_ = b
	for bindings != 0 {
		b = bindings
		/* m_startNamespaceDeclHandler will have been called for this
		 * binding in addBindings(), so call the end handler now.
		 */
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler != 0 {
			(*(*func(*libc.TLS, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endNamespaceDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Fprefix)).Fname)
		}
		bindings = (*TBINDING)(unsafe.Pointer(bindings)).FnextTagBinding
		(*TBINDING)(unsafe.Pointer(b)).FnextTagBinding = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList = b
		(*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Fprefix)).Fbinding = (*TBINDING)(unsafe.Pointer(b)).FprevPrefixBinding
	}
}

// C documentation
//
//	/* Precondition: all arguments must be non-NULL;
//	   Purpose:
//	   - normalize attributes
//	   - check attributes for well-formedness
//	   - generate namespace aware attribute names (URI, prefix)
//	   - build list of attributes for startElementHandler
//	   - default attributes
//	   - process namespace declarations (check and report them)
//	   - generate namespace aware element name (URI, prefix)
//	*/
func _storeAtts(tls *libc.TLS, parser TXML_Parser, enc uintptr, attStr uintptr, tagNamePtr uintptr, bindingsPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var appAtts, attId, b, binding, currAtt, da, dtd, elementType, id, localPart, name, p, s, s1, s2, temp, temp1, uri, v11, v18, v19, v20, v21, v24, v25, v28, v31, v32, v36 uintptr
	var attIndex, i, j, j1, n, nDefaultAtts, nPrefixes, nsAttsSize, oldAttsSize, prefixLen, v13, v17, v2, v23, v30, v33, v38, v40, v6, v7, v8, v9 int32
	var c TXML_Char
	var isCdata TXML_Bool
	var mask, uriHash, version, v14 uint64
	var oldNsAttsPower, step, v10 uint8
	var result, result1, result2 _XML_Error
	var _ /* sip_key at bp+56 */ Tsipkey
	var _ /* sip_state at bp+0 */ Tsiphash
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = appAtts, attId, attIndex, b, binding, c, currAtt, da, dtd, elementType, i, id, isCdata, j, j1, localPart, mask, n, nDefaultAtts, nPrefixes, name, nsAttsSize, oldAttsSize, oldNsAttsPower, p, prefixLen, result, result1, result2, s, s1, s2, step, temp, temp1, uri, uriHash, version, v10, v11, v13, v14, v17, v18, v19, v2, v20, v21, v23, v24, v25, v28, v30, v31, v32, v33, v36, v38, v40, v6, v7, v8, v9
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd /* the attribute list for the application */
	attIndex = 0
	nPrefixes = 0
	/* lookup the element type name */
	elementType = _lookup(tls, parser, dtd+40, (*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).Fstr, uint64(0))
	if !(elementType != 0) {
		name = _poolCopyString(tls, dtd+160, (*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).Fstr)
		if !(name != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		elementType = _lookup(tls, parser, dtd+40, name, uint64(40))
		if !(elementType != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 && !(_setElementTypePrefix(tls, parser, elementType) != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
	}
	nDefaultAtts = (*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FnDefaultAtts
	/* get the attributes from the tokenizer */
	n = (*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FgetAtts})))(tls, enc, attStr, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
	if n+nDefaultAtts > (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize {
		oldAttsSize = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize = n + nDefaultAtts + int32(m_INIT_ATTS_SIZE)
		temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts, libc.Uint64FromInt32((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize)*libc.Uint64FromInt64(32))
		if temp == libc.UintptrFromInt32(0) {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attsSize = oldAttsSize
			return int32(_XML_ERROR_NO_MEMORY)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts = temp
		if n > oldAttsSize {
			(*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FgetAtts})))(tls, enc, attStr, n, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts)
		}
	}
	appAtts = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts
	i = 0
	for {
		if !(i < n) {
			break
		}
		currAtt = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32
		/* add the name and value to the attribute list */
		attId = _getAttributeId(tls, parser, enc, (*TATTRIBUTE)(unsafe.Pointer(currAtt)).Fname, (*TATTRIBUTE)(unsafe.Pointer(currAtt)).Fname+uintptr((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameLength})))(tls, enc, (*TATTRIBUTE)(unsafe.Pointer(currAtt)).Fname)))
		if !(attId != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		/* Detect duplicate attributes by their QNames. This does not work when
		   namespace processing is turned on and different prefixes for the same
		   namespace are used. For this case we have a check further down.
		*/
		if *(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fname + uintptr(-libc.Int32FromInt32(1)))) != 0 {
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = (*(*TATTRIBUTE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32))).Fname
			}
			return int32(_XML_ERROR_DUPLICATE_ATTRIBUTE)
		}
		*(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fname + uintptr(-libc.Int32FromInt32(1)))) = uint8(1)
		v2 = attIndex
		attIndex++
		*(*uintptr)(unsafe.Pointer(appAtts + uintptr(v2)*8)) = (*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fname
		if !((*(*TATTRIBUTE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32))).Fnormalized != 0) {
			isCdata = libc.Uint8FromInt32(1)
			/* figure out whether declared as other than CDATA */
			if (*TATTRIBUTE_ID)(unsafe.Pointer(attId)).FmaybeTokenized != 0 {
				j = 0
				for {
					if !(j < nDefaultAtts) {
						break
					}
					if attId == (*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FdefaultAtts + uintptr(j)*24))).Fid {
						isCdata = (*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FdefaultAtts + uintptr(j)*24))).FisCdata
						break
					}
					goto _3
				_3:
					;
					j++
				}
			}
			/* normalize the attribute value */
			result = _storeAttributeValue(tls, parser, enc, isCdata, (*(*TATTRIBUTE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32))).FvaluePtr, (*(*TATTRIBUTE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32))).FvalueEnd, parser+784)
			if result != 0 {
				return result
			}
			*(*uintptr)(unsafe.Pointer(appAtts + uintptr(attIndex)*8)) = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
		} else {
			/* the value did not need normalizing */
			*(*uintptr)(unsafe.Pointer(appAtts + uintptr(attIndex)*8)) = _poolStoreString(tls, parser+784, enc, (*(*TATTRIBUTE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32))).FvaluePtr, (*(*TATTRIBUTE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_atts + uintptr(i)*32))).FvalueEnd)
			if *(*uintptr)(unsafe.Pointer(appAtts + uintptr(attIndex)*8)) == uintptr(0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
		}
		/* handle prefixed attribute names */
		if (*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fprefix != 0 {
			if (*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fxmlns != 0 {
				/* deal with namespace declarations here */
				result1 = _addBinding(tls, parser, (*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fprefix, attId, *(*uintptr)(unsafe.Pointer(appAtts + uintptr(attIndex)*8)), bindingsPtr)
				if result1 != 0 {
					return result1
				}
				attIndex--
			} else {
				/* deal with other prefixed names later */
				attIndex++
				nPrefixes++
				*(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fname + uintptr(-libc.Int32FromInt32(1)))) = uint8(2)
			}
		} else {
			attIndex++
		}
		goto _1
	_1:
		;
		i++
	}
	/* set-up for XML_GetSpecifiedAttributeCount and XML_GetIdAttributeIndex */
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nSpecifiedAtts = attIndex
	if (*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FidAtt != 0 && *(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FidAtt)).Fname + uintptr(-libc.Int32FromInt32(1)))) != 0 {
		i = 0
		for {
			if !(i < attIndex) {
				break
			}
			if *(*uintptr)(unsafe.Pointer(appAtts + uintptr(i)*8)) == (*TATTRIBUTE_ID)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FidAtt)).Fname {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_idAttIndex = i
				break
			}
			goto _4
		_4:
			;
			i += int32(2)
		}
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_idAttIndex = -int32(1)
	}
	/* do attribute defaulting */
	i = 0
	for {
		if !(i < nDefaultAtts) {
			break
		}
		da = (*TELEMENT_TYPE)(unsafe.Pointer(elementType)).FdefaultAtts + uintptr(i)*24
		if !(*(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fname + uintptr(-libc.Int32FromInt32(1)))) != 0) && (*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fvalue != 0 {
			if (*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fprefix != 0 {
				if (*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fxmlns != 0 {
					result2 = _addBinding(tls, parser, (*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fprefix, (*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid, (*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fvalue, bindingsPtr)
					if result2 != 0 {
						return result2
					}
				} else {
					*(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fname + uintptr(-libc.Int32FromInt32(1)))) = uint8(2)
					nPrefixes++
					v6 = attIndex
					attIndex++
					*(*uintptr)(unsafe.Pointer(appAtts + uintptr(v6)*8)) = (*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fname
					v7 = attIndex
					attIndex++
					*(*uintptr)(unsafe.Pointer(appAtts + uintptr(v7)*8)) = (*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fvalue
				}
			} else {
				*(*TXML_Char)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fname + uintptr(-libc.Int32FromInt32(1)))) = uint8(1)
				v8 = attIndex
				attIndex++
				*(*uintptr)(unsafe.Pointer(appAtts + uintptr(v8)*8)) = (*TATTRIBUTE_ID)(unsafe.Pointer((*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fid)).Fname
				v9 = attIndex
				attIndex++
				*(*uintptr)(unsafe.Pointer(appAtts + uintptr(v9)*8)) = (*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(da)).Fvalue
			}
		}
		goto _5
	_5:
		;
		i++
	}
	*(*uintptr)(unsafe.Pointer(appAtts + uintptr(attIndex)*8)) = uintptr(0)
	/* expand prefixed attribute names, check for duplicates,
	   and clear flags that say whether attributes were specified */
	i = 0
	if nPrefixes != 0 { /* hash table index */
		version = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsVersion
		nsAttsSize = libc.Int32FromInt32(1) << (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower
		oldNsAttsPower = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower
		/* size of hash table must be at least 2 * (# of prefixed attributes) */
		if nPrefixes<<int32(1)>>(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower != 0 {
			/* hash table size must also be a power of 2 and >= 8 */
			for {
				v11 = parser + 760
				v10 = *(*uint8)(unsafe.Pointer(v11))
				*(*uint8)(unsafe.Pointer(v11))++
				if !(nPrefixes>>v10 != 0) {
					break
				}
			}
			if libc.Int32FromUint8((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower) < int32(3) {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower = uint8(3)
			}
			nsAttsSize = libc.Int32FromInt32(1) << (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower
			temp1 = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts, libc.Uint64FromInt32(nsAttsSize)*libc.Uint64FromInt64(24))
			if !(temp1 != 0) {
				/* Restore actual size of memory in m_nsAtts */
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower = oldNsAttsPower
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts = temp1
			version = uint64(0) /* force re-initialization of m_nsAtts hash table */
		}
		/* using a version flag saves us from initializing m_nsAtts every time */
		if !(version != 0) { /* initialize version flags when version wraps around */
			version = uint64(m_INIT_ATTS_VERSION)
			j1 = nsAttsSize
			for {
				if !(j1 != 0) {
					break
				}
				j1--
				v13 = j1
				(*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(v13)*24))).Fversion = version
				goto _12
			_12:
			}
		}
		version--
		v14 = version
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsVersion = v14
		/* expand prefixed names and check for duplicates */
		for {
			if !(i < attIndex) {
				break
			}
			s = *(*uintptr)(unsafe.Pointer(appAtts + uintptr(i)*8))
			if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s + uintptr(-libc.Int32FromInt32(1))))) == int32(2) {
				_copy_salt_to_sipkey(tls, parser, bp+56)
				_sip24_init(tls, bp, bp+56)
				*(*TXML_Char)(unsafe.Pointer(s + uintptr(-libc.Int32FromInt32(1)))) = uint8(0) /* clear flag */
				id = _lookup(tls, parser, dtd+80, s, uint64(0))
				if !(id != 0) || !((*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix != 0) {
					/* This code is walking through the appAtts array, dealing
					 * with (in this case) a prefixed attribute name.  To be in
					 * the array, the attribute must have already been bound, so
					 * has to have passed through the hash table lookup once
					 * already.  That implies that an entry for it already
					 * exists, so the lookup above will return a pointer to
					 * already allocated memory.  There is no opportunaity for
					 * the allocator to fail, so the condition above cannot be
					 * fulfilled.
					 *
					 * Since it is difficult to be certain that the above
					 * analysis is complete, we retain the test and merely
					 * remove the code from coverage tests.
					 */
					return int32(_XML_ERROR_NO_MEMORY) /* LCOV_EXCL_LINE */
				}
				b = (*TPREFIX)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix)).Fbinding
				if !(b != 0) {
					return int32(_XML_ERROR_UNBOUND_PREFIX)
				}
				j1 = 0
				for {
					if !(j1 < (*TBINDING)(unsafe.Pointer(b)).FuriLen) {
						break
					}
					c = *(*TXML_Char)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Furi + uintptr(j1)))
					if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
						v17 = 0
					} else {
						v19 = parser + 784 + 24
						v18 = *(*uintptr)(unsafe.Pointer(v19))
						*(*uintptr)(unsafe.Pointer(v19))++
						*(*TXML_Char)(unsafe.Pointer(v18)) = c
						v17 = libc.Int32FromInt32(1)
					}
					if !(v17 != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					goto _16
				_16:
					;
					j1++
				}
				_sip24_update(tls, bp, (*TBINDING)(unsafe.Pointer(b)).Furi, libc.Uint64FromInt32((*TBINDING)(unsafe.Pointer(b)).FuriLen)*uint64(1))
				for {
					v20 = s
					s++
					if !(libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(v20))) != int32(m_ASCII_COLON)) {
						break
					}
				}
				_sip24_update(tls, bp, s, _keylen(tls, s)*uint64(1))
				for { /* copies null terminator */
					if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
						v23 = 0
					} else {
						v25 = parser + 784 + 24
						v24 = *(*uintptr)(unsafe.Pointer(v25))
						*(*uintptr)(unsafe.Pointer(v25))++
						*(*TXML_Char)(unsafe.Pointer(v24)) = *(*TXML_Char)(unsafe.Pointer(s))
						v23 = libc.Int32FromInt32(1)
					}
					if !(v23 != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					goto _22
				_22:
					;
					v21 = s
					s++
					if !(*(*TXML_Char)(unsafe.Pointer(v21)) != 0) {
						break
					}
				}
				uriHash = _sip24_final(tls, bp)
				/* Check hash table for duplicate of expanded name (uriName).
				   Derived from code in lookup(parser, HASH_TABLE *table, ...).
				*/
				step = uint8(0)
				mask = libc.Uint64FromInt32(nsAttsSize - int32(1))
				j1 = libc.Int32FromUint64(uriHash & mask) /* index into hash table */
				for (*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(j1)*24))).Fversion == version {
					/* for speed we compare stored hash values first */
					if uriHash == (*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(j1)*24))).Fhash {
						s1 = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
						s2 = (*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(j1)*24))).FuriName
						/* s1 is null terminated, but not s2 */
						for {
							if !(libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s1))) == libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s2))) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s1))) != 0) {
								break
							}
							goto _26
						_26:
							;
							s1++
							s2++
						}
						if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s1))) == 0 {
							return int32(_XML_ERROR_DUPLICATE_ATTRIBUTE)
						}
					}
					if !(step != 0) {
						step = uint8(uriHash & ^mask >> (libc.Int32FromUint8((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAttsPower)-libc.Int32FromInt32(1)) & (mask>>libc.Int32FromInt32(2)) | libc.Uint64FromInt32(1))
					}
					if j1 < libc.Int32FromUint8(step) {
						j1 += nsAttsSize - libc.Int32FromUint8(step)
					} else {
						j1 -= libc.Int32FromUint8(step)
					}
				}
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets != 0 { /* append namespace separator and prefix */
					*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tempPool.Fptr + uintptr(-libc.Int32FromInt32(1)))) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator
					s = (*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Fprefix)).Fname
					for {
						if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
							v30 = 0
						} else {
							v32 = parser + 784 + 24
							v31 = *(*uintptr)(unsafe.Pointer(v32))
							*(*uintptr)(unsafe.Pointer(v32))++
							*(*TXML_Char)(unsafe.Pointer(v31)) = *(*TXML_Char)(unsafe.Pointer(s))
							v30 = libc.Int32FromInt32(1)
						}
						if !(v30 != 0) {
							return int32(_XML_ERROR_NO_MEMORY)
						}
						goto _29
					_29:
						;
						v28 = s
						s++
						if !(*(*TXML_Char)(unsafe.Pointer(v28)) != 0) {
							break
						}
					}
				}
				/* store expanded name in attribute list */
				s = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
				(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
				*(*uintptr)(unsafe.Pointer(appAtts + uintptr(i)*8)) = s
				/* fill empty slot with new version, uriName and hash value */
				(*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(j1)*24))).Fversion = version
				(*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(j1)*24))).Fhash = uriHash
				(*(*TNS_ATT)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_nsAtts + uintptr(j1)*24))).FuriName = s
				nPrefixes--
				v33 = nPrefixes
				if !(v33 != 0) {
					i += int32(2)
					break
				}
			} else { /* not prefixed */
				*(*TXML_Char)(unsafe.Pointer(s + uintptr(-libc.Int32FromInt32(1)))) = uint8(0)
			} /* clear flag */
			goto _15
		_15:
			;
			i += int32(2)
		}
	}
	/* clear flags for the remaining attributes */
	for {
		if !(i < attIndex) {
			break
		}
		*(*TXML_Char)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(appAtts + uintptr(i)*8)) + uintptr(-libc.Int32FromInt32(1)))) = uint8(0)
		goto _34
	_34:
		;
		i += int32(2)
	}
	binding = *(*uintptr)(unsafe.Pointer(bindingsPtr))
	for {
		if !(binding != 0) {
			break
		}
		*(*TXML_Char)(unsafe.Pointer((*Tattribute_id)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(binding)).FattId)).Fname + uintptr(-libc.Int32FromInt32(1)))) = uint8(0)
		goto _35
	_35:
		;
		binding = (*TBINDING)(unsafe.Pointer(binding)).FnextTagBinding
	}
	if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0) {
		return int32(_XML_ERROR_NONE)
	}
	/* expand the element type name */
	if (*TELEMENT_TYPE)(unsafe.Pointer(elementType)).Fprefix != 0 {
		binding = (*TPREFIX)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(elementType)).Fprefix)).Fbinding
		if !(binding != 0) {
			return int32(_XML_ERROR_UNBOUND_PREFIX)
		}
		localPart = (*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).Fstr
		for {
			v36 = localPart
			localPart++
			if !(libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(v36))) != int32(m_ASCII_COLON)) {
				break
			}
		}
	} else {
		if (*TDTD)(unsafe.Pointer(dtd)).FdefaultPrefix.Fbinding != 0 {
			binding = (*TDTD)(unsafe.Pointer(dtd)).FdefaultPrefix.Fbinding
			localPart = (*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).Fstr
		} else {
			return int32(_XML_ERROR_NONE)
		}
	}
	prefixLen = 0
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns_triplets != 0 && (*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(binding)).Fprefix)).Fname != 0 {
		for {
			v38 = prefixLen
			prefixLen++
			if !(*(*TXML_Char)(unsafe.Pointer((*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(binding)).Fprefix)).Fname + uintptr(v38))) != 0) {
				break
			}
			goto _37
		_37:
		} /* prefixLen includes null terminator */
	}
	(*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).FlocalPart = localPart
	(*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).FuriLen = (*TBINDING)(unsafe.Pointer(binding)).FuriLen
	(*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).Fprefix = (*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(binding)).Fprefix)).Fname
	(*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).FprefixLen = prefixLen
	i = 0
	for {
		v40 = i
		i++
		if !(*(*TXML_Char)(unsafe.Pointer(localPart + uintptr(v40))) != 0) {
			break
		}
		goto _39
	_39:
	} /* i includes null terminator */
	n = i + (*TBINDING)(unsafe.Pointer(binding)).FuriLen + prefixLen
	if n > (*TBINDING)(unsafe.Pointer(binding)).FuriAlloc {
		uri = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(n+libc.Int32FromInt32(m_EXPAND_SPARE))*libc.Uint64FromInt64(1))
		if !(uri != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		(*TBINDING)(unsafe.Pointer(binding)).FuriAlloc = n + int32(m_EXPAND_SPARE)
		libc.Xmemcpy(tls, uri, (*TBINDING)(unsafe.Pointer(binding)).Furi, libc.Uint64FromInt32((*TBINDING)(unsafe.Pointer(binding)).FuriLen)*uint64(1))
		p = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagStack
		for {
			if !(p != 0) {
				break
			}
			if (*TTAG)(unsafe.Pointer(p)).Fname.Fstr == (*TBINDING)(unsafe.Pointer(binding)).Furi {
				(*TTAG)(unsafe.Pointer(p)).Fname.Fstr = uri
			}
			goto _41
		_41:
			;
			p = (*TTAG)(unsafe.Pointer(p)).Fparent
		}
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, (*TBINDING)(unsafe.Pointer(binding)).Furi)
		(*TBINDING)(unsafe.Pointer(binding)).Furi = uri
	}
	/* if m_namespaceSeparator != '\0' then uri includes it already */
	uri = (*TBINDING)(unsafe.Pointer(binding)).Furi + uintptr((*TBINDING)(unsafe.Pointer(binding)).FuriLen)
	libc.Xmemcpy(tls, uri, localPart, libc.Uint64FromInt32(i)*uint64(1))
	/* we always have a namespace separator between localPart and prefix */
	if prefixLen != 0 {
		uri += uintptr(i - int32(1))
		*(*TXML_Char)(unsafe.Pointer(uri)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator /* replace null terminator */
		libc.Xmemcpy(tls, uri+uintptr(1), (*Tprefix)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(binding)).Fprefix)).Fname, libc.Uint64FromInt32(prefixLen)*uint64(1))
	}
	(*TTAG_NAME)(unsafe.Pointer(tagNamePtr)).Fstr = (*TBINDING)(unsafe.Pointer(binding)).Furi
	return int32(_XML_ERROR_NONE)
}

// C documentation
//
//	/* addBinding() overwrites the value of prefix->binding without checking.
//	   Therefore one must keep track of the old value outside of addBinding().
//	*/
func _addBinding(tls *libc.TLS, parser TXML_Parser, prefix uintptr, attId uintptr, uri uintptr, bindingsPtr uintptr) (r _XML_Error) {
	var b, temp, v3 uintptr
	var isXML, isXMLNS, mustBeXML TXML_Bool
	var len1, v2 int32
	_, _, _, _, _, _, _, _ = b, isXML, isXMLNS, len1, mustBeXML, temp, v2, v3
	mustBeXML = libc.Uint8FromInt32(0)
	isXML = libc.Uint8FromInt32(1)
	isXMLNS = libc.Uint8FromInt32(1)
	/* empty URI is only valid for default namespace per XML NS 1.0 (not 1.1) */
	if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(uri))) == int32('\000') && (*TPREFIX)(unsafe.Pointer(prefix)).Fname != 0 {
		return int32(_XML_ERROR_UNDECLARING_PREFIX)
	}
	if (*TPREFIX)(unsafe.Pointer(prefix)).Fname != 0 && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname))) == int32(m_ASCII_x) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname + 1))) == int32(m_ASCII_m) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname + 2))) == int32(m_ASCII_l) {
		/* Not allowed to bind xmlns */
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname + 3))) == int32(m_ASCII_n) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname + 4))) == int32(m_ASCII_s) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname + 5))) == int32('\000') {
			return int32(_XML_ERROR_RESERVED_PREFIX_XMLNS)
		}
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fname + 3))) == int32('\000') {
			mustBeXML = libc.Uint8FromInt32(1)
		}
	}
	len1 = 0
	for {
		if !(*(*TXML_Char)(unsafe.Pointer(uri + uintptr(len1))) != 0) {
			break
		}
		if isXML != 0 && (len1 > _xmlLen || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(uri + uintptr(len1)))) != libc.Int32FromUint8(_xmlNamespace[len1])) {
			isXML = libc.Uint8FromInt32(0)
		}
		if !(mustBeXML != 0) && isXMLNS != 0 && (len1 > _xmlnsLen || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(uri + uintptr(len1)))) != libc.Int32FromUint8(_xmlnsNamespace[len1])) {
			isXMLNS = libc.Uint8FromInt32(0)
		}
		goto _1
	_1:
		;
		len1++
	}
	isXML = libc.BoolUint8(isXML != 0 && len1 == _xmlLen)
	isXMLNS = libc.BoolUint8(isXMLNS != 0 && len1 == _xmlnsLen)
	if libc.Int32FromUint8(mustBeXML) != libc.Int32FromUint8(isXML) {
		if mustBeXML != 0 {
			v2 = int32(_XML_ERROR_RESERVED_PREFIX_XML)
		} else {
			v2 = int32(_XML_ERROR_RESERVED_NAMESPACE_URI)
		}
		return v2
	}
	if isXMLNS != 0 {
		return int32(_XML_ERROR_RESERVED_NAMESPACE_URI)
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator != 0 {
		len1++
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList != 0 {
		b = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList
		if len1 > (*TBINDING)(unsafe.Pointer(b)).FuriAlloc {
			temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TBINDING)(unsafe.Pointer(b)).Furi, libc.Uint64FromInt64(1)*libc.Uint64FromInt32(len1+libc.Int32FromInt32(m_EXPAND_SPARE)))
			if temp == libc.UintptrFromInt32(0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TBINDING)(unsafe.Pointer(b)).Furi = temp
			(*TBINDING)(unsafe.Pointer(b)).FuriAlloc = len1 + int32(m_EXPAND_SPARE)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeBindingList = (*TBINDING)(unsafe.Pointer(b)).FnextTagBinding
	} else {
		b = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt64(48))
		if !(b != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		(*TBINDING)(unsafe.Pointer(b)).Furi = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt64(1)*libc.Uint64FromInt32(len1+libc.Int32FromInt32(m_EXPAND_SPARE)))
		if !((*TBINDING)(unsafe.Pointer(b)).Furi != 0) {
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Ffree_fcn})))(tls, b)
			return int32(_XML_ERROR_NO_MEMORY)
		}
		(*TBINDING)(unsafe.Pointer(b)).FuriAlloc = len1 + int32(m_EXPAND_SPARE)
	}
	(*TBINDING)(unsafe.Pointer(b)).FuriLen = len1
	libc.Xmemcpy(tls, (*TBINDING)(unsafe.Pointer(b)).Furi, uri, libc.Uint64FromInt32(len1)*uint64(1))
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator != 0 {
		*(*TXML_Char)(unsafe.Pointer((*TBINDING)(unsafe.Pointer(b)).Furi + uintptr(len1-int32(1)))) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator
	}
	(*TBINDING)(unsafe.Pointer(b)).Fprefix = prefix
	(*TBINDING)(unsafe.Pointer(b)).FattId = attId
	(*TBINDING)(unsafe.Pointer(b)).FprevPrefixBinding = (*TPREFIX)(unsafe.Pointer(prefix)).Fbinding
	/* NULL binding when default namespace undeclared */
	if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(uri))) == int32('\000') && prefix == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd+304 {
		(*TPREFIX)(unsafe.Pointer(prefix)).Fbinding = libc.UintptrFromInt32(0)
	} else {
		(*TPREFIX)(unsafe.Pointer(prefix)).Fbinding = b
	}
	(*TBINDING)(unsafe.Pointer(b)).FnextTagBinding = *(*uintptr)(unsafe.Pointer(bindingsPtr))
	*(*uintptr)(unsafe.Pointer(bindingsPtr)) = b
	/* if attId == NULL then we are not starting a namespace scope */
	if attId != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler != 0 {
		if (*TPREFIX)(unsafe.Pointer(prefix)).Fbinding != 0 {
			v3 = uri
		} else {
			v3 = uintptr(0)
		}
		(*(*func(*libc.TLS, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startNamespaceDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TPREFIX)(unsafe.Pointer(prefix)).Fname, v3)
	}
	return int32(_XML_ERROR_NONE)
}

var _xmlNamespace = [37]TXML_Char{
	0:  uint8(m_ASCII_h),
	1:  uint8(m_ASCII_t),
	2:  uint8(m_ASCII_t),
	3:  uint8(m_ASCII_p),
	4:  uint8(m_ASCII_COLON),
	5:  uint8(m_ASCII_SLASH),
	6:  uint8(m_ASCII_SLASH),
	7:  uint8(m_ASCII_w),
	8:  uint8(m_ASCII_w),
	9:  uint8(m_ASCII_w),
	10: uint8(m_ASCII_PERIOD),
	11: uint8(m_ASCII_w),
	12: uint8(m_ASCII_3),
	13: uint8(m_ASCII_PERIOD),
	14: uint8(m_ASCII_o),
	15: uint8(m_ASCII_r),
	16: uint8(m_ASCII_g),
	17: uint8(m_ASCII_SLASH),
	18: uint8(m_ASCII_X),
	19: uint8(m_ASCII_M),
	20: uint8(m_ASCII_L),
	21: uint8(m_ASCII_SLASH),
	22: uint8(m_ASCII_1),
	23: uint8(m_ASCII_9),
	24: uint8(m_ASCII_9),
	25: uint8(m_ASCII_8),
	26: uint8(m_ASCII_SLASH),
	27: uint8(m_ASCII_n),
	28: uint8(m_ASCII_a),
	29: uint8(m_ASCII_m),
	30: uint8(m_ASCII_e),
	31: uint8(m_ASCII_s),
	32: uint8(m_ASCII_p),
	33: uint8(m_ASCII_a),
	34: uint8(m_ASCII_c),
	35: uint8(m_ASCII_e),
}

var _xmlLen = libc.Int32FromUint64(libc.Uint64FromInt32(libc.Int32FromInt64(37))/libc.Uint64FromInt64(1) - libc.Uint64FromInt32(1))

var _xmlnsNamespace = [30]TXML_Char{
	0:  uint8(m_ASCII_h),
	1:  uint8(m_ASCII_t),
	2:  uint8(m_ASCII_t),
	3:  uint8(m_ASCII_p),
	4:  uint8(m_ASCII_COLON),
	5:  uint8(m_ASCII_SLASH),
	6:  uint8(m_ASCII_SLASH),
	7:  uint8(m_ASCII_w),
	8:  uint8(m_ASCII_w),
	9:  uint8(m_ASCII_w),
	10: uint8(m_ASCII_PERIOD),
	11: uint8(m_ASCII_w),
	12: uint8(m_ASCII_3),
	13: uint8(m_ASCII_PERIOD),
	14: uint8(m_ASCII_o),
	15: uint8(m_ASCII_r),
	16: uint8(m_ASCII_g),
	17: uint8(m_ASCII_SLASH),
	18: uint8(m_ASCII_2),
	19: uint8(m_ASCII_0),
	20: uint8(m_ASCII_0),
	21: uint8(m_ASCII_0),
	22: uint8(m_ASCII_SLASH),
	23: uint8(m_ASCII_x),
	24: uint8(m_ASCII_m),
	25: uint8(m_ASCII_l),
	26: uint8(m_ASCII_n),
	27: uint8(m_ASCII_s),
	28: uint8(m_ASCII_SLASH),
}

var _xmlnsLen = libc.Int32FromUint64(libc.Uint64FromInt32(libc.Int32FromInt64(30))/libc.Uint64FromInt64(1) - libc.Uint64FromInt32(1))

// C documentation
//
//	/* The idea here is to avoid using stack for each CDATA section when
//	   the whole file is parsed with one call.
//	*/
func _cdataSectionProcessor(tls *libc.TLS, parser TXML_Parser, _start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _start
	var result _XML_Error
	_ = result
	result = _doCdataSection(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, bp, end, endPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)))
	if result != int32(_XML_ERROR_NONE) {
		return result
	}
	if *(*uintptr)(unsafe.Pointer(bp)) != 0 {
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0 { /* we are parsing an external entity */
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalEntityContentProcessor)
			return _externalEntityContentProcessor(tls, parser, *(*uintptr)(unsafe.Pointer(bp)), end, endPtr)
		} else {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_contentProcessor)
			return _contentProcessor(tls, parser, *(*uintptr)(unsafe.Pointer(bp)), end, endPtr)
		}
	}
	return result
}

// C documentation
//
//	/* startPtr gets set to non-null if the section is closed, and to null if
//	   the section is not yet closed.
//	*/
func _doCdataSection(tls *libc.TLS, parser TXML_Parser, enc uintptr, startPtr uintptr, end uintptr, nextPtr uintptr, haveMore TXML_Bool) (r _XML_Error) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var charDataHandler TXML_CharacterDataHandler
	var convert_res _XML_Convert_Result
	var eventEndPP, eventPP, v3 uintptr
	var tok int32
	var _ /* c at bp+16 */ TXML_Char
	var _ /* dataPtr at bp+24 */ uintptr
	var _ /* next at bp+8 */ uintptr
	var _ /* s at bp+0 */ uintptr
	_, _, _, _, _, _ = charDataHandler, convert_res, eventEndPP, eventPP, tok, v3
	*(*uintptr)(unsafe.Pointer(bp)) = *(*uintptr)(unsafe.Pointer(startPtr))
	if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
		eventPP = parser + 544
		*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
		eventEndPP = parser + 552
	} else {
		eventPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
		eventEndPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities + 8
	}
	*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
	*(*uintptr)(unsafe.Pointer(startPtr)) = libc.UintptrFromInt32(0)
	for {
		*(*uintptr)(unsafe.Pointer(bp + 8)) = *(*uintptr)(unsafe.Pointer(bp)) /* in case of XML_TOK_NONE or XML_TOK_PARTIAL */
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc + 2*8))})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp)), end, bp+8)
		*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp + 8))
		switch tok {
		case int32(m_XML_TOK_CDATA_SECT_CLOSE):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler != 0 {
				(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endCdataSectionHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg)
			} else {
				if libc.Bool(0 != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf, 0)
				} else {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
						_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
					}
				}
			}
			*(*uintptr)(unsafe.Pointer(startPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_FINISHED) {
				return int32(_XML_ERROR_ABORTED)
			} else {
				return int32(_XML_ERROR_NONE)
			}
			fallthrough
		case int32(m_XML_TOK_DATA_NEWLINE):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler != 0 {
				*(*TXML_Char)(unsafe.Pointer(bp + 16)) = uint8(0xA)
				(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, bp+16, int32(1))
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
				}
			}
		case int32(m_XML_TOK_DATA_CHARS):
			charDataHandler = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_characterDataHandler
			if charDataHandler != 0 {
				if !((*TENCODING)(unsafe.Pointer(enc)).FisUtf8 != 0) {
					for {
						*(*uintptr)(unsafe.Pointer(bp + 24)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf
						convert_res = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, *(*uintptr)(unsafe.Pointer(bp + 8)), bp+24, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBufEnd)
						*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp + 8))
						(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{charDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf, int32(int64(*(*uintptr)(unsafe.Pointer(bp + 24)))-int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf)))
						if convert_res == int32(_XML_CONVERT_COMPLETED) || convert_res == int32(_XML_CONVERT_INPUT_INCOMPLETE) {
							break
						}
						*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
						goto _2
					_2:
					}
				} else {
					(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{charDataHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, *(*uintptr)(unsafe.Pointer(bp)), int32(int64(*(*uintptr)(unsafe.Pointer(bp + 8)))-int64(*(*uintptr)(unsafe.Pointer(bp)))))
				}
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
					_reportDefault(tls, parser, enc, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp + 8)))
				}
			}
		case m_XML_TOK_INVALID:
			*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return int32(_XML_ERROR_INVALID_TOKEN)
		case -int32(2):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_PARTIAL_CHAR)
		case -int32(1):
			fallthrough
		case -int32(4):
			if haveMore != 0 {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_UNCLOSED_CDATA_SECTION)
		default:
			/* Every token returned by XmlCdataSectionTok() has its own
			 * explicit case, so this default case will never be executed.
			 * We retain it as a safety net and exclude it from the coverage
			 * statistics.
			 *
			 * LCOV_EXCL_START
			 */
			*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return int32(_XML_ERROR_UNEXPECTED_STATE)
			/* LCOV_EXCL_STOP */
		}
		v3 = *(*uintptr)(unsafe.Pointer(bp + 8))
		*(*uintptr)(unsafe.Pointer(bp)) = v3
		*(*uintptr)(unsafe.Pointer(eventPP)) = v3
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return int32(_XML_ERROR_NONE)
		case int32(_XML_FINISHED):
			return int32(_XML_ERROR_ABORTED)
		default:
		}
		goto _1
	_1:
	}
	/* not reached */
	return r
}

// C documentation
//
//	/* The idea here is to avoid using stack for each IGNORE section when
//	   the whole file is parsed with one call.
//	*/
func _ignoreSectionProcessor(tls *libc.TLS, parser TXML_Parser, _start uintptr, end uintptr, endPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _start
	var result _XML_Error
	_ = result
	result = _doIgnoreSection(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, bp, end, endPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)))
	if result != int32(_XML_ERROR_NONE) {
		return result
	}
	if *(*uintptr)(unsafe.Pointer(bp)) != 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_prologProcessor)
		return _prologProcessor(tls, parser, *(*uintptr)(unsafe.Pointer(bp)), end, endPtr)
	}
	return result
}

// C documentation
//
//	/* startPtr gets set to non-null is the section is closed, and to null
//	   if the section is not yet closed.
//	*/
func _doIgnoreSection(tls *libc.TLS, parser TXML_Parser, enc uintptr, startPtr uintptr, end uintptr, nextPtr uintptr, haveMore TXML_Bool) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var eventEndPP, eventPP, s uintptr
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_, _, _, _ = eventEndPP, eventPP, s, tok
	*(*uintptr)(unsafe.Pointer(bp)) = *(*uintptr)(unsafe.Pointer(startPtr))
	s = *(*uintptr)(unsafe.Pointer(startPtr))
	if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
		eventPP = parser + 544
		*(*uintptr)(unsafe.Pointer(eventPP)) = s
		eventEndPP = parser + 552
	} else {
		/* It's not entirely clear, but it seems the following two lines
		 * of code cannot be executed.  The only occasions on which 'enc'
		 * is not 'encoding' are when this function is called
		 * from the internal entity processing, and IGNORE sections are an
		 * error in internal entities.
		 *
		 * Since it really isn't clear that this is true, we keep the code
		 * and just remove it from our coverage tests.
		 *
		 * LCOV_EXCL_START
		 */
		eventPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
		eventEndPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities + 8
		/* LCOV_EXCL_STOP */
	}
	*(*uintptr)(unsafe.Pointer(eventPP)) = s
	*(*uintptr)(unsafe.Pointer(startPtr)) = libc.UintptrFromInt32(0)
	tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc + 3*8))})))(tls, enc, s, end, bp)
	*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp))
	switch tok {
	case int32(m_XML_TOK_IGNORE_SECT):
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
			_reportDefault(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
		}
		*(*uintptr)(unsafe.Pointer(startPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_FINISHED) {
			return int32(_XML_ERROR_ABORTED)
		} else {
			return int32(_XML_ERROR_NONE)
		}
		fallthrough
	case m_XML_TOK_INVALID:
		*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
		return int32(_XML_ERROR_INVALID_TOKEN)
	case -int32(2):
		if haveMore != 0 {
			*(*uintptr)(unsafe.Pointer(nextPtr)) = s
			return int32(_XML_ERROR_NONE)
		}
		return int32(_XML_ERROR_PARTIAL_CHAR)
	case -int32(1):
		fallthrough
	case -int32(4):
		if haveMore != 0 {
			*(*uintptr)(unsafe.Pointer(nextPtr)) = s
			return int32(_XML_ERROR_NONE)
		}
		return int32(_XML_ERROR_SYNTAX) /* XML_ERROR_UNCLOSED_IGNORE_SECTION */
	default:
		/* All of the tokens that XmlIgnoreSectionTok() returns have
		 * explicit cases to handle them, so this default case is never
		 * executed.  We keep it as a safety net anyway, and remove it
		 * from our test coverage statistics.
		 *
		 * LCOV_EXCL_START
		 */
		*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
		return int32(_XML_ERROR_UNEXPECTED_STATE)
		/* LCOV_EXCL_STOP */
	}
	/* not reached */
	return r
}

func _initializeEncoding(tls *libc.TLS, parser TXML_Parser) (r _XML_Error) {
	var s uintptr
	var v1 func(*libc.TLS, uintptr, uintptr, uintptr) int32
	_, _ = s, v1
	s = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 {
		v1 = XXmlInitEncodingNS
	} else {
		v1 = XXmlInitEncoding
	}
	if v1(tls, parser+296, parser+288, s) != 0 {
		return int32(_XML_ERROR_NONE)
	}
	return _handleUnknownEncoding(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName)
}

func _processXmlDecl(tls *libc.TLS, parser TXML_Parser, isGeneralTextEntity int32, s uintptr, next uintptr) (r _XML_Error) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var result _XML_Error
	var storedEncName, storedversion uintptr
	var v1 func(*libc.TLS, int32, uintptr, uintptr, uintptr, uintptr, uintptr, uintptr, uintptr, uintptr, uintptr) int32
	var _ /* encodingName at bp+0 */ uintptr
	var _ /* newEncoding at bp+8 */ uintptr
	var _ /* standalone at bp+32 */ int32
	var _ /* version at bp+16 */ uintptr
	var _ /* versionend at bp+24 */ uintptr
	_, _, _, _ = result, storedEncName, storedversion, v1
	*(*uintptr)(unsafe.Pointer(bp)) = libc.UintptrFromInt32(0)
	storedEncName = libc.UintptrFromInt32(0)
	*(*uintptr)(unsafe.Pointer(bp + 8)) = libc.UintptrFromInt32(0)
	*(*uintptr)(unsafe.Pointer(bp + 16)) = libc.UintptrFromInt32(0)
	storedversion = libc.UintptrFromInt32(0)
	*(*int32)(unsafe.Pointer(bp + 32)) = -int32(1)
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 {
		v1 = XXmlParseXmlDeclNS
	} else {
		v1 = XXmlParseXmlDecl
	}
	if !(v1(tls, isGeneralTextEntity, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, next, parser+544, bp+16, bp+24, bp, bp+8, bp+32) != 0) {
		if isGeneralTextEntity != 0 {
			return int32(_XML_ERROR_TEXT_DECL)
		} else {
			return int32(_XML_ERROR_XML_DECL)
		}
	}
	if !(isGeneralTextEntity != 0) && *(*int32)(unsafe.Pointer(bp + 32)) == int32(1) {
		(*TDTD)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd)).Fstandalone = libc.Uint8FromInt32(1)
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing == int32(_XML_PARAM_ENTITY_PARSING_UNLESS_STANDALONE) {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing = int32(_XML_PARAM_ENTITY_PARSING_NEVER)
		}
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_xmlDeclHandler != 0 {
		if *(*uintptr)(unsafe.Pointer(bp)) != libc.UintptrFromInt32(0) {
			storedEncName = _poolStoreString(tls, parser+832, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp))+uintptr((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FnameLength})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, *(*uintptr)(unsafe.Pointer(bp)))))
			if !(storedEncName != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 832)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 832)).Fptr
		}
		if *(*uintptr)(unsafe.Pointer(bp + 16)) != 0 {
			storedversion = _poolStoreString(tls, parser+832, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, *(*uintptr)(unsafe.Pointer(bp + 16)), *(*uintptr)(unsafe.Pointer(bp + 24))-uintptr((*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FminBytesPerChar))
			if !(storedversion != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		}
		(*(*func(*libc.TLS, uintptr, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_xmlDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, storedversion, storedEncName, *(*int32)(unsafe.Pointer(bp + 32)))
	} else {
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
			_reportDefault(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, next)
		}
	}
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_protocolEncodingName == libc.UintptrFromInt32(0) {
		if *(*uintptr)(unsafe.Pointer(bp + 8)) != 0 {
			/* Check that the specified encoding does not conflict with what
			 * the parser has already deduced.  Do we have the same number
			 * of bytes in the smallest representation of a character?  If
			 * this is UTF-16, is it the same endianness?
			 */
			if (*TENCODING)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).FminBytesPerChar != (*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FminBytesPerChar || (*TENCODING)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 8)))).FminBytesPerChar == int32(2) && *(*uintptr)(unsafe.Pointer(bp + 8)) != (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_INCORRECT_ENCODING)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding = *(*uintptr)(unsafe.Pointer(bp + 8))
		} else {
			if *(*uintptr)(unsafe.Pointer(bp)) != 0 {
				if !(storedEncName != 0) {
					storedEncName = _poolStoreString(tls, parser+832, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, *(*uintptr)(unsafe.Pointer(bp)), *(*uintptr)(unsafe.Pointer(bp))+uintptr((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding)).FnameLength})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, *(*uintptr)(unsafe.Pointer(bp)))))
					if !(storedEncName != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
				}
				result = _handleUnknownEncoding(tls, parser, storedEncName)
				_poolClear(tls, parser+832)
				if result == int32(_XML_ERROR_UNKNOWN_ENCODING) {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = *(*uintptr)(unsafe.Pointer(bp))
				}
				return result
			}
		}
	}
	if storedEncName != 0 || storedversion != 0 {
		_poolClear(tls, parser+832)
	}
	return int32(_XML_ERROR_NONE)
}

func _handleUnknownEncoding(tls *libc.TLS, parser TXML_Parser, encodingName uintptr) (r _XML_Error) {
	bp := tls.Alloc(1056)
	defer tls.Free(1056)
	var enc uintptr
	var i int32
	var v2 func(*libc.TLS, uintptr, uintptr, TCONVERTER, uintptr) uintptr
	var _ /* info at bp+0 */ TXML_Encoding
	_, _, _ = enc, i, v2
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandler != 0 {
		i = 0
		for {
			if !(i < int32(256)) {
				break
			}
			*(*int32)(unsafe.Pointer(bp + uintptr(i)*4)) = -int32(1)
			goto _1
		_1:
			;
			i++
		}
		(*(*TXML_Encoding)(unsafe.Pointer(bp))).Fconvert = libc.UintptrFromInt32(0)
		(*(*TXML_Encoding)(unsafe.Pointer(bp))).Fdata = libc.UintptrFromInt32(0)
		(*(*TXML_Encoding)(unsafe.Pointer(bp))).Frelease = libc.UintptrFromInt32(0)
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingHandlerData, encodingName, bp) != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingMem = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(XXmlSizeOfUnknownEncoding(tls)))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingMem != 0) {
				if (*(*TXML_Encoding)(unsafe.Pointer(bp))).Frelease != 0 {
					(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*(*TXML_Encoding)(unsafe.Pointer(bp))).Frelease})))(tls, (*(*TXML_Encoding)(unsafe.Pointer(bp))).Fdata)
				}
				return int32(_XML_ERROR_NO_MEMORY)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0 {
				v2 = XXmlInitUnknownEncodingNS
			} else {
				v2 = XXmlInitUnknownEncoding
			}
			enc = v2(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingMem, bp, (*(*TXML_Encoding)(unsafe.Pointer(bp))).Fconvert, (*(*TXML_Encoding)(unsafe.Pointer(bp))).Fdata)
			if enc != 0 {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingData = (*(*TXML_Encoding)(unsafe.Pointer(bp))).Fdata
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unknownEncodingRelease = (*(*TXML_Encoding)(unsafe.Pointer(bp))).Frelease
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding = enc
				return int32(_XML_ERROR_NONE)
			}
		}
		if (*(*TXML_Encoding)(unsafe.Pointer(bp))).Frelease != libc.UintptrFromInt32(0) {
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*(*TXML_Encoding)(unsafe.Pointer(bp))).Frelease})))(tls, (*(*TXML_Encoding)(unsafe.Pointer(bp))).Fdata)
		}
	}
	return int32(_XML_ERROR_UNKNOWN_ENCODING)
}

func _prologInitProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	var result _XML_Error
	_ = result
	result = _initializeEncoding(tls, parser)
	if result != int32(_XML_ERROR_NONE) {
		return result
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_prologProcessor)
	return _prologProcessor(tls, parser, s, end, nextPtr)
}

func _externalParEntInitProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	var result _XML_Error
	_ = result
	result = _initializeEncoding(tls, parser)
	if result != int32(_XML_ERROR_NONE) {
		return result
	}
	/* we know now that XML_Parse(Buffer) has been called,
	   so we consider the external parameter entity read */
	(*TDTD)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd)).FparamEntityRead = libc.Uint8FromInt32(1)
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FinEntityValue != 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_entityValueInitProcessor)
		return _entityValueInitProcessor(tls, parser, s, end, nextPtr)
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_externalParEntProcessor)
		return _externalParEntProcessor(tls, parser, s, end, nextPtr)
	}
	return r
}

func _entityValueInitProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var result _XML_Error
	var start uintptr
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_, _, _ = result, start, tok
	start = s
	*(*uintptr)(unsafe.Pointer(bp)) = start
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = start
	for {
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, start, end, bp)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = *(*uintptr)(unsafe.Pointer(bp))
		if tok <= 0 {
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) && tok != m_XML_TOK_INVALID {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = s
				return int32(_XML_ERROR_NONE)
			}
			switch tok {
			case m_XML_TOK_INVALID:
				return int32(_XML_ERROR_INVALID_TOKEN)
			case -int32(1):
				return int32(_XML_ERROR_UNCLOSED_TOKEN)
			case -int32(2):
				return int32(_XML_ERROR_PARTIAL_CHAR)
			case -int32(4): /* start == end */
				fallthrough
			default:
				break
			}
			/* found end of entity value - can store it now */
			return _storeEntityValue(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end)
		} else {
			if tok == int32(m_XML_TOK_XML_DECL) {
				result = _processXmlDecl(tls, parser, 0, start, *(*uintptr)(unsafe.Pointer(bp)))
				if result != int32(_XML_ERROR_NONE) {
					return result
				}
				/* At this point, m_parsingStatus.parsing cannot be XML_SUSPENDED.  For
				 * that to happen, a parameter entity parsing handler must have attempted
				 * to suspend the parser, which fails and raises an error.  The parser can
				 * be aborted, but can't be suspended.
				 */
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_FINISHED) {
					return int32(_XML_ERROR_ABORTED)
				}
				*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				/* stop scanning for text declaration - we found one */
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_entityValueProcessor)
				return _entityValueProcessor(tls, parser, *(*uintptr)(unsafe.Pointer(bp)), end, nextPtr)
			} else {
				if tok == int32(m_XML_TOK_BOM) && *(*uintptr)(unsafe.Pointer(bp)) == end && !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
					*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return int32(_XML_ERROR_NONE)
				} else {
					if tok == int32(m_XML_TOK_INSTANCE_START) {
						*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
						return int32(_XML_ERROR_SYNTAX)
					}
				}
			}
		}
		start = *(*uintptr)(unsafe.Pointer(bp))
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = start
		goto _1
	_1:
	}
	return r
}

func _externalParEntProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_ = tok
	*(*uintptr)(unsafe.Pointer(bp)) = s
	tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, bp)
	if tok <= 0 {
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) && tok != m_XML_TOK_INVALID {
			*(*uintptr)(unsafe.Pointer(nextPtr)) = s
			return int32(_XML_ERROR_NONE)
		}
		switch tok {
		case m_XML_TOK_INVALID:
			return int32(_XML_ERROR_INVALID_TOKEN)
		case -int32(1):
			return int32(_XML_ERROR_UNCLOSED_TOKEN)
		case -int32(2):
			return int32(_XML_ERROR_PARTIAL_CHAR)
		case -int32(4): /* start == end */
			fallthrough
		default:
			break
		}
	} else {
		if tok == int32(m_XML_TOK_BOM) {
			s = *(*uintptr)(unsafe.Pointer(bp))
			tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, bp)
		}
	}
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_prologProcessor)
	return _doProlog(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, tok, *(*uintptr)(unsafe.Pointer(bp)), nextPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)), libc.Uint8FromInt32(1))
}

func _entityValueProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var enc, start uintptr
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_, _, _ = enc, start, tok
	start = s
	*(*uintptr)(unsafe.Pointer(bp)) = s
	enc = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding
	for {
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc))})))(tls, enc, start, end, bp)
		if tok <= 0 {
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) && tok != m_XML_TOK_INVALID {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = s
				return int32(_XML_ERROR_NONE)
			}
			switch tok {
			case m_XML_TOK_INVALID:
				return int32(_XML_ERROR_INVALID_TOKEN)
			case -int32(1):
				return int32(_XML_ERROR_UNCLOSED_TOKEN)
			case -int32(2):
				return int32(_XML_ERROR_PARTIAL_CHAR)
			case -int32(4): /* start == end */
				fallthrough
			default:
				break
			}
			/* found end of entity value - can store it now */
			return _storeEntityValue(tls, parser, enc, s, end)
		}
		start = *(*uintptr)(unsafe.Pointer(bp))
		goto _1
	_1:
	}
	return r
}

func _prologProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_ = tok
	*(*uintptr)(unsafe.Pointer(bp)) = s
	tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, bp)
	return _doProlog(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, tok, *(*uintptr)(unsafe.Pointer(bp)), nextPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)), libc.Uint8FromInt32(1))
}

func _doProlog(tls *libc.TLS, parser TXML_Parser, enc uintptr, s uintptr, end uintptr, tok int32, _next uintptr, nextPtr uintptr, haveMore TXML_Bool, allowClosingDoctype TXML_Bool) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _next
	var attVal, content, dtd, el, entity, entity1, entity2, eventEndPP, eventPP, model, name, name1, name2, name3, new_connector, new_scaff_index, nxt, prefix, pubId, systemId, tem, tem1, v65, v67, v68, v70, v71, v74, v75, v77, v78, v86, p80 uintptr
	var betweenDecl, hadParamEntityRefs, hadParamEntityRefs1, handleDefault TXML_Bool
	var myindex, myindex1, nameLen, role, v66, v69, v73, v76, v82, v84, v85, v88 int32
	var quant _XML_Content_Quant
	var result, result1, result2, result3, result4, result5 _XML_Error
	var v72, v79, v83 bool
	var v81 uint32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = attVal, betweenDecl, content, dtd, el, entity, entity1, entity2, eventEndPP, eventPP, hadParamEntityRefs, hadParamEntityRefs1, handleDefault, model, myindex, myindex1, name, name1, name2, name3, nameLen, new_connector, new_scaff_index, nxt, prefix, pubId, quant, result, result1, result2, result3, result4, result5, role, systemId, tem, tem1, v65, v66, v67, v68, v69, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v81, v82, v83, v84, v85, v86, v88, p80
	/* save one level of indirection */
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
		eventPP = parser + 544
		eventEndPP = parser + 552
	} else {
		eventPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
		eventEndPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities + 8
	}
	for {
		handleDefault = libc.Uint8FromInt32(1)
		*(*uintptr)(unsafe.Pointer(eventPP)) = s
		*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp))
		if tok <= 0 {
			if haveMore != 0 && tok != m_XML_TOK_INVALID {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = s
				return int32(_XML_ERROR_NONE)
			}
			switch tok {
			case m_XML_TOK_INVALID:
				*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
				return int32(_XML_ERROR_INVALID_TOKEN)
			case -int32(1):
				return int32(_XML_ERROR_UNCLOSED_TOKEN)
			case -int32(2):
				return int32(_XML_ERROR_PARTIAL_CHAR)
			case -int32(m_XML_TOK_PROLOG_S):
				tok = -tok
			case -int32(4):
				/* for internal PE NOT referenced between declarations */
				if enc != (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding && !((*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities)).FbetweenDecl != 0) {
					*(*uintptr)(unsafe.Pointer(nextPtr)) = s
					return int32(_XML_ERROR_NONE)
				}
				/* WFC: PE Between Declarations - must check that PE contains
				   complete markup, not only for external PEs, but also for
				   internal PEs if the reference occurs between declarations.
				*/
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_isParamEntity != 0 || enc != (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
					if (*(*func(*libc.TLS, uintptr, int32, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TPROLOG_STATE)(unsafe.Pointer(parser + 496)).Fhandler})))(tls, parser+496, -int32(4), end, end, enc) == int32(_XML_ROLE_ERROR) {
						return int32(_XML_ERROR_INCOMPLETE_PE)
					}
					*(*uintptr)(unsafe.Pointer(nextPtr)) = s
					return int32(_XML_ERROR_NONE)
				}
				return int32(_XML_ERROR_NO_ELEMENTS)
			default:
				tok = -tok
				*(*uintptr)(unsafe.Pointer(bp)) = end
				break
			}
		}
		role = (*(*func(*libc.TLS, uintptr, int32, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TPROLOG_STATE)(unsafe.Pointer(parser + 496)).Fhandler})))(tls, parser+496, tok, s, *(*uintptr)(unsafe.Pointer(bp)), enc)
		switch role {
		case int32(_XML_ROLE_XML_DECL):
			goto _2
		case int32(_XML_ROLE_DOCTYPE_NAME):
			goto _3
		case int32(_XML_ROLE_DOCTYPE_INTERNAL_SUBSET):
			goto _4
		case int32(_XML_ROLE_TEXT_DECL):
			goto _5
		case int32(_XML_ROLE_DOCTYPE_PUBLIC_ID):
			goto _6
		case int32(_XML_ROLE_ENTITY_PUBLIC_ID):
			goto _7
		case int32(_XML_ROLE_DOCTYPE_CLOSE):
			goto _8
		case int32(_XML_ROLE_INSTANCE_START):
			goto _9
		case int32(_XML_ROLE_ATTLIST_ELEMENT_NAME):
			goto _10
		case int32(_XML_ROLE_ATTRIBUTE_NAME):
			goto _11
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_CDATA):
			goto _12
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_ID):
			goto _13
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_IDREF):
			goto _14
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_IDREFS):
			goto _15
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_ENTITY):
			goto _16
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_ENTITIES):
			goto _17
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_NMTOKEN):
			goto _18
		case int32(_XML_ROLE_ATTRIBUTE_TYPE_NMTOKENS):
			goto _19
		case int32(_XML_ROLE_ATTRIBUTE_NOTATION_VALUE):
			goto _20
		case int32(_XML_ROLE_ATTRIBUTE_ENUM_VALUE):
			goto _21
		case int32(_XML_ROLE_REQUIRED_ATTRIBUTE_VALUE):
			goto _22
		case int32(_XML_ROLE_IMPLIED_ATTRIBUTE_VALUE):
			goto _23
		case int32(_XML_ROLE_FIXED_ATTRIBUTE_VALUE):
			goto _24
		case int32(_XML_ROLE_DEFAULT_ATTRIBUTE_VALUE):
			goto _25
		case int32(_XML_ROLE_ENTITY_VALUE):
			goto _26
		case int32(_XML_ROLE_DOCTYPE_SYSTEM_ID):
			goto _27
		case int32(_XML_ROLE_ENTITY_SYSTEM_ID):
			goto _28
		case int32(_XML_ROLE_ENTITY_COMPLETE):
			goto _29
		case int32(_XML_ROLE_ENTITY_NOTATION_NAME):
			goto _30
		case int32(_XML_ROLE_GENERAL_ENTITY_NAME):
			goto _31
		case int32(_XML_ROLE_PARAM_ENTITY_NAME):
			goto _32
		case int32(_XML_ROLE_NOTATION_NAME):
			goto _33
		case int32(_XML_ROLE_NOTATION_PUBLIC_ID):
			goto _34
		case int32(_XML_ROLE_NOTATION_SYSTEM_ID):
			goto _35
		case int32(_XML_ROLE_NOTATION_NO_SYSTEM_ID):
			goto _36
		case int32(_XML_ROLE_ERROR):
			goto _37
		case int32(_XML_ROLE_IGNORE_SECT):
			goto _38
		case int32(_XML_ROLE_GROUP_OPEN):
			goto _39
		case int32(_XML_ROLE_GROUP_SEQUENCE):
			goto _40
		case int32(_XML_ROLE_GROUP_CHOICE):
			goto _41
		case int32(_XML_ROLE_INNER_PARAM_ENTITY_REF):
			goto _42
		case int32(_XML_ROLE_PARAM_ENTITY_REF):
			goto _43
		case int32(_XML_ROLE_ELEMENT_NAME):
			goto _44
		case int32(_XML_ROLE_CONTENT_EMPTY):
			goto _45
		case int32(_XML_ROLE_CONTENT_ANY):
			goto _46
		case int32(_XML_ROLE_CONTENT_PCDATA):
			goto _47
		case int32(_XML_ROLE_CONTENT_ELEMENT):
			goto _48
		case int32(_XML_ROLE_CONTENT_ELEMENT_OPT):
			goto _49
		case int32(_XML_ROLE_CONTENT_ELEMENT_REP):
			goto _50
		case int32(_XML_ROLE_CONTENT_ELEMENT_PLUS):
			goto _51
		case int32(_XML_ROLE_GROUP_CLOSE):
			goto _52
		case int32(_XML_ROLE_GROUP_CLOSE_OPT):
			goto _53
		case int32(_XML_ROLE_GROUP_CLOSE_REP):
			goto _54
		case int32(_XML_ROLE_GROUP_CLOSE_PLUS):
			goto _55
		case int32(_XML_ROLE_PI):
			goto _56
		case int32(_XML_ROLE_COMMENT):
			goto _57
		case int32(_XML_ROLE_NONE):
			goto _58
		case int32(_XML_ROLE_DOCTYPE_NONE):
			goto _59
		case int32(_XML_ROLE_ENTITY_NONE):
			goto _60
		case int32(_XML_ROLE_NOTATION_NONE):
			goto _61
		case int32(_XML_ROLE_ATTLIST_NONE):
			goto _62
		case int32(_XML_ROLE_ELEMENT_NONE):
			goto _63
		}
		goto _64
	_2:
		;
		result = _processXmlDecl(tls, parser, 0, s, *(*uintptr)(unsafe.Pointer(bp)))
		if result != int32(_XML_ERROR_NONE) {
			return result
		}
		enc = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding
		handleDefault = libc.Uint8FromInt32(0)
		goto _64
	_3:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName = _poolStoreString(tls, parser+784, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypePubid = libc.UintptrFromInt32(0)
			handleDefault = libc.Uint8FromInt32(0)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid = libc.UintptrFromInt32(0) /* always initialize to NULL */
		goto _64
	_4:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler != 0 {
			(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypePubid, int32(1))
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName = libc.UintptrFromInt32(0)
			_poolClear(tls, parser+784)
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_5:
		;
		result1 = _processXmlDecl(tls, parser, int32(1), s, *(*uintptr)(unsafe.Pointer(bp)))
		if result1 != int32(_XML_ERROR_NONE) {
			return result1
		}
		enc = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding
		handleDefault = libc.Uint8FromInt32(0)
		goto _64
	_6:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD = libc.Uint8FromInt32(0)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = _lookup(tls, parser, dtd+264, uintptr(unsafe.Pointer(&_externalSubsetName)), uint64(64))
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = libc.Uint8FromInt32(1)
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler != 0 {
			if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FisPublicId})))(tls, enc, s, *(*uintptr)(unsafe.Pointer(bp)), eventPP) != 0) {
				return int32(_XML_ERROR_PUBLICID)
			}
			pubId = _poolStoreString(tls, parser+784, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(pubId != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			_normalizePublicId(tls, pubId)
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypePubid = pubId
			handleDefault = libc.Uint8FromInt32(0)
			goto alreadyChecked
		}
		/* fall through */
	_7:
		;
		if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FisPublicId})))(tls, enc, s, *(*uintptr)(unsafe.Pointer(bp)), eventPP) != 0) {
			return int32(_XML_ERROR_PUBLICID)
		}
		goto alreadyChecked
	alreadyChecked:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0 {
			tem = _poolStoreString(tls, dtd+160, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(tem != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			_normalizePublicId(tls, tem)
			(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId = tem
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
			/* Don't suppress the default handler if we fell through from
			 * the XML_ROLE_DOCTYPE_PUBLIC_ID case.
			 */
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 && role == int32(_XML_ROLE_ENTITY_PUBLIC_ID) {
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_8:
		;
		if libc.Int32FromUint8(allowClosingDoctype) != libc.Int32FromUint8(libc.Uint8FromInt32(1)) {
			/* Must not close doctype from within expanded parameter entities */
			return int32(_XML_ERROR_INVALID_TOKEN)
		}
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName != 0 {
			(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeName, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypePubid, 0)
			_poolClear(tls, parser+784)
			handleDefault = libc.Uint8FromInt32(0)
		}
		/* parser->m_doctypeSysid will be non-NULL in the case of a previous
		   XML_ROLE_DOCTYPE_SYSTEM_ID, even if parser->m_startDoctypeDeclHandler
		   was not set, indicating an external subset
		*/
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid != 0 || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD != 0 {
			hadParamEntityRefs = (*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs
			(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = libc.Uint8FromInt32(1)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler != 0 {
				entity = _lookup(tls, parser, dtd+264, uintptr(unsafe.Pointer(&_externalSubsetName)), uint64(64))
				if !(entity != 0) {
					/* The external subset name "#" will have already been
					 * inserted into the hash table at the start of the
					 * external entity parsing, so no allocation will happen
					 * and lookup() cannot fail.
					 */
					return int32(_XML_ERROR_NO_MEMORY) /* LCOV_EXCL_LINE */
				}
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD != 0 {
					(*TENTITY)(unsafe.Pointer(entity)).Fbase = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase
				}
				(*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead = libc.Uint8FromInt32(0)
				if !((*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg, uintptr(0), (*TENTITY)(unsafe.Pointer(entity)).Fbase, (*TENTITY)(unsafe.Pointer(entity)).FsystemId, (*TENTITY)(unsafe.Pointer(entity)).FpublicId) != 0) {
					return int32(_XML_ERROR_EXTERNAL_ENTITY_HANDLING)
				}
				if (*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead != 0 {
					if !((*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler != 0 && !((*(*func(*libc.TLS, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg) != 0) {
						return int32(_XML_ERROR_NOT_STANDALONE)
					}
				} else {
					if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid != 0) {
						(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = hadParamEntityRefs
					}
				}
				/* end of DTD - no need to update dtd->keepProcessing */
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD = libc.Uint8FromInt32(0)
		}
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endDoctypeDeclHandler != 0 {
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_endDoctypeDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg)
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_9:
		;
		/* if there is no DOCTYPE declaration then now is the
		   last chance to read the foreign DTD
		*/
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD != 0 {
			hadParamEntityRefs1 = (*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs
			(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = libc.Uint8FromInt32(1)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler != 0 {
				entity1 = _lookup(tls, parser, dtd+264, uintptr(unsafe.Pointer(&_externalSubsetName)), uint64(64))
				if !(entity1 != 0) {
					return int32(_XML_ERROR_NO_MEMORY)
				}
				(*TENTITY)(unsafe.Pointer(entity1)).Fbase = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase
				(*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead = libc.Uint8FromInt32(0)
				if !((*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg, uintptr(0), (*TENTITY)(unsafe.Pointer(entity1)).Fbase, (*TENTITY)(unsafe.Pointer(entity1)).FsystemId, (*TENTITY)(unsafe.Pointer(entity1)).FpublicId) != 0) {
					return int32(_XML_ERROR_EXTERNAL_ENTITY_HANDLING)
				}
				if (*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead != 0 {
					if !((*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler != 0 && !((*(*func(*libc.TLS, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg) != 0) {
						return int32(_XML_ERROR_NOT_STANDALONE)
					}
				} else {
					(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = hadParamEntityRefs1
				}
				/* end of DTD - no need to update dtd->keepProcessing */
			}
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_contentProcessor)
		return _contentProcessor(tls, parser, s, end, nextPtr)
	_10:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType = _getElementType(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		goto checkAttListDeclHandler
	_11:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId = _getAttributeId(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsCdata = libc.Uint8FromInt32(0)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = libc.UintptrFromInt32(0)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsId = libc.Uint8FromInt32(0)
		goto checkAttListDeclHandler
	_12:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsCdata = libc.Uint8FromInt32(1)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeCDATA))
		goto checkAttListDeclHandler
	_13:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsId = libc.Uint8FromInt32(1)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeID))
		goto checkAttListDeclHandler
	_14:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeIDREF))
		goto checkAttListDeclHandler
	_15:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeIDREFS))
		goto checkAttListDeclHandler
	_16:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeENTITY))
		goto checkAttListDeclHandler
	_17:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeENTITIES))
		goto checkAttListDeclHandler
	_18:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeNMTOKEN))
		goto checkAttListDeclHandler
	_19:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = uintptr(unsafe.Pointer(&_atypeNMTOKENS))
		goto checkAttListDeclHandler
	checkAttListDeclHandler:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_21:
		;
	_20:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler != 0 {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType != 0 {
				prefix = uintptr(unsafe.Pointer(&_enumValueSep))
			} else {
				if role == int32(_XML_ROLE_ATTRIBUTE_NOTATION_VALUE) {
					v65 = uintptr(unsafe.Pointer(&_notationPrefix))
				} else {
					v65 = uintptr(unsafe.Pointer(&_enumValueStart))
				}
				prefix = v65
			}
			if !(_poolAppendString(tls, parser+784, prefix) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			if !(_poolAppend(tls, parser+784, enc, s, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tempPool.Fstart
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_23:
		;
	_22:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 {
			if !(_defineAttribute(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsCdata, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsId, uintptr(0), parser) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType != 0 {
				if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType))) == int32(m_ASCII_LPAREN) || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType))) == int32(m_ASCII_N) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType + 1))) == int32(m_ASCII_O) {
					/* Enumerated or Notation type */
					if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
						v66 = 0
					} else {
						v68 = parser + 784 + 24
						v67 = *(*uintptr)(unsafe.Pointer(v68))
						*(*uintptr)(unsafe.Pointer(v68))++
						*(*TXML_Char)(unsafe.Pointer(v67)) = uint8(m_ASCII_RPAREN)
						v66 = libc.Int32FromInt32(1)
					}
					if v72 = !(v66 != 0); !v72 {
						if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
							v69 = 0
						} else {
							v71 = parser + 784 + 24
							v70 = *(*uintptr)(unsafe.Pointer(v71))
							*(*uintptr)(unsafe.Pointer(v71))++
							*(*TXML_Char)(unsafe.Pointer(v70)) = uint8('\000')
							v69 = libc.Int32FromInt32(1)
						}
					}
					if v72 || !(v69 != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tempPool.Fstart
					(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
				}
				*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TELEMENT_TYPE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType)).Fname, (*TATTRIBUTE_ID)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId)).Fname, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType, uintptr(0), libc.BoolInt32(role == int32(_XML_ROLE_REQUIRED_ATTRIBUTE_VALUE)))
				_poolClear(tls, parser+784)
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_25:
		;
	_24:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 {
			result2 = _storeAttributeValue(tls, parser, enc, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsCdata, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), dtd+160)
			if result2 != 0 {
				return result2
			}
			attVal = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
			/* ID attributes aren't allowed to have a default */
			if !(_defineAttribute(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeIsCdata, libc.Uint8FromInt32(0), attVal, parser) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType != 0 {
				if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType))) == int32(m_ASCII_LPAREN) || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType))) == int32(m_ASCII_N) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType + 1))) == int32(m_ASCII_O) {
					/* Enumerated or Notation type */
					if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
						v73 = 0
					} else {
						v75 = parser + 784 + 24
						v74 = *(*uintptr)(unsafe.Pointer(v75))
						*(*uintptr)(unsafe.Pointer(v75))++
						*(*TXML_Char)(unsafe.Pointer(v74)) = uint8(m_ASCII_RPAREN)
						v73 = libc.Int32FromInt32(1)
					}
					if v79 = !(v73 != 0); !v79 {
						if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
							v76 = 0
						} else {
							v78 = parser + 784 + 24
							v77 = *(*uintptr)(unsafe.Pointer(v78))
							*(*uintptr)(unsafe.Pointer(v78))++
							*(*TXML_Char)(unsafe.Pointer(v77)) = uint8('\000')
							v76 = libc.Int32FromInt32(1)
						}
					}
					if v79 || !(v76 != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tempPool.Fstart
					(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
				}
				*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TELEMENT_TYPE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType)).Fname, (*TATTRIBUTE_ID)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeId)).Fname, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declAttributeType, attVal, libc.BoolInt32(role == int32(_XML_ROLE_FIXED_ATTRIBUTE_VALUE)))
				_poolClear(tls, parser+784)
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_26:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 {
			result3 = _storeEntityValue(tls, parser, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0 {
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FtextPtr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 208)).Fstart
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FtextLen = int32(int64((*TSTRING_POOL)(unsafe.Pointer(dtd+208)).Fptr) - int64((*TSTRING_POOL)(unsafe.Pointer(dtd+208)).Fstart))
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 208)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 208)).Fptr
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 {
					*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
					(*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr, int32, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fname, libc.Int32FromUint8((*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fis_param), (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FtextPtr, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FtextLen, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase, uintptr(0), uintptr(0), uintptr(0))
					handleDefault = libc.Uint8FromInt32(0)
				}
			} else {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 208)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 208)).Fstart
			}
			if result3 != int32(_XML_ERROR_NONE) {
				return result3
			}
		}
		goto _64
	_27:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_useForeignDTD = libc.Uint8FromInt32(0)
		(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = libc.Uint8FromInt32(1)
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid = _poolStoreString(tls, parser+784, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid == libc.UintptrFromInt32(0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			handleDefault = libc.Uint8FromInt32(0)
		} else {
			/* use externalSubsetName to make parser->m_doctypeSysid non-NULL
			   for the case where no parser->m_startDoctypeDeclHandler is set */
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_doctypeSysid = uintptr(unsafe.Pointer(&_externalSubsetName))
		}
		if !((*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0) && !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler != 0 && !((*(*func(*libc.TLS, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg) != 0) {
			return int32(_XML_ERROR_NOT_STANDALONE)
		}
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0) {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = _lookup(tls, parser, dtd+264, uintptr(unsafe.Pointer(&_externalSubsetName)), uint64(64))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId = libc.UintptrFromInt32(0)
		}
		/* fall through */
	_28:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0 {
			(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FsystemId = _poolStoreString(tls, dtd+160, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !((*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FsystemId != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fbase = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
			/* Don't suppress the default handler if we fell through from
			 * the XML_ROLE_DOCTYPE_SYSTEM_ID case.
			 */
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 && role == int32(_XML_ROLE_ENTITY_SYSTEM_ID) {
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_29:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 {
			*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
			(*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr, int32, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fname, libc.Int32FromUint8((*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fis_param), uintptr(0), 0, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fbase, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FsystemId, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId, uintptr(0))
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_30:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0 {
			(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fnotation = _poolStoreString(tls, dtd+160, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
			if !((*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fnotation != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unparsedEntityDeclHandler != 0 {
				*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_unparsedEntityDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fname, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fbase, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FsystemId, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fnotation)
				handleDefault = libc.Uint8FromInt32(0)
			} else {
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 {
					*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
					(*(*func(*libc.TLS, uintptr, uintptr, int32, uintptr, int32, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fname, 0, uintptr(0), 0, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fbase, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FsystemId, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId, (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fnotation)
					handleDefault = libc.Uint8FromInt32(0)
				}
			}
		}
		goto _64
	_31:
		;
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FpredefinedEntityName})))(tls, enc, s, *(*uintptr)(unsafe.Pointer(bp))) != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = libc.UintptrFromInt32(0)
			goto _64
		}
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 {
			name = _poolStoreString(tls, dtd+160, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
			if !(name != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = _lookup(tls, parser, dtd, name, uint64(64))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			if (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fname != name {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = libc.UintptrFromInt32(0)
			} else {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId = libc.UintptrFromInt32(0)
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fis_param = libc.Uint8FromInt32(0)
				/* if we have a parent parser or are reading an internal parameter
				   entity, then the entity declaration is not considered "internal"
				*/
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fis_internal = libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0 || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities != 0))
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 {
					handleDefault = libc.Uint8FromInt32(0)
				}
			}
		} else {
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = libc.UintptrFromInt32(0)
		}
		goto _64
	_32:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 {
			name1 = _poolStoreString(tls, dtd+160, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
			if !(name1 != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = _lookup(tls, parser, dtd+264, name1, uint64(64))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			if (*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fname != name1 {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = libc.UintptrFromInt32(0)
			} else {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).FpublicId = libc.UintptrFromInt32(0)
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fis_param = libc.Uint8FromInt32(1)
				/* if we have a parent parser or are reading an internal parameter
				   entity, then the entity declaration is not considered "internal"
				*/
				(*TENTITY)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity)).Fis_internal = libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0 || (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities != 0))
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 {
					handleDefault = libc.Uint8FromInt32(0)
				}
			}
		} else {
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declEntity = libc.UintptrFromInt32(0)
		}
		goto _64
	_33:
		;
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationPublicId = libc.UintptrFromInt32(0)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName = libc.UintptrFromInt32(0)
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName = _poolStoreString(tls, parser+784, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_34:
		;
		if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FisPublicId})))(tls, enc, s, *(*uintptr)(unsafe.Pointer(bp)), eventPP) != 0) {
			return int32(_XML_ERROR_PUBLICID)
		}
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName != 0 { /* means m_notationDeclHandler != NULL */
			tem1 = _poolStoreString(tls, parser+784, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(tem1 != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			_normalizePublicId(tls, tem1)
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationPublicId = tem1
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_35:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler != 0 {
			systemId = _poolStoreString(tls, parser+784, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(systemId != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
			(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase, systemId, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationPublicId)
			handleDefault = libc.Uint8FromInt32(0)
		}
		_poolClear(tls, parser+784)
		goto _64
	_36:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationPublicId != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler != 0 {
			*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
			(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationName, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_curBase, uintptr(0), (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declNotationPublicId)
			handleDefault = libc.Uint8FromInt32(0)
		}
		_poolClear(tls, parser+784)
		goto _64
	_37:
		;
		switch tok {
		case int32(m_XML_TOK_PARAM_ENTITY_REF):
			/* PE references in internal subset are
			   not allowed within declarations. */
			return int32(_XML_ERROR_PARAM_ENTITY_REF)
		case int32(m_XML_TOK_XML_DECL):
			return int32(_XML_ERROR_MISPLACED_XML_PI)
		default:
			return int32(_XML_ERROR_SYNTAX)
		}
	_38:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
			_reportDefault(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
		}
		handleDefault = libc.Uint8FromInt32(0)
		result4 = _doIgnoreSection(tls, parser, enc, bp, end, nextPtr, haveMore)
		if result4 != int32(_XML_ERROR_NONE) {
			return result4
		} else {
			if !(*(*uintptr)(unsafe.Pointer(bp)) != 0) {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_ignoreSectionProcessor)
				return result4
			}
		}
		goto _64
	_39:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel >= (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize != 0 {
				p80 = parser + 888
				*(*uint32)(unsafe.Pointer(p80)) *= uint32(2)
				new_connector = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector, uint64(*(*uint32)(unsafe.Pointer(p80))))
				if new_connector == libc.UintptrFromInt32(0) {
					*(*uint32)(unsafe.Pointer(parser + 888)) /= uint32(2)
					return int32(_XML_ERROR_NO_MEMORY)
				}
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector = new_connector
				if (*TDTD)(unsafe.Pointer(dtd)).FscaffIndex != 0 {
					new_scaff_index = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TDTD)(unsafe.Pointer(dtd)).FscaffIndex, uint64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize)*libc.Uint64FromInt64(4))
					if new_scaff_index == libc.UintptrFromInt32(0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					(*TDTD)(unsafe.Pointer(dtd)).FscaffIndex = new_scaff_index
				}
			} else {
				v81 = libc.Uint32FromInt32(32)
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize = v81
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, uint64(v81))
				if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector != 0) {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize = uint32(0)
					return int32(_XML_ERROR_NO_MEMORY)
				}
			}
		}
		*(*uint8)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector + uintptr((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel))) = uint8(0)
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 {
			myindex = _nextScaffoldPart(tls, parser)
			if myindex < 0 {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffLevel)*4)) = myindex
			(*TDTD)(unsafe.Pointer(dtd)).FscaffLevel++
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(myindex)*32))).Ftype1 = int32(_XML_CTYPE_SEQ)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_40:
		;
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector + uintptr((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel)))) == int32(m_ASCII_PIPE) {
			return int32(_XML_ERROR_SYNTAX)
		}
		*(*uint8)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector + uintptr((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel))) = uint8(m_ASCII_COMMA)
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_41:
		;
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector + uintptr((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel)))) == int32(m_ASCII_COMMA) {
			return int32(_XML_ERROR_SYNTAX)
		}
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 && !(*(*uint8)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector + uintptr((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel))) != 0) && (*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffLevel-int32(1))*4)))*32))).Ftype1 != int32(_XML_CTYPE_MIXED) {
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffLevel-int32(1))*4)))*32))).Ftype1 = int32(_XML_CTYPE_CHOICE)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		*(*uint8)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupConnector + uintptr((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.Flevel))) = uint8(m_ASCII_PIPE)
		goto _64
	_43:
		;
	_42:
		;
		(*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs = libc.Uint8FromInt32(1)
		if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_paramEntityParsing != 0) {
			(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
		} else {
			name2 = _poolStoreString(tls, dtd+160, enc, s+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(name2 != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			entity2 = _lookup(tls, parser, dtd+264, name2, uint64(0))
			(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
			/* first, determine if a check for an existing declaration is needed;
			   if yes, check that the entity exists, and that it is internal,
			   otherwise call the skipped entity handler
			*/
			if v83 = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FdocumentEntity != 0; v83 {
				if (*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0 {
					v82 = libc.BoolInt32(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities != 0))
				} else {
					v82 = libc.BoolInt32(!((*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs != 0))
				}
			}
			if v83 && v82 != 0 {
				if !(entity2 != 0) {
					return int32(_XML_ERROR_UNDEFINED_ENTITY)
				} else {
					if !((*TENTITY)(unsafe.Pointer(entity2)).Fis_internal != 0) {
						/* It's hard to exhaustively search the code to be sure,
						 * but there doesn't seem to be a way of executing the
						 * following line.  There are two cases:
						 *
						 * If 'standalone' is false, the DTD must have no
						 * parameter entities or we wouldn't have passed the outer
						 * 'if' statement.  That measn the only entity in the hash
						 * table is the external subset name "#" which cannot be
						 * given as a parameter entity name in XML syntax, so the
						 * lookup must have returned NULL and we don't even reach
						 * the test for an internal entity.
						 *
						 * If 'standalone' is true, it does not seem to be
						 * possible to create entities taking this code path that
						 * are not internal entities, so fail the test above.
						 *
						 * Because this analysis is very uncertain, the code is
						 * being left in place and merely removed from the
						 * coverage test statistics.
						 */
						return int32(_XML_ERROR_ENTITY_DECLARED_IN_PE) /* LCOV_EXCL_LINE */
					}
				}
			} else {
				if !(entity2 != 0) {
					(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
					/* cannot report skipped entities in declarations */
					if role == int32(_XML_ROLE_PARAM_ENTITY_REF) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler != 0 {
						(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_skippedEntityHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, name2, int32(1))
						handleDefault = libc.Uint8FromInt32(0)
					}
					goto _64
				}
			}
			if (*TENTITY)(unsafe.Pointer(entity2)).Fopen != 0 {
				return int32(_XML_ERROR_RECURSIVE_ENTITY_REF)
			}
			if (*TENTITY)(unsafe.Pointer(entity2)).FtextPtr != 0 {
				if role == int32(_XML_ROLE_PARAM_ENTITY_REF) {
					v84 = libc.Int32FromUint8(libc.Uint8FromInt32(1))
				} else {
					v84 = libc.Int32FromUint8(libc.Uint8FromInt32(0))
				}
				betweenDecl = libc.Uint8FromInt32(v84)
				result5 = _processInternalEntity(tls, parser, entity2, betweenDecl)
				if result5 != int32(_XML_ERROR_NONE) {
					return result5
				}
				handleDefault = libc.Uint8FromInt32(0)
				goto _64
			}
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler != 0 {
				(*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead = libc.Uint8FromInt32(0)
				(*TENTITY)(unsafe.Pointer(entity2)).Fopen = libc.Uint8FromInt32(1)
				if !((*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg, uintptr(0), (*TENTITY)(unsafe.Pointer(entity2)).Fbase, (*TENTITY)(unsafe.Pointer(entity2)).FsystemId, (*TENTITY)(unsafe.Pointer(entity2)).FpublicId) != 0) {
					(*TENTITY)(unsafe.Pointer(entity2)).Fopen = libc.Uint8FromInt32(0)
					return int32(_XML_ERROR_EXTERNAL_ENTITY_HANDLING)
				}
				(*TENTITY)(unsafe.Pointer(entity2)).Fopen = libc.Uint8FromInt32(0)
				handleDefault = libc.Uint8FromInt32(0)
				if !((*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead != 0) {
					(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
					goto _64
				}
			} else {
				(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
				goto _64
			}
		}
		if !((*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler != 0 && !((*(*func(*libc.TLS, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notStandaloneHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg) != 0) {
			return int32(_XML_ERROR_NOT_STANDALONE)
		}
		goto _64
		/* Element declaration stuff */
	_44:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType = _getElementType(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*TDTD)(unsafe.Pointer(dtd)).FscaffLevel = 0
			(*TDTD)(unsafe.Pointer(dtd)).FscaffCount = uint32(0)
			(*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl = libc.Uint8FromInt32(1)
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_46:
		;
	_45:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
				content = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt64(32))
				if !(content != 0) {
					return int32(_XML_ERROR_NO_MEMORY)
				}
				(*TXML_Content)(unsafe.Pointer(content)).Fquant = int32(_XML_CQUANT_NONE)
				(*TXML_Content)(unsafe.Pointer(content)).Fname = libc.UintptrFromInt32(0)
				(*TXML_Content)(unsafe.Pointer(content)).Fnumchildren = uint32(0)
				(*TXML_Content)(unsafe.Pointer(content)).Fchildren = libc.UintptrFromInt32(0)
				if role == int32(_XML_ROLE_CONTENT_ANY) {
					v85 = int32(_XML_CTYPE_ANY)
				} else {
					v85 = int32(_XML_CTYPE_EMPTY)
				}
				(*TXML_Content)(unsafe.Pointer(content)).Ftype1 = v85
				*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
				(*(*func(*libc.TLS, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TELEMENT_TYPE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType)).Fname, content)
				handleDefault = libc.Uint8FromInt32(0)
			}
			(*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl = libc.Uint8FromInt32(0)
		}
		goto _64
	_47:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 {
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffLevel-int32(1))*4)))*32))).Ftype1 = int32(_XML_CTYPE_MIXED)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_48:
		;
		quant = int32(_XML_CQUANT_NONE)
		goto elementContent
	_49:
		;
		quant = int32(_XML_CQUANT_OPT)
		goto elementContent
	_50:
		;
		quant = int32(_XML_CQUANT_REP)
		goto elementContent
	_51:
		;
		quant = int32(_XML_CQUANT_PLUS)
		goto elementContent
	elementContent:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 {
			if quant == int32(_XML_CQUANT_NONE) {
				v86 = *(*uintptr)(unsafe.Pointer(bp))
			} else {
				v86 = *(*uintptr)(unsafe.Pointer(bp)) - uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
			}
			nxt = v86
			myindex1 = _nextScaffoldPart(tls, parser)
			if myindex1 < 0 {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(myindex1)*32))).Ftype1 = int32(_XML_CTYPE_NAME)
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(myindex1)*32))).Fquant = quant
			el = _getElementType(tls, parser, enc, s, nxt)
			if !(el != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			name3 = (*TELEMENT_TYPE)(unsafe.Pointer(el)).Fname
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(myindex1)*32))).Fname = name3
			nameLen = 0
			for {
				v88 = nameLen
				nameLen++
				if !(*(*TXML_Char)(unsafe.Pointer(name3 + uintptr(v88))) != 0) {
					break
				}
				goto _87
			_87:
			}
			*(*uint32)(unsafe.Pointer(dtd + 336)) += libc.Uint32FromInt32(nameLen)
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
				handleDefault = libc.Uint8FromInt32(0)
			}
		}
		goto _64
	_52:
		;
		quant = int32(_XML_CQUANT_NONE)
		goto closeGroup
	_53:
		;
		quant = int32(_XML_CQUANT_OPT)
		goto closeGroup
	_54:
		;
		quant = int32(_XML_CQUANT_REP)
		goto closeGroup
	_55:
		;
		quant = int32(_XML_CQUANT_PLUS)
		goto closeGroup
	closeGroup:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl != 0 {
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
				handleDefault = libc.Uint8FromInt32(0)
			}
			(*TDTD)(unsafe.Pointer(dtd)).FscaffLevel--
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffLevel)*4)))*32))).Fquant = quant
			if (*TDTD)(unsafe.Pointer(dtd)).FscaffLevel == 0 {
				if !(handleDefault != 0) {
					model = _build_model(tls, parser)
					if !(model != 0) {
						return int32(_XML_ERROR_NO_MEMORY)
					}
					*(*uintptr)(unsafe.Pointer(eventEndPP)) = s
					(*(*func(*libc.TLS, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TELEMENT_TYPE)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_declElementType)).Fname, model)
				}
				(*TDTD)(unsafe.Pointer(dtd)).Fin_eldecl = libc.Uint8FromInt32(0)
				(*TDTD)(unsafe.Pointer(dtd)).FcontentStringLen = uint32(0)
			}
		}
		goto _64
		/* End element declaration stuff */
	_56:
		;
		if !(_reportProcessingInstruction(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		handleDefault = libc.Uint8FromInt32(0)
		goto _64
	_57:
		;
		if !(_reportComment(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
		handleDefault = libc.Uint8FromInt32(0)
		goto _64
	_58:
		;
		switch tok {
		case int32(m_XML_TOK_BOM):
			handleDefault = libc.Uint8FromInt32(0)
			break
		}
		goto _64
	_59:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_startDoctypeDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_60:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_entityDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_61:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_notationDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_62:
		;
		if (*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_attlistDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_63:
		;
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_elementDeclHandler != 0 {
			handleDefault = libc.Uint8FromInt32(0)
		}
		goto _64
	_64:
		; /* end of big switch */
		if handleDefault != 0 && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
			_reportDefault(tls, parser, enc, s, *(*uintptr)(unsafe.Pointer(bp)))
		}
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return int32(_XML_ERROR_NONE)
		case int32(_XML_FINISHED):
			return int32(_XML_ERROR_ABORTED)
		default:
			s = *(*uintptr)(unsafe.Pointer(bp))
			tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc))})))(tls, enc, s, end, bp)
		}
		goto _1
	_1:
	}
	/* not reached */
	return r
}

var _externalSubsetName = [2]TXML_Char{
	0: uint8(m_ASCII_HASH),
}

var _atypeCDATA = [6]TXML_Char{
	0: uint8(m_ASCII_C),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_A),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_A),
}

var _atypeID = [3]TXML_Char{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_D),
}

var _atypeIDREF = [6]TXML_Char{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_R),
	3: uint8(m_ASCII_E),
	4: uint8(m_ASCII_F),
}

var _atypeIDREFS = [7]TXML_Char{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_R),
	3: uint8(m_ASCII_E),
	4: uint8(m_ASCII_F),
	5: uint8(m_ASCII_S),
}

var _atypeENTITY = [7]TXML_Char{
	0: uint8(m_ASCII_E),
	1: uint8(m_ASCII_N),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_I),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_Y),
}

var _atypeENTITIES = [9]TXML_Char{
	0: uint8(m_ASCII_E),
	1: uint8(m_ASCII_N),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_I),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_I),
	6: uint8(m_ASCII_E),
	7: uint8(m_ASCII_S),
}

var _atypeNMTOKEN = [8]TXML_Char{
	0: uint8(m_ASCII_N),
	1: uint8(m_ASCII_M),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_O),
	4: uint8(m_ASCII_K),
	5: uint8(m_ASCII_E),
	6: uint8(m_ASCII_N),
}

var _atypeNMTOKENS = [9]TXML_Char{
	0: uint8(m_ASCII_N),
	1: uint8(m_ASCII_M),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_O),
	4: uint8(m_ASCII_K),
	5: uint8(m_ASCII_E),
	6: uint8(m_ASCII_N),
	7: uint8(m_ASCII_S),
}

var _notationPrefix = [10]TXML_Char{
	0: uint8(m_ASCII_N),
	1: uint8(m_ASCII_O),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_A),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_I),
	6: uint8(m_ASCII_O),
	7: uint8(m_ASCII_N),
	8: uint8(m_ASCII_LPAREN),
}

var _enumValueSep = [2]TXML_Char{
	0: uint8(m_ASCII_PIPE),
}

var _enumValueStart = [2]TXML_Char{
	0: uint8(m_ASCII_LPAREN),
}

func _epilogProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var tok int32
	var v2 uintptr
	var _ /* next at bp+0 */ uintptr
	_, _ = tok, v2
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_epilogProcessor)
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = s
	for {
		*(*uintptr)(unsafe.Pointer(bp)) = libc.UintptrFromInt32(0)
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, bp)
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventEndPtr = *(*uintptr)(unsafe.Pointer(bp))
		switch tok {
		/* report partial linebreak - it might be the last token */
		case -int32(m_XML_TOK_PROLOG_S):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
				_reportDefault(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, *(*uintptr)(unsafe.Pointer(bp)))
				if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_FINISHED) {
					return int32(_XML_ERROR_ABORTED)
				}
			}
			*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return int32(_XML_ERROR_NONE)
		case -int32(4):
			*(*uintptr)(unsafe.Pointer(nextPtr)) = s
			return int32(_XML_ERROR_NONE)
		case int32(m_XML_TOK_PROLOG_S):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
				_reportDefault(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, *(*uintptr)(unsafe.Pointer(bp)))
			}
		case int32(m_XML_TOK_PI):
			if !(_reportProcessingInstruction(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		case int32(m_XML_TOK_COMMENT):
			if !(_reportComment(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		case m_XML_TOK_INVALID:
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = *(*uintptr)(unsafe.Pointer(bp))
			return int32(_XML_ERROR_INVALID_TOKEN)
		case -int32(1):
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = s
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_UNCLOSED_TOKEN)
		case -int32(2):
			if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0) {
				*(*uintptr)(unsafe.Pointer(nextPtr)) = s
				return int32(_XML_ERROR_NONE)
			}
			return int32(_XML_ERROR_PARTIAL_CHAR)
		default:
			return int32(_XML_ERROR_JUNK_AFTER_DOC_ELEMENT)
		}
		v2 = *(*uintptr)(unsafe.Pointer(bp))
		s = v2
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = v2
		switch (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing {
		case int32(_XML_SUSPENDED):
			*(*uintptr)(unsafe.Pointer(nextPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return int32(_XML_ERROR_NONE)
		case int32(_XML_FINISHED):
			return int32(_XML_ERROR_ABORTED)
		default:
		}
		goto _1
	_1:
	}
	return r
}

func _processInternalEntity(tls *libc.TLS, parser TXML_Parser, entity uintptr, betweenDecl TXML_Bool) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var openEntity, textEnd, textStart uintptr
	var result _XML_Error
	var tok int32
	var _ /* next at bp+0 */ uintptr
	_, _, _, _, _ = openEntity, result, textEnd, textStart, tok
	if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities != 0 {
		openEntity = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities = (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext
	} else {
		openEntity = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt64(40))
		if !(openEntity != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
	}
	(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(1)
	(*TENTITY)(unsafe.Pointer(entity)).Fprocessed = 0
	(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities = openEntity
	(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fentity = entity
	(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).FstartTagLevel = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel
	(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).FbetweenDecl = betweenDecl
	(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).FinternalEventPtr = libc.UintptrFromInt32(0)
	(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).FinternalEventEndPtr = libc.UintptrFromInt32(0)
	textStart = (*TENTITY)(unsafe.Pointer(entity)).FtextPtr
	textEnd = (*TENTITY)(unsafe.Pointer(entity)).FtextPtr + uintptr((*TENTITY)(unsafe.Pointer(entity)).FtextLen)
	/* Set a safe default value in case 'next' does not get set */
	*(*uintptr)(unsafe.Pointer(bp)) = textStart
	if (*TENTITY)(unsafe.Pointer(entity)).Fis_param != 0 {
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, textStart, textEnd, bp)
		result = _doProlog(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, textStart, textEnd, tok, *(*uintptr)(unsafe.Pointer(bp)), bp, libc.Uint8FromInt32(0), libc.Uint8FromInt32(0))
	} else {
		result = _doContent(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tagLevel, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, textStart, textEnd, bp, libc.Uint8FromInt32(0))
	}
	if result == int32(_XML_ERROR_NONE) {
		if textEnd != *(*uintptr)(unsafe.Pointer(bp)) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
			(*TENTITY)(unsafe.Pointer(entity)).Fprocessed = int32(int64(*(*uintptr)(unsafe.Pointer(bp))) - int64(textStart))
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_internalEntityProcessor)
		} else {
			(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities = (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext
			/* put openEntity back in list of free instances */
			(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities = openEntity
		}
	}
	return result
}

func _internalEntityProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var entity, openEntity, textEnd, textStart uintptr
	var result _XML_Error
	var tok, tok1, v1 int32
	var _ /* next at bp+0 */ uintptr
	_, _, _, _, _, _, _, _ = entity, openEntity, result, textEnd, textStart, tok, tok1, v1
	openEntity = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
	if !(openEntity != 0) {
		return int32(_XML_ERROR_UNEXPECTED_STATE)
	}
	entity = (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fentity
	textStart = (*TENTITY)(unsafe.Pointer(entity)).FtextPtr + uintptr((*TENTITY)(unsafe.Pointer(entity)).Fprocessed)
	textEnd = (*TENTITY)(unsafe.Pointer(entity)).FtextPtr + uintptr((*TENTITY)(unsafe.Pointer(entity)).FtextLen)
	/* Set a safe default value in case 'next' does not get set */
	*(*uintptr)(unsafe.Pointer(bp)) = textStart
	if (*TENTITY)(unsafe.Pointer(entity)).Fis_param != 0 {
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, textStart, textEnd, bp)
		result = _doProlog(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, textStart, textEnd, tok, *(*uintptr)(unsafe.Pointer(bp)), bp, libc.Uint8FromInt32(0), libc.Uint8FromInt32(1))
	} else {
		result = _doContent(tls, parser, (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).FstartTagLevel, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, textStart, textEnd, bp, libc.Uint8FromInt32(0))
	}
	if result != int32(_XML_ERROR_NONE) {
		return result
	} else {
		if textEnd != *(*uintptr)(unsafe.Pointer(bp)) && (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.Fparsing == int32(_XML_SUSPENDED) {
			(*TENTITY)(unsafe.Pointer(entity)).Fprocessed = int32(int64(*(*uintptr)(unsafe.Pointer(bp))) - int64((*TENTITY)(unsafe.Pointer(entity)).FtextPtr))
			return result
		} else {
			(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities = (*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext
			/* put openEntity back in list of free instances */
			(*TOPEN_INTERNAL_ENTITY)(unsafe.Pointer(openEntity)).Fnext = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_freeInternalEntities = openEntity
		}
	}
	if (*TENTITY)(unsafe.Pointer(entity)).Fis_param != 0 {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_prologProcessor)
		tok1 = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding))})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, bp)
		return _doProlog(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, tok1, *(*uintptr)(unsafe.Pointer(bp)), nextPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)), libc.Uint8FromInt32(1))
	} else {
		(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processor = __ccgo_fp(_contentProcessor)
		/* see externalEntityContentProcessor vs contentProcessor */
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parentParser != 0 {
			v1 = int32(1)
		} else {
			v1 = 0
		}
		return _doContent(tls, parser, v1, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding, s, end, nextPtr, libc.BoolUint8(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_parsingStatus.FfinalBuffer != 0)))
	}
	return r
}

func _errorProcessor(tls *libc.TLS, parser TXML_Parser, s uintptr, end uintptr, nextPtr uintptr) (r _XML_Error) {
	_ = s
	_ = end
	_ = nextPtr
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_errorCode
}

func _storeAttributeValue(tls *libc.TLS, parser TXML_Parser, enc uintptr, isCdata TXML_Bool, ptr uintptr, end uintptr, pool uintptr) (r _XML_Error) {
	var result _XML_Error
	var v1 int32
	var v2, v3 uintptr
	_, _, _, _ = result, v1, v2, v3
	result = _appendAttributeValue(tls, parser, enc, isCdata, ptr, end, pool)
	if result != 0 {
		return result
	}
	if !(isCdata != 0) && int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart) != 0 && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr + uintptr(-libc.Int32FromInt32(1))))) == int32(0x20) {
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr--
	}
	if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
		v1 = 0
	} else {
		v3 = pool + 24
		v2 = *(*uintptr)(unsafe.Pointer(v3))
		*(*uintptr)(unsafe.Pointer(v3))++
		*(*TXML_Char)(unsafe.Pointer(v2)) = uint8('\000')
		v1 = libc.Int32FromInt32(1)
	}
	if !(v1 != 0) {
		return int32(_XML_ERROR_NO_MEMORY)
	}
	return int32(_XML_ERROR_NONE)
}

func _appendAttributeValue(tls *libc.TLS, parser TXML_Parser, enc uintptr, isCdata TXML_Bool, ptr uintptr, end uintptr, pool uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var ch TXML_Char
	var checkEntityDecl uint8
	var dtd, entity, name, textEnd, v10, v11, v4, v5, v7, v8 uintptr
	var i, n, tok, v12, v3, v6, v9 int32
	var result _XML_Error
	var v13 bool
	var _ /* buf at bp+8 */ [4]TXML_Char
	var _ /* next at bp+0 */ uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = ch, checkEntityDecl, dtd, entity, i, n, name, result, textEnd, tok, v10, v11, v12, v13, v3, v4, v5, v6, v7, v8, v9
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd /* save one level of indirection */
	for {
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc + 32))})))(tls, enc, ptr, end, bp)
		switch tok {
		case -int32(4):
			return int32(_XML_ERROR_NONE)
		case m_XML_TOK_INVALID:
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = *(*uintptr)(unsafe.Pointer(bp))
			}
			return int32(_XML_ERROR_INVALID_TOKEN)
		case -int32(1):
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = ptr
			}
			return int32(_XML_ERROR_INVALID_TOKEN)
		case int32(m_XML_TOK_CHAR_REF):
			n = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FcharRefNumber})))(tls, enc, ptr)
			if n < 0 {
				if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = ptr
				}
				return int32(_XML_ERROR_BAD_CHAR_REF)
			}
			if !(isCdata != 0) && n == int32(0x20) && (int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart) == 0 || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr + uintptr(-libc.Int32FromInt32(1))))) == int32(0x20)) {
				break
			}
			n = XXmlUtf8Encode(tls, n, bp+8)
			/* The XmlEncode() functions can never return 0 here.  That
			 * error return happens if the code point passed in is either
			 * negative or greater than or equal to 0x110000.  The
			 * XmlCharRefNumber() functions will all return a number
			 * strictly less than 0x110000 or a negative value if an error
			 * occurred.  The negative value is intercepted above, so
			 * XmlEncode() is never passed a value it might return an
			 * error for.
			 */
			i = 0
			for {
				if !(i < n) {
					break
				}
				if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
					v3 = 0
				} else {
					v5 = pool + 24
					v4 = *(*uintptr)(unsafe.Pointer(v5))
					*(*uintptr)(unsafe.Pointer(v5))++
					*(*TXML_Char)(unsafe.Pointer(v4)) = (*(*[4]TXML_Char)(unsafe.Pointer(bp + 8)))[i]
					v3 = libc.Int32FromInt32(1)
				}
				if !(v3 != 0) {
					return int32(_XML_ERROR_NO_MEMORY)
				}
				goto _2
			_2:
				;
				i++
			}
		case int32(m_XML_TOK_DATA_CHARS):
			if !(_poolAppend(tls, pool, enc, ptr, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		case -int32(3):
			*(*uintptr)(unsafe.Pointer(bp)) = ptr + uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
			/* fall through */
			fallthrough
		case int32(m_XML_TOK_ATTRIBUTE_VALUE_S):
			fallthrough
		case int32(m_XML_TOK_DATA_NEWLINE):
			if !(isCdata != 0) && (int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart) == 0 || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr + uintptr(-libc.Int32FromInt32(1))))) == int32(0x20)) {
				break
			}
			if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
				v6 = 0
			} else {
				v8 = pool + 24
				v7 = *(*uintptr)(unsafe.Pointer(v8))
				*(*uintptr)(unsafe.Pointer(v8))++
				*(*TXML_Char)(unsafe.Pointer(v7)) = uint8(0x20)
				v6 = libc.Int32FromInt32(1)
			}
			if !(v6 != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
		case int32(m_XML_TOK_ENTITY_REF):
			ch = libc.Uint8FromInt32((*(*func(*libc.TLS, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FpredefinedEntityName})))(tls, enc, ptr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)))
			if ch != 0 {
				if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
					v9 = 0
				} else {
					v11 = pool + 24
					v10 = *(*uintptr)(unsafe.Pointer(v11))
					*(*uintptr)(unsafe.Pointer(v11))++
					*(*TXML_Char)(unsafe.Pointer(v10)) = ch
					v9 = libc.Int32FromInt32(1)
				}
				if !(v9 != 0) {
					return int32(_XML_ERROR_NO_MEMORY)
				}
				break
			}
			name = _poolStoreString(tls, parser+832, enc, ptr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
			if !(name != 0) {
				return int32(_XML_ERROR_NO_MEMORY)
			}
			entity = _lookup(tls, parser, dtd, name, uint64(0))
			(*TSTRING_POOL)(unsafe.Pointer(parser + 832)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(parser + 832)).Fstart
			/* First, determine if a check for an existing declaration is needed;
			   if yes, check that the entity exists, and that it is internal.
			*/
			if pool == dtd+160 { /* are we called from prolog? */
				if v13 = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FdocumentEntity != 0; v13 {
					if (*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0 {
						v12 = libc.BoolInt32(!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities != 0))
					} else {
						v12 = libc.BoolInt32(!((*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs != 0))
					}
				}
				checkEntityDecl = libc.BoolUint8(v13 && v12 != 0)
			} else { /* if (pool == &parser->m_tempPool): we are called from content */
				checkEntityDecl = libc.BoolUint8(!((*TDTD)(unsafe.Pointer(dtd)).FhasParamEntityRefs != 0) || (*TDTD)(unsafe.Pointer(dtd)).Fstandalone != 0)
			}
			if checkEntityDecl != 0 {
				if !(entity != 0) {
					return int32(_XML_ERROR_UNDEFINED_ENTITY)
				} else {
					if !((*TENTITY)(unsafe.Pointer(entity)).Fis_internal != 0) {
						return int32(_XML_ERROR_ENTITY_DECLARED_IN_PE)
					}
				}
			} else {
				if !(entity != 0) {
					/* Cannot report skipped entity here - see comments on
					      parser->m_skippedEntityHandler.
					   if (parser->m_skippedEntityHandler)
					     parser->m_skippedEntityHandler(parser->m_handlerArg, name, 0);
					*/
					/* Cannot call the default handler because this would be
					      out of sync with the call to the startElementHandler.
					   if ((pool == &parser->m_tempPool) && parser->m_defaultHandler)
					     reportDefault(parser, enc, ptr, next);
					*/
					break
				}
			}
			if (*TENTITY)(unsafe.Pointer(entity)).Fopen != 0 {
				if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
					/* It does not appear that this line can be executed.
					 *
					 * The "if (entity->open)" check catches recursive entity
					 * definitions.  In order to be called with an open
					 * entity, it must have gone through this code before and
					 * been through the recursive call to
					 * appendAttributeValue() some lines below.  That call
					 * sets the local encoding ("enc") to the parser's
					 * internal encoding (internal_utf8 or internal_utf16),
					 * which can never be the same as the principle encoding.
					 * It doesn't appear there is another code path that gets
					 * here with entity->open being TRUE.
					 *
					 * Since it is not certain that this logic is watertight,
					 * we keep the line and merely exclude it from coverage
					 * tests.
					 */
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = ptr /* LCOV_EXCL_LINE */
				}
				return int32(_XML_ERROR_RECURSIVE_ENTITY_REF)
			}
			if (*TENTITY)(unsafe.Pointer(entity)).Fnotation != 0 {
				if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = ptr
				}
				return int32(_XML_ERROR_BINARY_ENTITY_REF)
			}
			if !((*TENTITY)(unsafe.Pointer(entity)).FtextPtr != 0) {
				if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = ptr
				}
				return int32(_XML_ERROR_ATTRIBUTE_EXTERNAL_ENTITY_REF)
			} else {
				textEnd = (*TENTITY)(unsafe.Pointer(entity)).FtextPtr + uintptr((*TENTITY)(unsafe.Pointer(entity)).FtextLen)
				(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(1)
				result = _appendAttributeValue(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, isCdata, (*TENTITY)(unsafe.Pointer(entity)).FtextPtr, textEnd, pool)
				(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
				if result != 0 {
					return result
				}
			}
		default:
			/* The only token returned by XmlAttributeValueTok() that does
			 * not have an explicit case here is XML_TOK_PARTIAL_CHAR.
			 * Getting that would require an entity name to contain an
			 * incomplete XML character (e.g. \xE2\x82); however previous
			 * tokenisers will have already recognised and rejected such
			 * names before XmlAttributeValueTok() gets a look-in.  This
			 * default case should be retained as a safety net, but the code
			 * excluded from coverage tests.
			 *
			 * LCOV_EXCL_START
			 */
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = ptr
			}
			return int32(_XML_ERROR_UNEXPECTED_STATE)
			/* LCOV_EXCL_STOP */
		}
		ptr = *(*uintptr)(unsafe.Pointer(bp))
		goto _1
	_1:
	}
	/* not reached */
	return r
}

func _storeEntityValue(tls *libc.TLS, parser TXML_Parser, enc uintptr, entityTextPtr uintptr, entityTextEnd uintptr) (r _XML_Error) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var dtd, entity, name, pool, v2, v3, v5, v6 uintptr
	var i, n, oldInEntityValue, tok int32
	var result _XML_Error
	var _ /* buf at bp+8 */ [4]TXML_Char
	var _ /* next at bp+0 */ uintptr
	_, _, _, _, _, _, _, _, _, _, _, _, _ = dtd, entity, i, n, name, oldInEntityValue, pool, result, tok, v2, v3, v5, v6
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd /* save one level of indirection */
	pool = dtd + 208
	result = int32(_XML_ERROR_NONE)
	oldInEntityValue = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FinEntityValue
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FinEntityValue = int32(1)
	/* never return Null for the value argument in EntityDeclHandler,
	   since this would indicate an external entity; therefore we
	   have to make sure that entityValuePool.start is not null */
	if !((*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks != 0) {
		if !(_poolGrow(tls, pool) != 0) {
			return int32(_XML_ERROR_NO_MEMORY)
		}
	}
	for {
		tok = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(enc + 32 + 1*8))})))(tls, enc, entityTextPtr, entityTextEnd, bp)
		switch tok {
		case int32(m_XML_TOK_PARAM_ENTITY_REF):
			if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_isParamEntity != 0 || enc != (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				name = _poolStoreString(tls, parser+784, enc, entityTextPtr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
				if !(name != 0) {
					result = int32(_XML_ERROR_NO_MEMORY)
					goto endEntityValue
				}
				entity = _lookup(tls, parser, dtd+264, name, uint64(0))
				(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
				if !(entity != 0) {
					/* not a well-formedness error - see XML 1.0: WFC Entity Declared */
					/* cannot report skipped entity here - see comments on
					      parser->m_skippedEntityHandler
					   if (parser->m_skippedEntityHandler)
					     parser->m_skippedEntityHandler(parser->m_handlerArg, name, 0);
					*/
					(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
					goto endEntityValue
				}
				if (*TENTITY)(unsafe.Pointer(entity)).Fopen != 0 {
					if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
						(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = entityTextPtr
					}
					result = int32(_XML_ERROR_RECURSIVE_ENTITY_REF)
					goto endEntityValue
				}
				if (*TENTITY)(unsafe.Pointer(entity)).FsystemId != 0 {
					if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler != 0 {
						(*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead = libc.Uint8FromInt32(0)
						(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(1)
						if !((*(*func(*libc.TLS, TXML_Parser, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_externalEntityRefHandlerArg, uintptr(0), (*TENTITY)(unsafe.Pointer(entity)).Fbase, (*TENTITY)(unsafe.Pointer(entity)).FsystemId, (*TENTITY)(unsafe.Pointer(entity)).FpublicId) != 0) {
							(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
							result = int32(_XML_ERROR_EXTERNAL_ENTITY_HANDLING)
							goto endEntityValue
						}
						(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
						if !((*TDTD)(unsafe.Pointer(dtd)).FparamEntityRead != 0) {
							(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
						}
					} else {
						(*TDTD)(unsafe.Pointer(dtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(dtd)).Fstandalone
					}
				} else {
					(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(1)
					result = _storeEntityValue(tls, parser, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_internalEncoding, (*TENTITY)(unsafe.Pointer(entity)).FtextPtr, (*TENTITY)(unsafe.Pointer(entity)).FtextPtr+uintptr((*TENTITY)(unsafe.Pointer(entity)).FtextLen))
					(*TENTITY)(unsafe.Pointer(entity)).Fopen = libc.Uint8FromInt32(0)
					if result != 0 {
						goto endEntityValue
					}
				}
				break
			}
			/* In the internal subset, PE references are not legal
			   within markup declarations, e.g entity values in this case. */
			(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = entityTextPtr
			result = int32(_XML_ERROR_PARAM_ENTITY_REF)
			goto endEntityValue
		case -int32(4):
			result = int32(_XML_ERROR_NONE)
			goto endEntityValue
		case int32(m_XML_TOK_ENTITY_REF):
			fallthrough
		case int32(m_XML_TOK_DATA_CHARS):
			if !(_poolAppend(tls, pool, enc, entityTextPtr, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				result = int32(_XML_ERROR_NO_MEMORY)
				goto endEntityValue
			}
		case -int32(3):
			*(*uintptr)(unsafe.Pointer(bp)) = entityTextPtr + uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
			/* fall through */
			fallthrough
		case int32(m_XML_TOK_DATA_NEWLINE):
			if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr && !(_poolGrow(tls, pool) != 0) {
				result = int32(_XML_ERROR_NO_MEMORY)
				goto endEntityValue
			}
			v3 = pool + 24
			v2 = *(*uintptr)(unsafe.Pointer(v3))
			*(*uintptr)(unsafe.Pointer(v3))++
			*(*TXML_Char)(unsafe.Pointer(v2)) = uint8(0xA)
		case int32(m_XML_TOK_CHAR_REF):
			n = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FcharRefNumber})))(tls, enc, entityTextPtr)
			if n < 0 {
				if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
					(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = entityTextPtr
				}
				result = int32(_XML_ERROR_BAD_CHAR_REF)
				goto endEntityValue
			}
			n = XXmlUtf8Encode(tls, n, bp+8)
			/* The XmlEncode() functions can never return 0 here.  That
			 * error return happens if the code point passed in is either
			 * negative or greater than or equal to 0x110000.  The
			 * XmlCharRefNumber() functions will all return a number
			 * strictly less than 0x110000 or a negative value if an error
			 * occurred.  The negative value is intercepted above, so
			 * XmlEncode() is never passed a value it might return an
			 * error for.
			 */
			i = 0
			for {
				if !(i < n) {
					break
				}
				if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr && !(_poolGrow(tls, pool) != 0) {
					result = int32(_XML_ERROR_NO_MEMORY)
					goto endEntityValue
				}
				v6 = pool + 24
				v5 = *(*uintptr)(unsafe.Pointer(v6))
				*(*uintptr)(unsafe.Pointer(v6))++
				*(*TXML_Char)(unsafe.Pointer(v5)) = (*(*[4]TXML_Char)(unsafe.Pointer(bp + 8)))[i]
				goto _4
			_4:
				;
				i++
			}
		case -int32(1):
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = entityTextPtr
			}
			result = int32(_XML_ERROR_INVALID_TOKEN)
			goto endEntityValue
		case m_XML_TOK_INVALID:
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = *(*uintptr)(unsafe.Pointer(bp))
			}
			result = int32(_XML_ERROR_INVALID_TOKEN)
			goto endEntityValue
		default:
			/* This default case should be unnecessary -- all the tokens
			 * that XmlEntityValueTok() can return have their own explicit
			 * cases -- but should be retained for safety.  We do however
			 * exclude it from the coverage statistics.
			 *
			 * LCOV_EXCL_START
			 */
			if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
				(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_eventPtr = entityTextPtr
			}
			result = int32(_XML_ERROR_UNEXPECTED_STATE)
			goto endEntityValue
			/* LCOV_EXCL_STOP */
		}
		entityTextPtr = *(*uintptr)(unsafe.Pointer(bp))
		goto _1
	_1:
	}
	goto endEntityValue
endEntityValue:
	;
	(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_prologState.FinEntityValue = oldInEntityValue
	return result
}

func _normalizeLines(tls *libc.TLS, s uintptr) {
	var p, v2, v3, v4, v5 uintptr
	_, _, _, _, _ = p, v2, v3, v4, v5
	for {
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) == int32('\000') {
			return
		}
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) == int32(0xD) {
			break
		}
		goto _1
	_1:
		;
		s++
	}
	p = s
	for cond := true; cond; cond = *(*TXML_Char)(unsafe.Pointer(s)) != 0 {
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) == int32(0xD) {
			v2 = p
			p++
			*(*TXML_Char)(unsafe.Pointer(v2)) = uint8(0xA)
			s++
			v3 = s
			if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(v3))) == int32(0xA) {
				s++
			}
		} else {
			v4 = p
			p++
			v5 = s
			s++
			*(*TXML_Char)(unsafe.Pointer(v4)) = *(*TXML_Char)(unsafe.Pointer(v5))
		}
	}
	*(*TXML_Char)(unsafe.Pointer(p)) = uint8('\000')
}

func _reportProcessingInstruction(tls *libc.TLS, parser TXML_Parser, enc uintptr, start uintptr, end uintptr) (r int32) {
	var data, target, tem uintptr
	_, _, _ = data, target, tem
	if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processingInstructionHandler != 0) {
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
			_reportDefault(tls, parser, enc, start, end)
		}
		return int32(1)
	}
	start += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar * int32(2))
	tem = start + uintptr((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameLength})))(tls, enc, start))
	target = _poolStoreString(tls, parser+784, enc, start, tem)
	if !(target != 0) {
		return 0
	}
	(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr
	data = _poolStoreString(tls, parser+784, enc, (*(*func(*libc.TLS, uintptr, uintptr) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FskipS})))(tls, enc, tem), end-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar*int32(2)))
	if !(data != 0) {
		return 0
	}
	_normalizeLines(tls, data)
	(*(*func(*libc.TLS, uintptr, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_processingInstructionHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, target, data)
	_poolClear(tls, parser+784)
	return int32(1)
}

func _reportComment(tls *libc.TLS, parser TXML_Parser, enc uintptr, start uintptr, end uintptr) (r int32) {
	var data uintptr
	_ = data
	if !((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_commentHandler != 0) {
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler != 0 {
			_reportDefault(tls, parser, enc, start, end)
		}
		return int32(1)
	}
	data = _poolStoreString(tls, parser+784, enc, start+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar*int32(4)), end-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar*int32(3)))
	if !(data != 0) {
		return 0
	}
	_normalizeLines(tls, data)
	(*(*func(*libc.TLS, uintptr, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_commentHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, data)
	_poolClear(tls, parser+784)
	return int32(1)
}

func _reportDefault(tls *libc.TLS, parser TXML_Parser, enc uintptr, _s uintptr, end uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _s
	var convert_res _XML_Convert_Result
	var eventEndPP, eventPP uintptr
	var _ /* dataPtr at bp+8 */ uintptr
	_, _, _ = convert_res, eventEndPP, eventPP
	if !((*TENCODING)(unsafe.Pointer(enc)).FisUtf8 != 0) {
		if enc == (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_encoding {
			eventPP = parser + 544
			eventEndPP = parser + 552
		} else {
			/* To get here, two things must be true; the parser must be
			 * using a character encoding that is not the same as the
			 * encoding passed in, and the encoding passed in must need
			 * conversion to the internal format (UTF-8 unless XML_UNICODE
			 * is defined).  The only occasions on which the encoding passed
			 * in is not the same as the parser's encoding are when it is
			 * the internal encoding (e.g. a previously defined parameter
			 * entity, already converted to internal format).  This by
			 * definition doesn't need conversion, so the whole branch never
			 * gets executed.
			 *
			 * For safety's sake we don't delete these lines and merely
			 * exclude them from coverage statistics.
			 *
			 * LCOV_EXCL_START
			 */
			eventPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities
			eventEndPP = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_openInternalEntities + 8
			/* LCOV_EXCL_STOP */
		}
		for cond := true; cond; cond = convert_res != int32(_XML_CONVERT_COMPLETED) && convert_res != int32(_XML_CONVERT_INPUT_INCOMPLETE) {
			*(*uintptr)(unsafe.Pointer(bp + 8)) = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf
			convert_res = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, end, bp+8, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBufEnd)
			*(*uintptr)(unsafe.Pointer(eventEndPP)) = *(*uintptr)(unsafe.Pointer(bp))
			(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf, int32(int64(*(*uintptr)(unsafe.Pointer(bp + 8)))-int64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dataBuf)))
			*(*uintptr)(unsafe.Pointer(eventPP)) = *(*uintptr)(unsafe.Pointer(bp))
		}
	} else {
		(*(*func(*libc.TLS, uintptr, uintptr, int32))(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_defaultHandler})))(tls, (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_handlerArg, *(*uintptr)(unsafe.Pointer(bp)), int32(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp)))))
	}
}

func _defineAttribute(tls *libc.TLS, type1 uintptr, attId uintptr, isCdata TXML_Bool, isId TXML_Bool, value uintptr, parser TXML_Parser) (r int32) {
	var att, temp uintptr
	var count, i int32
	_, _, _, _ = att, count, i, temp
	if value != 0 || isId != 0 {
		i = 0
		for {
			if !(i < (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FnDefaultAtts) {
				break
			}
			if attId == (*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(type1)).FdefaultAtts + uintptr(i)*24))).Fid {
				return int32(1)
			}
			goto _1
		_1:
			;
			i++
		}
		if isId != 0 && !((*TELEMENT_TYPE)(unsafe.Pointer(type1)).FidAtt != 0) && !((*TATTRIBUTE_ID)(unsafe.Pointer(attId)).Fxmlns != 0) {
			(*TELEMENT_TYPE)(unsafe.Pointer(type1)).FidAtt = attId
		}
	}
	if (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FnDefaultAtts == (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts {
		if (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts == 0 {
			(*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts = int32(8)
			(*TELEMENT_TYPE)(unsafe.Pointer(type1)).FdefaultAtts = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32((*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts)*libc.Uint64FromInt64(24))
			if !((*TELEMENT_TYPE)(unsafe.Pointer(type1)).FdefaultAtts != 0) {
				(*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts = 0
				return 0
			}
		} else {
			count = (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts * int32(2)
			temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FdefaultAtts, libc.Uint64FromInt32(count)*libc.Uint64FromInt64(24))
			if temp == libc.UintptrFromInt32(0) {
				return 0
			}
			(*TELEMENT_TYPE)(unsafe.Pointer(type1)).FallocDefaultAtts = count
			(*TELEMENT_TYPE)(unsafe.Pointer(type1)).FdefaultAtts = temp
		}
	}
	att = (*TELEMENT_TYPE)(unsafe.Pointer(type1)).FdefaultAtts + uintptr((*TELEMENT_TYPE)(unsafe.Pointer(type1)).FnDefaultAtts)*24
	(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(att)).Fid = attId
	(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(att)).Fvalue = value
	(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer(att)).FisCdata = isCdata
	if !(isCdata != 0) {
		(*TATTRIBUTE_ID)(unsafe.Pointer(attId)).FmaybeTokenized = libc.Uint8FromInt32(1)
	}
	*(*int32)(unsafe.Pointer(type1 + 24)) += int32(1)
	return int32(1)
}

func _setElementTypePrefix(tls *libc.TLS, parser TXML_Parser, elementType uintptr) (r int32) {
	var dtd, name, prefix, s, v4, v5, v7, v8 uintptr
	var v3, v6 int32
	_, _, _, _, _, _, _, _, _, _ = dtd, name, prefix, s, v3, v4, v5, v6, v7, v8
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	name = (*TELEMENT_TYPE)(unsafe.Pointer(elementType)).Fname
	for {
		if !(*(*TXML_Char)(unsafe.Pointer(name)) != 0) {
			break
		}
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name))) == int32(m_ASCII_COLON) {
			s = (*TELEMENT_TYPE)(unsafe.Pointer(elementType)).Fname
			for {
				if !(s != name) {
					break
				}
				if (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fend && !(_poolGrow(tls, dtd+160) != 0) {
					v3 = 0
				} else {
					v5 = dtd + 160 + 24
					v4 = *(*uintptr)(unsafe.Pointer(v5))
					*(*uintptr)(unsafe.Pointer(v5))++
					*(*TXML_Char)(unsafe.Pointer(v4)) = *(*TXML_Char)(unsafe.Pointer(s))
					v3 = libc.Int32FromInt32(1)
				}
				if !(v3 != 0) {
					return 0
				}
				goto _2
			_2:
				;
				s++
			}
			if (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fend && !(_poolGrow(tls, dtd+160) != 0) {
				v6 = 0
			} else {
				v8 = dtd + 160 + 24
				v7 = *(*uintptr)(unsafe.Pointer(v8))
				*(*uintptr)(unsafe.Pointer(v8))++
				*(*TXML_Char)(unsafe.Pointer(v7)) = uint8('\000')
				v6 = libc.Int32FromInt32(1)
			}
			if !(v6 != 0) {
				return 0
			}
			prefix = _lookup(tls, parser, dtd+120, (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fstart, uint64(16))
			if !(prefix != 0) {
				return 0
			}
			if (*TPREFIX)(unsafe.Pointer(prefix)).Fname == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fstart {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
			} else {
				(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
			}
			(*TELEMENT_TYPE)(unsafe.Pointer(elementType)).Fprefix = prefix
			break
		}
		goto _1
	_1:
		;
		name++
	}
	return int32(1)
}

func _getAttributeId(tls *libc.TLS, parser TXML_Parser, enc uintptr, start uintptr, end uintptr) (r uintptr) {
	var dtd, id, name, v10, v11, v2, v3, v7, v8 uintptr
	var i, j, v1, v6, v9 int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _ = dtd, i, id, j, name, v1, v10, v11, v2, v3, v6, v7, v8, v9
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	if (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fend && !(_poolGrow(tls, dtd+160) != 0) {
		v1 = 0
	} else {
		v3 = dtd + 160 + 24
		v2 = *(*uintptr)(unsafe.Pointer(v3))
		*(*uintptr)(unsafe.Pointer(v3))++
		*(*TXML_Char)(unsafe.Pointer(v2)) = uint8('\000')
		v1 = libc.Int32FromInt32(1)
	}
	if !(v1 != 0) {
		return libc.UintptrFromInt32(0)
	}
	name = _poolStoreString(tls, dtd+160, enc, start, end)
	if !(name != 0) {
		return libc.UintptrFromInt32(0)
	}
	/* skip quotation mark - its storage will be re-used (like in name[-1]) */
	name++
	id = _lookup(tls, parser, dtd+80, name, uint64(24))
	if !(id != 0) {
		return libc.UintptrFromInt32(0)
	}
	if (*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fname != name {
		(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
	} else {
		(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
		if !!((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_ns != 0) {
			if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name))) == int32(m_ASCII_x) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 1))) == int32(m_ASCII_m) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 2))) == int32(m_ASCII_l) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 3))) == int32(m_ASCII_n) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 4))) == int32(m_ASCII_s) && (libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 5))) == int32('\000') || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 5))) == int32(m_ASCII_COLON)) {
				if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + 5))) == int32('\000') {
					(*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix = dtd + 304
				} else {
					(*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix = _lookup(tls, parser, dtd+120, name+uintptr(6), uint64(16))
				}
				(*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fxmlns = libc.Uint8FromInt32(1)
			} else {
				i = 0
				for {
					if !(*(*TXML_Char)(unsafe.Pointer(name + uintptr(i))) != 0) {
						break
					}
					/* attributes without prefix are *not* in the default namespace */
					if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(name + uintptr(i)))) == int32(m_ASCII_COLON) {
						j = 0
						for {
							if !(j < i) {
								break
							}
							if (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fend && !(_poolGrow(tls, dtd+160) != 0) {
								v6 = 0
							} else {
								v8 = dtd + 160 + 24
								v7 = *(*uintptr)(unsafe.Pointer(v8))
								*(*uintptr)(unsafe.Pointer(v8))++
								*(*TXML_Char)(unsafe.Pointer(v7)) = *(*TXML_Char)(unsafe.Pointer(name + uintptr(j)))
								v6 = libc.Int32FromInt32(1)
							}
							if !(v6 != 0) {
								return libc.UintptrFromInt32(0)
							}
							goto _5
						_5:
							;
							j++
						}
						if (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fend && !(_poolGrow(tls, dtd+160) != 0) {
							v9 = 0
						} else {
							v11 = dtd + 160 + 24
							v10 = *(*uintptr)(unsafe.Pointer(v11))
							*(*uintptr)(unsafe.Pointer(v11))++
							*(*TXML_Char)(unsafe.Pointer(v10)) = uint8('\000')
							v9 = libc.Int32FromInt32(1)
						}
						if !(v9 != 0) {
							return libc.UintptrFromInt32(0)
						}
						(*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix = _lookup(tls, parser, dtd+120, (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fstart, uint64(16))
						if !((*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix != 0) {
							return libc.UintptrFromInt32(0)
						}
						if (*TPREFIX)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer(id)).Fprefix)).Fname == (*TSTRING_POOL)(unsafe.Pointer(dtd+160)).Fstart {
							(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
						} else {
							(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
						}
						break
					}
					goto _4
				_4:
					;
					i++
				}
			}
		}
	}
	return id
}

func _getContext(tls *libc.TLS, parser TXML_Parser) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var dtd, e, prefix, s, s1, v10, v11, v15, v16, v18, v19, v2, v22, v23, v26, v27, v3, v31, v32, v34, v35, v6, v7 uintptr
	var i, i1, len1, len11, v1, v14, v17, v21, v25, v30, v33, v5, v9 int32
	var needSep TXML_Bool
	var v12, v28 bool
	var _ /* iter at bp+0 */ THASH_TABLE_ITER
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = dtd, e, i, i1, len1, len11, needSep, prefix, s, s1, v1, v10, v11, v12, v14, v15, v16, v17, v18, v19, v2, v21, v22, v23, v25, v26, v27, v28, v3, v30, v31, v32, v33, v34, v35, v5, v6, v7, v9
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	needSep = libc.Uint8FromInt32(0)
	if (*TDTD)(unsafe.Pointer(dtd)).FdefaultPrefix.Fbinding != 0 {
		if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
			v1 = 0
		} else {
			v3 = parser + 784 + 24
			v2 = *(*uintptr)(unsafe.Pointer(v3))
			*(*uintptr)(unsafe.Pointer(v3))++
			*(*TXML_Char)(unsafe.Pointer(v2)) = uint8(m_ASCII_EQUALS)
			v1 = libc.Int32FromInt32(1)
		}
		if !(v1 != 0) {
			return libc.UintptrFromInt32(0)
		}
		len1 = (*TBINDING)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FdefaultPrefix.Fbinding)).FuriLen
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator != 0 {
			len1--
		}
		i = 0
		for {
			if !(i < len1) {
				break
			}
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v5 = 0
			} else {
				v7 = parser + 784 + 24
				v6 = *(*uintptr)(unsafe.Pointer(v7))
				*(*uintptr)(unsafe.Pointer(v7))++
				*(*TXML_Char)(unsafe.Pointer(v6)) = *(*TXML_Char)(unsafe.Pointer((*TBINDING)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FdefaultPrefix.Fbinding)).Furi + uintptr(i)))
				v5 = libc.Int32FromInt32(1)
			}
			if !(v5 != 0) {
				/* Because of memory caching, I don't believe this line can be
				 * executed.
				 *
				 * This is part of a loop copying the default prefix binding
				 * URI into the parser's temporary string pool.  Previously,
				 * that URI was copied into the same string pool, with a
				 * terminating NUL character, as part of setContext().  When
				 * the pool was cleared, that leaves a block definitely big
				 * enough to hold the URI on the free block list of the pool.
				 * The URI copy in getContext() therefore cannot run out of
				 * memory.
				 *
				 * If the pool is used between the setContext() and
				 * getContext() calls, the worst it can do is leave a bigger
				 * block on the front of the free list.  Given that this is
				 * all somewhat inobvious and program logic can be changed, we
				 * don't delete the line but we do exclude it from the test
				 * coverage statistics.
				 */
				return libc.UintptrFromInt32(0) /* LCOV_EXCL_LINE */
			}
			goto _4
		_4:
			;
			i++
		}
		needSep = libc.Uint8FromInt32(1)
	}
	_hashTableIterInit(tls, bp, dtd+120)
	for {
		prefix = _hashTableIterNext(tls, bp)
		if !(prefix != 0) {
			break
		}
		if !((*TPREFIX)(unsafe.Pointer(prefix)).Fbinding != 0) {
			/* This test appears to be (justifiable) paranoia.  There does
			 * not seem to be a way of injecting a prefix without a binding
			 * that doesn't get errored long before this function is called.
			 * The test should remain for safety's sake, so we instead
			 * exclude the following line from the coverage statistics.
			 */
			goto _8 /* LCOV_EXCL_LINE */
		}
		if v12 = needSep != 0; v12 {
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v9 = 0
			} else {
				v11 = parser + 784 + 24
				v10 = *(*uintptr)(unsafe.Pointer(v11))
				*(*uintptr)(unsafe.Pointer(v11))++
				*(*TXML_Char)(unsafe.Pointer(v10)) = uint8(m_ASCII_FF)
				v9 = libc.Int32FromInt32(1)
			}
		}
		if v12 && !(v9 != 0) {
			return libc.UintptrFromInt32(0)
		}
		s = (*TPREFIX)(unsafe.Pointer(prefix)).Fname
		for {
			if !(*(*TXML_Char)(unsafe.Pointer(s)) != 0) {
				break
			}
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v14 = 0
			} else {
				v16 = parser + 784 + 24
				v15 = *(*uintptr)(unsafe.Pointer(v16))
				*(*uintptr)(unsafe.Pointer(v16))++
				*(*TXML_Char)(unsafe.Pointer(v15)) = *(*TXML_Char)(unsafe.Pointer(s))
				v14 = libc.Int32FromInt32(1)
			}
			if !(v14 != 0) {
				return libc.UintptrFromInt32(0)
			}
			goto _13
		_13:
			;
			s++
		}
		if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
			v17 = 0
		} else {
			v19 = parser + 784 + 24
			v18 = *(*uintptr)(unsafe.Pointer(v19))
			*(*uintptr)(unsafe.Pointer(v19))++
			*(*TXML_Char)(unsafe.Pointer(v18)) = uint8(m_ASCII_EQUALS)
			v17 = libc.Int32FromInt32(1)
		}
		if !(v17 != 0) {
			return libc.UintptrFromInt32(0)
		}
		len11 = (*TBINDING)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fbinding)).FuriLen
		if (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_namespaceSeparator != 0 {
			len11--
		}
		i1 = 0
		for {
			if !(i1 < len11) {
				break
			}
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v21 = 0
			} else {
				v23 = parser + 784 + 24
				v22 = *(*uintptr)(unsafe.Pointer(v23))
				*(*uintptr)(unsafe.Pointer(v23))++
				*(*TXML_Char)(unsafe.Pointer(v22)) = *(*TXML_Char)(unsafe.Pointer((*TBINDING)(unsafe.Pointer((*TPREFIX)(unsafe.Pointer(prefix)).Fbinding)).Furi + uintptr(i1)))
				v21 = libc.Int32FromInt32(1)
			}
			if !(v21 != 0) {
				return libc.UintptrFromInt32(0)
			}
			goto _20
		_20:
			;
			i1++
		}
		needSep = libc.Uint8FromInt32(1)
		goto _8
	_8:
	}
	_hashTableIterInit(tls, bp, dtd)
	for {
		e = _hashTableIterNext(tls, bp)
		if !(e != 0) {
			break
		}
		if !((*TENTITY)(unsafe.Pointer(e)).Fopen != 0) {
			goto _24
		}
		if v28 = needSep != 0; v28 {
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v25 = 0
			} else {
				v27 = parser + 784 + 24
				v26 = *(*uintptr)(unsafe.Pointer(v27))
				*(*uintptr)(unsafe.Pointer(v27))++
				*(*TXML_Char)(unsafe.Pointer(v26)) = uint8(m_ASCII_FF)
				v25 = libc.Int32FromInt32(1)
			}
		}
		if v28 && !(v25 != 0) {
			return libc.UintptrFromInt32(0)
		}
		s1 = (*TENTITY)(unsafe.Pointer(e)).Fname
		for {
			if !(*(*TXML_Char)(unsafe.Pointer(s1)) != 0) {
				break
			}
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v30 = 0
			} else {
				v32 = parser + 784 + 24
				v31 = *(*uintptr)(unsafe.Pointer(v32))
				*(*uintptr)(unsafe.Pointer(v32))++
				*(*TXML_Char)(unsafe.Pointer(v31)) = *(*TXML_Char)(unsafe.Pointer(s1))
				v30 = libc.Int32FromInt32(1)
			}
			if !(v30 != 0) {
				return uintptr(0)
			}
			goto _29
		_29:
			;
			s1++
		}
		needSep = libc.Uint8FromInt32(1)
		goto _24
	_24:
	}
	if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
		v33 = 0
	} else {
		v35 = parser + 784 + 24
		v34 = *(*uintptr)(unsafe.Pointer(v35))
		*(*uintptr)(unsafe.Pointer(v35))++
		*(*TXML_Char)(unsafe.Pointer(v34)) = uint8('\000')
		v33 = libc.Int32FromInt32(1)
	}
	if !(v33 != 0) {
		return libc.UintptrFromInt32(0)
	}
	return (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_tempPool.Fstart
}

func _setContext(tls *libc.TLS, parser TXML_Parser, context uintptr) (r TXML_Bool) {
	var dtd, e, prefix, s, v10, v12, v13, v15, v16, v2, v3, v5, v6, v9 uintptr
	var v1, v11, v14, v4, v8 int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = dtd, e, prefix, s, v1, v10, v11, v12, v13, v14, v15, v16, v2, v3, v4, v5, v6, v8, v9
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd /* save one level of indirection */
	s = context
	for libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(context))) != int32('\000') {
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) == int32(m_ASCII_FF) || libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) == int32('\000') {
			if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
				v1 = 0
			} else {
				v3 = parser + 784 + 24
				v2 = *(*uintptr)(unsafe.Pointer(v3))
				*(*uintptr)(unsafe.Pointer(v3))++
				*(*TXML_Char)(unsafe.Pointer(v2)) = uint8('\000')
				v1 = libc.Int32FromInt32(1)
			}
			if !(v1 != 0) {
				return libc.Uint8FromInt32(0)
			}
			e = _lookup(tls, parser, dtd, (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fstart, uint64(0))
			if e != 0 {
				(*TENTITY)(unsafe.Pointer(e)).Fopen = libc.Uint8FromInt32(1)
			}
			if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) != int32('\000') {
				s++
			}
			context = s
			(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
		} else {
			if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) == int32(m_ASCII_EQUALS) {
				if int64((*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fstart) == 0 {
					prefix = dtd + 304
				} else {
					if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
						v4 = 0
					} else {
						v6 = parser + 784 + 24
						v5 = *(*uintptr)(unsafe.Pointer(v6))
						*(*uintptr)(unsafe.Pointer(v6))++
						*(*TXML_Char)(unsafe.Pointer(v5)) = uint8('\000')
						v4 = libc.Int32FromInt32(1)
					}
					if !(v4 != 0) {
						return libc.Uint8FromInt32(0)
					}
					prefix = _lookup(tls, parser, dtd+120, (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fstart, uint64(16))
					if !(prefix != 0) {
						return libc.Uint8FromInt32(0)
					}
					if (*TPREFIX)(unsafe.Pointer(prefix)).Fname == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fstart {
						(*TPREFIX)(unsafe.Pointer(prefix)).Fname = _poolCopyString(tls, dtd+160, (*TPREFIX)(unsafe.Pointer(prefix)).Fname)
						if !((*TPREFIX)(unsafe.Pointer(prefix)).Fname != 0) {
							return libc.Uint8FromInt32(0)
						}
					}
					(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
				}
				context = s + uintptr(1)
				for {
					if !(libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(context))) != int32(m_ASCII_FF) && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(context))) != int32('\000')) {
						break
					}
					if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
						v8 = 0
					} else {
						v10 = parser + 784 + 24
						v9 = *(*uintptr)(unsafe.Pointer(v10))
						*(*uintptr)(unsafe.Pointer(v10))++
						*(*TXML_Char)(unsafe.Pointer(v9)) = *(*TXML_Char)(unsafe.Pointer(context))
						v8 = libc.Int32FromInt32(1)
					}
					if !(v8 != 0) {
						return libc.Uint8FromInt32(0)
					}
					goto _7
				_7:
					;
					context++
				}
				if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
					v11 = 0
				} else {
					v13 = parser + 784 + 24
					v12 = *(*uintptr)(unsafe.Pointer(v13))
					*(*uintptr)(unsafe.Pointer(v13))++
					*(*TXML_Char)(unsafe.Pointer(v12)) = uint8('\000')
					v11 = libc.Int32FromInt32(1)
				}
				if !(v11 != 0) {
					return libc.Uint8FromInt32(0)
				}
				if _addBinding(tls, parser, prefix, libc.UintptrFromInt32(0), (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fstart, parser+704) != int32(_XML_ERROR_NONE) {
					return libc.Uint8FromInt32(0)
				}
				(*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(parser + 784)).Fstart
				if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(context))) != int32('\000') {
					context++
				}
				s = context
			} else {
				if (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(parser+784)).Fend && !(_poolGrow(tls, parser+784) != 0) {
					v14 = 0
				} else {
					v16 = parser + 784 + 24
					v15 = *(*uintptr)(unsafe.Pointer(v16))
					*(*uintptr)(unsafe.Pointer(v16))++
					*(*TXML_Char)(unsafe.Pointer(v15)) = *(*TXML_Char)(unsafe.Pointer(s))
					v14 = libc.Int32FromInt32(1)
				}
				if !(v14 != 0) {
					return libc.Uint8FromInt32(0)
				}
				s++
			}
		}
	}
	return libc.Uint8FromInt32(1)
}

func _normalizePublicId(tls *libc.TLS, publicId uintptr) {
	var p, s, v2, v3 uintptr
	_, _, _, _ = p, s, v2, v3
	p = publicId
	s = publicId
	for {
		if !(*(*TXML_Char)(unsafe.Pointer(s)) != 0) {
			break
		}
		switch libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s))) {
		case int32(0x20):
			fallthrough
		case int32(0xD):
			fallthrough
		case int32(0xA):
			if p != publicId && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(p + uintptr(-libc.Int32FromInt32(1))))) != int32(0x20) {
				v2 = p
				p++
				*(*TXML_Char)(unsafe.Pointer(v2)) = uint8(0x20)
			}
		default:
			v3 = p
			p++
			*(*TXML_Char)(unsafe.Pointer(v3)) = *(*TXML_Char)(unsafe.Pointer(s))
		}
		goto _1
	_1:
		;
		s++
	}
	if p != publicId && libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(p + uintptr(-libc.Int32FromInt32(1))))) == int32(0x20) {
		p--
	}
	*(*TXML_Char)(unsafe.Pointer(p)) = uint8('\000')
}

func _dtdCreate(tls *libc.TLS, ms uintptr) (r uintptr) {
	var p uintptr
	_ = p
	p = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Fmalloc_fcn})))(tls, uint64(360))
	if p == libc.UintptrFromInt32(0) {
		return p
	}
	_poolInit(tls, p+160, ms)
	_poolInit(tls, p+208, ms)
	_hashTableInit(tls, p, ms)
	_hashTableInit(tls, p+40, ms)
	_hashTableInit(tls, p+80, ms)
	_hashTableInit(tls, p+120, ms)
	(*TDTD)(unsafe.Pointer(p)).FparamEntityRead = libc.Uint8FromInt32(0)
	_hashTableInit(tls, p+264, ms)
	(*TDTD)(unsafe.Pointer(p)).FdefaultPrefix.Fname = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).FdefaultPrefix.Fbinding = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).Fin_eldecl = libc.Uint8FromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).FscaffIndex = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).Fscaffold = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).FscaffLevel = 0
	(*TDTD)(unsafe.Pointer(p)).FscaffSize = uint32(0)
	(*TDTD)(unsafe.Pointer(p)).FscaffCount = uint32(0)
	(*TDTD)(unsafe.Pointer(p)).FcontentStringLen = uint32(0)
	(*TDTD)(unsafe.Pointer(p)).FkeepProcessing = libc.Uint8FromInt32(1)
	(*TDTD)(unsafe.Pointer(p)).FhasParamEntityRefs = libc.Uint8FromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).Fstandalone = libc.Uint8FromInt32(0)
	return p
}

func _dtdReset(tls *libc.TLS, p uintptr, ms uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var e uintptr
	var _ /* iter at bp+0 */ THASH_TABLE_ITER
	_ = e
	_hashTableIterInit(tls, bp, p+40)
	for {
		e = _hashTableIterNext(tls, bp)
		if !(e != 0) {
			break
		}
		if (*TELEMENT_TYPE)(unsafe.Pointer(e)).FallocDefaultAtts != 0 {
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, (*TELEMENT_TYPE)(unsafe.Pointer(e)).FdefaultAtts)
		}
		goto _1
	_1:
	}
	_hashTableClear(tls, p)
	(*TDTD)(unsafe.Pointer(p)).FparamEntityRead = libc.Uint8FromInt32(0)
	_hashTableClear(tls, p+264)
	_hashTableClear(tls, p+40)
	_hashTableClear(tls, p+80)
	_hashTableClear(tls, p+120)
	_poolClear(tls, p+160)
	_poolClear(tls, p+208)
	(*TDTD)(unsafe.Pointer(p)).FdefaultPrefix.Fname = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).FdefaultPrefix.Fbinding = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).Fin_eldecl = libc.Uint8FromInt32(0)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, (*TDTD)(unsafe.Pointer(p)).FscaffIndex)
	(*TDTD)(unsafe.Pointer(p)).FscaffIndex = libc.UintptrFromInt32(0)
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, (*TDTD)(unsafe.Pointer(p)).Fscaffold)
	(*TDTD)(unsafe.Pointer(p)).Fscaffold = libc.UintptrFromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).FscaffLevel = 0
	(*TDTD)(unsafe.Pointer(p)).FscaffSize = uint32(0)
	(*TDTD)(unsafe.Pointer(p)).FscaffCount = uint32(0)
	(*TDTD)(unsafe.Pointer(p)).FcontentStringLen = uint32(0)
	(*TDTD)(unsafe.Pointer(p)).FkeepProcessing = libc.Uint8FromInt32(1)
	(*TDTD)(unsafe.Pointer(p)).FhasParamEntityRefs = libc.Uint8FromInt32(0)
	(*TDTD)(unsafe.Pointer(p)).Fstandalone = libc.Uint8FromInt32(0)
}

func _dtdDestroy(tls *libc.TLS, p uintptr, isDocEntity TXML_Bool, ms uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var e uintptr
	var _ /* iter at bp+0 */ THASH_TABLE_ITER
	_ = e
	_hashTableIterInit(tls, bp, p+40)
	for {
		e = _hashTableIterNext(tls, bp)
		if !(e != 0) {
			break
		}
		if (*TELEMENT_TYPE)(unsafe.Pointer(e)).FallocDefaultAtts != 0 {
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, (*TELEMENT_TYPE)(unsafe.Pointer(e)).FdefaultAtts)
		}
		goto _1
	_1:
	}
	_hashTableDestroy(tls, p)
	_hashTableDestroy(tls, p+264)
	_hashTableDestroy(tls, p+40)
	_hashTableDestroy(tls, p+80)
	_hashTableDestroy(tls, p+120)
	_poolDestroy(tls, p+160)
	_poolDestroy(tls, p+208)
	if isDocEntity != 0 {
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, (*TDTD)(unsafe.Pointer(p)).FscaffIndex)
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, (*TDTD)(unsafe.Pointer(p)).Fscaffold)
	}
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Ffree_fcn})))(tls, p)
}

// C documentation
//
//	/* Do a deep copy of the DTD. Return 0 for out of memory, non-zero otherwise.
//	   The new DTD has already been initialized.
//	*/
func _dtdCopy(tls *libc.TLS, oldParser TXML_Parser, newDtd uintptr, oldDtd uintptr, ms uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i, v3, v7 int32
	var name, name1, name2, newA, newE, oldA, oldE, oldP, v4, v5 uintptr
	var _ /* iter at bp+0 */ THASH_TABLE_ITER
	_, _, _, _, _, _, _, _, _, _, _, _, _ = i, name, name1, name2, newA, newE, oldA, oldE, oldP, v3, v4, v5, v7
	/* Copy the prefix table. */
	_hashTableIterInit(tls, bp, oldDtd+120)
	for {
		oldP = _hashTableIterNext(tls, bp)
		if !(oldP != 0) {
			break
		}
		name = _poolCopyString(tls, newDtd+160, (*TPREFIX)(unsafe.Pointer(oldP)).Fname)
		if !(name != 0) {
			return 0
		}
		if !(_lookup(tls, oldParser, newDtd+120, name, uint64(16)) != 0) {
			return 0
		}
		goto _1
	_1:
	}
	_hashTableIterInit(tls, bp, oldDtd+80)
	/* Copy the attribute id table. */
	for {
		oldA = _hashTableIterNext(tls, bp)
		if !(oldA != 0) {
			break
		}
		/* Remember to allocate the scratch byte before the name. */
		if (*TSTRING_POOL)(unsafe.Pointer(newDtd+160)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(newDtd+160)).Fend && !(_poolGrow(tls, newDtd+160) != 0) {
			v3 = 0
		} else {
			v5 = newDtd + 160 + 24
			v4 = *(*uintptr)(unsafe.Pointer(v5))
			*(*uintptr)(unsafe.Pointer(v5))++
			*(*TXML_Char)(unsafe.Pointer(v4)) = uint8('\000')
			v3 = libc.Int32FromInt32(1)
		}
		if !(v3 != 0) {
			return 0
		}
		name1 = _poolCopyString(tls, newDtd+160, (*TATTRIBUTE_ID)(unsafe.Pointer(oldA)).Fname)
		if !(name1 != 0) {
			return 0
		}
		name1++
		newA = _lookup(tls, oldParser, newDtd+80, name1, uint64(24))
		if !(newA != 0) {
			return 0
		}
		(*TATTRIBUTE_ID)(unsafe.Pointer(newA)).FmaybeTokenized = (*TATTRIBUTE_ID)(unsafe.Pointer(oldA)).FmaybeTokenized
		if (*TATTRIBUTE_ID)(unsafe.Pointer(oldA)).Fprefix != 0 {
			(*TATTRIBUTE_ID)(unsafe.Pointer(newA)).Fxmlns = (*TATTRIBUTE_ID)(unsafe.Pointer(oldA)).Fxmlns
			if (*TATTRIBUTE_ID)(unsafe.Pointer(oldA)).Fprefix == oldDtd+304 {
				(*TATTRIBUTE_ID)(unsafe.Pointer(newA)).Fprefix = newDtd + 304
			} else {
				(*TATTRIBUTE_ID)(unsafe.Pointer(newA)).Fprefix = _lookup(tls, oldParser, newDtd+120, (*TPREFIX)(unsafe.Pointer((*TATTRIBUTE_ID)(unsafe.Pointer(oldA)).Fprefix)).Fname, uint64(0))
			}
		}
		goto _2
	_2:
	}
	/* Copy the element type table. */
	_hashTableIterInit(tls, bp, oldDtd+40)
	for {
		oldE = _hashTableIterNext(tls, bp)
		if !(oldE != 0) {
			break
		}
		name2 = _poolCopyString(tls, newDtd+160, (*TELEMENT_TYPE)(unsafe.Pointer(oldE)).Fname)
		if !(name2 != 0) {
			return 0
		}
		newE = _lookup(tls, oldParser, newDtd+40, name2, uint64(40))
		if !(newE != 0) {
			return 0
		}
		if (*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FnDefaultAtts != 0 {
			(*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(ms)).Fmalloc_fcn})))(tls, libc.Uint64FromInt32((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FnDefaultAtts)*uint64(24))
			if !((*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts != 0) {
				return 0
			}
		}
		if (*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FidAtt != 0 {
			(*TELEMENT_TYPE)(unsafe.Pointer(newE)).FidAtt = _lookup(tls, oldParser, newDtd+80, (*TATTRIBUTE_ID)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FidAtt)).Fname, uint64(0))
		}
		v7 = (*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FnDefaultAtts
		(*TELEMENT_TYPE)(unsafe.Pointer(newE)).FnDefaultAtts = v7
		(*TELEMENT_TYPE)(unsafe.Pointer(newE)).FallocDefaultAtts = v7
		if (*TELEMENT_TYPE)(unsafe.Pointer(oldE)).Fprefix != 0 {
			(*TELEMENT_TYPE)(unsafe.Pointer(newE)).Fprefix = _lookup(tls, oldParser, newDtd+120, (*TPREFIX)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).Fprefix)).Fname, uint64(0))
		}
		i = 0
		for {
			if !(i < (*TELEMENT_TYPE)(unsafe.Pointer(newE)).FnDefaultAtts) {
				break
			}
			(*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts + uintptr(i)*24))).Fid = _lookup(tls, oldParser, newDtd+80, (*TATTRIBUTE_ID)(unsafe.Pointer((*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FdefaultAtts + uintptr(i)*24))).Fid)).Fname, uint64(0))
			(*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts + uintptr(i)*24))).FisCdata = (*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FdefaultAtts + uintptr(i)*24))).FisCdata
			if (*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FdefaultAtts + uintptr(i)*24))).Fvalue != 0 {
				(*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts + uintptr(i)*24))).Fvalue = _poolCopyString(tls, newDtd+160, (*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(oldE)).FdefaultAtts + uintptr(i)*24))).Fvalue)
				if !((*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts + uintptr(i)*24))).Fvalue != 0) {
					return 0
				}
			} else {
				(*(*TDEFAULT_ATTRIBUTE)(unsafe.Pointer((*TELEMENT_TYPE)(unsafe.Pointer(newE)).FdefaultAtts + uintptr(i)*24))).Fvalue = libc.UintptrFromInt32(0)
			}
			goto _8
		_8:
			;
			i++
		}
		goto _6
	_6:
	}
	/* Copy the entity tables. */
	if !(_copyEntityTable(tls, oldParser, newDtd, newDtd+160, oldDtd) != 0) {
		return 0
	}
	if !(_copyEntityTable(tls, oldParser, newDtd+264, newDtd+160, oldDtd+264) != 0) {
		return 0
	}
	(*TDTD)(unsafe.Pointer(newDtd)).FparamEntityRead = (*TDTD)(unsafe.Pointer(oldDtd)).FparamEntityRead
	(*TDTD)(unsafe.Pointer(newDtd)).FkeepProcessing = (*TDTD)(unsafe.Pointer(oldDtd)).FkeepProcessing
	(*TDTD)(unsafe.Pointer(newDtd)).FhasParamEntityRefs = (*TDTD)(unsafe.Pointer(oldDtd)).FhasParamEntityRefs
	(*TDTD)(unsafe.Pointer(newDtd)).Fstandalone = (*TDTD)(unsafe.Pointer(oldDtd)).Fstandalone
	/* Don't want deep copying for scaffolding */
	(*TDTD)(unsafe.Pointer(newDtd)).Fin_eldecl = (*TDTD)(unsafe.Pointer(oldDtd)).Fin_eldecl
	(*TDTD)(unsafe.Pointer(newDtd)).Fscaffold = (*TDTD)(unsafe.Pointer(oldDtd)).Fscaffold
	(*TDTD)(unsafe.Pointer(newDtd)).FcontentStringLen = (*TDTD)(unsafe.Pointer(oldDtd)).FcontentStringLen
	(*TDTD)(unsafe.Pointer(newDtd)).FscaffSize = (*TDTD)(unsafe.Pointer(oldDtd)).FscaffSize
	(*TDTD)(unsafe.Pointer(newDtd)).FscaffLevel = (*TDTD)(unsafe.Pointer(oldDtd)).FscaffLevel
	(*TDTD)(unsafe.Pointer(newDtd)).FscaffIndex = (*TDTD)(unsafe.Pointer(oldDtd)).FscaffIndex
	return int32(1)
}

/* End dtdCopy */

func _copyEntityTable(tls *libc.TLS, oldParser TXML_Parser, newTable uintptr, newPool uintptr, oldTable uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var cachedNewBase, cachedOldBase, name, newE, oldE, tem, tem1, tem2, v2 uintptr
	var _ /* iter at bp+0 */ THASH_TABLE_ITER
	_, _, _, _, _, _, _, _, _ = cachedNewBase, cachedOldBase, name, newE, oldE, tem, tem1, tem2, v2
	cachedOldBase = libc.UintptrFromInt32(0)
	cachedNewBase = libc.UintptrFromInt32(0)
	_hashTableIterInit(tls, bp, oldTable)
	for {
		oldE = _hashTableIterNext(tls, bp)
		if !(oldE != 0) {
			break
		}
		name = _poolCopyString(tls, newPool, (*TENTITY)(unsafe.Pointer(oldE)).Fname)
		if !(name != 0) {
			return 0
		}
		newE = _lookup(tls, oldParser, newTable, name, uint64(64))
		if !(newE != 0) {
			return 0
		}
		if (*TENTITY)(unsafe.Pointer(oldE)).FsystemId != 0 {
			tem = _poolCopyString(tls, newPool, (*TENTITY)(unsafe.Pointer(oldE)).FsystemId)
			if !(tem != 0) {
				return 0
			}
			(*TENTITY)(unsafe.Pointer(newE)).FsystemId = tem
			if (*TENTITY)(unsafe.Pointer(oldE)).Fbase != 0 {
				if (*TENTITY)(unsafe.Pointer(oldE)).Fbase == cachedOldBase {
					(*TENTITY)(unsafe.Pointer(newE)).Fbase = cachedNewBase
				} else {
					cachedOldBase = (*TENTITY)(unsafe.Pointer(oldE)).Fbase
					tem = _poolCopyString(tls, newPool, cachedOldBase)
					if !(tem != 0) {
						return 0
					}
					v2 = tem
					(*TENTITY)(unsafe.Pointer(newE)).Fbase = v2
					cachedNewBase = v2
				}
			}
			if (*TENTITY)(unsafe.Pointer(oldE)).FpublicId != 0 {
				tem = _poolCopyString(tls, newPool, (*TENTITY)(unsafe.Pointer(oldE)).FpublicId)
				if !(tem != 0) {
					return 0
				}
				(*TENTITY)(unsafe.Pointer(newE)).FpublicId = tem
			}
		} else {
			tem1 = _poolCopyStringN(tls, newPool, (*TENTITY)(unsafe.Pointer(oldE)).FtextPtr, (*TENTITY)(unsafe.Pointer(oldE)).FtextLen)
			if !(tem1 != 0) {
				return 0
			}
			(*TENTITY)(unsafe.Pointer(newE)).FtextPtr = tem1
			(*TENTITY)(unsafe.Pointer(newE)).FtextLen = (*TENTITY)(unsafe.Pointer(oldE)).FtextLen
		}
		if (*TENTITY)(unsafe.Pointer(oldE)).Fnotation != 0 {
			tem2 = _poolCopyString(tls, newPool, (*TENTITY)(unsafe.Pointer(oldE)).Fnotation)
			if !(tem2 != 0) {
				return 0
			}
			(*TENTITY)(unsafe.Pointer(newE)).Fnotation = tem2
		}
		(*TENTITY)(unsafe.Pointer(newE)).Fis_param = (*TENTITY)(unsafe.Pointer(oldE)).Fis_param
		(*TENTITY)(unsafe.Pointer(newE)).Fis_internal = (*TENTITY)(unsafe.Pointer(oldE)).Fis_internal
		goto _1
	_1:
	}
	return int32(1)
}

func _keyeq(tls *libc.TLS, s1 TKEY, s2 TKEY) (r TXML_Bool) {
	for {
		if !(libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s1))) == libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s2)))) {
			break
		}
		if libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s1))) == 0 {
			return libc.Uint8FromInt32(1)
		}
		goto _1
	_1:
		;
		s1++
		s2++
	}
	return libc.Uint8FromInt32(0)
}

func _keylen(tls *libc.TLS, s TKEY) (r Tsize_t) {
	var len1 Tsize_t
	_ = len1
	len1 = uint64(0)
	for {
		if !(*(*TXML_Char)(unsafe.Pointer(s)) != 0) {
			break
		}
		goto _1
	_1:
		;
		s++
		len1++
	}
	return len1
}

func _copy_salt_to_sipkey(tls *libc.TLS, parser TXML_Parser, key uintptr) {
	*(*Tuint64_t)(unsafe.Pointer(key)) = uint64(0)
	*(*Tuint64_t)(unsafe.Pointer(key + 1*8)) = _get_hash_secret_salt(tls, parser)
}

func _hash(tls *libc.TLS, parser TXML_Parser, s TKEY) (r uint64) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var _ /* key at bp+56 */ Tsipkey
	var _ /* state at bp+0 */ Tsiphash
	_ = __ccgo_fp(_sip24_valid)
	_copy_salt_to_sipkey(tls, parser, bp+56)
	_sip24_init(tls, bp, bp+56)
	_sip24_update(tls, bp, s, _keylen(tls, s)*uint64(1))
	return _sip24_final(tls, bp)
}

func _lookup(tls *libc.TLS, parser TXML_Parser, table uintptr, name TKEY, createSize Tsize_t) (r uintptr) {
	var h, mask, newHash, newMask uint64
	var i, j, newSize, tsize, tsize1 Tsize_t
	var newPower, step uint8
	var newV uintptr
	_, _, _, _, _, _, _, _, _, _, _, _ = h, i, j, mask, newHash, newMask, newPower, newSize, newV, step, tsize, tsize1
	if (*THASH_TABLE)(unsafe.Pointer(table)).Fsize == uint64(0) {
		if !(createSize != 0) {
			return libc.UintptrFromInt32(0)
		}
		(*THASH_TABLE)(unsafe.Pointer(table)).Fpower = uint8(m_INIT_POWER)
		/* table->size is a power of 2 */
		(*THASH_TABLE)(unsafe.Pointer(table)).Fsize = libc.Uint64FromInt32(1) << libc.Int32FromInt32(m_INIT_POWER)
		tsize = (*THASH_TABLE)(unsafe.Pointer(table)).Fsize * uint64(8)
		(*THASH_TABLE)(unsafe.Pointer(table)).Fv = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Fmalloc_fcn})))(tls, tsize)
		if !((*THASH_TABLE)(unsafe.Pointer(table)).Fv != 0) {
			(*THASH_TABLE)(unsafe.Pointer(table)).Fsize = uint64(0)
			return libc.UintptrFromInt32(0)
		}
		libc.Xmemset(tls, (*THASH_TABLE)(unsafe.Pointer(table)).Fv, 0, tsize)
		i = _hash(tls, parser, name) & ((*THASH_TABLE)(unsafe.Pointer(table)).Fsize - uint64(1))
	} else {
		h = _hash(tls, parser, name)
		mask = (*THASH_TABLE)(unsafe.Pointer(table)).Fsize - uint64(1)
		step = uint8(0)
		i = h & mask
		for *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)) != 0 {
			if _keyeq(tls, name, (*TNAMED)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)))).Fname) != 0 {
				return *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8))
			}
			if !(step != 0) {
				step = uint8(h & ^mask >> (libc.Int32FromUint8((*THASH_TABLE)(unsafe.Pointer(table)).Fpower)-libc.Int32FromInt32(1)) & (mask>>libc.Int32FromInt32(2)) | libc.Uint64FromInt32(1))
			}
			if i < uint64(step) {
				i += (*THASH_TABLE)(unsafe.Pointer(table)).Fsize - uint64(step)
			} else {
				i -= uint64(step)
			}
		}
		if !(createSize != 0) {
			return libc.UintptrFromInt32(0)
		}
		/* check for overflow (table is half full) */
		if (*THASH_TABLE)(unsafe.Pointer(table)).Fused>>(libc.Int32FromUint8((*THASH_TABLE)(unsafe.Pointer(table)).Fpower)-int32(1)) != 0 {
			newPower = libc.Uint8FromInt32(libc.Int32FromUint8((*THASH_TABLE)(unsafe.Pointer(table)).Fpower) + int32(1))
			newSize = libc.Uint64FromInt32(1) << newPower
			newMask = newSize - uint64(1)
			tsize1 = newSize * uint64(8)
			newV = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Fmalloc_fcn})))(tls, tsize1)
			if !(newV != 0) {
				return libc.UintptrFromInt32(0)
			}
			libc.Xmemset(tls, newV, 0, tsize1)
			i = uint64(0)
			for {
				if !(i < (*THASH_TABLE)(unsafe.Pointer(table)).Fsize) {
					break
				}
				if *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)) != 0 {
					newHash = _hash(tls, parser, (*TNAMED)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)))).Fname)
					j = newHash & newMask
					step = uint8(0)
					for *(*uintptr)(unsafe.Pointer(newV + uintptr(j)*8)) != 0 {
						if !(step != 0) {
							step = uint8(newHash & ^newMask >> (libc.Int32FromUint8(newPower)-libc.Int32FromInt32(1)) & (newMask>>libc.Int32FromInt32(2)) | libc.Uint64FromInt32(1))
						}
						if j < uint64(step) {
							j += newSize - uint64(step)
						} else {
							j -= uint64(step)
						}
					}
					*(*uintptr)(unsafe.Pointer(newV + uintptr(j)*8)) = *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8))
				}
				goto _2
			_2:
				;
				i++
			}
			(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Ffree_fcn})))(tls, (*THASH_TABLE)(unsafe.Pointer(table)).Fv)
			(*THASH_TABLE)(unsafe.Pointer(table)).Fv = newV
			(*THASH_TABLE)(unsafe.Pointer(table)).Fpower = newPower
			(*THASH_TABLE)(unsafe.Pointer(table)).Fsize = newSize
			i = h & newMask
			step = uint8(0)
			for *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)) != 0 {
				if !(step != 0) {
					step = uint8(h & ^newMask >> (libc.Int32FromUint8(newPower)-libc.Int32FromInt32(1)) & (newMask>>libc.Int32FromInt32(2)) | libc.Uint64FromInt32(1))
				}
				if i < uint64(step) {
					i += newSize - uint64(step)
				} else {
					i -= uint64(step)
				}
			}
		}
	}
	*(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)) = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Fmalloc_fcn})))(tls, createSize)
	if !(*(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)) != 0) {
		return libc.UintptrFromInt32(0)
	}
	libc.Xmemset(tls, *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)), 0, createSize)
	(*TNAMED)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)))).Fname = name
	(*THASH_TABLE)(unsafe.Pointer(table)).Fused++
	return *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8))
}

func _hashTableClear(tls *libc.TLS, table uintptr) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < (*THASH_TABLE)(unsafe.Pointer(table)).Fsize) {
			break
		}
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Ffree_fcn})))(tls, *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)))
		*(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)) = libc.UintptrFromInt32(0)
		goto _1
	_1:
		;
		i++
	}
	(*THASH_TABLE)(unsafe.Pointer(table)).Fused = uint64(0)
}

func _hashTableDestroy(tls *libc.TLS, table uintptr) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < (*THASH_TABLE)(unsafe.Pointer(table)).Fsize) {
			break
		}
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Ffree_fcn})))(tls, *(*uintptr)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fv + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*THASH_TABLE)(unsafe.Pointer(table)).Fmem)).Ffree_fcn})))(tls, (*THASH_TABLE)(unsafe.Pointer(table)).Fv)
}

func _hashTableInit(tls *libc.TLS, p uintptr, ms uintptr) {
	(*THASH_TABLE)(unsafe.Pointer(p)).Fpower = uint8(0)
	(*THASH_TABLE)(unsafe.Pointer(p)).Fsize = uint64(0)
	(*THASH_TABLE)(unsafe.Pointer(p)).Fused = uint64(0)
	(*THASH_TABLE)(unsafe.Pointer(p)).Fv = libc.UintptrFromInt32(0)
	(*THASH_TABLE)(unsafe.Pointer(p)).Fmem = ms
}

func _hashTableIterInit(tls *libc.TLS, iter uintptr, table uintptr) {
	var v1 uintptr
	_ = v1
	(*THASH_TABLE_ITER)(unsafe.Pointer(iter)).Fp = (*THASH_TABLE)(unsafe.Pointer(table)).Fv
	if (*THASH_TABLE_ITER)(unsafe.Pointer(iter)).Fp != 0 {
		v1 = (*THASH_TABLE_ITER)(unsafe.Pointer(iter)).Fp + uintptr((*THASH_TABLE)(unsafe.Pointer(table)).Fsize)*8
	} else {
		v1 = libc.UintptrFromInt32(0)
	}
	(*THASH_TABLE_ITER)(unsafe.Pointer(iter)).Fend = v1
}

func _hashTableIterNext(tls *libc.TLS, iter uintptr) (r uintptr) {
	var tem, v1, v2 uintptr
	_, _, _ = tem, v1, v2
	for (*THASH_TABLE_ITER)(unsafe.Pointer(iter)).Fp != (*THASH_TABLE_ITER)(unsafe.Pointer(iter)).Fend {
		v2 = iter
		v1 = *(*uintptr)(unsafe.Pointer(v2))
		*(*uintptr)(unsafe.Pointer(v2)) += 8
		tem = *(*uintptr)(unsafe.Pointer(v1))
		if tem != 0 {
			return tem
		}
	}
	return libc.UintptrFromInt32(0)
}

func _poolInit(tls *libc.TLS, pool uintptr, ms uintptr) {
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fend = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fmem = ms
}

func _poolClear(tls *libc.TLS, pool uintptr) {
	var p, tem uintptr
	_, _ = p, tem
	if !((*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks != 0) {
		(*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks
	} else {
		p = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks
		for p != 0 {
			tem = (*TBLOCK)(unsafe.Pointer(p)).Fnext
			(*TBLOCK)(unsafe.Pointer(p)).Fnext = (*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks
			(*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks = p
			p = tem
		}
	}
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr = libc.UintptrFromInt32(0)
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fend = libc.UintptrFromInt32(0)
}

func _poolDestroy(tls *libc.TLS, pool uintptr) {
	var p, tem, tem1 uintptr
	_, _, _ = p, tem, tem1
	p = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks
	for p != 0 {
		tem = (*TBLOCK)(unsafe.Pointer(p)).Fnext
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fmem)).Ffree_fcn})))(tls, p)
		p = tem
	}
	p = (*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks
	for p != 0 {
		tem1 = (*TBLOCK)(unsafe.Pointer(p)).Fnext
		(*(*func(*libc.TLS, uintptr))(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fmem)).Ffree_fcn})))(tls, p)
		p = tem1
	}
}

func _poolAppend(tls *libc.TLS, pool uintptr, enc uintptr, _ptr uintptr, end uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var convert_res _XML_Convert_Result
	_ = convert_res
	if !((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr != 0) && !(_poolGrow(tls, pool) != 0) {
		return libc.UintptrFromInt32(0)
	}
	for {
		convert_res = (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, end, pool+24, (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend)
		if convert_res == int32(_XML_CONVERT_COMPLETED) || convert_res == int32(_XML_CONVERT_INPUT_INCOMPLETE) {
			break
		}
		if !(_poolGrow(tls, pool) != 0) {
			return libc.UintptrFromInt32(0)
		}
		goto _1
	_1:
	}
	return (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart
}

func _poolCopyString(tls *libc.TLS, pool uintptr, s uintptr) (r uintptr) {
	var v1, v4, v5 uintptr
	var v3 int32
	_, _, _, _ = v1, v3, v4, v5
	for {
		if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
			v3 = 0
		} else {
			v5 = pool + 24
			v4 = *(*uintptr)(unsafe.Pointer(v5))
			*(*uintptr)(unsafe.Pointer(v5))++
			*(*TXML_Char)(unsafe.Pointer(v4)) = *(*TXML_Char)(unsafe.Pointer(s))
			v3 = libc.Int32FromInt32(1)
		}
		if !(v3 != 0) {
			return libc.UintptrFromInt32(0)
		}
		goto _2
	_2:
		;
		v1 = s
		s++
		if !(*(*TXML_Char)(unsafe.Pointer(v1)) != 0) {
			break
		}
	}
	s = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr
	return s
}

func _poolCopyStringN(tls *libc.TLS, pool uintptr, s uintptr, n int32) (r uintptr) {
	var v2 int32
	var v3, v4 uintptr
	_, _, _ = v2, v3, v4
	if !((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr != 0) && !(_poolGrow(tls, pool) != 0) {
		/* The following line is unreachable given the current usage of
		 * poolCopyStringN().  Currently it is called from exactly one
		 * place to copy the text of a simple general entity.  By that
		 * point, the name of the entity is already stored in the pool, so
		 * pool->ptr cannot be NULL.
		 *
		 * If poolCopyStringN() is used elsewhere as it well might be,
		 * this line may well become executable again.  Regardless, this
		 * sort of check shouldn't be removed lightly, so we just exclude
		 * it from the coverage statistics.
		 */
		return libc.UintptrFromInt32(0) /* LCOV_EXCL_LINE */
	}
	for {
		if !(n > 0) {
			break
		}
		if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
			v2 = 0
		} else {
			v4 = pool + 24
			v3 = *(*uintptr)(unsafe.Pointer(v4))
			*(*uintptr)(unsafe.Pointer(v4))++
			*(*TXML_Char)(unsafe.Pointer(v3)) = *(*TXML_Char)(unsafe.Pointer(s))
			v2 = libc.Int32FromInt32(1)
		}
		if !(v2 != 0) {
			return libc.UintptrFromInt32(0)
		}
		goto _1
	_1:
		;
		n--
		s++
	}
	s = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart
	(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr
	return s
}

func _poolAppendString(tls *libc.TLS, pool uintptr, s uintptr) (r uintptr) {
	var v1 int32
	var v2, v3 uintptr
	_, _, _ = v1, v2, v3
	for *(*TXML_Char)(unsafe.Pointer(s)) != 0 {
		if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
			v1 = 0
		} else {
			v3 = pool + 24
			v2 = *(*uintptr)(unsafe.Pointer(v3))
			*(*uintptr)(unsafe.Pointer(v3))++
			*(*TXML_Char)(unsafe.Pointer(v2)) = *(*TXML_Char)(unsafe.Pointer(s))
			v1 = libc.Int32FromInt32(1)
		}
		if !(v1 != 0) {
			return libc.UintptrFromInt32(0)
		}
		s++
	}
	return (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart
}

func _poolStoreString(tls *libc.TLS, pool uintptr, enc uintptr, ptr uintptr, end uintptr) (r uintptr) {
	var v1, v2 uintptr
	_, _ = v1, v2
	if !(_poolAppend(tls, pool, enc, ptr, end) != 0) {
		return libc.UintptrFromInt32(0)
	}
	if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fend && !(_poolGrow(tls, pool) != 0) {
		return libc.UintptrFromInt32(0)
	}
	v2 = pool + 24
	v1 = *(*uintptr)(unsafe.Pointer(v2))
	*(*uintptr)(unsafe.Pointer(v2))++
	*(*TXML_Char)(unsafe.Pointer(v1)) = uint8(0)
	return (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart
}

func _poolBytesToAllocateFor(tls *libc.TLS, blockSize int32) (r Tsize_t) {
	var bytesToAllocate, stretchedBlockSize int32
	var stretch Tsize_t
	_, _, _ = bytesToAllocate, stretch, stretchedBlockSize
	/* Unprotected math would be:
	 ** return offsetof(BLOCK, s) + blockSize * sizeof(XML_Char);
	 **
	 ** Detect overflow, avoiding _signed_ overflow undefined behavior
	 ** For a + b * c we check b * c in isolation first, so that addition of a
	 ** on top has no chance of making us accept a small non-negative number
	 */
	stretch = uint64(1) /* can be 4 bytes */
	if blockSize <= 0 {
		return uint64(0)
	}
	if blockSize > libc.Int32FromUint64(libc.Uint64FromInt32(m_INT_MAX)/stretch) {
		return uint64(0)
	}
	stretchedBlockSize = blockSize * libc.Int32FromUint64(stretch)
	bytesToAllocate = libc.Int32FromUint64(uint64(libc.UintptrFromInt32(0)+12) + uint64(libc.Uint32FromInt32(stretchedBlockSize)))
	if bytesToAllocate < 0 {
		return uint64(0)
	}
	return libc.Uint64FromInt32(bytesToAllocate)
	return r
}

func _poolGrow(tls *libc.TLS, pool uintptr) (r TXML_Bool) {
	var blockSize, blockSize1 int32
	var bytesToAllocate, bytesToAllocate1 Tsize_t
	var offsetInsideBlock Tptrdiff_t
	var tem, tem1, temp uintptr
	_, _, _, _, _, _, _, _ = blockSize, blockSize1, bytesToAllocate, bytesToAllocate1, offsetInsideBlock, tem, tem1, temp
	if (*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks != 0 {
		if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart == uintptr(0) {
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks = (*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks
			(*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks = (*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks)).Fnext
			(*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks)).Fnext = libc.UintptrFromInt32(0)
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks + 12
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fend = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart + uintptr((*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks)).Fsize)
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart
			return libc.Uint8FromInt32(1)
		}
		if int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fend)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart) < int64((*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks)).Fsize) {
			tem = (*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks)).Fnext
			(*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks)).Fnext = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks = (*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks
			(*TSTRING_POOL)(unsafe.Pointer(pool)).FfreeBlocks = tem
			libc.Xmemcpy(tls, (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks+12, (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart, libc.Uint64FromInt64(int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fend)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart))*uint64(1))
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks + 12 + uintptr(int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart))
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks + 12
			(*TSTRING_POOL)(unsafe.Pointer(pool)).Fend = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart + uintptr((*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks)).Fsize)
			return libc.Uint8FromInt32(1)
		}
	}
	if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks != 0 && (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart == (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks+12 {
		blockSize = libc.Int32FromUint32(libc.Uint32FromInt64(int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fend)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart)) * libc.Uint32FromUint32(2))
		/* NOTE: Needs to be calculated prior to calling `realloc`
		   to avoid dangling pointers: */
		offsetInsideBlock = int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr) - int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart)
		if blockSize < 0 {
			/* This condition traps a situation where either more than
			 * INT_MAX/2 bytes have already been allocated.  This isn't
			 * readily testable, since it is unlikely that an average
			 * machine will have that much memory, so we exclude it from the
			 * coverage statistics.
			 */
			return libc.Uint8FromInt32(0) /* LCOV_EXCL_LINE */
		}
		bytesToAllocate = _poolBytesToAllocateFor(tls, blockSize)
		if bytesToAllocate == uint64(0) {
			return libc.Uint8FromInt32(0)
		}
		temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fmem)).Frealloc_fcn})))(tls, (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks, uint64(uint32(bytesToAllocate)))
		if temp == libc.UintptrFromInt32(0) {
			return libc.Uint8FromInt32(0)
		}
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks = temp
		(*TBLOCK)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks)).Fsize = blockSize
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks + 12 + uintptr(offsetInsideBlock)
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks + 12
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fend = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart + uintptr(blockSize)
	} else {
		blockSize1 = int32(int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fend) - int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart))
		if blockSize1 < 0 {
			/* This condition traps a situation where either more than
			 * INT_MAX bytes have already been allocated (which is prevented
			 * by various pieces of program logic, not least this one, never
			 * mind the unlikelihood of actually having that much memory) or
			 * the pool control fields have been corrupted (which could
			 * conceivably happen in an extremely buggy user handler
			 * function).  Either way it isn't readily testable, so we
			 * exclude it from the coverage statistics.
			 */
			return libc.Uint8FromInt32(0) /* LCOV_EXCL_LINE */
		}
		if blockSize1 < int32(m_INIT_BLOCK_SIZE) {
			blockSize1 = int32(m_INIT_BLOCK_SIZE)
		} else {
			/* Detect overflow, avoiding _signed_ overflow undefined behavior */
			if libc.Int32FromUint32(libc.Uint32FromInt32(blockSize1)*libc.Uint32FromUint32(2)) < 0 {
				return libc.Uint8FromInt32(0)
			}
			blockSize1 *= int32(2)
		}
		bytesToAllocate1 = _poolBytesToAllocateFor(tls, blockSize1)
		if bytesToAllocate1 == uint64(0) {
			return libc.Uint8FromInt32(0)
		}
		tem1 = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer((*TSTRING_POOL)(unsafe.Pointer(pool)).Fmem)).Fmalloc_fcn})))(tls, bytesToAllocate1)
		if !(tem1 != 0) {
			return libc.Uint8FromInt32(0)
		}
		(*TBLOCK)(unsafe.Pointer(tem1)).Fsize = blockSize1
		(*TBLOCK)(unsafe.Pointer(tem1)).Fnext = (*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fblocks = tem1
		if (*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr != (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart {
			libc.Xmemcpy(tls, tem1+12, (*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart, libc.Uint64FromInt64(int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart))*uint64(1))
		}
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr = tem1 + 12 + uintptr(int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fptr)-int64((*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart))
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fstart = tem1 + 12
		(*TSTRING_POOL)(unsafe.Pointer(pool)).Fend = tem1 + 12 + uintptr(blockSize1)
	}
	return libc.Uint8FromInt32(1)
}

func _nextScaffoldPart(tls *libc.TLS, parser TXML_Parser) (r int32) {
	var dtd, me, parent, temp, v2 uintptr
	var next, v3, v4, v5 int32
	var v1 uint32
	_, _, _, _, _, _, _, _, _, _ = dtd, me, next, parent, temp, v1, v2, v3, v4, v5
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	if !((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex != 0) {
		(*TDTD)(unsafe.Pointer(dtd)).FscaffIndex = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, uint64((*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_groupSize)*libc.Uint64FromInt64(4))
		if !((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex != 0) {
			return -int32(1)
		}
		*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex)) = 0
	}
	if (*TDTD)(unsafe.Pointer(dtd)).FscaffCount >= (*TDTD)(unsafe.Pointer(dtd)).FscaffSize {
		if (*TDTD)(unsafe.Pointer(dtd)).Fscaffold != 0 {
			temp = (*(*func(*libc.TLS, uintptr, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Frealloc_fcn})))(tls, (*TDTD)(unsafe.Pointer(dtd)).Fscaffold, uint64((*TDTD)(unsafe.Pointer(dtd)).FscaffSize*libc.Uint32FromInt32(2))*libc.Uint64FromInt64(32))
			if temp == libc.UintptrFromInt32(0) {
				return -int32(1)
			}
			*(*uint32)(unsafe.Pointer(dtd + 340)) *= uint32(2)
		} else {
			temp = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(m_INIT_SCAFFOLD_ELEMENTS)*libc.Uint64FromInt64(32))
			if temp == libc.UintptrFromInt32(0) {
				return -int32(1)
			}
			(*TDTD)(unsafe.Pointer(dtd)).FscaffSize = uint32(m_INIT_SCAFFOLD_ELEMENTS)
		}
		(*TDTD)(unsafe.Pointer(dtd)).Fscaffold = temp
	}
	v2 = dtd + 344
	v1 = *(*uint32)(unsafe.Pointer(v2))
	*(*uint32)(unsafe.Pointer(v2))++
	next = libc.Int32FromUint32(v1)
	me = (*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(next)*32
	if (*TDTD)(unsafe.Pointer(dtd)).FscaffLevel != 0 {
		parent = (*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(*(*int32)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).FscaffIndex + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffLevel-int32(1))*4)))*32
		if (*TCONTENT_SCAFFOLD)(unsafe.Pointer(parent)).Flastchild != 0 {
			(*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr((*TCONTENT_SCAFFOLD)(unsafe.Pointer(parent)).Flastchild)*32))).Fnextsib = next
		}
		if !((*TCONTENT_SCAFFOLD)(unsafe.Pointer(parent)).Fchildcnt != 0) {
			(*TCONTENT_SCAFFOLD)(unsafe.Pointer(parent)).Ffirstchild = next
		}
		(*TCONTENT_SCAFFOLD)(unsafe.Pointer(parent)).Flastchild = next
		(*TCONTENT_SCAFFOLD)(unsafe.Pointer(parent)).Fchildcnt++
	}
	v5 = libc.Int32FromInt32(0)
	(*TCONTENT_SCAFFOLD)(unsafe.Pointer(me)).Fnextsib = v5
	v4 = v5
	(*TCONTENT_SCAFFOLD)(unsafe.Pointer(me)).Fchildcnt = v4
	v3 = v4
	(*TCONTENT_SCAFFOLD)(unsafe.Pointer(me)).Flastchild = v3
	(*TCONTENT_SCAFFOLD)(unsafe.Pointer(me)).Ffirstchild = v3
	return next
}

func _build_node(tls *libc.TLS, parser TXML_Parser, src_node int32, dest uintptr, contpos uintptr, strpos uintptr) {
	var cn int32
	var dtd, src, v2, v3 uintptr
	var i uint32
	_, _, _, _, _, _ = cn, dtd, i, src, v2, v3
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd /* save one level of indirection */
	(*TXML_Content)(unsafe.Pointer(dest)).Ftype1 = (*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(src_node)*32))).Ftype1
	(*TXML_Content)(unsafe.Pointer(dest)).Fquant = (*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(src_node)*32))).Fquant
	if (*TXML_Content)(unsafe.Pointer(dest)).Ftype1 == int32(_XML_CTYPE_NAME) {
		(*TXML_Content)(unsafe.Pointer(dest)).Fname = *(*uintptr)(unsafe.Pointer(strpos))
		src = (*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(src_node)*32))).Fname
		for {
			v3 = strpos
			v2 = *(*uintptr)(unsafe.Pointer(v3))
			*(*uintptr)(unsafe.Pointer(v3))++
			*(*TXML_Char)(unsafe.Pointer(v2)) = *(*TXML_Char)(unsafe.Pointer(src))
			if !(*(*TXML_Char)(unsafe.Pointer(src)) != 0) {
				break
			}
			src++
			goto _1
		_1:
		}
		(*TXML_Content)(unsafe.Pointer(dest)).Fnumchildren = uint32(0)
		(*TXML_Content)(unsafe.Pointer(dest)).Fchildren = libc.UintptrFromInt32(0)
	} else {
		(*TXML_Content)(unsafe.Pointer(dest)).Fnumchildren = libc.Uint32FromInt32((*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(src_node)*32))).Fchildcnt)
		(*TXML_Content)(unsafe.Pointer(dest)).Fchildren = *(*uintptr)(unsafe.Pointer(contpos))
		*(*uintptr)(unsafe.Pointer(contpos)) += uintptr((*TXML_Content)(unsafe.Pointer(dest)).Fnumchildren) * 32
		i = uint32(0)
		cn = (*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(src_node)*32))).Ffirstchild
		for {
			if !(i < (*TXML_Content)(unsafe.Pointer(dest)).Fnumchildren) {
				break
			}
			_build_node(tls, parser, cn, (*TXML_Content)(unsafe.Pointer(dest)).Fchildren+uintptr(i)*32, contpos, strpos)
			goto _4
		_4:
			;
			i++
			cn = (*(*TCONTENT_SCAFFOLD)(unsafe.Pointer((*TDTD)(unsafe.Pointer(dtd)).Fscaffold + uintptr(cn)*32))).Fnextsib
		}
		(*TXML_Content)(unsafe.Pointer(dest)).Fname = libc.UintptrFromInt32(0)
	}
}

func _build_model(tls *libc.TLS, parser TXML_Parser) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var allocsize int32
	var dtd, ret uintptr
	var _ /* cpos at bp+0 */ uintptr
	var _ /* str at bp+8 */ uintptr
	_, _, _ = allocsize, dtd, ret
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd
	allocsize = libc.Int32FromUint64(uint64((*TDTD)(unsafe.Pointer(dtd)).FscaffCount)*libc.Uint64FromInt64(32) + uint64((*TDTD)(unsafe.Pointer(dtd)).FcontentStringLen)*libc.Uint64FromInt64(1))
	ret = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_mem.Fmalloc_fcn})))(tls, libc.Uint64FromInt32(allocsize))
	if !(ret != 0) {
		return libc.UintptrFromInt32(0)
	}
	*(*uintptr)(unsafe.Pointer(bp + 8)) = ret + uintptr((*TDTD)(unsafe.Pointer(dtd)).FscaffCount)*32
	*(*uintptr)(unsafe.Pointer(bp)) = ret + 1*32
	_build_node(tls, parser, 0, ret, bp, bp+8)
	return ret
}

func _getElementType(tls *libc.TLS, parser TXML_Parser, enc uintptr, ptr uintptr, end uintptr) (r uintptr) {
	var dtd, name, ret uintptr
	_, _, _ = dtd, name, ret
	dtd = (*TXML_ParserStruct)(unsafe.Pointer(parser)).Fm_dtd /* save one level of indirection */
	name = _poolStoreString(tls, dtd+160, enc, ptr, end)
	if !(name != 0) {
		return libc.UintptrFromInt32(0)
	}
	ret = _lookup(tls, parser, dtd+40, name, uint64(40))
	if !(ret != 0) {
		return libc.UintptrFromInt32(0)
	}
	if (*TELEMENT_TYPE)(unsafe.Pointer(ret)).Fname != name {
		(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart
	} else {
		(*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fstart = (*TSTRING_POOL)(unsafe.Pointer(dtd + 160)).Fptr
		if !(_setElementTypePrefix(tls, parser, ret) != 0) {
			return libc.UintptrFromInt32(0)
		}
	}
	return ret
}

func _copyString(tls *libc.TLS, s uintptr, memsuite uintptr) (r uintptr) {
	var charsRequired int32
	var result uintptr
	_, _ = charsRequired, result
	charsRequired = 0
	/* First determine how long the string is */
	for libc.Int32FromUint8(*(*TXML_Char)(unsafe.Pointer(s + uintptr(charsRequired)))) != 0 {
		charsRequired++
	}
	/* Include the terminator */
	charsRequired++
	/* Now allocate space for the copy */
	result = (*(*func(*libc.TLS, Tsize_t) uintptr)(unsafe.Pointer(&struct{ uintptr }{(*TXML_Memory_Handling_Suite)(unsafe.Pointer(memsuite)).Fmalloc_fcn})))(tls, libc.Uint64FromInt32(charsRequired)*uint64(1))
	if result == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	/* Copy the original into place */
	libc.Xmemcpy(tls, result, s, libc.Uint64FromInt32(charsRequired)*uint64(1))
	return result
}

const m_ASCII_41 = 52
const m_ASCII_51 = 53
const m_ASCII_61 = 54
const m_ASCII_71 = 55
const m_ASCII_AMP1 = 38
const m_ASCII_APOS1 = 39
const m_ASCII_B1 = 66
const m_ASCII_COMMA1 = 0x2C
const m_ASCII_FF1 = 0x0C
const m_ASCII_GT1 = 62
const m_ASCII_HASH1 = 0x23
const m_ASCII_K1 = 0x4B
const m_ASCII_LPAREN1 = 0x28
const m_ASCII_LSQB1 = 91
const m_ASCII_LT1 = 60
const m_ASCII_MINUS1 = 45
const m_ASCII_N1 = 0x4E
const m_ASCII_PIPE1 = 0x7C
const m_ASCII_QUOT1 = 34
const m_ASCII_R1 = 0x52
const m_ASCII_RPAREN1 = 0x29
const m_ASCII_RSQB1 = 93
const m_ASCII_SEMI1 = 59
const m_ASCII_SLASH1 = 0x2F
const m_ASCII_SPACE1 = 32
const m_ASCII_TAB1 = 9
const m_ASCII_U1 = 85
const m_ASCII_UNDERSCORE1 = 95
const m_ASCII_Y1 = 0x59
const m_ASCII_Z1 = 90
const m_ASCII_b1 = 98
const m_ASCII_d1 = 100
const m_ASCII_f1 = 102
const m_ASCII_h1 = 0x68
const m_ASCII_i1 = 105
const m_ASCII_q1 = 113
const m_ASCII_u1 = 117
const m_ASCII_v1 = 118
const m_ASCII_w1 = 0x77
const m_ASCII_y1 = 121
const m_ASCII_z1 = 122
const m_ENCODING_MAX = 128
const m___bool_true_false_are_defined = 1
const m_bool = "_Bool"
const m_false = 0
const m_true = 1
const m_utf8_isName4 = "isNever"
const m_utf8_isNmstrt4 = "isNever"

var _namingBitmap = [320]uint32{
	8:   uint32(0xFFFFFFFF),
	9:   uint32(0xFFFFFFFF),
	10:  uint32(0xFFFFFFFF),
	11:  uint32(0xFFFFFFFF),
	12:  uint32(0xFFFFFFFF),
	13:  uint32(0xFFFFFFFF),
	14:  uint32(0xFFFFFFFF),
	15:  uint32(0xFFFFFFFF),
	17:  uint32(0x04000000),
	18:  uint32(0x87FFFFFE),
	19:  uint32(0x07FFFFFE),
	22:  uint32(0xFF7FFFFF),
	23:  uint32(0xFF7FFFFF),
	24:  uint32(0xFFFFFFFF),
	25:  uint32(0x7FF3FFFF),
	26:  uint32(0xFFFFFDFE),
	27:  uint32(0x7FFFFFFF),
	28:  uint32(0xFFFFFFFF),
	29:  uint32(0xFFFFFFFF),
	30:  uint32(0xFFFFE00F),
	31:  uint32(0xFC31FFFF),
	32:  uint32(0x00FFFFFF),
	34:  uint32(0xFFFF0000),
	35:  uint32(0xFFFFFFFF),
	36:  uint32(0xFFFFFFFF),
	37:  uint32(0xF80001FF),
	38:  uint32(0x00000003),
	44:  uint32(0xFFFFD740),
	45:  uint32(0xFFFFFFFB),
	46:  uint32(0x547F7FFF),
	47:  uint32(0x000FFFFD),
	48:  uint32(0xFFFFDFFE),
	49:  uint32(0xFFFFFFFF),
	50:  uint32(0xDFFEFFFF),
	51:  uint32(0xFFFFFFFF),
	52:  uint32(0xFFFF0003),
	53:  uint32(0xFFFFFFFF),
	54:  uint32(0xFFFF199F),
	55:  uint32(0x033FCFFF),
	57:  uint32(0xFFFE0000),
	58:  uint32(0x027FFFFF),
	59:  uint32(0xFFFFFFFE),
	60:  uint32(0x0000007F),
	62:  uint32(0xFFFF0000),
	63:  uint32(0x000707FF),
	65:  uint32(0x07FFFFFE),
	66:  uint32(0x000007FE),
	67:  uint32(0xFFFE0000),
	68:  uint32(0xFFFFFFFF),
	69:  uint32(0x7CFFFFFF),
	70:  uint32(0x002F7FFF),
	71:  uint32(0x00000060),
	72:  uint32(0xFFFFFFE0),
	73:  uint32(0x23FFFFFF),
	74:  uint32(0xFF000000),
	75:  uint32(0x00000003),
	76:  uint32(0xFFF99FE0),
	77:  uint32(0x03C5FDFF),
	78:  uint32(0xB0000000),
	79:  uint32(0x00030003),
	80:  uint32(0xFFF987E0),
	81:  uint32(0x036DFDFF),
	82:  uint32(0x5E000000),
	83:  uint32(0x001C0000),
	84:  uint32(0xFFFBAFE0),
	85:  uint32(0x23EDFDFF),
	87:  uint32(0x00000001),
	88:  uint32(0xFFF99FE0),
	89:  uint32(0x23CDFDFF),
	90:  uint32(0xB0000000),
	91:  uint32(0x00000003),
	92:  uint32(0xD63DC7E0),
	93:  uint32(0x03BFC718),
	96:  uint32(0xFFFDDFE0),
	97:  uint32(0x03EFFDFF),
	99:  uint32(0x00000003),
	100: uint32(0xFFFDDFE0),
	101: uint32(0x03EFFDFF),
	102: uint32(0x40000000),
	103: uint32(0x00000003),
	104: uint32(0xFFFDDFE0),
	105: uint32(0x03FFFDFF),
	107: uint32(0x00000003),
	112: uint32(0xFFFFFFFE),
	113: uint32(0x000D7FFF),
	114: uint32(0x0000003F),
	116: uint32(0xFEF02596),
	117: uint32(0x200D6CAE),
	118: uint32(0x0000001F),
	122: uint32(0xFFFFFEFF),
	123: uint32(0x000003FF),
	133: uint32(0xFFFFFFFF),
	134: uint32(0xFFFF003F),
	135: uint32(0x007FFFFF),
	136: uint32(0x0007DAED),
	137: uint32(0x50000000),
	138: uint32(0x82315001),
	139: uint32(0x002C62AB),
	140: uint32(0x40000000),
	141: uint32(0xF580C900),
	142: uint32(0x00000007),
	143: uint32(0x02010800),
	144: uint32(0xFFFFFFFF),
	145: uint32(0xFFFFFFFF),
	146: uint32(0xFFFFFFFF),
	147: uint32(0xFFFFFFFF),
	148: uint32(0x0FFFFFFF),
	149: uint32(0xFFFFFFFF),
	150: uint32(0xFFFFFFFF),
	151: uint32(0x03FFFFFF),
	152: uint32(0x3F3FFFFF),
	153: uint32(0xFFFFFFFF),
	154: uint32(0xAAFF3F3F),
	155: uint32(0x3FFFFFFF),
	156: uint32(0xFFFFFFFF),
	157: uint32(0x5FDFFFFF),
	158: uint32(0x0FCF1FDC),
	159: uint32(0x1FDC1FFF),
	161: uint32(0x00004C40),
	164: uint32(0x00000007),
	168: uint32(0x00000080),
	169: uint32(0x000003FE),
	170: uint32(0xFFFFFFFE),
	171: uint32(0xFFFFFFFF),
	172: uint32(0x001FFFFF),
	173: uint32(0xFFFFFFFE),
	174: uint32(0xFFFFFFFF),
	175: uint32(0x07FFFFFF),
	176: uint32(0xFFFFFFE0),
	177: uint32(0x00001FFF),
	184: uint32(0xFFFFFFFF),
	185: uint32(0xFFFFFFFF),
	186: uint32(0xFFFFFFFF),
	187: uint32(0xFFFFFFFF),
	188: uint32(0xFFFFFFFF),
	189: uint32(0x0000003F),
	192: uint32(0xFFFFFFFF),
	193: uint32(0xFFFFFFFF),
	194: uint32(0xFFFFFFFF),
	195: uint32(0xFFFFFFFF),
	196: uint32(0xFFFFFFFF),
	197: uint32(0x0000000F),
	201: uint32(0x07FF6000),
	202: uint32(0x87FFFFFE),
	203: uint32(0x07FFFFFE),
	205: uint32(0x00800000),
	206: uint32(0xFF7FFFFF),
	207: uint32(0xFF7FFFFF),
	208: uint32(0x00FFFFFF),
	210: uint32(0xFFFF0000),
	211: uint32(0xFFFFFFFF),
	212: uint32(0xFFFFFFFF),
	213: uint32(0xF80001FF),
	214: uint32(0x00030003),
	216: uint32(0xFFFFFFFF),
	217: uint32(0xFFFFFFFF),
	218: uint32(0x0000003F),
	219: uint32(0x00000003),
	220: uint32(0xFFFFD7C0),
	221: uint32(0xFFFFFFFB),
	222: uint32(0x547F7FFF),
	223: uint32(0x000FFFFD),
	224: uint32(0xFFFFDFFE),
	225: uint32(0xFFFFFFFF),
	226: uint32(0xDFFEFFFF),
	227: uint32(0xFFFFFFFF),
	228: uint32(0xFFFF007B),
	229: uint32(0xFFFFFFFF),
	230: uint32(0xFFFF199F),
	231: uint32(0x033FCFFF),
	233: uint32(0xFFFE0000),
	234: uint32(0x027FFFFF),
	235: uint32(0xFFFFFFFE),
	236: uint32(0xFFFE007F),
	237: uint32(0xBBFFFFFB),
	238: uint32(0xFFFF0016),
	239: uint32(0x000707FF),
	241: uint32(0x07FFFFFE),
	242: uint32(0x0007FFFF),
	243: uint32(0xFFFF03FF),
	244: uint32(0xFFFFFFFF),
	245: uint32(0x7CFFFFFF),
	246: uint32(0xFFEF7FFF),
	247: uint32(0x03FF3DFF),
	248: uint32(0xFFFFFFEE),
	249: uint32(0xF3FFFFFF),
	250: uint32(0xFF1E3FFF),
	251: uint32(0x0000FFCF),
	252: uint32(0xFFF99FEE),
	253: uint32(0xD3C5FDFF),
	254: uint32(0xB080399F),
	255: uint32(0x0003FFCF),
	256: uint32(0xFFF987E4),
	257: uint32(0xD36DFDFF),
	258: uint32(0x5E003987),
	259: uint32(0x001FFFC0),
	260: uint32(0xFFFBAFEE),
	261: uint32(0xF3EDFDFF),
	262: uint32(0x00003BBF),
	263: uint32(0x0000FFC1),
	264: uint32(0xFFF99FEE),
	265: uint32(0xF3CDFDFF),
	266: uint32(0xB0C0398F),
	267: uint32(0x0000FFC3),
	268: uint32(0xD63DC7EC),
	269: uint32(0xC3BFC718),
	270: uint32(0x00803DC7),
	271: uint32(0x0000FF80),
	272: uint32(0xFFFDDFEE),
	273: uint32(0xC3EFFDFF),
	274: uint32(0x00603DDF),
	275: uint32(0x0000FFC3),
	276: uint32(0xFFFDDFEC),
	277: uint32(0xC3EFFDFF),
	278: uint32(0x40603DDF),
	279: uint32(0x0000FFC3),
	280: uint32(0xFFFDDFEC),
	281: uint32(0xC3FFFDFF),
	282: uint32(0x00803DCF),
	283: uint32(0x0000FFC3),
	288: uint32(0xFFFFFFFE),
	289: uint32(0x07FF7FFF),
	290: uint32(0x03FF7FFF),
	292: uint32(0xFEF02596),
	293: uint32(0x3BFF6CAE),
	294: uint32(0x03FF3F5F),
	296: uint32(0x03000000),
	297: uint32(0xC2A003FF),
	298: uint32(0xFFFFFEFF),
	299: uint32(0xFFFE03FF),
	300: uint32(0xFEBF0FDF),
	301: uint32(0x02FE3FFF),
	310: uint32(0x1FFF0000),
	311: uint32(0x00000002),
	312: uint32(0x000000A0),
	313: uint32(0x003EFFFE),
	314: uint32(0xFFFFFFFE),
	315: uint32(0xFFFFFFFF),
	316: uint32(0x661FFFFF),
	317: uint32(0xFFFFFFFE),
	318: uint32(0xFFFFFFFF),
	319: uint32(0x77FFFFFF),
}
var _nmstrtPages = [256]uint8{
	0:   uint8(0x02),
	1:   uint8(0x03),
	2:   uint8(0x04),
	3:   uint8(0x05),
	4:   uint8(0x06),
	5:   uint8(0x07),
	6:   uint8(0x08),
	9:   uint8(0x09),
	10:  uint8(0x0A),
	11:  uint8(0x0B),
	12:  uint8(0x0C),
	13:  uint8(0x0D),
	14:  uint8(0x0E),
	15:  uint8(0x0F),
	16:  uint8(0x10),
	17:  uint8(0x11),
	30:  uint8(0x12),
	31:  uint8(0x13),
	33:  uint8(0x14),
	48:  uint8(0x15),
	49:  uint8(0x16),
	78:  uint8(0x01),
	79:  uint8(0x01),
	80:  uint8(0x01),
	81:  uint8(0x01),
	82:  uint8(0x01),
	83:  uint8(0x01),
	84:  uint8(0x01),
	85:  uint8(0x01),
	86:  uint8(0x01),
	87:  uint8(0x01),
	88:  uint8(0x01),
	89:  uint8(0x01),
	90:  uint8(0x01),
	91:  uint8(0x01),
	92:  uint8(0x01),
	93:  uint8(0x01),
	94:  uint8(0x01),
	95:  uint8(0x01),
	96:  uint8(0x01),
	97:  uint8(0x01),
	98:  uint8(0x01),
	99:  uint8(0x01),
	100: uint8(0x01),
	101: uint8(0x01),
	102: uint8(0x01),
	103: uint8(0x01),
	104: uint8(0x01),
	105: uint8(0x01),
	106: uint8(0x01),
	107: uint8(0x01),
	108: uint8(0x01),
	109: uint8(0x01),
	110: uint8(0x01),
	111: uint8(0x01),
	112: uint8(0x01),
	113: uint8(0x01),
	114: uint8(0x01),
	115: uint8(0x01),
	116: uint8(0x01),
	117: uint8(0x01),
	118: uint8(0x01),
	119: uint8(0x01),
	120: uint8(0x01),
	121: uint8(0x01),
	122: uint8(0x01),
	123: uint8(0x01),
	124: uint8(0x01),
	125: uint8(0x01),
	126: uint8(0x01),
	127: uint8(0x01),
	128: uint8(0x01),
	129: uint8(0x01),
	130: uint8(0x01),
	131: uint8(0x01),
	132: uint8(0x01),
	133: uint8(0x01),
	134: uint8(0x01),
	135: uint8(0x01),
	136: uint8(0x01),
	137: uint8(0x01),
	138: uint8(0x01),
	139: uint8(0x01),
	140: uint8(0x01),
	141: uint8(0x01),
	142: uint8(0x01),
	143: uint8(0x01),
	144: uint8(0x01),
	145: uint8(0x01),
	146: uint8(0x01),
	147: uint8(0x01),
	148: uint8(0x01),
	149: uint8(0x01),
	150: uint8(0x01),
	151: uint8(0x01),
	152: uint8(0x01),
	153: uint8(0x01),
	154: uint8(0x01),
	155: uint8(0x01),
	156: uint8(0x01),
	157: uint8(0x01),
	158: uint8(0x01),
	159: uint8(0x17),
	172: uint8(0x01),
	173: uint8(0x01),
	174: uint8(0x01),
	175: uint8(0x01),
	176: uint8(0x01),
	177: uint8(0x01),
	178: uint8(0x01),
	179: uint8(0x01),
	180: uint8(0x01),
	181: uint8(0x01),
	182: uint8(0x01),
	183: uint8(0x01),
	184: uint8(0x01),
	185: uint8(0x01),
	186: uint8(0x01),
	187: uint8(0x01),
	188: uint8(0x01),
	189: uint8(0x01),
	190: uint8(0x01),
	191: uint8(0x01),
	192: uint8(0x01),
	193: uint8(0x01),
	194: uint8(0x01),
	195: uint8(0x01),
	196: uint8(0x01),
	197: uint8(0x01),
	198: uint8(0x01),
	199: uint8(0x01),
	200: uint8(0x01),
	201: uint8(0x01),
	202: uint8(0x01),
	203: uint8(0x01),
	204: uint8(0x01),
	205: uint8(0x01),
	206: uint8(0x01),
	207: uint8(0x01),
	208: uint8(0x01),
	209: uint8(0x01),
	210: uint8(0x01),
	211: uint8(0x01),
	212: uint8(0x01),
	213: uint8(0x01),
	214: uint8(0x01),
	215: uint8(0x18),
}
var _namePages = [256]uint8{
	0:   uint8(0x19),
	1:   uint8(0x03),
	2:   uint8(0x1A),
	3:   uint8(0x1B),
	4:   uint8(0x1C),
	5:   uint8(0x1D),
	6:   uint8(0x1E),
	9:   uint8(0x1F),
	10:  uint8(0x20),
	11:  uint8(0x21),
	12:  uint8(0x22),
	13:  uint8(0x23),
	14:  uint8(0x24),
	15:  uint8(0x25),
	16:  uint8(0x10),
	17:  uint8(0x11),
	30:  uint8(0x12),
	31:  uint8(0x13),
	32:  uint8(0x26),
	33:  uint8(0x14),
	48:  uint8(0x27),
	49:  uint8(0x16),
	78:  uint8(0x01),
	79:  uint8(0x01),
	80:  uint8(0x01),
	81:  uint8(0x01),
	82:  uint8(0x01),
	83:  uint8(0x01),
	84:  uint8(0x01),
	85:  uint8(0x01),
	86:  uint8(0x01),
	87:  uint8(0x01),
	88:  uint8(0x01),
	89:  uint8(0x01),
	90:  uint8(0x01),
	91:  uint8(0x01),
	92:  uint8(0x01),
	93:  uint8(0x01),
	94:  uint8(0x01),
	95:  uint8(0x01),
	96:  uint8(0x01),
	97:  uint8(0x01),
	98:  uint8(0x01),
	99:  uint8(0x01),
	100: uint8(0x01),
	101: uint8(0x01),
	102: uint8(0x01),
	103: uint8(0x01),
	104: uint8(0x01),
	105: uint8(0x01),
	106: uint8(0x01),
	107: uint8(0x01),
	108: uint8(0x01),
	109: uint8(0x01),
	110: uint8(0x01),
	111: uint8(0x01),
	112: uint8(0x01),
	113: uint8(0x01),
	114: uint8(0x01),
	115: uint8(0x01),
	116: uint8(0x01),
	117: uint8(0x01),
	118: uint8(0x01),
	119: uint8(0x01),
	120: uint8(0x01),
	121: uint8(0x01),
	122: uint8(0x01),
	123: uint8(0x01),
	124: uint8(0x01),
	125: uint8(0x01),
	126: uint8(0x01),
	127: uint8(0x01),
	128: uint8(0x01),
	129: uint8(0x01),
	130: uint8(0x01),
	131: uint8(0x01),
	132: uint8(0x01),
	133: uint8(0x01),
	134: uint8(0x01),
	135: uint8(0x01),
	136: uint8(0x01),
	137: uint8(0x01),
	138: uint8(0x01),
	139: uint8(0x01),
	140: uint8(0x01),
	141: uint8(0x01),
	142: uint8(0x01),
	143: uint8(0x01),
	144: uint8(0x01),
	145: uint8(0x01),
	146: uint8(0x01),
	147: uint8(0x01),
	148: uint8(0x01),
	149: uint8(0x01),
	150: uint8(0x01),
	151: uint8(0x01),
	152: uint8(0x01),
	153: uint8(0x01),
	154: uint8(0x01),
	155: uint8(0x01),
	156: uint8(0x01),
	157: uint8(0x01),
	158: uint8(0x01),
	159: uint8(0x17),
	172: uint8(0x01),
	173: uint8(0x01),
	174: uint8(0x01),
	175: uint8(0x01),
	176: uint8(0x01),
	177: uint8(0x01),
	178: uint8(0x01),
	179: uint8(0x01),
	180: uint8(0x01),
	181: uint8(0x01),
	182: uint8(0x01),
	183: uint8(0x01),
	184: uint8(0x01),
	185: uint8(0x01),
	186: uint8(0x01),
	187: uint8(0x01),
	188: uint8(0x01),
	189: uint8(0x01),
	190: uint8(0x01),
	191: uint8(0x01),
	192: uint8(0x01),
	193: uint8(0x01),
	194: uint8(0x01),
	195: uint8(0x01),
	196: uint8(0x01),
	197: uint8(0x01),
	198: uint8(0x01),
	199: uint8(0x01),
	200: uint8(0x01),
	201: uint8(0x01),
	202: uint8(0x01),
	203: uint8(0x01),
	204: uint8(0x01),
	205: uint8(0x01),
	206: uint8(0x01),
	207: uint8(0x01),
	208: uint8(0x01),
	209: uint8(0x01),
	210: uint8(0x01),
	211: uint8(0x01),
	212: uint8(0x01),
	213: uint8(0x01),
	214: uint8(0x01),
	215: uint8(0x18),
}

/* A 2 byte UTF-8 representation splits the characters 11 bits between
   the bottom 5 and 6 bits of the bytes.  We need 8 bits to index into
   pages, 3 bits to add to that index and 5 bits to generate the mask.
*/

/* A 3 byte UTF-8 representation splits the characters 16 bits between
   the bottom 4, 6 and 6 bits of the bytes.  We need 8 bits to index
   into pages, 3 bits to add to that index and 5 bits to generate the
   mask.
*/

/* Detection of invalid UTF-8 sequences is based on Table 3.1B
   of Unicode 3.2: http://www.unicode.org/unicode/reports/tr28/
   with the additional restriction of not allowing the Unicode
   code points 0xFFFF and 0xFFFE (sequences EF,BF,BF and EF,BF,BE).
   Implementation details:
     (A & 0x80) == 0     means A < 0x80
   and
     (A & 0xC0) == 0xC0  means A > 0xBF
*/

func _isNever(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	_ = enc
	_ = p
	return 0
}

func _utf8_isName2(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	_ = enc
	return libc.Int32FromUint32(_namingBitmap[libc.Int32FromUint8(_namePages[libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p)))>>int32(2)&int32(7)])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p)))&int32(3)<<int32(1)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))>>int32(5)&int32(1)] & (libc.Uint32FromUint32(1) << (libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) & libc.Int32FromInt32(0x1F))))
}

func _utf8_isName3(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	_ = enc
	return libc.Int32FromUint32(_namingBitmap[libc.Int32FromUint8(_namePages[libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p)))&int32(0xF)<<int32(4)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))>>int32(2)&int32(0xF)])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(3)<<int32(1)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2)))>>int32(5)&int32(1)] & (libc.Uint32FromUint32(1) << (libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2))) & libc.Int32FromInt32(0x1F))))
}

func _utf8_isNmstrt2(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	_ = enc
	return libc.Int32FromUint32(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p)))>>int32(2)&int32(7)])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p)))&int32(3)<<int32(1)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))>>int32(5)&int32(1)] & (libc.Uint32FromUint32(1) << (libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) & libc.Int32FromInt32(0x1F))))
}

func _utf8_isNmstrt3(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	_ = enc
	return libc.Int32FromUint32(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p)))&int32(0xF)<<int32(4)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))>>int32(2)&int32(0xF)])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(3)<<int32(1)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2)))>>int32(5)&int32(1)] & (libc.Uint32FromUint32(1) << (libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2))) & libc.Int32FromInt32(0x1F))))
}

func _utf8_isInvalid2(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	_ = enc
	return libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p))) < int32(0xC2) || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0x80) == 0 || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0xC0) == int32(0xC0))
}

func _utf8_isInvalid3(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	var v1, v3, v4 int32
	var v2, v5, v6 bool
	_, _, _, _, _, _ = v1, v2, v3, v4, v5, v6
	_ = enc
	if v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2)))&int32(0x80) == 0; !v2 {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p))) == int32(0xEF) && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) == int32(0xBF) {
			v1 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2))) > int32(0xBD))
		} else {
			v1 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2)))&int32(0xC0) == int32(0xC0))
		}
	}
	if v6 = v2 || v1 != 0; !v6 {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p))) == int32(0xE0) {
			v3 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) < int32(0xA0) || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0xC0) == int32(0xC0))
		} else {
			if v5 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0x80) == 0; !v5 {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p))) == int32(0xED) {
					v4 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) > int32(0x9F))
				} else {
					v4 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0xC0) == int32(0xC0))
				}
			}
			v3 = libc.BoolInt32(v5 || v4 != 0)
		}
	}
	return libc.BoolInt32(v6 || v3 != 0)
}

func _utf8_isInvalid4(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	var v1, v2 int32
	var v3, v4 bool
	_, _, _, _ = v1, v2, v3, v4
	_ = enc
	if v4 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 3)))&int32(0x80) == 0 || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 3)))&int32(0xC0) == int32(0xC0) || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2)))&int32(0x80) == 0 || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 2)))&int32(0xC0) == int32(0xC0); !v4 {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p))) == int32(0xF0) {
			v1 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) < int32(0x90) || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0xC0) == int32(0xC0))
		} else {
			if v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0x80) == 0; !v3 {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p))) == int32(0xF4) {
					v2 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1))) > int32(0x8F))
				} else {
					v2 = libc.BoolInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(p + 1)))&int32(0xC0) == int32(0xC0))
				}
			}
			v1 = libc.BoolInt32(v3 || v2 != 0)
		}
	}
	return libc.BoolInt32(v4 || v1 != 0)
}

type Tnormal_encoding = struct {
	Fenc        TENCODING
	Ftype1      [256]uint8
	FisName2    uintptr
	FisName3    uintptr
	FisName4    uintptr
	FisNmstrt2  uintptr
	FisNmstrt3  uintptr
	FisNmstrt4  uintptr
	FisInvalid2 uintptr
	FisInvalid3 uintptr
	FisInvalid4 uintptr
}

const _BT_NONXML = 0
const _BT_MALFORM = 1
const _BT_LT = 2
const _BT_AMP = 3
const _BT_RSQB = 4
const _BT_LEAD2 = 5
const _BT_LEAD3 = 6
const _BT_LEAD4 = 7
const _BT_TRAIL = 8
const _BT_CR = 9
const _BT_LF = 10
const _BT_GT = 11
const _BT_QUOT = 12
const _BT_APOS = 13
const _BT_EQUALS = 14
const _BT_QUEST = 15
const _BT_EXCL = 16
const _BT_SOL = 17
const _BT_SEMI = 18
const _BT_NUM = 19
const _BT_LSQB = 20
const _BT_S = 21
const _BT_NMSTRT = 22
const _BT_COLON = 23
const _BT_HEX = 24
const _BT_DIGIT = 25
const _BT_NAME = 26
const _BT_MINUS = 27
const _BT_OTHER = 28
const _BT_NONASCII = 29
const _BT_PERCNT = 30
const _BT_LPAR = 31
const _BT_RPAR = 32
const _BT_AST = 33
const _BT_PLUS = 34
const _BT_COMMA = 35
const _BT_VERBAR = 36

/*
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* minimum bytes per character */

/* c is an ASCII character */

/* This file is included!
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* ptr points to character following "<!-" */

func _normal_scanComment(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_MINUS1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(1)
		for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
			case int32(_BT_LEAD2):
				if int64(end)-int64(ptr) < int64(2) {
					return -int32(2)
				}
				if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(ptr) < int64(3) {
					return -int32(2)
				}
				if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(ptr) < int64(4) {
					return -int32(2)
				}
				if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(4)
			case int32(_BT_NONXML):
				fallthrough
			case int32(_BT_MALFORM):
				fallthrough
			case int32(_BT_TRAIL):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			case int32(_BT_MINUS):
				ptr += uintptr(1)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_MINUS1) {
					ptr += uintptr(1)
					if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
						return -int32(1)
					}
					if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_GT1)) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
					return int32(m_XML_TOK_COMMENT)
				}
			default:
				ptr += uintptr(1)
				break
			}
		}
	}
	return -int32(1)
}

/* ptr points to character following "<!" */

func _normal_scanDecl(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_MINUS):
		return _normal_scanComment(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	case int32(_BT_LSQB):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_COND_SECT_OPEN)
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_PERCNT):
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(1))) {
				return -int32(1)
			}
			/* don't allow <!ENTITY% foo "whatever"> */
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(1))))))) {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_PERCNT):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			/* fall through */
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DECL_OPEN)
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			ptr += uintptr(1)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _normal_checkPiTarget(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, tokPtr uintptr) (r int32) {
	var upper int32
	_ = upper
	upper = 0
	_ = enc
	*(*int32)(unsafe.Pointer(tokPtr)) = int32(m_XML_TOK_PI)
	if int64(end)-int64(ptr) != int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(3)) {
		return int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
	case int32(m_ASCII_x):
	case int32(m_ASCII_X):
		upper = int32(1)
	default:
		return int32(1)
	}
	ptr += uintptr(1)
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
	case int32(m_ASCII_m):
	case int32(m_ASCII_M):
		upper = int32(1)
	default:
		return int32(1)
	}
	ptr += uintptr(1)
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
	case int32(m_ASCII_l):
	case int32(m_ASCII_L):
		upper = int32(1)
	default:
		return int32(1)
	}
	if upper != 0 {
		return 0
	}
	*(*int32)(unsafe.Pointer(tokPtr)) = int32(m_XML_TOK_XML_DECL)
	return int32(1)
}

/* ptr points to character following "<?" */

func _normal_scanPi(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var target uintptr
	var _ /* tok at bp+0 */ int32
	_ = target
	target = ptr
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_NONASCII):
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			if !(_normal_checkPiTarget(tls, enc, target, ptr, bp) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(1)
			for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
				switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
				case int32(_BT_LEAD2):
					if int64(end)-int64(ptr) < int64(2) {
						return -int32(2)
					}
					if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(2)
				case int32(_BT_LEAD3):
					if int64(end)-int64(ptr) < int64(3) {
						return -int32(2)
					}
					if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(3)
				case int32(_BT_LEAD4):
					if int64(end)-int64(ptr) < int64(4) {
						return -int32(2)
					}
					if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(4)
				case int32(_BT_NONXML):
					fallthrough
				case int32(_BT_MALFORM):
					fallthrough
				case int32(_BT_TRAIL):
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				case int32(_BT_QUEST):
					ptr += uintptr(1)
					if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
						return -int32(1)
					}
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
						return *(*int32)(unsafe.Pointer(bp))
					}
				default:
					ptr += uintptr(1)
					break
				}
			}
			return -int32(1)
		case int32(_BT_QUEST):
			if !(_normal_checkPiTarget(tls, enc, target, ptr, bp) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(1)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
				return *(*int32)(unsafe.Pointer(bp))
			}
			/* fall through */
			fallthrough
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _normal_scanCdataSection(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var i int32
	_ = i
	_ = enc
	/* CDATA[ */
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(6)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	i = 0
	for {
		if !(i < int32(6)) {
			break
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromUint8(_CDATA_LSQB[i])) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _1
	_1:
		;
		i++
		ptr += uintptr(1)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_CDATA_SECT_OPEN)
}

var _CDATA_LSQB = [6]uint8{
	0: uint8(m_ASCII_C),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_A),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_A),
	5: uint8(m_ASCII_LSQB1),
}

func _normal_cdataSectionTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	_ = n
	if ptr >= end {
		return -int32(4)
	}
	if int32(1) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(1)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(1) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_RSQB):
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_RSQB1)) {
			break
		}
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_GT1)) {
			ptr -= uintptr(1)
			break
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_CDATA_SECT_CLOSE)
	case int32(_BT_CR):
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) == int32(_BT_LF) {
			ptr += uintptr(1)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LF):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NONXML):
		fallthrough
	case int32(_BT_MALFORM):
		fallthrough
	case int32(_BT_TRAIL):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	default:
		ptr += uintptr(1)
		break
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) || (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) || (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) || (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_RSQB):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(1)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

/* ptr points to character following "</" */

func _normal_scanEndTag(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_NONASCII):
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			goto _1
		case int32(_BT_MINUS):
			goto _2
		case int32(_BT_NAME):
			goto _3
		case int32(_BT_DIGIT):
			goto _4
		case int32(_BT_HEX):
			goto _5
		case int32(_BT_NMSTRT):
			goto _6
		case int32(_BT_LEAD2):
			goto _7
		case int32(_BT_LEAD3):
			goto _8
		case int32(_BT_LEAD4):
			goto _9
		case int32(_BT_LF):
			goto _10
		case int32(_BT_CR):
			goto _11
		case int32(_BT_S):
			goto _12
		case int32(_BT_COLON):
			goto _13
		case int32(_BT_GT):
			goto _14
		default:
			goto _15
		}
		goto _16
	_1:
		;
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
	_6:
		;
	_5:
		;
	_4:
		;
	_3:
		;
	_2:
		;
		ptr += uintptr(1)
		goto _16
	_7:
		;
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		goto _16
	_8:
		;
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
		goto _16
	_9:
		;
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
		goto _16
	_12:
		;
	_11:
		;
	_10:
		;
		ptr += uintptr(1)
	_19:
		;
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			goto _17
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
		case int32(_BT_GT):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
			return int32(m_XML_TOK_END_TAG)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _18
	_18:
		;
		ptr += uintptr(1)
		goto _19
		goto _17
	_17:
		;
		return -int32(1)
	_13:
		;
		/* no need to check qname syntax here,
		   since end-tag must match exactly */
		ptr += uintptr(1)
		goto _16
	_14:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_END_TAG)
	_15:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	_16:
	}
	return -int32(1)
}

/* ptr points to character following "&#X" */

func _normal_scanHexCharRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_HEX):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(1)
		for {
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				break
			}
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
			case int32(_BT_DIGIT):
				fallthrough
			case int32(_BT_HEX):
			case int32(_BT_SEMI):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
				return int32(m_XML_TOK_CHAR_REF)
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			goto _1
		_1:
			;
			ptr += uintptr(1)
		}
	}
	return -int32(1)
}

/* ptr points to character following "&#" */

func _normal_scanCharRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_x) {
			return _normal_scanHexCharRef(tls, enc, ptr+uintptr(1), end, nextTokPtr)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_DIGIT):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(1)
		for {
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				break
			}
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
			case int32(_BT_DIGIT):
			case int32(_BT_SEMI):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
				return int32(m_XML_TOK_CHAR_REF)
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			goto _1
		_1:
			;
			ptr += uintptr(1)
		}
	}
	return -int32(1)
}

/* ptr points to character following "&" */

func _normal_scanRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_NONASCII):
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NUM):
		return _normal_scanCharRef(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_SEMI):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
			return int32(m_XML_TOK_ENTITY_REF)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

/* ptr points to character following first character of attribute name */

func _normal_scanAtts(tls *libc.TLS, enc uintptr, _ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var hadColon, open, t, t1, tok int32
	_, _, _, _, _ = hadColon, open, t, t1, tok
	hadColon = 0
	for int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))))) {
		case int32(_BT_NONASCII):
			goto _1
		case int32(_BT_MINUS):
			goto _2
		case int32(_BT_NAME):
			goto _3
		case int32(_BT_DIGIT):
			goto _4
		case int32(_BT_HEX):
			goto _5
		case int32(_BT_NMSTRT):
			goto _6
		case int32(_BT_LEAD2):
			goto _7
		case int32(_BT_LEAD3):
			goto _8
		case int32(_BT_LEAD4):
			goto _9
		case int32(_BT_COLON):
			goto _10
		case int32(_BT_LF):
			goto _11
		case int32(_BT_CR):
			goto _12
		case int32(_BT_S):
			goto _13
		case int32(_BT_EQUALS):
			goto _14
		default:
			goto _15
		}
		goto _16
	_1:
		;
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		} /* fall through */
	_6:
		;
	_5:
		;
	_4:
		;
	_3:
		;
	_2:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		goto _16
	_7:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _16
	_8:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		goto _16
	_9:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		goto _16
	_10:
		;
		if hadColon != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		hadColon = int32(1)
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		goto _16
	_13:
		;
	_12:
		;
	_11:
		;
	_19:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		t = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
		if t == int32(_BT_EQUALS) {
			goto _17
		}
		switch t {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_CR):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		goto _18
	_18:
		;
		goto _19
		goto _17
	_17:
		;
		/* fall through */
	_14:
		;
		hadColon = 0
		for {
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
			if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				return -int32(1)
			}
			open = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
			if open == int32(_BT_QUOT) || open == int32(_BT_APOS) {
				break
			}
			switch open {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_CR):
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			goto _20
		_20:
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		/* in attribute value */
		for {
			if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				return -int32(1)
			}
			t1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
			if t1 == open {
				break
			}
			switch t1 {
			case int32(_BT_LEAD2):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
					return -int32(2)
				}
				if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
					return -int32(2)
				}
				if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
					return -int32(2)
				}
				if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
			case int32(_BT_NONXML):
				fallthrough
			case int32(_BT_MALFORM):
				fallthrough
			case int32(_BT_TRAIL):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			case int32(_BT_AMP):
				tok = _normal_scanRef(tls, enc, *(*uintptr)(unsafe.Pointer(bp))+uintptr(1), end, bp)
				if tok <= 0 {
					if tok == m_XML_TOK_INVALID {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					}
					return tok
				}
			case int32(_BT_LT):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			default:
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
				break
			}
			goto _21
		_21:
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))))) {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
		case int32(_BT_SOL):
			goto sol
		case int32(_BT_GT):
			goto gt
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		/* ptr points to closing quote */
	_24:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))))) {
		case int32(_BT_NONASCII):
			goto _25
		case int32(_BT_HEX):
			goto _26
		case int32(_BT_NMSTRT):
			goto _27
		case int32(_BT_LEAD2):
			goto _28
		case int32(_BT_LEAD3):
			goto _29
		case int32(_BT_LEAD4):
			goto _30
		case int32(_BT_LF):
			goto _31
		case int32(_BT_CR):
			goto _32
		case int32(_BT_S):
			goto _33
		case int32(_BT_GT):
			goto _34
		case int32(_BT_SOL):
			goto _35
		default:
			goto _36
		}
		goto _37
	_25:
		;
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		} /* fall through */
	_27:
		;
	_26:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		goto _37
	_28:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _37
	_29:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		goto _37
	_30:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		goto _37
	_33:
		;
	_32:
		;
	_31:
		;
		goto _23
	_34:
		;
		goto gt
	gt:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp)) + uintptr(1)
		return int32(m_XML_TOK_START_TAG_WITH_ATTS)
	_35:
		;
		goto sol
	sol:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(1)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == libc.Int32FromInt32(m_ASCII_GT1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp)) + uintptr(1)
		return int32(m_XML_TOK_EMPTY_ELEMENT_WITH_ATTS)
	_36:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return m_XML_TOK_INVALID
	_37:
		;
		goto _22
		goto _23
	_23:
		;
		goto _24
		goto _22
	_22:
		;
		goto _16
	_15:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return m_XML_TOK_INVALID
	_16:
	}
	return -int32(1)
}

/* ptr points to character following "<" */

func _normal_scanLt(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var hadColon int32
	_ = hadColon
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_NONASCII):
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_EXCL):
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_MINUS):
			return _normal_scanComment(tls, enc, ptr+uintptr(1), end, nextTokPtr)
		case int32(_BT_LSQB):
			return _normal_scanCdataSection(tls, enc, ptr+uintptr(1), end, nextTokPtr)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	case int32(_BT_QUEST):
		return _normal_scanPi(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	case int32(_BT_SOL):
		return _normal_scanEndTag(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	hadColon = 0
	/* we have a start-tag */
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			goto _1
		case int32(_BT_MINUS):
			goto _2
		case int32(_BT_NAME):
			goto _3
		case int32(_BT_DIGIT):
			goto _4
		case int32(_BT_HEX):
			goto _5
		case int32(_BT_NMSTRT):
			goto _6
		case int32(_BT_LEAD2):
			goto _7
		case int32(_BT_LEAD3):
			goto _8
		case int32(_BT_LEAD4):
			goto _9
		case int32(_BT_COLON):
			goto _10
		case int32(_BT_LF):
			goto _11
		case int32(_BT_CR):
			goto _12
		case int32(_BT_S):
			goto _13
		case int32(_BT_GT):
			goto _14
		case int32(_BT_SOL):
			goto _15
		default:
			goto _16
		}
		goto _17
	_1:
		;
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
	_6:
		;
	_5:
		;
	_4:
		;
	_3:
		;
	_2:
		;
		ptr += uintptr(1)
		goto _17
	_7:
		;
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		goto _17
	_8:
		;
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
		goto _17
	_9:
		;
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
		goto _17
	_10:
		;
		if hadColon != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		hadColon = int32(1)
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			ptr += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _17
	_13:
		;
	_12:
		;
	_11:
		;
		ptr += uintptr(1)
		for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
			case int32(_BT_NONASCII):
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				} /* fall through */
				fallthrough
			case int32(_BT_NMSTRT):
				fallthrough
			case int32(_BT_HEX):
				ptr += uintptr(1)
			case int32(_BT_LEAD2):
				if int64(end)-int64(ptr) < int64(2) {
					return -int32(2)
				}
				if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(ptr) < int64(3) {
					return -int32(2)
				}
				if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(ptr) < int64(4) {
					return -int32(2)
				}
				if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(4)
			case int32(_BT_GT):
				goto gt
			case int32(_BT_SOL):
				goto sol
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				ptr += uintptr(1)
				continue
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			return _normal_scanAtts(tls, enc, ptr, end, nextTokPtr)
		}
		return -int32(1)
	_14:
		;
		goto gt
	gt:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_START_TAG_NO_ATTS)
	_15:
		;
		goto sol
	sol:
		;
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_GT1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_EMPTY_ELEMENT_NO_ATTS)
	_16:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	_17:
	}
	return -int32(1)
}

func _normal_contentTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	_ = n
	if ptr >= end {
		return -int32(4)
	}
	if int32(1) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(1)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(1) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_LT):
		return _normal_scanLt(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	case int32(_BT_AMP):
		return _normal_scanRef(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	case int32(_BT_CR):
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(3)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) == int32(_BT_LF) {
			ptr += uintptr(1)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LF):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_RSQB):
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(5)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_RSQB1)) {
			break
		}
		ptr += uintptr(1)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			return -int32(5)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_GT1)) {
			ptr -= uintptr(1)
			break
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NONXML):
		fallthrough
	case int32(_BT_MALFORM):
		fallthrough
	case int32(_BT_TRAIL):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	default:
		ptr += uintptr(1)
		break
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) || (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) || (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) || (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(4)
		case int32(_BT_RSQB):
			if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(1)) {
				if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(1)))) == libc.Int32FromInt32(m_ASCII_RSQB1)) {
					ptr += uintptr(1)
					break
				}
				if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(3)*libc.Int32FromInt32(1)) {
					if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(1))))) == libc.Int32FromInt32(m_ASCII_GT1)) {
						ptr += uintptr(1)
						break
					}
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(1))
					return m_XML_TOK_INVALID
				}
			}
			/* fall through */
			fallthrough
		case int32(_BT_AMP):
			fallthrough
		case int32(_BT_LT):
			fallthrough
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(1)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

/* ptr points to character following "%" */

func _normal_scanPercent(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_NONASCII):
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_S):
		fallthrough
	case int32(_BT_LF):
		fallthrough
	case int32(_BT_CR):
		fallthrough
	case int32(_BT_PERCNT):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_PERCENT)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_SEMI):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
			return int32(m_XML_TOK_PARAM_ENTITY_REF)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _normal_scanPoundName(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_NONASCII):
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(1)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_VERBAR):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_POUND_NAME)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(m_XML_TOK_POUND_NAME)
}

func _normal_scanLit(tls *libc.TLS, open int32, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var t int32
	_ = t
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		t = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		switch t {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_QUOT):
			fallthrough
		case int32(_BT_APOS):
			ptr += uintptr(1)
			if t != open {
				break
			}
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				return -int32(m_XML_TOK_LITERAL)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_GT):
				fallthrough
			case int32(_BT_PERCNT):
				fallthrough
			case int32(_BT_LSQB):
				return int32(m_XML_TOK_LITERAL)
			default:
				return m_XML_TOK_INVALID
			}
			fallthrough
		default:
			ptr += uintptr(1)
			break
		}
	}
	return -int32(1)
}

func _normal_prologTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var tok int32
	_, _ = n, tok
	if ptr >= end {
		return -int32(4)
	}
	if int32(1) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(1)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(1) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_QUOT):
		goto _1
	case int32(_BT_APOS):
		goto _2
	case int32(_BT_LT):
		goto _3
	case int32(_BT_CR):
		goto _4
	case int32(_BT_LF):
		goto _5
	case int32(_BT_S):
		goto _6
	case int32(_BT_PERCNT):
		goto _7
	case int32(_BT_COMMA):
		goto _8
	case int32(_BT_LSQB):
		goto _9
	case int32(_BT_RSQB):
		goto _10
	case int32(_BT_LPAR):
		goto _11
	case int32(_BT_RPAR):
		goto _12
	case int32(_BT_VERBAR):
		goto _13
	case int32(_BT_GT):
		goto _14
	case int32(_BT_NUM):
		goto _15
	case int32(_BT_LEAD2):
		goto _16
	case int32(_BT_LEAD3):
		goto _17
	case int32(_BT_LEAD4):
		goto _18
	case int32(_BT_HEX):
		goto _19
	case int32(_BT_NMSTRT):
		goto _20
	case int32(_BT_COLON):
		goto _21
	case int32(_BT_MINUS):
		goto _22
	case int32(_BT_NAME):
		goto _23
	case int32(_BT_DIGIT):
		goto _24
	case int32(_BT_NONASCII):
		goto _25
	default:
		goto _26
	}
	goto _27
_1:
	;
	return _normal_scanLit(tls, int32(_BT_QUOT), enc, ptr+uintptr(1), end, nextTokPtr)
_2:
	;
	return _normal_scanLit(tls, int32(_BT_APOS), enc, ptr+uintptr(1), end, nextTokPtr)
_3:
	;
	ptr += uintptr(1)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(1)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_EXCL):
		return _normal_scanDecl(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	case int32(_BT_QUEST):
		return _normal_scanPi(tls, enc, ptr+uintptr(1), end, nextTokPtr)
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		fallthrough
	case int32(_BT_NONASCII):
		fallthrough
	case int32(_BT_LEAD2):
		fallthrough
	case int32(_BT_LEAD3):
		fallthrough
	case int32(_BT_LEAD4):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr - uintptr(1)
		return int32(m_XML_TOK_INSTANCE_START)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_4:
	;
	if ptr+uintptr(1) == end {
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = end
		/* indicate that this might be part of a CR/LF pair */
		return -int32(m_XML_TOK_PROLOG_S)
	}
	/* fall through */
_6:
	;
_5:
	;
_30:
	;
	ptr += uintptr(1)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		goto _28
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_S):
		fallthrough
	case int32(_BT_LF):
	case int32(_BT_CR):
		/* don't split CR/LF pair */
		if ptr+uintptr(1) != end {
			break
		}
		/* fall through */
		fallthrough
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_PROLOG_S)
	}
	goto _29
_29:
	;
	goto _30
	goto _28
_28:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_PROLOG_S)
_7:
	;
	return _normal_scanPercent(tls, enc, ptr+uintptr(1), end, nextTokPtr)
_8:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
	return int32(m_XML_TOK_COMMA)
_9:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
	return int32(m_XML_TOK_OPEN_BRACKET)
_10:
	;
	ptr += uintptr(1)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(m_XML_TOK_CLOSE_BRACKET)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_RSQB1) {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(1))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(1)))) == int32(m_ASCII_GT1) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(1))
			return int32(m_XML_TOK_COND_SECT_CLOSE)
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_CLOSE_BRACKET)
_11:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
	return int32(m_XML_TOK_OPEN_PAREN)
_12:
	;
	ptr += uintptr(1)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
		return -int32(m_XML_TOK_CLOSE_PAREN)
	}
	switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
	case int32(_BT_AST):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_CLOSE_PAREN_ASTERISK)
	case int32(_BT_QUEST):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_CLOSE_PAREN_QUESTION)
	case int32(_BT_PLUS):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
		return int32(m_XML_TOK_CLOSE_PAREN_PLUS)
	case int32(_BT_CR):
		fallthrough
	case int32(_BT_LF):
		fallthrough
	case int32(_BT_S):
		fallthrough
	case int32(_BT_GT):
		fallthrough
	case int32(_BT_COMMA):
		fallthrough
	case int32(_BT_VERBAR):
		fallthrough
	case int32(_BT_RPAR):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_CLOSE_PAREN)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_13:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
	return int32(m_XML_TOK_OR)
_14:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
	return int32(m_XML_TOK_DECL_CLOSE)
_15:
	;
	return _normal_scanPoundName(tls, enc, ptr+uintptr(1), end, nextTokPtr)
_16:
	;
	if int64(end)-int64(ptr) < int64(2) {
		return -int32(2)
	}
	if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt2})))(tls, enc, ptr) != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NAME)
		goto _27
	}
	if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _27
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_17:
	;
	if int64(end)-int64(ptr) < int64(3) {
		return -int32(2)
	}
	if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt3})))(tls, enc, ptr) != 0 {
		ptr += uintptr(3)
		tok = int32(m_XML_TOK_NAME)
		goto _27
	}
	if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0 {
		ptr += uintptr(3)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _27
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_18:
	;
	if int64(end)-int64(ptr) < int64(4) {
		return -int32(2)
	}
	if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisNmstrt4})))(tls, enc, ptr) != 0 {
		ptr += uintptr(4)
		tok = int32(m_XML_TOK_NAME)
		goto _27
	}
	if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0 {
		ptr += uintptr(4)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _27
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_20:
	;
_19:
	;
	tok = int32(m_XML_TOK_NAME)
	ptr += uintptr(1)
	goto _27
_24:
	;
_23:
	;
_22:
	;
_21:
	;
	tok = int32(m_XML_TOK_NMTOKEN)
	ptr += uintptr(1)
	goto _27
_25:
	;
	if 0 != 0 {
		ptr += uintptr(1)
		tok = int32(m_XML_TOK_NAME)
		goto _27
	}
	if 0 != 0 {
		ptr += uintptr(1)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _27
	}
	/* fall through */
_26:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_27:
	;
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_NONASCII):
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(1)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_COMMA):
			fallthrough
		case int32(_BT_VERBAR):
			fallthrough
		case int32(_BT_LSQB):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return tok
		case int32(_BT_COLON):
			ptr += uintptr(1)
			switch tok {
			case int32(m_XML_TOK_NAME):
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
					return -int32(1)
				}
				tok = int32(m_XML_TOK_PREFIXED_NAME)
				switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
				case int32(_BT_NONASCII):
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					} /* fall through */
					fallthrough
				case int32(_BT_NMSTRT):
					fallthrough
				case int32(_BT_HEX):
					fallthrough
				case int32(_BT_DIGIT):
					fallthrough
				case int32(_BT_NAME):
					fallthrough
				case int32(_BT_MINUS):
					ptr += uintptr(1)
				case int32(_BT_LEAD2):
					if int64(end)-int64(ptr) < int64(2) {
						return -int32(2)
					}
					if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName2})))(tls, enc, ptr) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(2)
				case int32(_BT_LEAD3):
					if int64(end)-int64(ptr) < int64(3) {
						return -int32(2)
					}
					if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName3})))(tls, enc, ptr) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(3)
				case int32(_BT_LEAD4):
					if int64(end)-int64(ptr) < int64(4) {
						return -int32(2)
					}
					if !((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisName4})))(tls, enc, ptr) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(4)
				default:
					tok = int32(m_XML_TOK_NMTOKEN)
					break
				}
			case int32(m_XML_TOK_PREFIXED_NAME):
				tok = int32(m_XML_TOK_NMTOKEN)
				break
			}
		case int32(_BT_PLUS):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
			return int32(m_XML_TOK_NAME_PLUS)
		case int32(_BT_AST):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
			return int32(m_XML_TOK_NAME_ASTERISK)
		case int32(_BT_QUEST):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
			return int32(m_XML_TOK_NAME_QUESTION)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -tok
}

func _normal_attributeValueTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var start uintptr
	_ = start
	if ptr >= end {
		return -int32(4)
	} else {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the paranoia
			 * check is still valuable, however.
			 */
			return -int32(1) /* LCOV_EXCL_LINE */
		}
	}
	start = ptr
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_AMP):
			if ptr == start {
				return _normal_scanRef(tls, enc, ptr+uintptr(1), end, nextTokPtr)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_LT):
			/* this is for inside entity references */
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_LF):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_CR):
			if ptr == start {
				ptr += uintptr(1)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
					return -int32(3)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) == int32(_BT_LF) {
					ptr += uintptr(1)
				}
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_S):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
				return int32(m_XML_TOK_ATTRIBUTE_VALUE_S)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(1)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

func _normal_entityValueTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var start uintptr
	var tok, v1 int32
	_, _, _ = start, tok, v1
	if ptr >= end {
		return -int32(4)
	} else {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the paranoia
			 * check is still valuable, however.
			 */
			return -int32(1) /* LCOV_EXCL_LINE */
		}
	}
	start = ptr
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_AMP):
			if ptr == start {
				return _normal_scanRef(tls, enc, ptr+uintptr(1), end, nextTokPtr)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_PERCNT):
			if ptr == start {
				tok = _normal_scanPercent(tls, enc, ptr+uintptr(1), end, nextTokPtr)
				if tok == int32(m_XML_TOK_PERCENT) {
					v1 = m_XML_TOK_INVALID
				} else {
					v1 = tok
				}
				return v1
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_LF):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(1)
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_CR):
			if ptr == start {
				ptr += uintptr(1)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
					return -int32(3)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) == int32(_BT_LF) {
					ptr += uintptr(1)
				}
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(1)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

func _normal_ignoreSectionTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var level int32
	var n Tsize_t
	_, _ = level, n
	level = 0
	if int32(1) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(1)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(1) - libc.Int32FromInt32(1)))
			end = ptr + uintptr(n)
		}
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid2})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid3})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tnormal_encoding)(unsafe.Pointer(enc)).FisInvalid4})))(tls, enc, ptr) != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_LT):
			ptr += uintptr(1)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_EXCL) {
				ptr += uintptr(1)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_LSQB1) {
					level++
					ptr += uintptr(1)
				}
			}
		case int32(_BT_RSQB):
			ptr += uintptr(1)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_RSQB1) {
				ptr += uintptr(1)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1) {
					ptr += uintptr(1)
					if level == 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return int32(m_XML_TOK_IGNORE_SECT)
					}
					level--
				}
			}
		default:
			ptr += uintptr(1)
			break
		}
	}
	return -int32(1)
}

func _normal_isPublicId(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, badPtr uintptr) (r int32) {
	ptr += uintptr(1)
	end -= uintptr(1)
	for {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1))) {
			break
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_MINUS):
			fallthrough
		case int32(_BT_APOS):
			fallthrough
		case int32(_BT_LPAR):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_PLUS):
			fallthrough
		case int32(_BT_COMMA):
			fallthrough
		case int32(_BT_SOL):
			fallthrough
		case int32(_BT_EQUALS):
			fallthrough
		case int32(_BT_QUEST):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_SEMI):
			fallthrough
		case int32(_BT_EXCL):
			fallthrough
		case int32(_BT_AST):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_NUM):
			fallthrough
		case int32(_BT_COLON):
		case int32(_BT_S):
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_TAB1) {
				*(*uintptr)(unsafe.Pointer(badPtr)) = ptr
				return 0
			}
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_NMSTRT):
			if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) & ^libc.Int32FromInt32(0x7f) != 0) {
				break
			}
			/* fall through */
			fallthrough
		default:
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
			case int32(0x24): /* $ */
				fallthrough
			case int32(0x40): /* @ */
			default:
				*(*uintptr)(unsafe.Pointer(badPtr)) = ptr
				return 0
			}
			break
		}
		goto _1
	_1:
		;
		ptr += uintptr(1)
	}
	return int32(1)
}

/* This must only be called for a well-formed start-tag or empty
   element tag.  Returns the number of attributes.  Pointers to the
   first attsMax attributes are stored in atts.
*/

func _normal_getAtts(tls *libc.TLS, enc uintptr, ptr uintptr, attsMax int32, atts uintptr) (r int32) {
	var nAtts, open, state int32
	_, _, _ = nAtts, open, state
	state = 1
	nAtts = 0
	open = 0 /* defined when state == inValue;
	   initialization just to shut up compilers */
	ptr += uintptr(1)
	for {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(2) - libc.Int32FromInt32(1))
		case int32(_BT_LEAD3):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(3) - libc.Int32FromInt32(1))
		case int32(_BT_LEAD4):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(4) - libc.Int32FromInt32(1))
		case int32(_BT_NONASCII):
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
		case int32(_BT_QUOT):
			if state != 2 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr = ptr + uintptr(1)
				}
				state = 2
				open = int32(_BT_QUOT)
			} else {
				if open == int32(_BT_QUOT) {
					state = 0
					if nAtts < attsMax {
						(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvalueEnd = ptr
					}
					nAtts++
				}
			}
		case int32(_BT_APOS):
			if state != 2 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr = ptr + uintptr(1)
				}
				state = 2
				open = int32(_BT_APOS)
			} else {
				if open == int32(_BT_APOS) {
					state = 0
					if nAtts < attsMax {
						(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvalueEnd = ptr
					}
					nAtts++
				}
			}
		case int32(_BT_AMP):
			if nAtts < attsMax {
				(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
			}
		case int32(_BT_S):
			if state == 1 {
				state = 0
			} else {
				if state == 2 && nAtts < attsMax && (*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized != 0 && (ptr == (*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) != int32(m_ASCII_SPACE1) || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(1)))) == int32(m_ASCII_SPACE1) || libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(1))))))) == open) {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
				}
			}
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			/* This case ensures that the first attribute name is counted
			   Apart from that we could just change state on the quote. */
			if state == 1 {
				state = 0
			} else {
				if state == 2 && nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
				}
			}
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_SOL):
			if state != 2 {
				return nAtts
			}
		default:
			break
		}
		goto _1
	_1:
		;
		ptr += uintptr(1)
	}
	/* not reached */
	return r
}

func _normal_charRefNumber(tls *libc.TLS, enc uintptr, ptr uintptr) (r int32) {
	var c, c1, result int32
	_, _, _ = c, c1, result
	result = 0
	/* skip &# */
	_ = enc
	ptr += uintptr(libc.Int32FromInt32(2) * libc.Int32FromInt32(1))
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_x) {
		ptr += uintptr(1)
		for {
			if !!(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_SEMI1)) {
				break
			}
			c = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			switch c {
			case int32(m_ASCII_0):
				fallthrough
			case int32(m_ASCII_1):
				fallthrough
			case int32(m_ASCII_2):
				fallthrough
			case int32(m_ASCII_3):
				fallthrough
			case int32(m_ASCII_41):
				fallthrough
			case int32(m_ASCII_51):
				fallthrough
			case int32(m_ASCII_61):
				fallthrough
			case int32(m_ASCII_71):
				fallthrough
			case int32(m_ASCII_8):
				fallthrough
			case int32(m_ASCII_9):
				result <<= int32(4)
				result |= c - int32(m_ASCII_0)
			case int32(m_ASCII_A):
				fallthrough
			case int32(m_ASCII_B1):
				fallthrough
			case int32(m_ASCII_C):
				fallthrough
			case int32(m_ASCII_D):
				fallthrough
			case int32(m_ASCII_E):
				fallthrough
			case int32(m_ASCII_F):
				result <<= int32(4)
				result += int32(10) + (c - int32(m_ASCII_A))
			case int32(m_ASCII_a):
				fallthrough
			case int32(m_ASCII_b1):
				fallthrough
			case int32(m_ASCII_c):
				fallthrough
			case int32(m_ASCII_d1):
				fallthrough
			case int32(m_ASCII_e):
				fallthrough
			case int32(m_ASCII_f1):
				result <<= int32(4)
				result += int32(10) + (c - int32(m_ASCII_a))
				break
			}
			if result >= int32(0x110000) {
				return -int32(1)
			}
			goto _1
		_1:
			;
			ptr += uintptr(1)
		}
	} else {
		for {
			if !!(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromInt32(m_ASCII_SEMI1)) {
				break
			}
			c1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			result *= int32(10)
			result += c1 - int32(m_ASCII_0)
			if result >= int32(0x110000) {
				return -int32(1)
			}
			goto _2
		_2:
			;
			ptr += uintptr(1)
		}
	}
	return _checkCharRefNumber(tls, result)
}

func _normal_predefinedEntityName(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr) (r int32) {
	_ = enc
	switch (int64(end) - int64(ptr)) / libc.Int64FromInt32(1) {
	case int64(2):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(1)))) == int32(m_ASCII_t) {
			switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
			case int32(m_ASCII_l):
				return int32(m_ASCII_LT1)
			case int32(m_ASCII_g):
				return int32(m_ASCII_GT1)
			}
		}
	case int64(3):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_a) {
			ptr += uintptr(1)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_m) {
				ptr += uintptr(1)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_p) {
					return int32(m_ASCII_AMP1)
				}
			}
		}
	case int64(4):
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
		case int32(m_ASCII_q1):
			ptr += uintptr(1)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_u1) {
				ptr += uintptr(1)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_o) {
					ptr += uintptr(1)
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_t) {
						return int32(m_ASCII_QUOT1)
					}
				}
			}
		case int32(m_ASCII_a):
			ptr += uintptr(1)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_p) {
				ptr += uintptr(1)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_o) {
					ptr += uintptr(1)
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_s) {
						return int32(m_ASCII_APOS1)
					}
				}
			}
			break
		}
	}
	return 0
}

func _normal_nameMatchesAscii(tls *libc.TLS, enc uintptr, ptr1 uintptr, end1 uintptr, ptr2 uintptr) (r int32) {
	_ = enc
	for {
		if !(*(*uint8)(unsafe.Pointer(ptr2)) != 0) {
			break
		}
		if int64(end1)-int64(ptr1) < int64(1) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the
			 * paranoia check is still valuable, however.
			 */
			return 0 /* LCOV_EXCL_LINE */
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr1))) == libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr2)))) {
			return 0
		}
		goto _1
	_1:
		;
		ptr1 += uintptr(1)
		ptr2++
	}
	return libc.BoolInt32(ptr1 == end1)
}

func _normal_nameLength(tls *libc.TLS, enc uintptr, ptr uintptr) (r int32) {
	var start uintptr
	_ = start
	start = ptr
	for {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_NONASCII):
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_COLON):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(1)
		default:
			return int32(int64(ptr) - int64(start))
		}
		goto _1
	_1:
	}
	return r
}

func _normal_skipS(tls *libc.TLS, enc uintptr, ptr uintptr) (r uintptr) {
	for {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_S):
			ptr += uintptr(1)
		default:
			return ptr
		}
		goto _1
	_1:
	}
	return r
}

func _normal_updatePosition(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, pos uintptr) {
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LF):
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber = uint64(0)
			(*TPOSITION)(unsafe.Pointer(pos)).FlineNumber++
			ptr += uintptr(1)
		case int32(_BT_CR):
			(*TPOSITION)(unsafe.Pointer(pos)).FlineNumber++
			ptr += uintptr(1)
			if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(1)) && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr)))))) == int32(_BT_LF) {
				ptr += uintptr(1)
			}
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber = uint64(0)
		default:
			ptr += uintptr(1)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
			break
		}
	}
}

const /* UTF8_cvalN is value of masked first byte of N byte sequence */
_UTF8_cval1 = 0
const _UTF8_cval2 = 192
const _UTF8_cval3 = 224
const _UTF8_cval4 = 240

func X_INTERNAL_trim_to_complete_utf8_characters(tls *libc.TLS, from uintptr, fromLimRef uintptr) {
	var fromLim uintptr
	var prev uint8
	var walked Tsize_t
	_, _, _ = fromLim, prev, walked
	fromLim = *(*uintptr)(unsafe.Pointer(fromLimRef))
	walked = uint64(0)
	for {
		if !(fromLim > from) {
			break
		}
		prev = *(*uint8)(unsafe.Pointer(fromLim + uintptr(-libc.Int32FromInt32(1))))
		if uint32(prev)&uint32(0xf8) == uint32(0xf0) { /* 4-byte character, lead by 0b11110xxx byte */
			if walked+uint64(1) >= uint64(4) {
				fromLim += uintptr(libc.Int32FromInt32(4) - libc.Int32FromInt32(1))
				break
			} else {
				walked = uint64(0)
			}
		} else {
			if uint32(prev)&uint32(0xf0) == uint32(0xe0) { /* 3-byte character, lead by 0b1110xxxx byte */
				if walked+uint64(1) >= uint64(3) {
					fromLim += uintptr(libc.Int32FromInt32(3) - libc.Int32FromInt32(1))
					break
				} else {
					walked = uint64(0)
				}
			} else {
				if uint32(prev)&uint32(0xe0) == uint32(0xc0) { /* 2-byte character, lead by 0b110xxxxx byte */
					if walked+uint64(1) >= uint64(2) {
						fromLim += uintptr(libc.Int32FromInt32(2) - libc.Int32FromInt32(1))
						break
					} else {
						walked = uint64(0)
					}
				} else {
					if uint32(prev)&uint32(0x80) == 0x00 { /* 1-byte character, matching 0b0xxxxxxx */
						break
					}
				}
			}
		}
		goto _1
	_1:
		;
		fromLim--
		walked++
	}
	*(*uintptr)(unsafe.Pointer(fromLimRef)) = fromLim
}

func _utf8_toUtf8(tls *libc.TLS, enc uintptr, fromP uintptr, _fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _fromLim
	var bytesAvailable, bytesStorable, bytesToCopy Tptrdiff_t
	var fromLimBefore uintptr
	var input_incomplete, output_exhausted uint8
	_, _, _, _, _, _ = bytesAvailable, bytesStorable, bytesToCopy, fromLimBefore, input_incomplete, output_exhausted
	input_incomplete = uint8(m_false)
	output_exhausted = uint8(m_false)
	/* Avoid copying partial characters (due to limited space). */
	bytesAvailable = int64(*(*uintptr)(unsafe.Pointer(bp))) - int64(*(*uintptr)(unsafe.Pointer(fromP)))
	bytesStorable = int64(toLim) - int64(*(*uintptr)(unsafe.Pointer(toP)))
	_ = enc
	if bytesAvailable > bytesStorable {
		*(*uintptr)(unsafe.Pointer(bp)) = *(*uintptr)(unsafe.Pointer(fromP)) + uintptr(bytesStorable)
		output_exhausted = uint8(m_true)
	}
	/* Avoid copying partial characters (from incomplete input). */
	fromLimBefore = *(*uintptr)(unsafe.Pointer(bp))
	X_INTERNAL_trim_to_complete_utf8_characters(tls, *(*uintptr)(unsafe.Pointer(fromP)), bp)
	if *(*uintptr)(unsafe.Pointer(bp)) < fromLimBefore {
		input_incomplete = uint8(m_true)
	}
	bytesToCopy = int64(*(*uintptr)(unsafe.Pointer(bp))) - int64(*(*uintptr)(unsafe.Pointer(fromP)))
	libc.Xmemcpy(tls, *(*uintptr)(unsafe.Pointer(toP)), *(*uintptr)(unsafe.Pointer(fromP)), libc.Uint64FromInt64(bytesToCopy))
	*(*uintptr)(unsafe.Pointer(fromP)) += uintptr(bytesToCopy)
	*(*uintptr)(unsafe.Pointer(toP)) += uintptr(bytesToCopy)
	if output_exhausted != 0 { /* needs to go first */
		return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	} else {
		if input_incomplete != 0 {
			return int32(_XML_CONVERT_INPUT_INCOMPLETE)
		} else {
			return int32(_XML_CONVERT_COMPLETED)
		}
	}
	return r
}

func _utf8_toUtf16(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var from, to, v1, v2, v3, v4 uintptr
	var n uint64
	var res _XML_Convert_Result
	_, _, _, _, _, _, _, _ = from, n, res, to, v1, v2, v3, v4
	res = int32(_XML_CONVERT_COMPLETED)
	to = *(*uintptr)(unsafe.Pointer(toP))
	from = *(*uintptr)(unsafe.Pointer(fromP))
	for from < fromLim && to < toLim {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(from)))))) {
		case int32(_BT_LEAD2):
			if int64(fromLim)-int64(from) < int64(2) {
				res = int32(_XML_CONVERT_INPUT_INCOMPLETE)
				goto after
			}
			v1 = to
			to += 2
			*(*uint16)(unsafe.Pointer(v1)) = libc.Uint16FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from)))&libc.Int32FromInt32(0x1f)<<libc.Int32FromInt32(6) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 1)))&libc.Int32FromInt32(0x3f))
			from += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(fromLim)-int64(from) < int64(3) {
				res = int32(_XML_CONVERT_INPUT_INCOMPLETE)
				goto after
			}
			v2 = to
			to += 2
			*(*uint16)(unsafe.Pointer(v2)) = libc.Uint16FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from)))&libc.Int32FromInt32(0xf)<<libc.Int32FromInt32(12) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 1)))&libc.Int32FromInt32(0x3f)<<libc.Int32FromInt32(6) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 2)))&libc.Int32FromInt32(0x3f))
			from += uintptr(3)
		case int32(_BT_LEAD4):
			if (int64(toLim)-int64(to))/2 < int64(2) {
				res = int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
				goto after
			}
			if int64(fromLim)-int64(from) < int64(4) {
				res = int32(_XML_CONVERT_INPUT_INCOMPLETE)
				goto after
			}
			n = libc.Uint64FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from)))&int32(0x7)<<int32(18) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 1)))&int32(0x3f)<<int32(12) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 2)))&int32(0x3f)<<int32(6) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 3)))&int32(0x3f))
			n -= uint64(0x10000)
			*(*uint16)(unsafe.Pointer(to)) = uint16(n>>libc.Int32FromInt32(10) | libc.Uint64FromInt32(0xD800))
			*(*uint16)(unsafe.Pointer(to + 1*2)) = uint16(n&libc.Uint64FromInt32(0x3FF) | libc.Uint64FromInt32(0xDC00))
			to += uintptr(2) * 2
			from += uintptr(4)
		default:
			v3 = to
			to += 2
			v4 = from
			from++
			*(*uint16)(unsafe.Pointer(v3)) = uint16(*(*uint8)(unsafe.Pointer(v4)))
			break
		}
	}
	if from < fromLim {
		res = int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	}
	goto after
after:
	;
	*(*uintptr)(unsafe.Pointer(fromP)) = from
	*(*uintptr)(unsafe.Pointer(toP)) = to
	return res
}

var _utf8_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
		FisUtf8:          uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_TRAIL),
		129: uint8(_BT_TRAIL),
		130: uint8(_BT_TRAIL),
		131: uint8(_BT_TRAIL),
		132: uint8(_BT_TRAIL),
		133: uint8(_BT_TRAIL),
		134: uint8(_BT_TRAIL),
		135: uint8(_BT_TRAIL),
		136: uint8(_BT_TRAIL),
		137: uint8(_BT_TRAIL),
		138: uint8(_BT_TRAIL),
		139: uint8(_BT_TRAIL),
		140: uint8(_BT_TRAIL),
		141: uint8(_BT_TRAIL),
		142: uint8(_BT_TRAIL),
		143: uint8(_BT_TRAIL),
		144: uint8(_BT_TRAIL),
		145: uint8(_BT_TRAIL),
		146: uint8(_BT_TRAIL),
		147: uint8(_BT_TRAIL),
		148: uint8(_BT_TRAIL),
		149: uint8(_BT_TRAIL),
		150: uint8(_BT_TRAIL),
		151: uint8(_BT_TRAIL),
		152: uint8(_BT_TRAIL),
		153: uint8(_BT_TRAIL),
		154: uint8(_BT_TRAIL),
		155: uint8(_BT_TRAIL),
		156: uint8(_BT_TRAIL),
		157: uint8(_BT_TRAIL),
		158: uint8(_BT_TRAIL),
		159: uint8(_BT_TRAIL),
		160: uint8(_BT_TRAIL),
		161: uint8(_BT_TRAIL),
		162: uint8(_BT_TRAIL),
		163: uint8(_BT_TRAIL),
		164: uint8(_BT_TRAIL),
		165: uint8(_BT_TRAIL),
		166: uint8(_BT_TRAIL),
		167: uint8(_BT_TRAIL),
		168: uint8(_BT_TRAIL),
		169: uint8(_BT_TRAIL),
		170: uint8(_BT_TRAIL),
		171: uint8(_BT_TRAIL),
		172: uint8(_BT_TRAIL),
		173: uint8(_BT_TRAIL),
		174: uint8(_BT_TRAIL),
		175: uint8(_BT_TRAIL),
		176: uint8(_BT_TRAIL),
		177: uint8(_BT_TRAIL),
		178: uint8(_BT_TRAIL),
		179: uint8(_BT_TRAIL),
		180: uint8(_BT_TRAIL),
		181: uint8(_BT_TRAIL),
		182: uint8(_BT_TRAIL),
		183: uint8(_BT_TRAIL),
		184: uint8(_BT_TRAIL),
		185: uint8(_BT_TRAIL),
		186: uint8(_BT_TRAIL),
		187: uint8(_BT_TRAIL),
		188: uint8(_BT_TRAIL),
		189: uint8(_BT_TRAIL),
		190: uint8(_BT_TRAIL),
		191: uint8(_BT_TRAIL),
		192: uint8(_BT_LEAD2),
		193: uint8(_BT_LEAD2),
		194: uint8(_BT_LEAD2),
		195: uint8(_BT_LEAD2),
		196: uint8(_BT_LEAD2),
		197: uint8(_BT_LEAD2),
		198: uint8(_BT_LEAD2),
		199: uint8(_BT_LEAD2),
		200: uint8(_BT_LEAD2),
		201: uint8(_BT_LEAD2),
		202: uint8(_BT_LEAD2),
		203: uint8(_BT_LEAD2),
		204: uint8(_BT_LEAD2),
		205: uint8(_BT_LEAD2),
		206: uint8(_BT_LEAD2),
		207: uint8(_BT_LEAD2),
		208: uint8(_BT_LEAD2),
		209: uint8(_BT_LEAD2),
		210: uint8(_BT_LEAD2),
		211: uint8(_BT_LEAD2),
		212: uint8(_BT_LEAD2),
		213: uint8(_BT_LEAD2),
		214: uint8(_BT_LEAD2),
		215: uint8(_BT_LEAD2),
		216: uint8(_BT_LEAD2),
		217: uint8(_BT_LEAD2),
		218: uint8(_BT_LEAD2),
		219: uint8(_BT_LEAD2),
		220: uint8(_BT_LEAD2),
		221: uint8(_BT_LEAD2),
		222: uint8(_BT_LEAD2),
		223: uint8(_BT_LEAD2),
		224: uint8(_BT_LEAD3),
		225: uint8(_BT_LEAD3),
		226: uint8(_BT_LEAD3),
		227: uint8(_BT_LEAD3),
		228: uint8(_BT_LEAD3),
		229: uint8(_BT_LEAD3),
		230: uint8(_BT_LEAD3),
		231: uint8(_BT_LEAD3),
		232: uint8(_BT_LEAD3),
		233: uint8(_BT_LEAD3),
		234: uint8(_BT_LEAD3),
		235: uint8(_BT_LEAD3),
		236: uint8(_BT_LEAD3),
		237: uint8(_BT_LEAD3),
		238: uint8(_BT_LEAD3),
		239: uint8(_BT_LEAD3),
		240: uint8(_BT_LEAD4),
		241: uint8(_BT_LEAD4),
		242: uint8(_BT_LEAD4),
		243: uint8(_BT_LEAD4),
		244: uint8(_BT_LEAD4),
		254: uint8(_BT_MALFORM),
		255: uint8(_BT_MALFORM),
	},
}

func init() {
	p := unsafe.Pointer(&_utf8_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_utf8_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_utf8_toUtf16)
	*(*uintptr)(unsafe.Add(p, 392)) = __ccgo_fp(_utf8_isName2)
	*(*uintptr)(unsafe.Add(p, 400)) = __ccgo_fp(_utf8_isName3)
	*(*uintptr)(unsafe.Add(p, 408)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 416)) = __ccgo_fp(_utf8_isNmstrt2)
	*(*uintptr)(unsafe.Add(p, 424)) = __ccgo_fp(_utf8_isNmstrt3)
	*(*uintptr)(unsafe.Add(p, 432)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 440)) = __ccgo_fp(_utf8_isInvalid2)
	*(*uintptr)(unsafe.Add(p, 448)) = __ccgo_fp(_utf8_isInvalid3)
	*(*uintptr)(unsafe.Add(p, 456)) = __ccgo_fp(_utf8_isInvalid4)
}

var _utf8_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
		FisUtf8:          uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_TRAIL),
		129: uint8(_BT_TRAIL),
		130: uint8(_BT_TRAIL),
		131: uint8(_BT_TRAIL),
		132: uint8(_BT_TRAIL),
		133: uint8(_BT_TRAIL),
		134: uint8(_BT_TRAIL),
		135: uint8(_BT_TRAIL),
		136: uint8(_BT_TRAIL),
		137: uint8(_BT_TRAIL),
		138: uint8(_BT_TRAIL),
		139: uint8(_BT_TRAIL),
		140: uint8(_BT_TRAIL),
		141: uint8(_BT_TRAIL),
		142: uint8(_BT_TRAIL),
		143: uint8(_BT_TRAIL),
		144: uint8(_BT_TRAIL),
		145: uint8(_BT_TRAIL),
		146: uint8(_BT_TRAIL),
		147: uint8(_BT_TRAIL),
		148: uint8(_BT_TRAIL),
		149: uint8(_BT_TRAIL),
		150: uint8(_BT_TRAIL),
		151: uint8(_BT_TRAIL),
		152: uint8(_BT_TRAIL),
		153: uint8(_BT_TRAIL),
		154: uint8(_BT_TRAIL),
		155: uint8(_BT_TRAIL),
		156: uint8(_BT_TRAIL),
		157: uint8(_BT_TRAIL),
		158: uint8(_BT_TRAIL),
		159: uint8(_BT_TRAIL),
		160: uint8(_BT_TRAIL),
		161: uint8(_BT_TRAIL),
		162: uint8(_BT_TRAIL),
		163: uint8(_BT_TRAIL),
		164: uint8(_BT_TRAIL),
		165: uint8(_BT_TRAIL),
		166: uint8(_BT_TRAIL),
		167: uint8(_BT_TRAIL),
		168: uint8(_BT_TRAIL),
		169: uint8(_BT_TRAIL),
		170: uint8(_BT_TRAIL),
		171: uint8(_BT_TRAIL),
		172: uint8(_BT_TRAIL),
		173: uint8(_BT_TRAIL),
		174: uint8(_BT_TRAIL),
		175: uint8(_BT_TRAIL),
		176: uint8(_BT_TRAIL),
		177: uint8(_BT_TRAIL),
		178: uint8(_BT_TRAIL),
		179: uint8(_BT_TRAIL),
		180: uint8(_BT_TRAIL),
		181: uint8(_BT_TRAIL),
		182: uint8(_BT_TRAIL),
		183: uint8(_BT_TRAIL),
		184: uint8(_BT_TRAIL),
		185: uint8(_BT_TRAIL),
		186: uint8(_BT_TRAIL),
		187: uint8(_BT_TRAIL),
		188: uint8(_BT_TRAIL),
		189: uint8(_BT_TRAIL),
		190: uint8(_BT_TRAIL),
		191: uint8(_BT_TRAIL),
		192: uint8(_BT_LEAD2),
		193: uint8(_BT_LEAD2),
		194: uint8(_BT_LEAD2),
		195: uint8(_BT_LEAD2),
		196: uint8(_BT_LEAD2),
		197: uint8(_BT_LEAD2),
		198: uint8(_BT_LEAD2),
		199: uint8(_BT_LEAD2),
		200: uint8(_BT_LEAD2),
		201: uint8(_BT_LEAD2),
		202: uint8(_BT_LEAD2),
		203: uint8(_BT_LEAD2),
		204: uint8(_BT_LEAD2),
		205: uint8(_BT_LEAD2),
		206: uint8(_BT_LEAD2),
		207: uint8(_BT_LEAD2),
		208: uint8(_BT_LEAD2),
		209: uint8(_BT_LEAD2),
		210: uint8(_BT_LEAD2),
		211: uint8(_BT_LEAD2),
		212: uint8(_BT_LEAD2),
		213: uint8(_BT_LEAD2),
		214: uint8(_BT_LEAD2),
		215: uint8(_BT_LEAD2),
		216: uint8(_BT_LEAD2),
		217: uint8(_BT_LEAD2),
		218: uint8(_BT_LEAD2),
		219: uint8(_BT_LEAD2),
		220: uint8(_BT_LEAD2),
		221: uint8(_BT_LEAD2),
		222: uint8(_BT_LEAD2),
		223: uint8(_BT_LEAD2),
		224: uint8(_BT_LEAD3),
		225: uint8(_BT_LEAD3),
		226: uint8(_BT_LEAD3),
		227: uint8(_BT_LEAD3),
		228: uint8(_BT_LEAD3),
		229: uint8(_BT_LEAD3),
		230: uint8(_BT_LEAD3),
		231: uint8(_BT_LEAD3),
		232: uint8(_BT_LEAD3),
		233: uint8(_BT_LEAD3),
		234: uint8(_BT_LEAD3),
		235: uint8(_BT_LEAD3),
		236: uint8(_BT_LEAD3),
		237: uint8(_BT_LEAD3),
		238: uint8(_BT_LEAD3),
		239: uint8(_BT_LEAD3),
		240: uint8(_BT_LEAD4),
		241: uint8(_BT_LEAD4),
		242: uint8(_BT_LEAD4),
		243: uint8(_BT_LEAD4),
		244: uint8(_BT_LEAD4),
		254: uint8(_BT_MALFORM),
		255: uint8(_BT_MALFORM),
	},
}

func init() {
	p := unsafe.Pointer(&_utf8_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_utf8_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_utf8_toUtf16)
	*(*uintptr)(unsafe.Add(p, 392)) = __ccgo_fp(_utf8_isName2)
	*(*uintptr)(unsafe.Add(p, 400)) = __ccgo_fp(_utf8_isName3)
	*(*uintptr)(unsafe.Add(p, 408)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 416)) = __ccgo_fp(_utf8_isNmstrt2)
	*(*uintptr)(unsafe.Add(p, 424)) = __ccgo_fp(_utf8_isNmstrt3)
	*(*uintptr)(unsafe.Add(p, 432)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 440)) = __ccgo_fp(_utf8_isInvalid2)
	*(*uintptr)(unsafe.Add(p, 448)) = __ccgo_fp(_utf8_isInvalid3)
	*(*uintptr)(unsafe.Add(p, 456)) = __ccgo_fp(_utf8_isInvalid4)
}

var _internal_utf8_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
		FisUtf8:          uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_S),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_TRAIL),
		129: uint8(_BT_TRAIL),
		130: uint8(_BT_TRAIL),
		131: uint8(_BT_TRAIL),
		132: uint8(_BT_TRAIL),
		133: uint8(_BT_TRAIL),
		134: uint8(_BT_TRAIL),
		135: uint8(_BT_TRAIL),
		136: uint8(_BT_TRAIL),
		137: uint8(_BT_TRAIL),
		138: uint8(_BT_TRAIL),
		139: uint8(_BT_TRAIL),
		140: uint8(_BT_TRAIL),
		141: uint8(_BT_TRAIL),
		142: uint8(_BT_TRAIL),
		143: uint8(_BT_TRAIL),
		144: uint8(_BT_TRAIL),
		145: uint8(_BT_TRAIL),
		146: uint8(_BT_TRAIL),
		147: uint8(_BT_TRAIL),
		148: uint8(_BT_TRAIL),
		149: uint8(_BT_TRAIL),
		150: uint8(_BT_TRAIL),
		151: uint8(_BT_TRAIL),
		152: uint8(_BT_TRAIL),
		153: uint8(_BT_TRAIL),
		154: uint8(_BT_TRAIL),
		155: uint8(_BT_TRAIL),
		156: uint8(_BT_TRAIL),
		157: uint8(_BT_TRAIL),
		158: uint8(_BT_TRAIL),
		159: uint8(_BT_TRAIL),
		160: uint8(_BT_TRAIL),
		161: uint8(_BT_TRAIL),
		162: uint8(_BT_TRAIL),
		163: uint8(_BT_TRAIL),
		164: uint8(_BT_TRAIL),
		165: uint8(_BT_TRAIL),
		166: uint8(_BT_TRAIL),
		167: uint8(_BT_TRAIL),
		168: uint8(_BT_TRAIL),
		169: uint8(_BT_TRAIL),
		170: uint8(_BT_TRAIL),
		171: uint8(_BT_TRAIL),
		172: uint8(_BT_TRAIL),
		173: uint8(_BT_TRAIL),
		174: uint8(_BT_TRAIL),
		175: uint8(_BT_TRAIL),
		176: uint8(_BT_TRAIL),
		177: uint8(_BT_TRAIL),
		178: uint8(_BT_TRAIL),
		179: uint8(_BT_TRAIL),
		180: uint8(_BT_TRAIL),
		181: uint8(_BT_TRAIL),
		182: uint8(_BT_TRAIL),
		183: uint8(_BT_TRAIL),
		184: uint8(_BT_TRAIL),
		185: uint8(_BT_TRAIL),
		186: uint8(_BT_TRAIL),
		187: uint8(_BT_TRAIL),
		188: uint8(_BT_TRAIL),
		189: uint8(_BT_TRAIL),
		190: uint8(_BT_TRAIL),
		191: uint8(_BT_TRAIL),
		192: uint8(_BT_LEAD2),
		193: uint8(_BT_LEAD2),
		194: uint8(_BT_LEAD2),
		195: uint8(_BT_LEAD2),
		196: uint8(_BT_LEAD2),
		197: uint8(_BT_LEAD2),
		198: uint8(_BT_LEAD2),
		199: uint8(_BT_LEAD2),
		200: uint8(_BT_LEAD2),
		201: uint8(_BT_LEAD2),
		202: uint8(_BT_LEAD2),
		203: uint8(_BT_LEAD2),
		204: uint8(_BT_LEAD2),
		205: uint8(_BT_LEAD2),
		206: uint8(_BT_LEAD2),
		207: uint8(_BT_LEAD2),
		208: uint8(_BT_LEAD2),
		209: uint8(_BT_LEAD2),
		210: uint8(_BT_LEAD2),
		211: uint8(_BT_LEAD2),
		212: uint8(_BT_LEAD2),
		213: uint8(_BT_LEAD2),
		214: uint8(_BT_LEAD2),
		215: uint8(_BT_LEAD2),
		216: uint8(_BT_LEAD2),
		217: uint8(_BT_LEAD2),
		218: uint8(_BT_LEAD2),
		219: uint8(_BT_LEAD2),
		220: uint8(_BT_LEAD2),
		221: uint8(_BT_LEAD2),
		222: uint8(_BT_LEAD2),
		223: uint8(_BT_LEAD2),
		224: uint8(_BT_LEAD3),
		225: uint8(_BT_LEAD3),
		226: uint8(_BT_LEAD3),
		227: uint8(_BT_LEAD3),
		228: uint8(_BT_LEAD3),
		229: uint8(_BT_LEAD3),
		230: uint8(_BT_LEAD3),
		231: uint8(_BT_LEAD3),
		232: uint8(_BT_LEAD3),
		233: uint8(_BT_LEAD3),
		234: uint8(_BT_LEAD3),
		235: uint8(_BT_LEAD3),
		236: uint8(_BT_LEAD3),
		237: uint8(_BT_LEAD3),
		238: uint8(_BT_LEAD3),
		239: uint8(_BT_LEAD3),
		240: uint8(_BT_LEAD4),
		241: uint8(_BT_LEAD4),
		242: uint8(_BT_LEAD4),
		243: uint8(_BT_LEAD4),
		244: uint8(_BT_LEAD4),
		254: uint8(_BT_MALFORM),
		255: uint8(_BT_MALFORM),
	},
}

func init() {
	p := unsafe.Pointer(&_internal_utf8_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_utf8_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_utf8_toUtf16)
	*(*uintptr)(unsafe.Add(p, 392)) = __ccgo_fp(_utf8_isName2)
	*(*uintptr)(unsafe.Add(p, 400)) = __ccgo_fp(_utf8_isName3)
	*(*uintptr)(unsafe.Add(p, 408)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 416)) = __ccgo_fp(_utf8_isNmstrt2)
	*(*uintptr)(unsafe.Add(p, 424)) = __ccgo_fp(_utf8_isNmstrt3)
	*(*uintptr)(unsafe.Add(p, 432)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 440)) = __ccgo_fp(_utf8_isInvalid2)
	*(*uintptr)(unsafe.Add(p, 448)) = __ccgo_fp(_utf8_isInvalid3)
	*(*uintptr)(unsafe.Add(p, 456)) = __ccgo_fp(_utf8_isInvalid4)
}

var _internal_utf8_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
		FisUtf8:          uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_S),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_TRAIL),
		129: uint8(_BT_TRAIL),
		130: uint8(_BT_TRAIL),
		131: uint8(_BT_TRAIL),
		132: uint8(_BT_TRAIL),
		133: uint8(_BT_TRAIL),
		134: uint8(_BT_TRAIL),
		135: uint8(_BT_TRAIL),
		136: uint8(_BT_TRAIL),
		137: uint8(_BT_TRAIL),
		138: uint8(_BT_TRAIL),
		139: uint8(_BT_TRAIL),
		140: uint8(_BT_TRAIL),
		141: uint8(_BT_TRAIL),
		142: uint8(_BT_TRAIL),
		143: uint8(_BT_TRAIL),
		144: uint8(_BT_TRAIL),
		145: uint8(_BT_TRAIL),
		146: uint8(_BT_TRAIL),
		147: uint8(_BT_TRAIL),
		148: uint8(_BT_TRAIL),
		149: uint8(_BT_TRAIL),
		150: uint8(_BT_TRAIL),
		151: uint8(_BT_TRAIL),
		152: uint8(_BT_TRAIL),
		153: uint8(_BT_TRAIL),
		154: uint8(_BT_TRAIL),
		155: uint8(_BT_TRAIL),
		156: uint8(_BT_TRAIL),
		157: uint8(_BT_TRAIL),
		158: uint8(_BT_TRAIL),
		159: uint8(_BT_TRAIL),
		160: uint8(_BT_TRAIL),
		161: uint8(_BT_TRAIL),
		162: uint8(_BT_TRAIL),
		163: uint8(_BT_TRAIL),
		164: uint8(_BT_TRAIL),
		165: uint8(_BT_TRAIL),
		166: uint8(_BT_TRAIL),
		167: uint8(_BT_TRAIL),
		168: uint8(_BT_TRAIL),
		169: uint8(_BT_TRAIL),
		170: uint8(_BT_TRAIL),
		171: uint8(_BT_TRAIL),
		172: uint8(_BT_TRAIL),
		173: uint8(_BT_TRAIL),
		174: uint8(_BT_TRAIL),
		175: uint8(_BT_TRAIL),
		176: uint8(_BT_TRAIL),
		177: uint8(_BT_TRAIL),
		178: uint8(_BT_TRAIL),
		179: uint8(_BT_TRAIL),
		180: uint8(_BT_TRAIL),
		181: uint8(_BT_TRAIL),
		182: uint8(_BT_TRAIL),
		183: uint8(_BT_TRAIL),
		184: uint8(_BT_TRAIL),
		185: uint8(_BT_TRAIL),
		186: uint8(_BT_TRAIL),
		187: uint8(_BT_TRAIL),
		188: uint8(_BT_TRAIL),
		189: uint8(_BT_TRAIL),
		190: uint8(_BT_TRAIL),
		191: uint8(_BT_TRAIL),
		192: uint8(_BT_LEAD2),
		193: uint8(_BT_LEAD2),
		194: uint8(_BT_LEAD2),
		195: uint8(_BT_LEAD2),
		196: uint8(_BT_LEAD2),
		197: uint8(_BT_LEAD2),
		198: uint8(_BT_LEAD2),
		199: uint8(_BT_LEAD2),
		200: uint8(_BT_LEAD2),
		201: uint8(_BT_LEAD2),
		202: uint8(_BT_LEAD2),
		203: uint8(_BT_LEAD2),
		204: uint8(_BT_LEAD2),
		205: uint8(_BT_LEAD2),
		206: uint8(_BT_LEAD2),
		207: uint8(_BT_LEAD2),
		208: uint8(_BT_LEAD2),
		209: uint8(_BT_LEAD2),
		210: uint8(_BT_LEAD2),
		211: uint8(_BT_LEAD2),
		212: uint8(_BT_LEAD2),
		213: uint8(_BT_LEAD2),
		214: uint8(_BT_LEAD2),
		215: uint8(_BT_LEAD2),
		216: uint8(_BT_LEAD2),
		217: uint8(_BT_LEAD2),
		218: uint8(_BT_LEAD2),
		219: uint8(_BT_LEAD2),
		220: uint8(_BT_LEAD2),
		221: uint8(_BT_LEAD2),
		222: uint8(_BT_LEAD2),
		223: uint8(_BT_LEAD2),
		224: uint8(_BT_LEAD3),
		225: uint8(_BT_LEAD3),
		226: uint8(_BT_LEAD3),
		227: uint8(_BT_LEAD3),
		228: uint8(_BT_LEAD3),
		229: uint8(_BT_LEAD3),
		230: uint8(_BT_LEAD3),
		231: uint8(_BT_LEAD3),
		232: uint8(_BT_LEAD3),
		233: uint8(_BT_LEAD3),
		234: uint8(_BT_LEAD3),
		235: uint8(_BT_LEAD3),
		236: uint8(_BT_LEAD3),
		237: uint8(_BT_LEAD3),
		238: uint8(_BT_LEAD3),
		239: uint8(_BT_LEAD3),
		240: uint8(_BT_LEAD4),
		241: uint8(_BT_LEAD4),
		242: uint8(_BT_LEAD4),
		243: uint8(_BT_LEAD4),
		244: uint8(_BT_LEAD4),
		254: uint8(_BT_MALFORM),
		255: uint8(_BT_MALFORM),
	},
}

func init() {
	p := unsafe.Pointer(&_internal_utf8_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_utf8_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_utf8_toUtf16)
	*(*uintptr)(unsafe.Add(p, 392)) = __ccgo_fp(_utf8_isName2)
	*(*uintptr)(unsafe.Add(p, 400)) = __ccgo_fp(_utf8_isName3)
	*(*uintptr)(unsafe.Add(p, 408)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 416)) = __ccgo_fp(_utf8_isNmstrt2)
	*(*uintptr)(unsafe.Add(p, 424)) = __ccgo_fp(_utf8_isNmstrt3)
	*(*uintptr)(unsafe.Add(p, 432)) = __ccgo_fp(_isNever)
	*(*uintptr)(unsafe.Add(p, 440)) = __ccgo_fp(_utf8_isInvalid2)
	*(*uintptr)(unsafe.Add(p, 448)) = __ccgo_fp(_utf8_isInvalid3)
	*(*uintptr)(unsafe.Add(p, 456)) = __ccgo_fp(_utf8_isInvalid4)
}

func _latin1_toUtf8(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var c uint8
	var v2, v3, v4, v5, v6, v7, v8, v9 uintptr
	_, _, _, _, _, _, _, _, _ = c, v2, v3, v4, v5, v6, v7, v8, v9
	_ = enc
	for {
		if *(*uintptr)(unsafe.Pointer(fromP)) == fromLim {
			return int32(_XML_CONVERT_COMPLETED)
		}
		c = *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP))))
		if libc.Int32FromUint8(c)&int32(0x80) != 0 {
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(2) {
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			v3 = toP
			v2 = *(*uintptr)(unsafe.Pointer(v3))
			*(*uintptr)(unsafe.Pointer(v3))++
			*(*uint8)(unsafe.Pointer(v2)) = libc.Uint8FromInt32(libc.Int32FromUint8(c)>>libc.Int32FromInt32(6) | int32(_UTF8_cval2))
			v5 = toP
			v4 = *(*uintptr)(unsafe.Pointer(v5))
			*(*uintptr)(unsafe.Pointer(v5))++
			*(*uint8)(unsafe.Pointer(v4)) = libc.Uint8FromInt32(libc.Int32FromUint8(c)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
			*(*uintptr)(unsafe.Pointer(fromP))++
		} else {
			if *(*uintptr)(unsafe.Pointer(toP)) == toLim {
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			v7 = toP
			v6 = *(*uintptr)(unsafe.Pointer(v7))
			*(*uintptr)(unsafe.Pointer(v7))++
			v9 = fromP
			v8 = *(*uintptr)(unsafe.Pointer(v9))
			*(*uintptr)(unsafe.Pointer(v9))++
			*(*uint8)(unsafe.Pointer(v6)) = *(*uint8)(unsafe.Pointer(v8))
		}
		goto _1
	_1:
	}
	return r
}

func _latin1_toUtf16(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var v1, v2, v3, v4 uintptr
	_, _, _, _ = v1, v2, v3, v4
	_ = enc
	for *(*uintptr)(unsafe.Pointer(fromP)) < fromLim && *(*uintptr)(unsafe.Pointer(toP)) < toLim {
		v2 = toP
		v1 = *(*uintptr)(unsafe.Pointer(v2))
		*(*uintptr)(unsafe.Pointer(v2)) += 2
		v4 = fromP
		v3 = *(*uintptr)(unsafe.Pointer(v4))
		*(*uintptr)(unsafe.Pointer(v4))++
		*(*uint16)(unsafe.Pointer(v1)) = uint16(*(*uint8)(unsafe.Pointer(v3)))
	}
	if *(*uintptr)(unsafe.Pointer(toP)) == toLim && *(*uintptr)(unsafe.Pointer(fromP)) < fromLim {
		return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	} else {
		return int32(_XML_CONVERT_COMPLETED)
	}
	return r
}

var _latin1_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_latin1_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_latin1_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_latin1_toUtf16)
}

var _latin1_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_latin1_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_latin1_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_latin1_toUtf16)
}

func _ascii_toUtf8(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var v1, v2, v3, v4 uintptr
	_, _, _, _ = v1, v2, v3, v4
	_ = enc
	for *(*uintptr)(unsafe.Pointer(fromP)) < fromLim && *(*uintptr)(unsafe.Pointer(toP)) < toLim {
		v2 = toP
		v1 = *(*uintptr)(unsafe.Pointer(v2))
		*(*uintptr)(unsafe.Pointer(v2))++
		v4 = fromP
		v3 = *(*uintptr)(unsafe.Pointer(v4))
		*(*uintptr)(unsafe.Pointer(v4))++
		*(*uint8)(unsafe.Pointer(v1)) = *(*uint8)(unsafe.Pointer(v3))
	}
	if *(*uintptr)(unsafe.Pointer(toP)) == toLim && *(*uintptr)(unsafe.Pointer(fromP)) < fromLim {
		return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	} else {
		return int32(_XML_CONVERT_COMPLETED)
	}
	return r
}

var _ascii_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
		FisUtf8:          uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
	},
}

func init() {
	p := unsafe.Pointer(&_ascii_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_ascii_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_latin1_toUtf16)
}

var _ascii_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(1),
		FisUtf8:          uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
	},
}

func init() {
	p := unsafe.Pointer(&_ascii_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_normal_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_normal_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_normal_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_normal_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_normal_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_normal_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_normal_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_normal_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_normal_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_normal_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_normal_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_normal_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_normal_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_normal_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_ascii_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_latin1_toUtf16)
}

func _unicode_byte_type(tls *libc.TLS, hi uint8, lo uint8) (r int32) {
	switch libc.Int32FromUint8(hi) {
	/* 0xD800-0xDBFF first 16-bit code unit or high surrogate (W1) */
	case int32(0xD8):
		fallthrough
	case int32(0xD9):
		fallthrough
	case int32(0xDA):
		fallthrough
	case int32(0xDB):
		return int32(_BT_LEAD4)
		/* 0xDC00-0xDFFF second 16-bit code unit or low surrogate (W2) */
		fallthrough
	case int32(0xDC):
		fallthrough
	case int32(0xDD):
		fallthrough
	case int32(0xDE):
		fallthrough
	case int32(0xDF):
		return int32(_BT_TRAIL)
	case int32(0xFF):
		switch libc.Int32FromUint8(lo) {
		case int32(0xFF): /* noncharacter-FFFF */
			fallthrough
		case int32(0xFE): /* noncharacter-FFFE */
			return int32(_BT_NONXML)
		}
		break
	}
	return int32(_BT_NONASCII)
}

func _little2_toUtf8(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var from, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v3, v4, v5, v6, v7, v8, v9 uintptr
	var hi, lo, lo2 uint8
	var plane int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = from, hi, lo, lo2, plane, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v3, v4, v5, v6, v7, v8, v9
	from = *(*uintptr)(unsafe.Pointer(fromP))
	_ = enc
	fromLim = from + uintptr((int64(fromLim)-int64(from))>>libc.Int32FromInt32(1)<<libc.Int32FromInt32(1)) /* shrink to even */
	for {
		if !(from < fromLim) {
			break
		}
		lo = *(*uint8)(unsafe.Pointer(from))
		hi = *(*uint8)(unsafe.Pointer(from + 1))
		switch libc.Int32FromUint8(hi) {
		case 0:
			if libc.Int32FromUint8(lo) < int32(0x80) {
				if *(*uintptr)(unsafe.Pointer(toP)) == toLim {
					*(*uintptr)(unsafe.Pointer(fromP)) = from
					return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
				}
				v3 = toP
				v2 = *(*uintptr)(unsafe.Pointer(v3))
				*(*uintptr)(unsafe.Pointer(v3))++
				*(*uint8)(unsafe.Pointer(v2)) = lo
				break
			} /* fall through */
			fallthrough
		case int32(0x1):
			fallthrough
		case int32(0x2):
			fallthrough
		case int32(0x3):
			fallthrough
		case int32(0x4):
			fallthrough
		case int32(0x5):
			fallthrough
		case int32(0x6):
			fallthrough
		case int32(0x7):
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(2) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			v5 = toP
			v4 = *(*uintptr)(unsafe.Pointer(v5))
			*(*uintptr)(unsafe.Pointer(v5))++
			*(*uint8)(unsafe.Pointer(v4)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)>>libc.Int32FromInt32(6) | libc.Int32FromUint8(hi)<<libc.Int32FromInt32(2) | int32(_UTF8_cval2))
			v7 = toP
			v6 = *(*uintptr)(unsafe.Pointer(v7))
			*(*uintptr)(unsafe.Pointer(v7))++
			*(*uint8)(unsafe.Pointer(v6)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		default:
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(3) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			} /* 16 bits divided 4, 6, 6 amongst 3 bytes */
			v9 = toP
			v8 = *(*uintptr)(unsafe.Pointer(v9))
			*(*uintptr)(unsafe.Pointer(v9))++
			*(*uint8)(unsafe.Pointer(v8)) = libc.Uint8FromInt32(libc.Int32FromUint8(hi)>>libc.Int32FromInt32(4) | int32(_UTF8_cval3))
			v11 = toP
			v10 = *(*uintptr)(unsafe.Pointer(v11))
			*(*uintptr)(unsafe.Pointer(v11))++
			*(*uint8)(unsafe.Pointer(v10)) = libc.Uint8FromInt32(libc.Int32FromUint8(hi)&libc.Int32FromInt32(0xf)<<libc.Int32FromInt32(2) | libc.Int32FromUint8(lo)>>libc.Int32FromInt32(6) | libc.Int32FromInt32(0x80))
			v13 = toP
			v12 = *(*uintptr)(unsafe.Pointer(v13))
			*(*uintptr)(unsafe.Pointer(v13))++
			*(*uint8)(unsafe.Pointer(v12)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		case int32(0xD8):
			fallthrough
		case int32(0xD9):
			fallthrough
		case int32(0xDA):
			fallthrough
		case int32(0xDB):
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(4) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			if int64(fromLim)-int64(from) < int64(4) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_INPUT_INCOMPLETE)
			}
			plane = libc.Int32FromUint8(hi)&int32(0x3)<<int32(2) | libc.Int32FromUint8(lo)>>int32(6)&int32(0x3) + int32(1)
			v15 = toP
			v14 = *(*uintptr)(unsafe.Pointer(v15))
			*(*uintptr)(unsafe.Pointer(v15))++
			*(*uint8)(unsafe.Pointer(v14)) = libc.Uint8FromInt32(plane>>libc.Int32FromInt32(2) | int32(_UTF8_cval4))
			v17 = toP
			v16 = *(*uintptr)(unsafe.Pointer(v17))
			*(*uintptr)(unsafe.Pointer(v17))++
			*(*uint8)(unsafe.Pointer(v16)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)>>libc.Int32FromInt32(2)&libc.Int32FromInt32(0xF) | plane&libc.Int32FromInt32(0x3)<<libc.Int32FromInt32(4) | libc.Int32FromInt32(0x80))
			from += uintptr(2)
			lo2 = *(*uint8)(unsafe.Pointer(from))
			v19 = toP
			v18 = *(*uintptr)(unsafe.Pointer(v19))
			*(*uintptr)(unsafe.Pointer(v19))++
			*(*uint8)(unsafe.Pointer(v18)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)&libc.Int32FromInt32(0x3)<<libc.Int32FromInt32(4) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from + 1)))&libc.Int32FromInt32(0x3)<<libc.Int32FromInt32(2) | libc.Int32FromUint8(lo2)>>libc.Int32FromInt32(6) | libc.Int32FromInt32(0x80))
			v21 = toP
			v20 = *(*uintptr)(unsafe.Pointer(v21))
			*(*uintptr)(unsafe.Pointer(v21))++
			*(*uint8)(unsafe.Pointer(v20)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo2)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
			break
		}
		goto _1
	_1:
		;
		from += uintptr(2)
	}
	*(*uintptr)(unsafe.Pointer(fromP)) = from
	if from < fromLim {
		return int32(_XML_CONVERT_INPUT_INCOMPLETE)
	} else {
		return int32(_XML_CONVERT_COMPLETED)
	}
	return r
}

func _little2_toUtf16(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var res _XML_Convert_Result
	var v2, v3 uintptr
	_, _, _ = res, v2, v3
	res = int32(_XML_CONVERT_COMPLETED)
	_ = enc
	fromLim = *(*uintptr)(unsafe.Pointer(fromP)) + uintptr((int64(fromLim)-int64(*(*uintptr)(unsafe.Pointer(fromP))))>>libc.Int32FromInt32(1)<<libc.Int32FromInt32(1)) /* shrink to even */ /* Avoid copying first half only of surrogate */
	if int64(fromLim)-int64(*(*uintptr)(unsafe.Pointer(fromP))) > (int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))))/2<<libc.Int32FromInt32(1) && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(fromLim - libc.UintptrFromInt32(2) + 1)))&int32(0xF8) == int32(0xD8) {
		fromLim -= uintptr(2)
		res = int32(_XML_CONVERT_INPUT_INCOMPLETE)
	}
	for {
		if !(*(*uintptr)(unsafe.Pointer(fromP)) < fromLim && *(*uintptr)(unsafe.Pointer(toP)) < toLim) {
			break
		}
		v3 = toP
		v2 = *(*uintptr)(unsafe.Pointer(v3))
		*(*uintptr)(unsafe.Pointer(v3)) += 2
		*(*uint16)(unsafe.Pointer(v2)) = libc.Uint16FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)) + 1)))<<int32(8) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP))))))
		goto _1
	_1:
		;
		*(*uintptr)(unsafe.Pointer(fromP)) += uintptr(2)
	}
	if *(*uintptr)(unsafe.Pointer(toP)) == toLim && *(*uintptr)(unsafe.Pointer(fromP)) < fromLim {
		return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	} else {
		return res
	}
	return r
}

func _big2_toUtf8(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var from, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v3, v4, v5, v6, v7, v8, v9 uintptr
	var hi, lo, lo2 uint8
	var plane int32
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = from, hi, lo, lo2, plane, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v3, v4, v5, v6, v7, v8, v9
	from = *(*uintptr)(unsafe.Pointer(fromP))
	_ = enc
	fromLim = from + uintptr((int64(fromLim)-int64(from))>>libc.Int32FromInt32(1)<<libc.Int32FromInt32(1)) /* shrink to even */
	for {
		if !(from < fromLim) {
			break
		}
		lo = *(*uint8)(unsafe.Pointer(from + 1))
		hi = *(*uint8)(unsafe.Pointer(from))
		switch libc.Int32FromUint8(hi) {
		case 0:
			if libc.Int32FromUint8(lo) < int32(0x80) {
				if *(*uintptr)(unsafe.Pointer(toP)) == toLim {
					*(*uintptr)(unsafe.Pointer(fromP)) = from
					return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
				}
				v3 = toP
				v2 = *(*uintptr)(unsafe.Pointer(v3))
				*(*uintptr)(unsafe.Pointer(v3))++
				*(*uint8)(unsafe.Pointer(v2)) = lo
				break
			} /* fall through */
			fallthrough
		case int32(0x1):
			fallthrough
		case int32(0x2):
			fallthrough
		case int32(0x3):
			fallthrough
		case int32(0x4):
			fallthrough
		case int32(0x5):
			fallthrough
		case int32(0x6):
			fallthrough
		case int32(0x7):
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(2) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			v5 = toP
			v4 = *(*uintptr)(unsafe.Pointer(v5))
			*(*uintptr)(unsafe.Pointer(v5))++
			*(*uint8)(unsafe.Pointer(v4)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)>>libc.Int32FromInt32(6) | libc.Int32FromUint8(hi)<<libc.Int32FromInt32(2) | int32(_UTF8_cval2))
			v7 = toP
			v6 = *(*uintptr)(unsafe.Pointer(v7))
			*(*uintptr)(unsafe.Pointer(v7))++
			*(*uint8)(unsafe.Pointer(v6)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		default:
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(3) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			} /* 16 bits divided 4, 6, 6 amongst 3 bytes */
			v9 = toP
			v8 = *(*uintptr)(unsafe.Pointer(v9))
			*(*uintptr)(unsafe.Pointer(v9))++
			*(*uint8)(unsafe.Pointer(v8)) = libc.Uint8FromInt32(libc.Int32FromUint8(hi)>>libc.Int32FromInt32(4) | int32(_UTF8_cval3))
			v11 = toP
			v10 = *(*uintptr)(unsafe.Pointer(v11))
			*(*uintptr)(unsafe.Pointer(v11))++
			*(*uint8)(unsafe.Pointer(v10)) = libc.Uint8FromInt32(libc.Int32FromUint8(hi)&libc.Int32FromInt32(0xf)<<libc.Int32FromInt32(2) | libc.Int32FromUint8(lo)>>libc.Int32FromInt32(6) | libc.Int32FromInt32(0x80))
			v13 = toP
			v12 = *(*uintptr)(unsafe.Pointer(v13))
			*(*uintptr)(unsafe.Pointer(v13))++
			*(*uint8)(unsafe.Pointer(v12)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		case int32(0xD8):
			fallthrough
		case int32(0xD9):
			fallthrough
		case int32(0xDA):
			fallthrough
		case int32(0xDB):
			if int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) < int64(4) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			if int64(fromLim)-int64(from) < int64(4) {
				*(*uintptr)(unsafe.Pointer(fromP)) = from
				return int32(_XML_CONVERT_INPUT_INCOMPLETE)
			}
			plane = libc.Int32FromUint8(hi)&int32(0x3)<<int32(2) | libc.Int32FromUint8(lo)>>int32(6)&int32(0x3) + int32(1)
			v15 = toP
			v14 = *(*uintptr)(unsafe.Pointer(v15))
			*(*uintptr)(unsafe.Pointer(v15))++
			*(*uint8)(unsafe.Pointer(v14)) = libc.Uint8FromInt32(plane>>libc.Int32FromInt32(2) | int32(_UTF8_cval4))
			v17 = toP
			v16 = *(*uintptr)(unsafe.Pointer(v17))
			*(*uintptr)(unsafe.Pointer(v17))++
			*(*uint8)(unsafe.Pointer(v16)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)>>libc.Int32FromInt32(2)&libc.Int32FromInt32(0xF) | plane&libc.Int32FromInt32(0x3)<<libc.Int32FromInt32(4) | libc.Int32FromInt32(0x80))
			from += uintptr(2)
			lo2 = *(*uint8)(unsafe.Pointer(from + 1))
			v19 = toP
			v18 = *(*uintptr)(unsafe.Pointer(v19))
			*(*uintptr)(unsafe.Pointer(v19))++
			*(*uint8)(unsafe.Pointer(v18)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo)&libc.Int32FromInt32(0x3)<<libc.Int32FromInt32(4) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(from)))&libc.Int32FromInt32(0x3)<<libc.Int32FromInt32(2) | libc.Int32FromUint8(lo2)>>libc.Int32FromInt32(6) | libc.Int32FromInt32(0x80))
			v21 = toP
			v20 = *(*uintptr)(unsafe.Pointer(v21))
			*(*uintptr)(unsafe.Pointer(v21))++
			*(*uint8)(unsafe.Pointer(v20)) = libc.Uint8FromInt32(libc.Int32FromUint8(lo2)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
			break
		}
		goto _1
	_1:
		;
		from += uintptr(2)
	}
	*(*uintptr)(unsafe.Pointer(fromP)) = from
	if from < fromLim {
		return int32(_XML_CONVERT_INPUT_INCOMPLETE)
	} else {
		return int32(_XML_CONVERT_COMPLETED)
	}
	return r
}

func _big2_toUtf16(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var res _XML_Convert_Result
	var v2, v3 uintptr
	_, _, _ = res, v2, v3
	res = int32(_XML_CONVERT_COMPLETED)
	_ = enc
	fromLim = *(*uintptr)(unsafe.Pointer(fromP)) + uintptr((int64(fromLim)-int64(*(*uintptr)(unsafe.Pointer(fromP))))>>libc.Int32FromInt32(1)<<libc.Int32FromInt32(1)) /* shrink to even */ /* Avoid copying first half only of surrogate */
	if int64(fromLim)-int64(*(*uintptr)(unsafe.Pointer(fromP))) > (int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))))/2<<libc.Int32FromInt32(1) && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(fromLim - libc.UintptrFromInt32(2))))&int32(0xF8) == int32(0xD8) {
		fromLim -= uintptr(2)
		res = int32(_XML_CONVERT_INPUT_INCOMPLETE)
	}
	for {
		if !(*(*uintptr)(unsafe.Pointer(fromP)) < fromLim && *(*uintptr)(unsafe.Pointer(toP)) < toLim) {
			break
		}
		v3 = toP
		v2 = *(*uintptr)(unsafe.Pointer(v3))
		*(*uintptr)(unsafe.Pointer(v3)) += 2
		*(*uint16)(unsafe.Pointer(v2)) = libc.Uint16FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)))))<<int32(8) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)) + 1))))
		goto _1
	_1:
		;
		*(*uintptr)(unsafe.Pointer(fromP)) += uintptr(2)
	}
	if *(*uintptr)(unsafe.Pointer(toP)) == toLim && *(*uintptr)(unsafe.Pointer(fromP)) < fromLim {
		return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	} else {
		return res
	}
	return r
}

/* CHAR_MATCHES is guaranteed to have MINBPC bytes available. */

/* This file is included!
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* ptr points to character following "<!-" */

func _little2_scanComment(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1 int32
	_ = v1
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_MINUS1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
			} else {
				v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
			}
			switch v1 {
			case int32(_BT_LEAD2):
				if int64(end)-int64(ptr) < int64(2) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(ptr) < int64(3) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(ptr) < int64(4) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(4)
			case int32(_BT_NONXML):
				fallthrough
			case int32(_BT_MALFORM):
				fallthrough
			case int32(_BT_TRAIL):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			case int32(_BT_MINUS):
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_MINUS1) {
					ptr += uintptr(2)
					if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
						return -int32(1)
					}
					if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1)) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
					return int32(m_XML_TOK_COMMENT)
				}
			default:
				ptr += uintptr(2)
				break
			}
		}
	}
	return -int32(1)
}

/* ptr points to character following "<!" */

func _little2_scanDecl(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2, v3 int32
	_, _, _ = v1, v2, v3
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_MINUS):
		return _little2_scanComment(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_LSQB):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_COND_SECT_OPEN)
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_PERCNT):
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			/* don't allow <!ENTITY% foo "whatever"> */
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))))))
			} else {
				v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1)), *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2))))
			}
			switch v3 {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_PERCNT):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			/* fall through */
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DECL_OPEN)
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			ptr += uintptr(2)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _little2_checkPiTarget(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, tokPtr uintptr) (r int32) {
	var upper, v1, v2, v3 int32
	_, _, _, _ = upper, v1, v2, v3
	upper = 0
	_ = enc
	*(*int32)(unsafe.Pointer(tokPtr)) = int32(m_XML_TOK_PI)
	if int64(end)-int64(ptr) != int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(3)) {
		return int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
	} else {
		v1 = -int32(1)
	}
	switch v1 {
	case int32(m_ASCII_x):
	case int32(m_ASCII_X):
		upper = int32(1)
	default:
		return int32(1)
	}
	ptr += uintptr(2)
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
	} else {
		v2 = -int32(1)
	}
	switch v2 {
	case int32(m_ASCII_m):
	case int32(m_ASCII_M):
		upper = int32(1)
	default:
		return int32(1)
	}
	ptr += uintptr(2)
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
	} else {
		v3 = -int32(1)
	}
	switch v3 {
	case int32(m_ASCII_l):
	case int32(m_ASCII_L):
		upper = int32(1)
	default:
		return int32(1)
	}
	if upper != 0 {
		return 0
	}
	*(*int32)(unsafe.Pointer(tokPtr)) = int32(m_XML_TOK_XML_DECL)
	return int32(1)
}

/* ptr points to character following "<?" */

func _little2_scanPi(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var target uintptr
	var v1, v2, v3 int32
	var _ /* tok at bp+0 */ int32
	_, _, _, _ = target, v1, v2, v3
	target = ptr
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			if !(_little2_checkPiTarget(tls, enc, target, ptr, bp) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
			for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
					v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
				} else {
					v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
				}
				switch v3 {
				case int32(_BT_LEAD2):
					if int64(end)-int64(ptr) < int64(2) {
						return -int32(2)
					}
					if 0 != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(2)
				case int32(_BT_LEAD3):
					if int64(end)-int64(ptr) < int64(3) {
						return -int32(2)
					}
					if 0 != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(3)
				case int32(_BT_LEAD4):
					if int64(end)-int64(ptr) < int64(4) {
						return -int32(2)
					}
					if 0 != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(4)
				case int32(_BT_NONXML):
					fallthrough
				case int32(_BT_MALFORM):
					fallthrough
				case int32(_BT_TRAIL):
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				case int32(_BT_QUEST):
					ptr += uintptr(2)
					if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
						return -int32(1)
					}
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
						return *(*int32)(unsafe.Pointer(bp))
					}
				default:
					ptr += uintptr(2)
					break
				}
			}
			return -int32(1)
		case int32(_BT_QUEST):
			if !(_little2_checkPiTarget(tls, enc, target, ptr, bp) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return *(*int32)(unsafe.Pointer(bp))
			}
			/* fall through */
			fallthrough
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _little2_scanCdataSection(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var i int32
	_ = i
	_ = enc
	/* CDATA[ */
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(6)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	i = 0
	for {
		if !(i < int32(6)) {
			break
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == libc.Int32FromUint8(_CDATA_LSQB1[i])) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _1
	_1:
		;
		i++
		ptr += uintptr(2)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_CDATA_SECT_OPEN)
}

var _CDATA_LSQB1 = [6]uint8{
	0: uint8(m_ASCII_C),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_A),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_A),
	5: uint8(m_ASCII_LSQB1),
}

func _little2_cdataSectionTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var v1, v2, v3 int32
	_, _, _, _ = n, v1, v2, v3
	if ptr >= end {
		return -int32(4)
	}
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_RSQB):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_RSQB1)) {
			break
		}
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1)) {
			ptr -= uintptr(2)
			break
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CDATA_SECT_CLOSE)
	case int32(_BT_CR):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		if v2 == int32(_BT_LF) {
			ptr += uintptr(2)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LF):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NONXML):
		fallthrough
	case int32(_BT_MALFORM):
		fallthrough
	case int32(_BT_TRAIL):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	default:
		ptr += uintptr(2)
		break
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v3 {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_RSQB):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

/* ptr points to character following "</" */

func _little2_scanEndTag(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2, v22 int32
	_, _, _ = v1, v2, v22
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			goto _3
		case int32(_BT_MINUS):
			goto _4
		case int32(_BT_NAME):
			goto _5
		case int32(_BT_DIGIT):
			goto _6
		case int32(_BT_HEX):
			goto _7
		case int32(_BT_NMSTRT):
			goto _8
		case int32(_BT_LEAD2):
			goto _9
		case int32(_BT_LEAD3):
			goto _10
		case int32(_BT_LEAD4):
			goto _11
		case int32(_BT_LF):
			goto _12
		case int32(_BT_CR):
			goto _13
		case int32(_BT_S):
			goto _14
		case int32(_BT_COLON):
			goto _15
		case int32(_BT_GT):
			goto _16
		default:
			goto _17
		}
		goto _18
	_3:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
	_8:
		;
	_7:
		;
	_6:
		;
	_5:
		;
	_4:
		;
		ptr += uintptr(2)
		goto _18
	_9:
		;
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		goto _18
	_10:
		;
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
		goto _18
	_11:
		;
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
		goto _18
	_14:
		;
	_13:
		;
	_12:
		;
		ptr += uintptr(2)
	_21:
		;
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			goto _19
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v22 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v22 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v22 {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
		case int32(_BT_GT):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_END_TAG)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _20
	_20:
		;
		ptr += uintptr(2)
		goto _21
		goto _19
	_19:
		;
		return -int32(1)
	_15:
		;
		/* no need to check qname syntax here,
		   since end-tag must match exactly */
		ptr += uintptr(2)
		goto _18
	_16:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_END_TAG)
	_17:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	_18:
	}
	return -int32(1)
}

/* ptr points to character following "&#X" */

func _little2_scanHexCharRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v3 int32
	_, _ = v1, v3
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v1 {
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_HEX):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		for {
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
			} else {
				v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
			}
			switch v3 {
			case int32(_BT_DIGIT):
				fallthrough
			case int32(_BT_HEX):
			case int32(_BT_SEMI):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_CHAR_REF)
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			goto _2
		_2:
			;
			ptr += uintptr(2)
		}
	}
	return -int32(1)
}

/* ptr points to character following "&#" */

func _little2_scanCharRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v3 int32
	_, _ = v1, v3
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_x) {
			return _little2_scanHexCharRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v1 {
		case int32(_BT_DIGIT):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		for {
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
			} else {
				v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
			}
			switch v3 {
			case int32(_BT_DIGIT):
			case int32(_BT_SEMI):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_CHAR_REF)
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			goto _2
		_2:
			;
			ptr += uintptr(2)
		}
	}
	return -int32(1)
}

/* ptr points to character following "&" */

func _little2_scanRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NUM):
		return _little2_scanCharRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_SEMI):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_ENTITY_REF)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

/* ptr points to character following first character of attribute name */

func _little2_scanAtts(tls *libc.TLS, enc uintptr, _ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var hadColon, open, t, t1, tok, v1, v18, v22, v24, v26, v27, v31 int32
	_, _, _, _, _, _, _, _, _, _, _, _ = hadColon, open, t, t1, tok, v1, v18, v22, v24, v26, v27, v31
	hadColon = 0
	for int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
		}
		switch v1 {
		case int32(_BT_NONASCII):
			goto _2
		case int32(_BT_MINUS):
			goto _3
		case int32(_BT_NAME):
			goto _4
		case int32(_BT_DIGIT):
			goto _5
		case int32(_BT_HEX):
			goto _6
		case int32(_BT_NMSTRT):
			goto _7
		case int32(_BT_LEAD2):
			goto _8
		case int32(_BT_LEAD3):
			goto _9
		case int32(_BT_LEAD4):
			goto _10
		case int32(_BT_COLON):
			goto _11
		case int32(_BT_LF):
			goto _12
		case int32(_BT_CR):
			goto _13
		case int32(_BT_S):
			goto _14
		case int32(_BT_EQUALS):
			goto _15
		default:
			goto _16
		}
		goto _17
	_2:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		} /* fall through */
	_7:
		;
	_6:
		;
	_5:
		;
	_4:
		;
	_3:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _17
	_8:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _17
	_9:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		goto _17
	_10:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		goto _17
	_11:
		;
		if hadColon != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		hadColon = int32(1)
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
			v18 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
		} else {
			v18 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
		}
		switch v18 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		goto _17
	_14:
		;
	_13:
		;
	_12:
		;
	_21:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
			v22 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
		} else {
			v22 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
		}
		t = v22
		if t == int32(_BT_EQUALS) {
			goto _19
		}
		switch t {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_CR):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		goto _20
	_20:
		;
		goto _21
		goto _19
	_19:
		;
		/* fall through */
	_15:
		;
		hadColon = 0
		for {
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
			if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
				v24 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
			} else {
				v24 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
			}
			open = v24
			if open == int32(_BT_QUOT) || open == int32(_BT_APOS) {
				break
			}
			switch open {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_CR):
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			goto _23
		_23:
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		/* in attribute value */
		for {
			if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
				v26 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
			} else {
				v26 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
			}
			t1 = v26
			if t1 == open {
				break
			}
			switch t1 {
			case int32(_BT_LEAD2):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
			case int32(_BT_NONXML):
				fallthrough
			case int32(_BT_MALFORM):
				fallthrough
			case int32(_BT_TRAIL):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			case int32(_BT_AMP):
				tok = _little2_scanRef(tls, enc, *(*uintptr)(unsafe.Pointer(bp))+uintptr(2), end, bp)
				if tok <= 0 {
					if tok == m_XML_TOK_INVALID {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					}
					return tok
				}
			case int32(_BT_LT):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			default:
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
				break
			}
			goto _25
		_25:
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
			v27 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
		} else {
			v27 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
		}
		switch v27 {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
		case int32(_BT_SOL):
			goto sol
		case int32(_BT_GT):
			goto gt
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		/* ptr points to closing quote */
	_30:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 {
			v31 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))))))
		} else {
			v31 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))
		}
		switch v31 {
		case int32(_BT_NONASCII):
			goto _32
		case int32(_BT_HEX):
			goto _33
		case int32(_BT_NMSTRT):
			goto _34
		case int32(_BT_LEAD2):
			goto _35
		case int32(_BT_LEAD3):
			goto _36
		case int32(_BT_LEAD4):
			goto _37
		case int32(_BT_LF):
			goto _38
		case int32(_BT_CR):
			goto _39
		case int32(_BT_S):
			goto _40
		case int32(_BT_GT):
			goto _41
		case int32(_BT_SOL):
			goto _42
		default:
			goto _43
		}
		goto _44
	_32:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		} /* fall through */
	_34:
		;
	_33:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _44
	_35:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _44
	_36:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		goto _44
	_37:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		goto _44
	_40:
		;
	_39:
		;
	_38:
		;
		goto _29
	_41:
		;
		goto gt
	gt:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp)) + uintptr(2)
		return int32(m_XML_TOK_START_TAG_WITH_ATTS)
	_42:
		;
		goto sol
	sol:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == int32(m_ASCII_GT1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp)) + uintptr(2)
		return int32(m_XML_TOK_EMPTY_ELEMENT_WITH_ATTS)
	_43:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return m_XML_TOK_INVALID
	_44:
		;
		goto _28
		goto _29
	_29:
		;
		goto _30
		goto _28
	_28:
		;
		goto _17
	_16:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return m_XML_TOK_INVALID
	_17:
	}
	return -int32(1)
}

/* ptr points to character following "<" */

func _little2_scanLt(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var hadColon, v1, v2, v21, v22, v3 int32
	_, _, _, _, _, _ = hadColon, v1, v2, v21, v22, v3
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_EXCL):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_MINUS):
			return _little2_scanComment(tls, enc, ptr+uintptr(2), end, nextTokPtr)
		case int32(_BT_LSQB):
			return _little2_scanCdataSection(tls, enc, ptr+uintptr(2), end, nextTokPtr)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	case int32(_BT_QUEST):
		return _little2_scanPi(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_SOL):
		return _little2_scanEndTag(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	hadColon = 0
	/* we have a start-tag */
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v3 {
		case int32(_BT_NONASCII):
			goto _4
		case int32(_BT_MINUS):
			goto _5
		case int32(_BT_NAME):
			goto _6
		case int32(_BT_DIGIT):
			goto _7
		case int32(_BT_HEX):
			goto _8
		case int32(_BT_NMSTRT):
			goto _9
		case int32(_BT_LEAD2):
			goto _10
		case int32(_BT_LEAD3):
			goto _11
		case int32(_BT_LEAD4):
			goto _12
		case int32(_BT_COLON):
			goto _13
		case int32(_BT_LF):
			goto _14
		case int32(_BT_CR):
			goto _15
		case int32(_BT_S):
			goto _16
		case int32(_BT_GT):
			goto _17
		case int32(_BT_SOL):
			goto _18
		default:
			goto _19
		}
		goto _20
	_4:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
	_9:
		;
	_8:
		;
	_7:
		;
	_6:
		;
	_5:
		;
		ptr += uintptr(2)
		goto _20
	_10:
		;
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		goto _20
	_11:
		;
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
		goto _20
	_12:
		;
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
		goto _20
	_13:
		;
		if hadColon != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		hadColon = int32(1)
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v21 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v21 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v21 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _20
	_16:
		;
	_15:
		;
	_14:
		;
		ptr += uintptr(2)
		for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v22 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
			} else {
				v22 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
			}
			switch v22 {
			case int32(_BT_NONASCII):
				if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				} /* fall through */
				fallthrough
			case int32(_BT_NMSTRT):
				fallthrough
			case int32(_BT_HEX):
				ptr += uintptr(2)
			case int32(_BT_LEAD2):
				if int64(end)-int64(ptr) < int64(2) {
					return -int32(2)
				}
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(ptr) < int64(3) {
					return -int32(2)
				}
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(ptr) < int64(4) {
					return -int32(2)
				}
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(4)
			case int32(_BT_GT):
				goto gt
			case int32(_BT_SOL):
				goto sol
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				ptr += uintptr(2)
				continue
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			return _little2_scanAtts(tls, enc, ptr, end, nextTokPtr)
		}
		return -int32(1)
	_17:
		;
		goto gt
	gt:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_START_TAG_NO_ATTS)
	_18:
		;
		goto sol
	sol:
		;
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_EMPTY_ELEMENT_NO_ATTS)
	_19:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	_20:
	}
	return -int32(1)
}

func _little2_contentTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var v1, v2, v3 int32
	_, _, _, _ = n, v1, v2, v3
	if ptr >= end {
		return -int32(4)
	}
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_LT):
		return _little2_scanLt(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_AMP):
		return _little2_scanRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_CR):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(3)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		if v2 == int32(_BT_LF) {
			ptr += uintptr(2)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LF):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_RSQB):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(5)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_RSQB1)) {
			break
		}
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(5)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1)) {
			ptr -= uintptr(2)
			break
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NONXML):
		fallthrough
	case int32(_BT_MALFORM):
		fallthrough
	case int32(_BT_TRAIL):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	default:
		ptr += uintptr(2)
		break
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v3 {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(4)
		case int32(_BT_RSQB):
			if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(2)) {
				if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == int32(m_ASCII_RSQB1)) {
					ptr += uintptr(2)
					break
				}
				if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(3)*libc.Int32FromInt32(2)) {
					if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2)) + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))))) == int32(m_ASCII_GT1)) {
						ptr += uintptr(2)
						break
					}
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))
					return m_XML_TOK_INVALID
				}
			}
			/* fall through */
			fallthrough
		case int32(_BT_AMP):
			fallthrough
		case int32(_BT_LT):
			fallthrough
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

/* ptr points to character following "%" */

func _little2_scanPercent(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_S):
		fallthrough
	case int32(_BT_LF):
		fallthrough
	case int32(_BT_CR):
		fallthrough
	case int32(_BT_PERCNT):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_PERCENT)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_SEMI):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_PARAM_ENTITY_REF)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _little2_scanPoundName(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_VERBAR):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_POUND_NAME)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(m_XML_TOK_POUND_NAME)
}

func _little2_scanLit(tls *libc.TLS, open int32, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var t, v1, v2 int32
	_, _, _ = t, v1, v2
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		t = v1
		switch t {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_QUOT):
			fallthrough
		case int32(_BT_APOS):
			ptr += uintptr(2)
			if t != open {
				break
			}
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(m_XML_TOK_LITERAL)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
			} else {
				v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
			}
			switch v2 {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_GT):
				fallthrough
			case int32(_BT_PERCNT):
				fallthrough
			case int32(_BT_LSQB):
				return int32(m_XML_TOK_LITERAL)
			default:
				return m_XML_TOK_INVALID
			}
			fallthrough
		default:
			ptr += uintptr(2)
			break
		}
	}
	return -int32(1)
}

func _little2_prologTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var tok, v1, v29, v33, v34, v35, v36 int32
	_, _, _, _, _, _, _, _ = n, tok, v1, v29, v33, v34, v35, v36
	if ptr >= end {
		return -int32(4)
	}
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v1 {
	case int32(_BT_QUOT):
		goto _2
	case int32(_BT_APOS):
		goto _3
	case int32(_BT_LT):
		goto _4
	case int32(_BT_CR):
		goto _5
	case int32(_BT_LF):
		goto _6
	case int32(_BT_S):
		goto _7
	case int32(_BT_PERCNT):
		goto _8
	case int32(_BT_COMMA):
		goto _9
	case int32(_BT_LSQB):
		goto _10
	case int32(_BT_RSQB):
		goto _11
	case int32(_BT_LPAR):
		goto _12
	case int32(_BT_RPAR):
		goto _13
	case int32(_BT_VERBAR):
		goto _14
	case int32(_BT_GT):
		goto _15
	case int32(_BT_NUM):
		goto _16
	case int32(_BT_LEAD2):
		goto _17
	case int32(_BT_LEAD3):
		goto _18
	case int32(_BT_LEAD4):
		goto _19
	case int32(_BT_HEX):
		goto _20
	case int32(_BT_NMSTRT):
		goto _21
	case int32(_BT_COLON):
		goto _22
	case int32(_BT_MINUS):
		goto _23
	case int32(_BT_NAME):
		goto _24
	case int32(_BT_DIGIT):
		goto _25
	case int32(_BT_NONASCII):
		goto _26
	default:
		goto _27
	}
	goto _28
_2:
	;
	return _little2_scanLit(tls, int32(_BT_QUOT), enc, ptr+uintptr(2), end, nextTokPtr)
_3:
	;
	return _little2_scanLit(tls, int32(_BT_APOS), enc, ptr+uintptr(2), end, nextTokPtr)
_4:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v29 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v29 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v29 {
	case int32(_BT_EXCL):
		return _little2_scanDecl(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_QUEST):
		return _little2_scanPi(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		fallthrough
	case int32(_BT_NONASCII):
		fallthrough
	case int32(_BT_LEAD2):
		fallthrough
	case int32(_BT_LEAD3):
		fallthrough
	case int32(_BT_LEAD4):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr - uintptr(2)
		return int32(m_XML_TOK_INSTANCE_START)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_5:
	;
	if ptr+uintptr(2) == end {
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = end
		/* indicate that this might be part of a CR/LF pair */
		return -int32(m_XML_TOK_PROLOG_S)
	}
	/* fall through */
_7:
	;
_6:
	;
_32:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		goto _30
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v33 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v33 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v33 {
	case int32(_BT_S):
		fallthrough
	case int32(_BT_LF):
	case int32(_BT_CR):
		/* don't split CR/LF pair */
		if ptr+uintptr(2) != end {
			break
		}
		/* fall through */
		fallthrough
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_PROLOG_S)
	}
	goto _31
_31:
	;
	goto _32
	goto _30
_30:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_PROLOG_S)
_8:
	;
	return _little2_scanPercent(tls, enc, ptr+uintptr(2), end, nextTokPtr)
_9:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_COMMA)
_10:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_OPEN_BRACKET)
_11:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(m_XML_TOK_CLOSE_BRACKET)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_RSQB1) {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == int32(m_ASCII_GT1) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))
			return int32(m_XML_TOK_COND_SECT_CLOSE)
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_CLOSE_BRACKET)
_12:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_OPEN_PAREN)
_13:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(m_XML_TOK_CLOSE_PAREN)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
		v34 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
	} else {
		v34 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
	}
	switch v34 {
	case int32(_BT_AST):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CLOSE_PAREN_ASTERISK)
	case int32(_BT_QUEST):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CLOSE_PAREN_QUESTION)
	case int32(_BT_PLUS):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CLOSE_PAREN_PLUS)
	case int32(_BT_CR):
		fallthrough
	case int32(_BT_LF):
		fallthrough
	case int32(_BT_S):
		fallthrough
	case int32(_BT_GT):
		fallthrough
	case int32(_BT_COMMA):
		fallthrough
	case int32(_BT_VERBAR):
		fallthrough
	case int32(_BT_RPAR):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_CLOSE_PAREN)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_14:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_OR)
_15:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_DECL_CLOSE)
_16:
	;
	return _little2_scanPoundName(tls, enc, ptr+uintptr(2), end, nextTokPtr)
_17:
	;
	if int64(end)-int64(ptr) < int64(2) {
		return -int32(2)
	}
	if 0 != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if 0 != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_18:
	;
	if int64(end)-int64(ptr) < int64(3) {
		return -int32(2)
	}
	if 0 != 0 {
		ptr += uintptr(3)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if 0 != 0 {
		ptr += uintptr(3)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_19:
	;
	if int64(end)-int64(ptr) < int64(4) {
		return -int32(2)
	}
	if 0 != 0 {
		ptr += uintptr(4)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if 0 != 0 {
		ptr += uintptr(4)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_21:
	;
_20:
	;
	tok = int32(m_XML_TOK_NAME)
	ptr += uintptr(2)
	goto _28
_25:
	;
_24:
	;
_23:
	;
_22:
	;
	tok = int32(m_XML_TOK_NMTOKEN)
	ptr += uintptr(2)
	goto _28
_26:
	;
	if _namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(uint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&int32(0x1F))) != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if _namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(uint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&int32(0x1F))) != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	/* fall through */
_27:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_28:
	;
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v35 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v35 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v35 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_COMMA):
			fallthrough
		case int32(_BT_VERBAR):
			fallthrough
		case int32(_BT_LSQB):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return tok
		case int32(_BT_COLON):
			ptr += uintptr(2)
			switch tok {
			case int32(m_XML_TOK_NAME):
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				tok = int32(m_XML_TOK_PREFIXED_NAME)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
					v36 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
				} else {
					v36 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
				}
				switch v36 {
				case int32(_BT_NONASCII):
					if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr + 1))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))&libc.Int32FromInt32(0x1F))) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					} /* fall through */
					fallthrough
				case int32(_BT_NMSTRT):
					fallthrough
				case int32(_BT_HEX):
					fallthrough
				case int32(_BT_DIGIT):
					fallthrough
				case int32(_BT_NAME):
					fallthrough
				case int32(_BT_MINUS):
					ptr += uintptr(2)
				case int32(_BT_LEAD2):
					if int64(end)-int64(ptr) < int64(2) {
						return -int32(2)
					}
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(2)
				case int32(_BT_LEAD3):
					if int64(end)-int64(ptr) < int64(3) {
						return -int32(2)
					}
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(3)
				case int32(_BT_LEAD4):
					if int64(end)-int64(ptr) < int64(4) {
						return -int32(2)
					}
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(4)
				default:
					tok = int32(m_XML_TOK_NMTOKEN)
					break
				}
			case int32(m_XML_TOK_PREFIXED_NAME):
				tok = int32(m_XML_TOK_NMTOKEN)
				break
			}
		case int32(_BT_PLUS):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_NAME_PLUS)
		case int32(_BT_AST):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_NAME_ASTERISK)
		case int32(_BT_QUEST):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_NAME_QUESTION)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -tok
}

func _little2_attributeValueTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var start uintptr
	var v1, v2 int32
	_, _, _ = start, v1, v2
	if ptr >= end {
		return -int32(4)
	} else {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the paranoia
			 * check is still valuable, however.
			 */
			return -int32(1) /* LCOV_EXCL_LINE */
		}
	}
	start = ptr
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_AMP):
			if ptr == start {
				return _little2_scanRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_LT):
			/* this is for inside entity references */
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_LF):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_CR):
			if ptr == start {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(3)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
					v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
				} else {
					v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
				}
				if v2 == int32(_BT_LF) {
					ptr += uintptr(2)
				}
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_S):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_ATTRIBUTE_VALUE_S)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

func _little2_entityValueTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var start uintptr
	var tok, v1, v2, v3 int32
	_, _, _, _, _ = start, tok, v1, v2, v3
	if ptr >= end {
		return -int32(4)
	} else {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the paranoia
			 * check is still valuable, however.
			 */
			return -int32(1) /* LCOV_EXCL_LINE */
		}
	}
	start = ptr
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_AMP):
			if ptr == start {
				return _little2_scanRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_PERCNT):
			if ptr == start {
				tok = _little2_scanPercent(tls, enc, ptr+uintptr(2), end, nextTokPtr)
				if tok == int32(m_XML_TOK_PERCENT) {
					v2 = m_XML_TOK_INVALID
				} else {
					v2 = tok
				}
				return v2
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_LF):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_CR):
			if ptr == start {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(3)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
					v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
				} else {
					v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
				}
				if v3 == int32(_BT_LF) {
					ptr += uintptr(2)
				}
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

func _little2_ignoreSectionTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var level, v1 int32
	var n Tsize_t
	_, _, _ = level, n, v1
	level = 0
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			end = ptr + uintptr(n)
		}
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_LT):
			ptr += uintptr(2)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_EXCL) {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_LSQB1) {
					level++
					ptr += uintptr(2)
				}
			}
		case int32(_BT_RSQB):
			ptr += uintptr(2)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_RSQB1) {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_GT1) {
					ptr += uintptr(2)
					if level == 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return int32(m_XML_TOK_IGNORE_SECT)
					}
					level--
				}
			}
		default:
			ptr += uintptr(2)
			break
		}
	}
	return -int32(1)
}

func _little2_isPublicId(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, badPtr uintptr) (r int32) {
	var v2, v3, v4 int32
	_, _, _ = v2, v3, v4
	ptr += uintptr(2)
	end -= uintptr(2)
	for {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			break
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_MINUS):
			fallthrough
		case int32(_BT_APOS):
			fallthrough
		case int32(_BT_LPAR):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_PLUS):
			fallthrough
		case int32(_BT_COMMA):
			fallthrough
		case int32(_BT_SOL):
			fallthrough
		case int32(_BT_EQUALS):
			fallthrough
		case int32(_BT_QUEST):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_SEMI):
			fallthrough
		case int32(_BT_EXCL):
			fallthrough
		case int32(_BT_AST):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_NUM):
			fallthrough
		case int32(_BT_COLON):
		case int32(_BT_S):
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_TAB1) {
				*(*uintptr)(unsafe.Pointer(badPtr)) = ptr
				return 0
			}
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_NMSTRT):
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			} else {
				v3 = -int32(1)
			}
			if !(v3 & ^libc.Int32FromInt32(0x7f) != 0) {
				break
			}
			/* fall through */
			fallthrough
		default:
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v4 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			} else {
				v4 = -int32(1)
			}
			switch v4 {
			case int32(0x24): /* $ */
				fallthrough
			case int32(0x40): /* @ */
			default:
				*(*uintptr)(unsafe.Pointer(badPtr)) = ptr
				return 0
			}
			break
		}
		goto _1
	_1:
		;
		ptr += uintptr(2)
	}
	return int32(1)
}

/* This must only be called for a well-formed start-tag or empty
   element tag.  Returns the number of attributes.  Pointers to the
   first attsMax attributes are stored in atts.
*/

func _little2_getAtts(tls *libc.TLS, enc uintptr, ptr uintptr, attsMax int32, atts uintptr) (r int32) {
	var nAtts, open, state, v2, v3, v5, v7 int32
	var v4, v6, v8, v9 bool
	_, _, _, _, _, _, _, _, _, _, _ = nAtts, open, state, v2, v3, v4, v5, v6, v7, v8, v9
	state = 1
	nAtts = 0
	open = 0 /* defined when state == inValue;
	   initialization just to shut up compilers */
	ptr += uintptr(2)
	for {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_LEAD2):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(2) - libc.Int32FromInt32(2))
		case int32(_BT_LEAD3):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(3) - libc.Int32FromInt32(2))
		case int32(_BT_LEAD4):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(4) - libc.Int32FromInt32(2))
		case int32(_BT_NONASCII):
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
		case int32(_BT_QUOT):
			if state != 2 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr = ptr + uintptr(2)
				}
				state = 2
				open = int32(_BT_QUOT)
			} else {
				if open == int32(_BT_QUOT) {
					state = 0
					if nAtts < attsMax {
						(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvalueEnd = ptr
					}
					nAtts++
				}
			}
		case int32(_BT_APOS):
			if state != 2 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr = ptr + uintptr(2)
				}
				state = 2
				open = int32(_BT_APOS)
			} else {
				if open == int32(_BT_APOS) {
					state = 0
					if nAtts < attsMax {
						(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvalueEnd = ptr
					}
					nAtts++
				}
			}
		case int32(_BT_AMP):
			if nAtts < attsMax {
				(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
			}
		case int32(_BT_S):
			if state == 1 {
				state = 0
			} else {
				if v9 = state == 2 && nAtts < attsMax && (*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized != 0; v9 {
					if v4 = ptr == (*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr; !v4 {
						if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
							v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
						} else {
							v3 = -int32(1)
						}
					}
					if v6 = v4 || v3 != int32(m_ASCII_SPACE1); !v6 {
						if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == 0 {
							v5 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2))))
						} else {
							v5 = -int32(1)
						}
					}
					if v8 = v6 || v5 == int32(m_ASCII_SPACE1); !v8 {
						if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == 0 {
							v7 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))))))
						} else {
							v7 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1)), *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2))))
						}
					}
				}
				if v9 && (v8 || v7 == open) {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
				}
			}
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			/* This case ensures that the first attribute name is counted
			   Apart from that we could just change state on the quote. */
			if state == 1 {
				state = 0
			} else {
				if state == 2 && nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
				}
			}
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_SOL):
			if state != 2 {
				return nAtts
			}
		default:
			break
		}
		goto _1
	_1:
		;
		ptr += uintptr(2)
	}
	/* not reached */
	return r
}

func _little2_charRefNumber(tls *libc.TLS, enc uintptr, ptr uintptr) (r int32) {
	var c, c1, result, v2, v4 int32
	_, _, _, _, _ = c, c1, result, v2, v4
	result = 0
	/* skip &# */
	_ = enc
	ptr += uintptr(libc.Int32FromInt32(2) * libc.Int32FromInt32(2))
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_x) {
		ptr += uintptr(2)
		for {
			if !!(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_SEMI1)) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			} else {
				v2 = -int32(1)
			}
			c = v2
			switch c {
			case int32(m_ASCII_0):
				fallthrough
			case int32(m_ASCII_1):
				fallthrough
			case int32(m_ASCII_2):
				fallthrough
			case int32(m_ASCII_3):
				fallthrough
			case int32(m_ASCII_41):
				fallthrough
			case int32(m_ASCII_51):
				fallthrough
			case int32(m_ASCII_61):
				fallthrough
			case int32(m_ASCII_71):
				fallthrough
			case int32(m_ASCII_8):
				fallthrough
			case int32(m_ASCII_9):
				result <<= int32(4)
				result |= c - int32(m_ASCII_0)
			case int32(m_ASCII_A):
				fallthrough
			case int32(m_ASCII_B1):
				fallthrough
			case int32(m_ASCII_C):
				fallthrough
			case int32(m_ASCII_D):
				fallthrough
			case int32(m_ASCII_E):
				fallthrough
			case int32(m_ASCII_F):
				result <<= int32(4)
				result += int32(10) + (c - int32(m_ASCII_A))
			case int32(m_ASCII_a):
				fallthrough
			case int32(m_ASCII_b1):
				fallthrough
			case int32(m_ASCII_c):
				fallthrough
			case int32(m_ASCII_d1):
				fallthrough
			case int32(m_ASCII_e):
				fallthrough
			case int32(m_ASCII_f1):
				result <<= int32(4)
				result += int32(10) + (c - int32(m_ASCII_a))
				break
			}
			if result >= int32(0x110000) {
				return -int32(1)
			}
			goto _1
		_1:
			;
			ptr += uintptr(2)
		}
	} else {
		for {
			if !!(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_SEMI1)) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v4 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			} else {
				v4 = -int32(1)
			}
			c1 = v4
			result *= int32(10)
			result += c1 - int32(m_ASCII_0)
			if result >= int32(0x110000) {
				return -int32(1)
			}
			goto _3
		_3:
			;
			ptr += uintptr(2)
		}
	}
	return _checkCharRefNumber(tls, result)
}

func _little2_predefinedEntityName(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	_ = enc
	switch (int64(end) - int64(ptr)) / libc.Int64FromInt32(2) {
	case int64(2):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == int32(m_ASCII_t) {
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
				v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
			} else {
				v1 = -int32(1)
			}
			switch v1 {
			case int32(m_ASCII_l):
				return int32(m_ASCII_LT1)
			case int32(m_ASCII_g):
				return int32(m_ASCII_GT1)
			}
		}
	case int64(3):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_a) {
			ptr += uintptr(2)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_m) {
				ptr += uintptr(2)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_p) {
					return int32(m_ASCII_AMP1)
				}
			}
		}
	case int64(4):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))
		} else {
			v2 = -int32(1)
		}
		switch v2 {
		case int32(m_ASCII_q1):
			ptr += uintptr(2)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_u1) {
				ptr += uintptr(2)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_o) {
					ptr += uintptr(2)
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_t) {
						return int32(m_ASCII_QUOT1)
					}
				}
			}
		case int32(m_ASCII_a):
			ptr += uintptr(2)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_p) {
				ptr += uintptr(2)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_o) {
					ptr += uintptr(2)
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32(m_ASCII_s) {
						return int32(m_ASCII_APOS1)
					}
				}
			}
			break
		}
	}
	return 0
}

func _little2_nameMatchesAscii(tls *libc.TLS, enc uintptr, ptr1 uintptr, end1 uintptr, ptr2 uintptr) (r int32) {
	_ = enc
	for {
		if !(*(*uint8)(unsafe.Pointer(ptr2)) != 0) {
			break
		}
		if int64(end1)-int64(ptr1) < int64(2) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the
			 * paranoia check is still valuable, however.
			 */
			return 0 /* LCOV_EXCL_LINE */
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr1 + 1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr1))) == libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr2)))) {
			return 0
		}
		goto _1
	_1:
		;
		ptr1 += uintptr(2)
		ptr2++
	}
	return libc.BoolInt32(ptr1 == end1)
}

func _little2_nameLength(tls *libc.TLS, enc uintptr, ptr uintptr) (r int32) {
	var start uintptr
	var v2 int32
	_, _ = start, v2
	start = ptr
	for {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_NONASCII):
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_COLON):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		default:
			return int32(int64(ptr) - int64(start))
		}
		goto _1
	_1:
	}
	return r
}

func _little2_skipS(tls *libc.TLS, enc uintptr, ptr uintptr) (r uintptr) {
	var v2 int32
	_ = v2
	for {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v2 {
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_S):
			ptr += uintptr(2)
		default:
			return ptr
		}
		goto _1
	_1:
	}
	return r
}

func _little2_updatePosition(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, pos uintptr) {
	var v1, v2 int32
	var v3 bool
	_, _, _ = v1, v2, v3
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LF):
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber = uint64(0)
			(*TPOSITION)(unsafe.Pointer(pos)).FlineNumber++
			ptr += uintptr(2)
		case int32(_BT_CR):
			(*TPOSITION)(unsafe.Pointer(pos)).FlineNumber++
			ptr += uintptr(2)
			if v3 = int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)); v3 {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == 0 {
					v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr))))))
				} else {
					v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + 1)), *(*uint8)(unsafe.Pointer(ptr)))
				}
			}
			if v3 && v2 == int32(_BT_LF) {
				ptr += uintptr(2)
			}
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber = uint64(0)
		default:
			ptr += uintptr(2)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
			break
		}
	}
}

var _little2_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(2),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_little2_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_little2_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_little2_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_little2_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_little2_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_little2_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_little2_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_little2_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_little2_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_little2_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_little2_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_little2_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_little2_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_little2_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_little2_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_little2_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_little2_toUtf16)
}

var _little2_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(2),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_little2_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_little2_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_little2_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_little2_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_little2_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_little2_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_little2_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_little2_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_little2_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_little2_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_little2_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_little2_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_little2_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_little2_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_little2_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_little2_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_little2_toUtf16)
}

/* CHAR_MATCHES is guaranteed to have MINBPC bytes available. */

/* This file is included!
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* ptr points to character following "<!-" */

func _big2_scanComment(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1 int32
	_ = v1
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_MINUS1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
			} else {
				v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
			}
			switch v1 {
			case int32(_BT_LEAD2):
				if int64(end)-int64(ptr) < int64(2) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(ptr) < int64(3) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(ptr) < int64(4) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(4)
			case int32(_BT_NONXML):
				fallthrough
			case int32(_BT_MALFORM):
				fallthrough
			case int32(_BT_TRAIL):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			case int32(_BT_MINUS):
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_MINUS1) {
					ptr += uintptr(2)
					if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
						return -int32(1)
					}
					if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1)) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
					return int32(m_XML_TOK_COMMENT)
				}
			default:
				ptr += uintptr(2)
				break
			}
		}
	}
	return -int32(1)
}

/* ptr points to character following "<!" */

func _big2_scanDecl(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2, v3 int32
	_, _, _ = v1, v2, v3
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_MINUS):
		return _big2_scanComment(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_LSQB):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_COND_SECT_OPEN)
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_PERCNT):
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			/* don't allow <!ENTITY% foo "whatever"> */
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))))))
			} else {
				v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2))), *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1)))
			}
			switch v3 {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_PERCNT):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			/* fall through */
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DECL_OPEN)
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			ptr += uintptr(2)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _big2_checkPiTarget(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, tokPtr uintptr) (r int32) {
	var upper, v1, v2, v3 int32
	_, _, _, _ = upper, v1, v2, v3
	upper = 0
	_ = enc
	*(*int32)(unsafe.Pointer(tokPtr)) = int32(m_XML_TOK_PI)
	if int64(end)-int64(ptr) != int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(3)) {
		return int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
	} else {
		v1 = -int32(1)
	}
	switch v1 {
	case int32(m_ASCII_x):
	case int32(m_ASCII_X):
		upper = int32(1)
	default:
		return int32(1)
	}
	ptr += uintptr(2)
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
	} else {
		v2 = -int32(1)
	}
	switch v2 {
	case int32(m_ASCII_m):
	case int32(m_ASCII_M):
		upper = int32(1)
	default:
		return int32(1)
	}
	ptr += uintptr(2)
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
	} else {
		v3 = -int32(1)
	}
	switch v3 {
	case int32(m_ASCII_l):
	case int32(m_ASCII_L):
		upper = int32(1)
	default:
		return int32(1)
	}
	if upper != 0 {
		return 0
	}
	*(*int32)(unsafe.Pointer(tokPtr)) = int32(m_XML_TOK_XML_DECL)
	return int32(1)
}

/* ptr points to character following "<?" */

func _big2_scanPi(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var target uintptr
	var v1, v2, v3 int32
	var _ /* tok at bp+0 */ int32
	_, _, _, _ = target, v1, v2, v3
	target = ptr
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			if !(_big2_checkPiTarget(tls, enc, target, ptr, bp) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
			for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
					v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
				} else {
					v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
				}
				switch v3 {
				case int32(_BT_LEAD2):
					if int64(end)-int64(ptr) < int64(2) {
						return -int32(2)
					}
					if 0 != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(2)
				case int32(_BT_LEAD3):
					if int64(end)-int64(ptr) < int64(3) {
						return -int32(2)
					}
					if 0 != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(3)
				case int32(_BT_LEAD4):
					if int64(end)-int64(ptr) < int64(4) {
						return -int32(2)
					}
					if 0 != 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(4)
				case int32(_BT_NONXML):
					fallthrough
				case int32(_BT_MALFORM):
					fallthrough
				case int32(_BT_TRAIL):
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				case int32(_BT_QUEST):
					ptr += uintptr(2)
					if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
						return -int32(1)
					}
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
						return *(*int32)(unsafe.Pointer(bp))
					}
				default:
					ptr += uintptr(2)
					break
				}
			}
			return -int32(1)
		case int32(_BT_QUEST):
			if !(_big2_checkPiTarget(tls, enc, target, ptr, bp) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return *(*int32)(unsafe.Pointer(bp))
			}
			/* fall through */
			fallthrough
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _big2_scanCdataSection(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var i int32
	_ = i
	_ = enc
	/* CDATA[ */
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(6)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	i = 0
	for {
		if !(i < int32(6)) {
			break
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == libc.Int32FromUint8(_CDATA_LSQB2[i])) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _1
	_1:
		;
		i++
		ptr += uintptr(2)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_CDATA_SECT_OPEN)
}

var _CDATA_LSQB2 = [6]uint8{
	0: uint8(m_ASCII_C),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_A),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_A),
	5: uint8(m_ASCII_LSQB1),
}

func _big2_cdataSectionTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var v1, v2, v3 int32
	_, _, _, _ = n, v1, v2, v3
	if ptr >= end {
		return -int32(4)
	}
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_RSQB):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_RSQB1)) {
			break
		}
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1)) {
			ptr -= uintptr(2)
			break
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CDATA_SECT_CLOSE)
	case int32(_BT_CR):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		if v2 == int32(_BT_LF) {
			ptr += uintptr(2)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LF):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NONXML):
		fallthrough
	case int32(_BT_MALFORM):
		fallthrough
	case int32(_BT_TRAIL):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	default:
		ptr += uintptr(2)
		break
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v3 {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_RSQB):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

/* ptr points to character following "</" */

func _big2_scanEndTag(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2, v22 int32
	_, _, _ = v1, v2, v22
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			goto _3
		case int32(_BT_MINUS):
			goto _4
		case int32(_BT_NAME):
			goto _5
		case int32(_BT_DIGIT):
			goto _6
		case int32(_BT_HEX):
			goto _7
		case int32(_BT_NMSTRT):
			goto _8
		case int32(_BT_LEAD2):
			goto _9
		case int32(_BT_LEAD3):
			goto _10
		case int32(_BT_LEAD4):
			goto _11
		case int32(_BT_LF):
			goto _12
		case int32(_BT_CR):
			goto _13
		case int32(_BT_S):
			goto _14
		case int32(_BT_COLON):
			goto _15
		case int32(_BT_GT):
			goto _16
		default:
			goto _17
		}
		goto _18
	_3:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
	_8:
		;
	_7:
		;
	_6:
		;
	_5:
		;
	_4:
		;
		ptr += uintptr(2)
		goto _18
	_9:
		;
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		goto _18
	_10:
		;
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
		goto _18
	_11:
		;
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
		goto _18
	_14:
		;
	_13:
		;
	_12:
		;
		ptr += uintptr(2)
	_21:
		;
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			goto _19
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v22 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v22 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v22 {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
		case int32(_BT_GT):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_END_TAG)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _20
	_20:
		;
		ptr += uintptr(2)
		goto _21
		goto _19
	_19:
		;
		return -int32(1)
	_15:
		;
		/* no need to check qname syntax here,
		   since end-tag must match exactly */
		ptr += uintptr(2)
		goto _18
	_16:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_END_TAG)
	_17:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	_18:
	}
	return -int32(1)
}

/* ptr points to character following "&#X" */

func _big2_scanHexCharRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v3 int32
	_, _ = v1, v3
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v1 {
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_HEX):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		for {
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
			} else {
				v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
			}
			switch v3 {
			case int32(_BT_DIGIT):
				fallthrough
			case int32(_BT_HEX):
			case int32(_BT_SEMI):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_CHAR_REF)
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			goto _2
		_2:
			;
			ptr += uintptr(2)
		}
	}
	return -int32(1)
}

/* ptr points to character following "&#" */

func _big2_scanCharRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v3 int32
	_, _ = v1, v3
	if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_x) {
			return _big2_scanHexCharRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v1 {
		case int32(_BT_DIGIT):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		for {
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
			} else {
				v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
			}
			switch v3 {
			case int32(_BT_DIGIT):
			case int32(_BT_SEMI):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_CHAR_REF)
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			goto _2
		_2:
			;
			ptr += uintptr(2)
		}
	}
	return -int32(1)
}

/* ptr points to character following "&" */

func _big2_scanRef(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NUM):
		return _big2_scanCharRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_SEMI):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_ENTITY_REF)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

/* ptr points to character following first character of attribute name */

func _big2_scanAtts(tls *libc.TLS, enc uintptr, _ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var hadColon, open, t, t1, tok, v1, v18, v22, v24, v26, v27, v31 int32
	_, _, _, _, _, _, _, _, _, _, _, _ = hadColon, open, t, t1, tok, v1, v18, v22, v24, v26, v27, v31
	hadColon = 0
	for int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
		}
		switch v1 {
		case int32(_BT_NONASCII):
			goto _2
		case int32(_BT_MINUS):
			goto _3
		case int32(_BT_NAME):
			goto _4
		case int32(_BT_DIGIT):
			goto _5
		case int32(_BT_HEX):
			goto _6
		case int32(_BT_NMSTRT):
			goto _7
		case int32(_BT_LEAD2):
			goto _8
		case int32(_BT_LEAD3):
			goto _9
		case int32(_BT_LEAD4):
			goto _10
		case int32(_BT_COLON):
			goto _11
		case int32(_BT_LF):
			goto _12
		case int32(_BT_CR):
			goto _13
		case int32(_BT_S):
			goto _14
		case int32(_BT_EQUALS):
			goto _15
		default:
			goto _16
		}
		goto _17
	_2:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		} /* fall through */
	_7:
		;
	_6:
		;
	_5:
		;
	_4:
		;
	_3:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _17
	_8:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _17
	_9:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		goto _17
	_10:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		goto _17
	_11:
		;
		if hadColon != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		hadColon = int32(1)
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
			v18 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
		} else {
			v18 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
		}
		switch v18 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		goto _17
	_14:
		;
	_13:
		;
	_12:
		;
	_21:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
			v22 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
		} else {
			v22 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
		}
		t = v22
		if t == int32(_BT_EQUALS) {
			goto _19
		}
		switch t {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_CR):
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		goto _20
	_20:
		;
		goto _21
		goto _19
	_19:
		;
		/* fall through */
	_15:
		;
		hadColon = 0
		for {
			*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
			if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
				v24 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
			} else {
				v24 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
			}
			open = v24
			if open == int32(_BT_QUOT) || open == int32(_BT_APOS) {
				break
			}
			switch open {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_CR):
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			}
			goto _23
		_23:
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		/* in attribute value */
		for {
			if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
				v26 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
			} else {
				v26 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
			}
			t1 = v26
			if t1 == open {
				break
			}
			switch t1 {
			case int32(_BT_LEAD2):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
					return -int32(2)
				}
				if 0 != 0 {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					return m_XML_TOK_INVALID
				}
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
			case int32(_BT_NONXML):
				fallthrough
			case int32(_BT_MALFORM):
				fallthrough
			case int32(_BT_TRAIL):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			case int32(_BT_AMP):
				tok = _big2_scanRef(tls, enc, *(*uintptr)(unsafe.Pointer(bp))+uintptr(2), end, bp)
				if tok <= 0 {
					if tok == m_XML_TOK_INVALID {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
					}
					return tok
				}
			case int32(_BT_LT):
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return m_XML_TOK_INVALID
			default:
				*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
				break
			}
			goto _25
		_25:
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
			v27 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
		} else {
			v27 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
		}
		switch v27 {
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
		case int32(_BT_SOL):
			goto sol
		case int32(_BT_GT):
			goto gt
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		/* ptr points to closing quote */
	_30:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 {
			v31 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))))))
		} else {
			v31 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)))), *(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))
		}
		switch v31 {
		case int32(_BT_NONASCII):
			goto _32
		case int32(_BT_HEX):
			goto _33
		case int32(_BT_NMSTRT):
			goto _34
		case int32(_BT_LEAD2):
			goto _35
		case int32(_BT_LEAD3):
			goto _36
		case int32(_BT_LEAD4):
			goto _37
		case int32(_BT_LF):
			goto _38
		case int32(_BT_CR):
			goto _39
		case int32(_BT_S):
			goto _40
		case int32(_BT_GT):
			goto _41
		case int32(_BT_SOL):
			goto _42
		default:
			goto _43
		}
		goto _44
	_32:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		} /* fall through */
	_34:
		;
	_33:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _44
	_35:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		goto _44
	_36:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(3)
		goto _44
	_37:
		;
		if int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(4)
		goto _44
	_40:
		;
	_39:
		;
	_38:
		;
		goto _29
	_41:
		;
		goto gt
	gt:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp)) + uintptr(2)
		return int32(m_XML_TOK_START_TAG_WITH_ATTS)
	_42:
		;
		goto sol
	sol:
		;
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr(2)
		if !(int64(end)-int64(*(*uintptr)(unsafe.Pointer(bp))) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp))))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp)) + 1))) == int32(m_ASCII_GT1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp)) + uintptr(2)
		return int32(m_XML_TOK_EMPTY_ELEMENT_WITH_ATTS)
	_43:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return m_XML_TOK_INVALID
	_44:
		;
		goto _28
		goto _29
	_29:
		;
		goto _30
		goto _28
	_28:
		;
		goto _17
	_16:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return m_XML_TOK_INVALID
	_17:
	}
	return -int32(1)
}

/* ptr points to character following "<" */

func _big2_scanLt(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var hadColon, v1, v2, v21, v22, v3 int32
	_, _, _, _, _, _ = hadColon, v1, v2, v21, v22, v3
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_EXCL):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_MINUS):
			return _big2_scanComment(tls, enc, ptr+uintptr(2), end, nextTokPtr)
		case int32(_BT_LSQB):
			return _big2_scanCdataSection(tls, enc, ptr+uintptr(2), end, nextTokPtr)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	case int32(_BT_QUEST):
		return _big2_scanPi(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_SOL):
		return _big2_scanEndTag(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	hadColon = 0
	/* we have a start-tag */
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v3 {
		case int32(_BT_NONASCII):
			goto _4
		case int32(_BT_MINUS):
			goto _5
		case int32(_BT_NAME):
			goto _6
		case int32(_BT_DIGIT):
			goto _7
		case int32(_BT_HEX):
			goto _8
		case int32(_BT_NMSTRT):
			goto _9
		case int32(_BT_LEAD2):
			goto _10
		case int32(_BT_LEAD3):
			goto _11
		case int32(_BT_LEAD4):
			goto _12
		case int32(_BT_COLON):
			goto _13
		case int32(_BT_LF):
			goto _14
		case int32(_BT_CR):
			goto _15
		case int32(_BT_S):
			goto _16
		case int32(_BT_GT):
			goto _17
		case int32(_BT_SOL):
			goto _18
		default:
			goto _19
		}
		goto _20
	_4:
		;
		if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
	_9:
		;
	_8:
		;
	_7:
		;
	_6:
		;
	_5:
		;
		ptr += uintptr(2)
		goto _20
	_10:
		;
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
		goto _20
	_11:
		;
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
		goto _20
	_12:
		;
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
		goto _20
	_13:
		;
		if hadColon != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		hadColon = int32(1)
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v21 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v21 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v21 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		goto _20
	_16:
		;
	_15:
		;
	_14:
		;
		ptr += uintptr(2)
		for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v22 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
			} else {
				v22 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
			}
			switch v22 {
			case int32(_BT_NONASCII):
				if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				} /* fall through */
				fallthrough
			case int32(_BT_NMSTRT):
				fallthrough
			case int32(_BT_HEX):
				ptr += uintptr(2)
			case int32(_BT_LEAD2):
				if int64(end)-int64(ptr) < int64(2) {
					return -int32(2)
				}
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(2)
			case int32(_BT_LEAD3):
				if int64(end)-int64(ptr) < int64(3) {
					return -int32(2)
				}
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(3)
			case int32(_BT_LEAD4):
				if int64(end)-int64(ptr) < int64(4) {
					return -int32(2)
				}
				if !(libc.Int32FromInt32(0) != 0) {
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
					return m_XML_TOK_INVALID
				}
				ptr += uintptr(4)
			case int32(_BT_GT):
				goto gt
			case int32(_BT_SOL):
				goto sol
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				ptr += uintptr(2)
				continue
			default:
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			return _big2_scanAtts(tls, enc, ptr, end, nextTokPtr)
		}
		return -int32(1)
	_17:
		;
		goto gt
	gt:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_START_TAG_NO_ATTS)
	_18:
		;
		goto sol
	sol:
		;
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1)) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_EMPTY_ELEMENT_NO_ATTS)
	_19:
		;
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	_20:
	}
	return -int32(1)
}

func _big2_contentTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var v1, v2, v3 int32
	_, _, _, _ = n, v1, v2, v3
	if ptr >= end {
		return -int32(4)
	}
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_LT):
		return _big2_scanLt(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_AMP):
		return _big2_scanRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_CR):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(3)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		if v2 == int32(_BT_LF) {
			ptr += uintptr(2)
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_LF):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_DATA_NEWLINE)
	case int32(_BT_RSQB):
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(5)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_RSQB1)) {
			break
		}
		ptr += uintptr(2)
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			return -int32(5)
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1)) {
			ptr -= uintptr(2)
			break
		}
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if 0 != 0 {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_NONXML):
		fallthrough
	case int32(_BT_MALFORM):
		fallthrough
	case int32(_BT_TRAIL):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	default:
		ptr += uintptr(2)
		break
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v3 {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) || libc.Bool(0 != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_CHARS)
			}
			ptr += uintptr(4)
		case int32(_BT_RSQB):
			if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(2)) {
				if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == int32(m_ASCII_RSQB1)) {
					ptr += uintptr(2)
					break
				}
				if int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(3)*libc.Int32FromInt32(2)) {
					if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2)) + 1))) == int32(m_ASCII_GT1)) {
						ptr += uintptr(2)
						break
					}
					*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))
					return m_XML_TOK_INVALID
				}
			}
			/* fall through */
			fallthrough
		case int32(_BT_AMP):
			fallthrough
		case int32(_BT_LT):
			fallthrough
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

/* ptr points to character following "%" */

func _big2_scanPercent(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	case int32(_BT_S):
		fallthrough
	case int32(_BT_LF):
		fallthrough
	case int32(_BT_CR):
		fallthrough
	case int32(_BT_PERCNT):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_PERCENT)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_SEMI):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_PARAM_ENTITY_REF)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(1)
}

func _big2_scanPoundName(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_NONASCII):
		if !(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		} /* fall through */
		fallthrough
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		ptr += uintptr(2)
	case int32(_BT_LEAD2):
		if int64(end)-int64(ptr) < int64(2) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(2)
	case int32(_BT_LEAD3):
		if int64(end)-int64(ptr) < int64(3) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(3)
	case int32(_BT_LEAD4):
		if int64(end)-int64(ptr) < int64(4) {
			return -int32(2)
		}
		if !(libc.Int32FromInt32(0) != 0) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
		ptr += uintptr(4)
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return m_XML_TOK_INVALID
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_VERBAR):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_POUND_NAME)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -int32(m_XML_TOK_POUND_NAME)
}

func _big2_scanLit(tls *libc.TLS, open int32, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var t, v1, v2 int32
	_, _, _ = t, v1, v2
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		t = v1
		switch t {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_QUOT):
			fallthrough
		case int32(_BT_APOS):
			ptr += uintptr(2)
			if t != open {
				break
			}
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(m_XML_TOK_LITERAL)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
			} else {
				v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
			}
			switch v2 {
			case int32(_BT_S):
				fallthrough
			case int32(_BT_CR):
				fallthrough
			case int32(_BT_LF):
				fallthrough
			case int32(_BT_GT):
				fallthrough
			case int32(_BT_PERCNT):
				fallthrough
			case int32(_BT_LSQB):
				return int32(m_XML_TOK_LITERAL)
			default:
				return m_XML_TOK_INVALID
			}
			fallthrough
		default:
			ptr += uintptr(2)
			break
		}
	}
	return -int32(1)
}

func _big2_prologTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var n Tsize_t
	var tok, v1, v29, v33, v34, v35, v36 int32
	_, _, _, _, _, _, _, _ = n, tok, v1, v29, v33, v34, v35, v36
	if ptr >= end {
		return -int32(4)
	}
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			if n == uint64(0) {
				return -int32(1)
			}
			end = ptr + uintptr(n)
		}
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v1 {
	case int32(_BT_QUOT):
		goto _2
	case int32(_BT_APOS):
		goto _3
	case int32(_BT_LT):
		goto _4
	case int32(_BT_CR):
		goto _5
	case int32(_BT_LF):
		goto _6
	case int32(_BT_S):
		goto _7
	case int32(_BT_PERCNT):
		goto _8
	case int32(_BT_COMMA):
		goto _9
	case int32(_BT_LSQB):
		goto _10
	case int32(_BT_RSQB):
		goto _11
	case int32(_BT_LPAR):
		goto _12
	case int32(_BT_RPAR):
		goto _13
	case int32(_BT_VERBAR):
		goto _14
	case int32(_BT_GT):
		goto _15
	case int32(_BT_NUM):
		goto _16
	case int32(_BT_LEAD2):
		goto _17
	case int32(_BT_LEAD3):
		goto _18
	case int32(_BT_LEAD4):
		goto _19
	case int32(_BT_HEX):
		goto _20
	case int32(_BT_NMSTRT):
		goto _21
	case int32(_BT_COLON):
		goto _22
	case int32(_BT_MINUS):
		goto _23
	case int32(_BT_NAME):
		goto _24
	case int32(_BT_DIGIT):
		goto _25
	case int32(_BT_NONASCII):
		goto _26
	default:
		goto _27
	}
	goto _28
_2:
	;
	return _big2_scanLit(tls, int32(_BT_QUOT), enc, ptr+uintptr(2), end, nextTokPtr)
_3:
	;
	return _big2_scanLit(tls, int32(_BT_APOS), enc, ptr+uintptr(2), end, nextTokPtr)
_4:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(1)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v29 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v29 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v29 {
	case int32(_BT_EXCL):
		return _big2_scanDecl(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_QUEST):
		return _big2_scanPi(tls, enc, ptr+uintptr(2), end, nextTokPtr)
	case int32(_BT_NMSTRT):
		fallthrough
	case int32(_BT_HEX):
		fallthrough
	case int32(_BT_NONASCII):
		fallthrough
	case int32(_BT_LEAD2):
		fallthrough
	case int32(_BT_LEAD3):
		fallthrough
	case int32(_BT_LEAD4):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr - uintptr(2)
		return int32(m_XML_TOK_INSTANCE_START)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_5:
	;
	if ptr+uintptr(2) == end {
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = end
		/* indicate that this might be part of a CR/LF pair */
		return -int32(m_XML_TOK_PROLOG_S)
	}
	/* fall through */
_7:
	;
_6:
	;
_32:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		goto _30
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v33 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v33 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v33 {
	case int32(_BT_S):
		fallthrough
	case int32(_BT_LF):
	case int32(_BT_CR):
		/* don't split CR/LF pair */
		if ptr+uintptr(2) != end {
			break
		}
		/* fall through */
		fallthrough
	default:
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_PROLOG_S)
	}
	goto _31
_31:
	;
	goto _32
	goto _30
_30:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_PROLOG_S)
_8:
	;
	return _big2_scanPercent(tls, enc, ptr+uintptr(2), end, nextTokPtr)
_9:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_COMMA)
_10:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_OPEN_BRACKET)
_11:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(m_XML_TOK_CLOSE_BRACKET)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_RSQB1) {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))) {
			return -int32(1)
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == int32(m_ASCII_GT1) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(libc.Int32FromInt32(2)*libc.Int32FromInt32(2))
			return int32(m_XML_TOK_COND_SECT_CLOSE)
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_CLOSE_BRACKET)
_12:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_OPEN_PAREN)
_13:
	;
	ptr += uintptr(2)
	if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
		return -int32(m_XML_TOK_CLOSE_PAREN)
	}
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
		v34 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
	} else {
		v34 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
	}
	switch v34 {
	case int32(_BT_AST):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CLOSE_PAREN_ASTERISK)
	case int32(_BT_QUEST):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CLOSE_PAREN_QUESTION)
	case int32(_BT_PLUS):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
		return int32(m_XML_TOK_CLOSE_PAREN_PLUS)
	case int32(_BT_CR):
		fallthrough
	case int32(_BT_LF):
		fallthrough
	case int32(_BT_S):
		fallthrough
	case int32(_BT_GT):
		fallthrough
	case int32(_BT_COMMA):
		fallthrough
	case int32(_BT_VERBAR):
		fallthrough
	case int32(_BT_RPAR):
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return int32(m_XML_TOK_CLOSE_PAREN)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_14:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_OR)
_15:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
	return int32(m_XML_TOK_DECL_CLOSE)
_16:
	;
	return _big2_scanPoundName(tls, enc, ptr+uintptr(2), end, nextTokPtr)
_17:
	;
	if int64(end)-int64(ptr) < int64(2) {
		return -int32(2)
	}
	if 0 != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if 0 != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_18:
	;
	if int64(end)-int64(ptr) < int64(3) {
		return -int32(2)
	}
	if 0 != 0 {
		ptr += uintptr(3)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if 0 != 0 {
		ptr += uintptr(3)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_19:
	;
	if int64(end)-int64(ptr) < int64(4) {
		return -int32(2)
	}
	if 0 != 0 {
		ptr += uintptr(4)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if 0 != 0 {
		ptr += uintptr(4)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_21:
	;
_20:
	;
	tok = int32(m_XML_TOK_NAME)
	ptr += uintptr(2)
	goto _28
_25:
	;
_24:
	;
_23:
	;
_22:
	;
	tok = int32(m_XML_TOK_NMTOKEN)
	ptr += uintptr(2)
	goto _28
_26:
	;
	if _namingBitmap[libc.Int32FromUint8(_nmstrtPages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(uint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&int32(0x1F))) != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NAME)
		goto _28
	}
	if _namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(uint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&int32(0x1F))) != 0 {
		ptr += uintptr(2)
		tok = int32(m_XML_TOK_NMTOKEN)
		goto _28
	}
	/* fall through */
_27:
	;
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return m_XML_TOK_INVALID
_28:
	;
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v35 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v35 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v35 {
		case int32(_BT_NONASCII):
			if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			} /* fall through */
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if !(libc.Int32FromInt32(0) != 0) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_COMMA):
			fallthrough
		case int32(_BT_VERBAR):
			fallthrough
		case int32(_BT_LSQB):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_S):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return tok
		case int32(_BT_COLON):
			ptr += uintptr(2)
			switch tok {
			case int32(m_XML_TOK_NAME):
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				tok = int32(m_XML_TOK_PREFIXED_NAME)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
					v36 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
				} else {
					v36 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
				}
				switch v36 {
				case int32(_BT_NONASCII):
					if !(_namingBitmap[libc.Int32FromUint8(_namePages[*(*uint8)(unsafe.Pointer(ptr))])<<int32(3)+libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))>>int32(5)]&(libc.Uint32FromUint32(1)<<(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))&libc.Int32FromInt32(0x1F))) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					} /* fall through */
					fallthrough
				case int32(_BT_NMSTRT):
					fallthrough
				case int32(_BT_HEX):
					fallthrough
				case int32(_BT_DIGIT):
					fallthrough
				case int32(_BT_NAME):
					fallthrough
				case int32(_BT_MINUS):
					ptr += uintptr(2)
				case int32(_BT_LEAD2):
					if int64(end)-int64(ptr) < int64(2) {
						return -int32(2)
					}
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(2)
				case int32(_BT_LEAD3):
					if int64(end)-int64(ptr) < int64(3) {
						return -int32(2)
					}
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(3)
				case int32(_BT_LEAD4):
					if int64(end)-int64(ptr) < int64(4) {
						return -int32(2)
					}
					if !(libc.Int32FromInt32(0) != 0) {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return m_XML_TOK_INVALID
					}
					ptr += uintptr(4)
				default:
					tok = int32(m_XML_TOK_NMTOKEN)
					break
				}
			case int32(m_XML_TOK_PREFIXED_NAME):
				tok = int32(m_XML_TOK_NMTOKEN)
				break
			}
		case int32(_BT_PLUS):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_NAME_PLUS)
		case int32(_BT_AST):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_NAME_ASTERISK)
		case int32(_BT_QUEST):
			if tok == int32(m_XML_TOK_NMTOKEN) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			return int32(m_XML_TOK_NAME_QUESTION)
		default:
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		}
	}
	return -tok
}

func _big2_attributeValueTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var start uintptr
	var v1, v2 int32
	_, _, _ = start, v1, v2
	if ptr >= end {
		return -int32(4)
	} else {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the paranoia
			 * check is still valuable, however.
			 */
			return -int32(1) /* LCOV_EXCL_LINE */
		}
	}
	start = ptr
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_AMP):
			if ptr == start {
				return _big2_scanRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_LT):
			/* this is for inside entity references */
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_LF):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_CR):
			if ptr == start {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(3)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
					v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
				} else {
					v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
				}
				if v2 == int32(_BT_LF) {
					ptr += uintptr(2)
				}
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_S):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_ATTRIBUTE_VALUE_S)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

func _big2_entityValueTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var start uintptr
	var tok, v1, v2, v3 int32
	_, _, _, _, _ = start, tok, v1, v2, v3
	if ptr >= end {
		return -int32(4)
	} else {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the paranoia
			 * check is still valuable, however.
			 */
			return -int32(1) /* LCOV_EXCL_LINE */
		}
	}
	start = ptr
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_AMP):
			if ptr == start {
				return _big2_scanRef(tls, enc, ptr+uintptr(2), end, nextTokPtr)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_PERCNT):
			if ptr == start {
				tok = _big2_scanPercent(tls, enc, ptr+uintptr(2), end, nextTokPtr)
				if tok == int32(m_XML_TOK_PERCENT) {
					v2 = m_XML_TOK_INVALID
				} else {
					v2 = tok
				}
				return v2
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_LF):
			if ptr == start {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		case int32(_BT_CR):
			if ptr == start {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(3)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
					v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
				} else {
					v3 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
				}
				if v3 == int32(_BT_LF) {
					ptr += uintptr(2)
				}
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return int32(m_XML_TOK_DATA_NEWLINE)
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return int32(m_XML_TOK_DATA_CHARS)
		default:
			ptr += uintptr(2)
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
	return int32(m_XML_TOK_DATA_CHARS)
}

func _big2_ignoreSectionTok(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var level, v1 int32
	var n Tsize_t
	_, _, _ = level, n, v1
	level = 0
	if int32(2) > int32(1) {
		n = libc.Uint64FromInt64(int64(end) - int64(ptr))
		if n&libc.Uint64FromInt32(libc.Int32FromInt32(2)-libc.Int32FromInt32(1)) != 0 {
			n &= libc.Uint64FromInt32(^(libc.Int32FromInt32(2) - libc.Int32FromInt32(1)))
			end = ptr + uintptr(n)
		}
	}
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			if int64(end)-int64(ptr) < int64(2) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			if int64(end)-int64(ptr) < int64(3) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			if int64(end)-int64(ptr) < int64(4) {
				return -int32(2)
			}
			if 0 != 0 {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return m_XML_TOK_INVALID
			}
			ptr += uintptr(4)
		case int32(_BT_NONXML):
			fallthrough
		case int32(_BT_MALFORM):
			fallthrough
		case int32(_BT_TRAIL):
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return m_XML_TOK_INVALID
		case int32(_BT_LT):
			ptr += uintptr(2)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_EXCL) {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_LSQB1) {
					level++
					ptr += uintptr(2)
				}
			}
		case int32(_BT_RSQB):
			ptr += uintptr(2)
			if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_RSQB1) {
				ptr += uintptr(2)
				if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
					return -int32(1)
				}
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_GT1) {
					ptr += uintptr(2)
					if level == 0 {
						*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
						return int32(m_XML_TOK_IGNORE_SECT)
					}
					level--
				}
			}
		default:
			ptr += uintptr(2)
			break
		}
	}
	return -int32(1)
}

func _big2_isPublicId(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, badPtr uintptr) (r int32) {
	var v2, v3, v4 int32
	_, _, _ = v2, v3, v4
	ptr += uintptr(2)
	end -= uintptr(2)
	for {
		if !(int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2))) {
			break
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_MINUS):
			fallthrough
		case int32(_BT_APOS):
			fallthrough
		case int32(_BT_LPAR):
			fallthrough
		case int32(_BT_RPAR):
			fallthrough
		case int32(_BT_PLUS):
			fallthrough
		case int32(_BT_COMMA):
			fallthrough
		case int32(_BT_SOL):
			fallthrough
		case int32(_BT_EQUALS):
			fallthrough
		case int32(_BT_QUEST):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_SEMI):
			fallthrough
		case int32(_BT_EXCL):
			fallthrough
		case int32(_BT_AST):
			fallthrough
		case int32(_BT_PERCNT):
			fallthrough
		case int32(_BT_NUM):
			fallthrough
		case int32(_BT_COLON):
		case int32(_BT_S):
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_TAB1) {
				*(*uintptr)(unsafe.Pointer(badPtr)) = ptr
				return 0
			}
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_NMSTRT):
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
			} else {
				v3 = -int32(1)
			}
			if !(v3 & ^libc.Int32FromInt32(0x7f) != 0) {
				break
			}
			/* fall through */
			fallthrough
		default:
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v4 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
			} else {
				v4 = -int32(1)
			}
			switch v4 {
			case int32(0x24): /* $ */
				fallthrough
			case int32(0x40): /* @ */
			default:
				*(*uintptr)(unsafe.Pointer(badPtr)) = ptr
				return 0
			}
			break
		}
		goto _1
	_1:
		;
		ptr += uintptr(2)
	}
	return int32(1)
}

/* This must only be called for a well-formed start-tag or empty
   element tag.  Returns the number of attributes.  Pointers to the
   first attsMax attributes are stored in atts.
*/

func _big2_getAtts(tls *libc.TLS, enc uintptr, ptr uintptr, attsMax int32, atts uintptr) (r int32) {
	var nAtts, open, state, v2, v3, v5, v7 int32
	var v4, v6, v8, v9 bool
	_, _, _, _, _, _, _, _, _, _, _ = nAtts, open, state, v2, v3, v4, v5, v6, v7, v8, v9
	state = 1
	nAtts = 0
	open = 0 /* defined when state == inValue;
	   initialization just to shut up compilers */
	ptr += uintptr(2)
	for {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_LEAD2):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(2) - libc.Int32FromInt32(2))
		case int32(_BT_LEAD3):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(3) - libc.Int32FromInt32(2))
		case int32(_BT_LEAD4):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
			ptr += uintptr(libc.Int32FromInt32(4) - libc.Int32FromInt32(2))
		case int32(_BT_NONASCII):
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_HEX):
			if state == 0 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fname = ptr
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(1)
				}
				state = 1
			}
		case int32(_BT_QUOT):
			if state != 2 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr = ptr + uintptr(2)
				}
				state = 2
				open = int32(_BT_QUOT)
			} else {
				if open == int32(_BT_QUOT) {
					state = 0
					if nAtts < attsMax {
						(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvalueEnd = ptr
					}
					nAtts++
				}
			}
		case int32(_BT_APOS):
			if state != 2 {
				if nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr = ptr + uintptr(2)
				}
				state = 2
				open = int32(_BT_APOS)
			} else {
				if open == int32(_BT_APOS) {
					state = 0
					if nAtts < attsMax {
						(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvalueEnd = ptr
					}
					nAtts++
				}
			}
		case int32(_BT_AMP):
			if nAtts < attsMax {
				(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
			}
		case int32(_BT_S):
			if state == 1 {
				state = 0
			} else {
				if v9 = state == 2 && nAtts < attsMax && (*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized != 0; v9 {
					if v4 = ptr == (*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).FvaluePtr; !v4 {
						if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
							v3 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
						} else {
							v3 = -int32(1)
						}
					}
					if v6 = v4 || v3 != int32(m_ASCII_SPACE1); !v6 {
						if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == 0 {
							v5 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1)))
						} else {
							v5 = -int32(1)
						}
					}
					if v8 = v6 || v5 == int32(m_ASCII_SPACE1); !v8 {
						if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == 0 {
							v7 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))))))
						} else {
							v7 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2))), *(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1)))
						}
					}
				}
				if v9 && (v8 || v7 == open) {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
				}
			}
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_LF):
			/* This case ensures that the first attribute name is counted
			   Apart from that we could just change state on the quote. */
			if state == 1 {
				state = 0
			} else {
				if state == 2 && nAtts < attsMax {
					(*(*TATTRIBUTE)(unsafe.Pointer(atts + uintptr(nAtts)*32))).Fnormalized = uint8(0)
				}
			}
		case int32(_BT_GT):
			fallthrough
		case int32(_BT_SOL):
			if state != 2 {
				return nAtts
			}
		default:
			break
		}
		goto _1
	_1:
		;
		ptr += uintptr(2)
	}
	/* not reached */
	return r
}

func _big2_charRefNumber(tls *libc.TLS, enc uintptr, ptr uintptr) (r int32) {
	var c, c1, result, v2, v4 int32
	_, _, _, _, _ = c, c1, result, v2, v4
	result = 0
	/* skip &# */
	_ = enc
	ptr += uintptr(libc.Int32FromInt32(2) * libc.Int32FromInt32(2))
	if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_x) {
		ptr += uintptr(2)
		for {
			if !!(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_SEMI1)) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
			} else {
				v2 = -int32(1)
			}
			c = v2
			switch c {
			case int32(m_ASCII_0):
				fallthrough
			case int32(m_ASCII_1):
				fallthrough
			case int32(m_ASCII_2):
				fallthrough
			case int32(m_ASCII_3):
				fallthrough
			case int32(m_ASCII_41):
				fallthrough
			case int32(m_ASCII_51):
				fallthrough
			case int32(m_ASCII_61):
				fallthrough
			case int32(m_ASCII_71):
				fallthrough
			case int32(m_ASCII_8):
				fallthrough
			case int32(m_ASCII_9):
				result <<= int32(4)
				result |= c - int32(m_ASCII_0)
			case int32(m_ASCII_A):
				fallthrough
			case int32(m_ASCII_B1):
				fallthrough
			case int32(m_ASCII_C):
				fallthrough
			case int32(m_ASCII_D):
				fallthrough
			case int32(m_ASCII_E):
				fallthrough
			case int32(m_ASCII_F):
				result <<= int32(4)
				result += int32(10) + (c - int32(m_ASCII_A))
			case int32(m_ASCII_a):
				fallthrough
			case int32(m_ASCII_b1):
				fallthrough
			case int32(m_ASCII_c):
				fallthrough
			case int32(m_ASCII_d1):
				fallthrough
			case int32(m_ASCII_e):
				fallthrough
			case int32(m_ASCII_f1):
				result <<= int32(4)
				result += int32(10) + (c - int32(m_ASCII_a))
				break
			}
			if result >= int32(0x110000) {
				return -int32(1)
			}
			goto _1
		_1:
			;
			ptr += uintptr(2)
		}
	} else {
		for {
			if !!(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_SEMI1)) {
				break
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v4 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
			} else {
				v4 = -int32(1)
			}
			c1 = v4
			result *= int32(10)
			result += c1 - int32(m_ASCII_0)
			if result >= int32(0x110000) {
				return -int32(1)
			}
			goto _3
		_3:
			;
			ptr += uintptr(2)
		}
	}
	return _checkCharRefNumber(tls, result)
}

func _big2_predefinedEntityName(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr) (r int32) {
	var v1, v2 int32
	_, _ = v1, v2
	_ = enc
	switch (int64(end) - int64(ptr)) / libc.Int64FromInt32(2) {
	case int64(2):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2)))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + libc.UintptrFromInt32(2) + 1))) == int32(m_ASCII_t) {
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
				v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
			} else {
				v1 = -int32(1)
			}
			switch v1 {
			case int32(m_ASCII_l):
				return int32(m_ASCII_LT1)
			case int32(m_ASCII_g):
				return int32(m_ASCII_GT1)
			}
		}
	case int64(3):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_a) {
			ptr += uintptr(2)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_m) {
				ptr += uintptr(2)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_p) {
					return int32(m_ASCII_AMP1)
				}
			}
		}
	case int64(4):
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1)))
		} else {
			v2 = -int32(1)
		}
		switch v2 {
		case int32(m_ASCII_q1):
			ptr += uintptr(2)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_u1) {
				ptr += uintptr(2)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_o) {
					ptr += uintptr(2)
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_t) {
						return int32(m_ASCII_QUOT1)
					}
				}
			}
		case int32(m_ASCII_a):
			ptr += uintptr(2)
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_p) {
				ptr += uintptr(2)
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_o) {
					ptr += uintptr(2)
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32(m_ASCII_s) {
						return int32(m_ASCII_APOS1)
					}
				}
			}
			break
		}
	}
	return 0
}

func _big2_nameMatchesAscii(tls *libc.TLS, enc uintptr, ptr1 uintptr, end1 uintptr, ptr2 uintptr) (r int32) {
	_ = enc
	for {
		if !(*(*uint8)(unsafe.Pointer(ptr2)) != 0) {
			break
		}
		if int64(end1)-int64(ptr1) < int64(2) {
			/* This line cannot be executed.  The incoming data has already
			 * been tokenized once, so incomplete characters like this have
			 * already been eliminated from the input.  Retaining the
			 * paranoia check is still valuable, however.
			 */
			return 0 /* LCOV_EXCL_LINE */
		}
		if !(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr1))) == 0 && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr1 + 1))) == libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr2)))) {
			return 0
		}
		goto _1
	_1:
		;
		ptr1 += uintptr(2)
		ptr2++
	}
	return libc.BoolInt32(ptr1 == end1)
}

func _big2_nameLength(tls *libc.TLS, enc uintptr, ptr uintptr) (r int32) {
	var start uintptr
	var v2 int32
	_, _ = start, v2
	start = ptr
	for {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
		case int32(_BT_NONASCII):
			fallthrough
		case int32(_BT_NMSTRT):
			fallthrough
		case int32(_BT_COLON):
			fallthrough
		case int32(_BT_HEX):
			fallthrough
		case int32(_BT_DIGIT):
			fallthrough
		case int32(_BT_NAME):
			fallthrough
		case int32(_BT_MINUS):
			ptr += uintptr(2)
		default:
			return int32(int64(ptr) - int64(start))
		}
		goto _1
	_1:
	}
	return r
}

func _big2_skipS(tls *libc.TLS, enc uintptr, ptr uintptr) (r uintptr) {
	var v2 int32
	_ = v2
	for {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v2 {
		case int32(_BT_LF):
			fallthrough
		case int32(_BT_CR):
			fallthrough
		case int32(_BT_S):
			ptr += uintptr(2)
		default:
			return ptr
		}
		goto _1
	_1:
	}
	return r
}

func _big2_updatePosition(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, pos uintptr) {
	var v1, v2 int32
	var v3 bool
	_, _, _ = v1, v2, v3
	for int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)) {
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
			v1 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
		} else {
			v1 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
		}
		switch v1 {
		case int32(_BT_LEAD2):
			ptr += uintptr(2)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LEAD3):
			ptr += uintptr(3)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LEAD4):
			ptr += uintptr(4)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
		case int32(_BT_LF):
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber = uint64(0)
			(*TPOSITION)(unsafe.Pointer(pos)).FlineNumber++
			ptr += uintptr(2)
		case int32(_BT_CR):
			(*TPOSITION)(unsafe.Pointer(pos)).FlineNumber++
			ptr += uintptr(2)
			if v3 = int64(end)-int64(ptr) >= int64(libc.Int32FromInt32(1)*libc.Int32FromInt32(2)); v3 {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == 0 {
					v2 = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(ptr + 1))))))
				} else {
					v2 = _unicode_byte_type(tls, *(*uint8)(unsafe.Pointer(ptr)), *(*uint8)(unsafe.Pointer(ptr + 1)))
				}
			}
			if v3 && v2 == int32(_BT_LF) {
				ptr += uintptr(2)
			}
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber = uint64(0)
		default:
			ptr += uintptr(2)
			(*TPOSITION)(unsafe.Pointer(pos)).FcolumnNumber++
			break
		}
	}
}

var _big2_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(2),
		FisUtf16:         uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_big2_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_big2_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_big2_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_big2_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_big2_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_big2_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_big2_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_big2_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_big2_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_big2_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_big2_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_big2_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_big2_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_big2_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_big2_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_big2_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_big2_toUtf16)
}

var _big2_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(2),
		FisUtf16:         uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_CR),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_big2_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_big2_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_big2_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_big2_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_big2_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_big2_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_big2_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_big2_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_big2_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_big2_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_big2_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_big2_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_big2_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_big2_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_big2_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_big2_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_big2_toUtf16)
}

var _internal_big2_encoding_ns = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(2),
		FisUtf16:         uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_S),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_COLON),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_internal_big2_encoding_ns)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_big2_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_big2_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_big2_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_big2_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_big2_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_big2_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_big2_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_big2_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_big2_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_big2_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_big2_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_big2_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_big2_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_big2_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_big2_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_big2_toUtf16)
}

var _internal_big2_encoding = Tnormal_encoding{
	Fenc: TENCODING{
		Fscanners:        [4]TSCANNER{},
		FliteralScanners: [2]TSCANNER{},
		FminBytesPerChar: int32(2),
		FisUtf16:         uint8(1),
	},
	Ftype1: [256]uint8{
		9:   uint8(_BT_S),
		10:  uint8(_BT_LF),
		13:  uint8(_BT_S),
		32:  uint8(_BT_S),
		33:  uint8(_BT_EXCL),
		34:  uint8(_BT_QUOT),
		35:  uint8(_BT_NUM),
		36:  uint8(_BT_OTHER),
		37:  uint8(_BT_PERCNT),
		38:  uint8(_BT_AMP),
		39:  uint8(_BT_APOS),
		40:  uint8(_BT_LPAR),
		41:  uint8(_BT_RPAR),
		42:  uint8(_BT_AST),
		43:  uint8(_BT_PLUS),
		44:  uint8(_BT_COMMA),
		45:  uint8(_BT_MINUS),
		46:  uint8(_BT_NAME),
		47:  uint8(_BT_SOL),
		48:  uint8(_BT_DIGIT),
		49:  uint8(_BT_DIGIT),
		50:  uint8(_BT_DIGIT),
		51:  uint8(_BT_DIGIT),
		52:  uint8(_BT_DIGIT),
		53:  uint8(_BT_DIGIT),
		54:  uint8(_BT_DIGIT),
		55:  uint8(_BT_DIGIT),
		56:  uint8(_BT_DIGIT),
		57:  uint8(_BT_DIGIT),
		58:  uint8(_BT_NMSTRT),
		59:  uint8(_BT_SEMI),
		60:  uint8(_BT_LT),
		61:  uint8(_BT_EQUALS),
		62:  uint8(_BT_GT),
		63:  uint8(_BT_QUEST),
		64:  uint8(_BT_OTHER),
		65:  uint8(_BT_HEX),
		66:  uint8(_BT_HEX),
		67:  uint8(_BT_HEX),
		68:  uint8(_BT_HEX),
		69:  uint8(_BT_HEX),
		70:  uint8(_BT_HEX),
		71:  uint8(_BT_NMSTRT),
		72:  uint8(_BT_NMSTRT),
		73:  uint8(_BT_NMSTRT),
		74:  uint8(_BT_NMSTRT),
		75:  uint8(_BT_NMSTRT),
		76:  uint8(_BT_NMSTRT),
		77:  uint8(_BT_NMSTRT),
		78:  uint8(_BT_NMSTRT),
		79:  uint8(_BT_NMSTRT),
		80:  uint8(_BT_NMSTRT),
		81:  uint8(_BT_NMSTRT),
		82:  uint8(_BT_NMSTRT),
		83:  uint8(_BT_NMSTRT),
		84:  uint8(_BT_NMSTRT),
		85:  uint8(_BT_NMSTRT),
		86:  uint8(_BT_NMSTRT),
		87:  uint8(_BT_NMSTRT),
		88:  uint8(_BT_NMSTRT),
		89:  uint8(_BT_NMSTRT),
		90:  uint8(_BT_NMSTRT),
		91:  uint8(_BT_LSQB),
		92:  uint8(_BT_OTHER),
		93:  uint8(_BT_RSQB),
		94:  uint8(_BT_OTHER),
		95:  uint8(_BT_NMSTRT),
		96:  uint8(_BT_OTHER),
		97:  uint8(_BT_HEX),
		98:  uint8(_BT_HEX),
		99:  uint8(_BT_HEX),
		100: uint8(_BT_HEX),
		101: uint8(_BT_HEX),
		102: uint8(_BT_HEX),
		103: uint8(_BT_NMSTRT),
		104: uint8(_BT_NMSTRT),
		105: uint8(_BT_NMSTRT),
		106: uint8(_BT_NMSTRT),
		107: uint8(_BT_NMSTRT),
		108: uint8(_BT_NMSTRT),
		109: uint8(_BT_NMSTRT),
		110: uint8(_BT_NMSTRT),
		111: uint8(_BT_NMSTRT),
		112: uint8(_BT_NMSTRT),
		113: uint8(_BT_NMSTRT),
		114: uint8(_BT_NMSTRT),
		115: uint8(_BT_NMSTRT),
		116: uint8(_BT_NMSTRT),
		117: uint8(_BT_NMSTRT),
		118: uint8(_BT_NMSTRT),
		119: uint8(_BT_NMSTRT),
		120: uint8(_BT_NMSTRT),
		121: uint8(_BT_NMSTRT),
		122: uint8(_BT_NMSTRT),
		123: uint8(_BT_OTHER),
		124: uint8(_BT_VERBAR),
		125: uint8(_BT_OTHER),
		126: uint8(_BT_OTHER),
		127: uint8(_BT_OTHER),
		128: uint8(_BT_OTHER),
		129: uint8(_BT_OTHER),
		130: uint8(_BT_OTHER),
		131: uint8(_BT_OTHER),
		132: uint8(_BT_OTHER),
		133: uint8(_BT_OTHER),
		134: uint8(_BT_OTHER),
		135: uint8(_BT_OTHER),
		136: uint8(_BT_OTHER),
		137: uint8(_BT_OTHER),
		138: uint8(_BT_OTHER),
		139: uint8(_BT_OTHER),
		140: uint8(_BT_OTHER),
		141: uint8(_BT_OTHER),
		142: uint8(_BT_OTHER),
		143: uint8(_BT_OTHER),
		144: uint8(_BT_OTHER),
		145: uint8(_BT_OTHER),
		146: uint8(_BT_OTHER),
		147: uint8(_BT_OTHER),
		148: uint8(_BT_OTHER),
		149: uint8(_BT_OTHER),
		150: uint8(_BT_OTHER),
		151: uint8(_BT_OTHER),
		152: uint8(_BT_OTHER),
		153: uint8(_BT_OTHER),
		154: uint8(_BT_OTHER),
		155: uint8(_BT_OTHER),
		156: uint8(_BT_OTHER),
		157: uint8(_BT_OTHER),
		158: uint8(_BT_OTHER),
		159: uint8(_BT_OTHER),
		160: uint8(_BT_OTHER),
		161: uint8(_BT_OTHER),
		162: uint8(_BT_OTHER),
		163: uint8(_BT_OTHER),
		164: uint8(_BT_OTHER),
		165: uint8(_BT_OTHER),
		166: uint8(_BT_OTHER),
		167: uint8(_BT_OTHER),
		168: uint8(_BT_OTHER),
		169: uint8(_BT_OTHER),
		170: uint8(_BT_NMSTRT),
		171: uint8(_BT_OTHER),
		172: uint8(_BT_OTHER),
		173: uint8(_BT_OTHER),
		174: uint8(_BT_OTHER),
		175: uint8(_BT_OTHER),
		176: uint8(_BT_OTHER),
		177: uint8(_BT_OTHER),
		178: uint8(_BT_OTHER),
		179: uint8(_BT_OTHER),
		180: uint8(_BT_OTHER),
		181: uint8(_BT_NMSTRT),
		182: uint8(_BT_OTHER),
		183: uint8(_BT_NAME),
		184: uint8(_BT_OTHER),
		185: uint8(_BT_OTHER),
		186: uint8(_BT_NMSTRT),
		187: uint8(_BT_OTHER),
		188: uint8(_BT_OTHER),
		189: uint8(_BT_OTHER),
		190: uint8(_BT_OTHER),
		191: uint8(_BT_OTHER),
		192: uint8(_BT_NMSTRT),
		193: uint8(_BT_NMSTRT),
		194: uint8(_BT_NMSTRT),
		195: uint8(_BT_NMSTRT),
		196: uint8(_BT_NMSTRT),
		197: uint8(_BT_NMSTRT),
		198: uint8(_BT_NMSTRT),
		199: uint8(_BT_NMSTRT),
		200: uint8(_BT_NMSTRT),
		201: uint8(_BT_NMSTRT),
		202: uint8(_BT_NMSTRT),
		203: uint8(_BT_NMSTRT),
		204: uint8(_BT_NMSTRT),
		205: uint8(_BT_NMSTRT),
		206: uint8(_BT_NMSTRT),
		207: uint8(_BT_NMSTRT),
		208: uint8(_BT_NMSTRT),
		209: uint8(_BT_NMSTRT),
		210: uint8(_BT_NMSTRT),
		211: uint8(_BT_NMSTRT),
		212: uint8(_BT_NMSTRT),
		213: uint8(_BT_NMSTRT),
		214: uint8(_BT_NMSTRT),
		215: uint8(_BT_OTHER),
		216: uint8(_BT_NMSTRT),
		217: uint8(_BT_NMSTRT),
		218: uint8(_BT_NMSTRT),
		219: uint8(_BT_NMSTRT),
		220: uint8(_BT_NMSTRT),
		221: uint8(_BT_NMSTRT),
		222: uint8(_BT_NMSTRT),
		223: uint8(_BT_NMSTRT),
		224: uint8(_BT_NMSTRT),
		225: uint8(_BT_NMSTRT),
		226: uint8(_BT_NMSTRT),
		227: uint8(_BT_NMSTRT),
		228: uint8(_BT_NMSTRT),
		229: uint8(_BT_NMSTRT),
		230: uint8(_BT_NMSTRT),
		231: uint8(_BT_NMSTRT),
		232: uint8(_BT_NMSTRT),
		233: uint8(_BT_NMSTRT),
		234: uint8(_BT_NMSTRT),
		235: uint8(_BT_NMSTRT),
		236: uint8(_BT_NMSTRT),
		237: uint8(_BT_NMSTRT),
		238: uint8(_BT_NMSTRT),
		239: uint8(_BT_NMSTRT),
		240: uint8(_BT_NMSTRT),
		241: uint8(_BT_NMSTRT),
		242: uint8(_BT_NMSTRT),
		243: uint8(_BT_NMSTRT),
		244: uint8(_BT_NMSTRT),
		245: uint8(_BT_NMSTRT),
		246: uint8(_BT_NMSTRT),
		247: uint8(_BT_OTHER),
		248: uint8(_BT_NMSTRT),
		249: uint8(_BT_NMSTRT),
		250: uint8(_BT_NMSTRT),
		251: uint8(_BT_NMSTRT),
		252: uint8(_BT_NMSTRT),
		253: uint8(_BT_NMSTRT),
		254: uint8(_BT_NMSTRT),
		255: uint8(_BT_NMSTRT),
	},
}

func init() {
	p := unsafe.Pointer(&_internal_big2_encoding)
	*(*uintptr)(unsafe.Add(p, 0)) = __ccgo_fp(_big2_prologTok)
	*(*uintptr)(unsafe.Add(p, 8)) = __ccgo_fp(_big2_contentTok)
	*(*uintptr)(unsafe.Add(p, 16)) = __ccgo_fp(_big2_cdataSectionTok)
	*(*uintptr)(unsafe.Add(p, 24)) = __ccgo_fp(_big2_ignoreSectionTok)
	*(*uintptr)(unsafe.Add(p, 32)) = __ccgo_fp(_big2_attributeValueTok)
	*(*uintptr)(unsafe.Add(p, 40)) = __ccgo_fp(_big2_entityValueTok)
	*(*uintptr)(unsafe.Add(p, 48)) = __ccgo_fp(_big2_nameMatchesAscii)
	*(*uintptr)(unsafe.Add(p, 56)) = __ccgo_fp(_big2_nameLength)
	*(*uintptr)(unsafe.Add(p, 64)) = __ccgo_fp(_big2_skipS)
	*(*uintptr)(unsafe.Add(p, 72)) = __ccgo_fp(_big2_getAtts)
	*(*uintptr)(unsafe.Add(p, 80)) = __ccgo_fp(_big2_charRefNumber)
	*(*uintptr)(unsafe.Add(p, 88)) = __ccgo_fp(_big2_predefinedEntityName)
	*(*uintptr)(unsafe.Add(p, 96)) = __ccgo_fp(_big2_updatePosition)
	*(*uintptr)(unsafe.Add(p, 104)) = __ccgo_fp(_big2_isPublicId)
	*(*uintptr)(unsafe.Add(p, 112)) = __ccgo_fp(_big2_toUtf8)
	*(*uintptr)(unsafe.Add(p, 120)) = __ccgo_fp(_big2_toUtf16)
}

func _streqci(tls *libc.TLS, s1 uintptr, s2 uintptr) (r int32) {
	var c1, c2 uint8
	var v2, v3 uintptr
	_, _, _, _ = c1, c2, v2, v3
	for {
		v2 = s1
		s1++
		c1 = *(*uint8)(unsafe.Pointer(v2))
		v3 = s2
		s2++
		c2 = *(*uint8)(unsafe.Pointer(v3))
		if int32(m_ASCII_a) <= libc.Int32FromUint8(c1) && libc.Int32FromUint8(c1) <= int32(m_ASCII_z1) {
			c1 = uint8(int32(c1) + (libc.Int32FromInt32(m_ASCII_A) - libc.Int32FromInt32(m_ASCII_a)))
		}
		if int32(m_ASCII_a) <= libc.Int32FromUint8(c2) && libc.Int32FromUint8(c2) <= int32(m_ASCII_z1) {
			/* The following line will never get executed.  streqci() is
			 * only called from two places, both of which guarantee to put
			 * upper-case strings into s2.
			 */
			c2 = uint8(int32(c2) + (libc.Int32FromInt32(m_ASCII_A) - libc.Int32FromInt32(m_ASCII_a)))
		} /* LCOV_EXCL_LINE */
		if libc.Int32FromUint8(c1) != libc.Int32FromUint8(c2) {
			return 0
		}
		if !(c1 != 0) {
			break
		}
		goto _1
	_1:
	}
	return int32(1)
}

func _initUpdatePosition(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, pos uintptr) {
	_ = enc
	_normal_updatePosition(tls, uintptr(unsafe.Pointer(&_utf8_encoding)), ptr, end, pos)
}

func _toAscii(tls *libc.TLS, enc uintptr, _ptr uintptr, end uintptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var _ /* buf at bp+8 */ [1]uint8
	var _ /* p at bp+16 */ uintptr
	*(*uintptr)(unsafe.Pointer(bp + 16)) = bp + 8
	(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, end, bp+16, *(*uintptr)(unsafe.Pointer(bp + 16))+uintptr(1))
	if *(*uintptr)(unsafe.Pointer(bp + 16)) == bp+8 {
		return -int32(1)
	} else {
		return libc.Int32FromUint8((*(*[1]uint8)(unsafe.Pointer(bp + 8)))[0])
	}
	return r
}

func _isSpace(tls *libc.TLS, c int32) (r int32) {
	switch c {
	case int32(0x20):
		fallthrough
	case int32(0xD):
		fallthrough
	case int32(0xA):
		fallthrough
	case int32(0x9):
		return int32(1)
	}
	return 0
}

// C documentation
//
//	/* Return 1 if there's just optional white space or there's an S
//	   followed by name=val.
//	*/
func _parsePseudoAttribute(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, namePtr uintptr, nameEndPtr uintptr, valPtr uintptr, nextTokPtr uintptr) (r int32) {
	var c, v2 int32
	var open uint8
	_, _, _ = c, open, v2
	if ptr == end {
		*(*uintptr)(unsafe.Pointer(namePtr)) = libc.UintptrFromInt32(0)
		return int32(1)
	}
	if !(_isSpace(tls, _toAscii(tls, enc, ptr, end)) != 0) {
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return 0
	}
	for cond := true; cond; cond = _isSpace(tls, _toAscii(tls, enc, ptr, end)) != 0 {
		ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	}
	if ptr == end {
		*(*uintptr)(unsafe.Pointer(namePtr)) = libc.UintptrFromInt32(0)
		return int32(1)
	}
	*(*uintptr)(unsafe.Pointer(namePtr)) = ptr
	for {
		c = _toAscii(tls, enc, ptr, end)
		if c == -int32(1) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return 0
		}
		if c == int32(m_ASCII_EQUALS) {
			*(*uintptr)(unsafe.Pointer(nameEndPtr)) = ptr
			break
		}
		if _isSpace(tls, c) != 0 {
			*(*uintptr)(unsafe.Pointer(nameEndPtr)) = ptr
			for {
				ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
				goto _3
			_3:
				;
				v2 = _toAscii(tls, enc, ptr, end)
				c = v2
				if !(_isSpace(tls, v2) != 0) {
					break
				}
			}
			if c != int32(m_ASCII_EQUALS) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
				return 0
			}
			break
		}
		ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
		goto _1
	_1:
	}
	if ptr == *(*uintptr)(unsafe.Pointer(namePtr)) {
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return 0
	}
	ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	c = _toAscii(tls, enc, ptr, end)
	for _isSpace(tls, c) != 0 {
		ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
		c = _toAscii(tls, enc, ptr, end)
	}
	if c != int32(m_ASCII_QUOT1) && c != int32(m_ASCII_APOS1) {
		*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
		return 0
	}
	open = libc.Uint8FromInt32(c)
	ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	*(*uintptr)(unsafe.Pointer(valPtr)) = ptr
	for {
		c = _toAscii(tls, enc, ptr, end)
		if c == libc.Int32FromUint8(open) {
			break
		}
		if !(int32(m_ASCII_a) <= c && c <= int32(m_ASCII_z1)) && !(int32(m_ASCII_A) <= c && c <= int32(m_ASCII_Z1)) && !(int32(m_ASCII_0) <= c && c <= int32(m_ASCII_9)) && c != int32(m_ASCII_PERIOD) && c != int32(m_ASCII_MINUS1) && c != int32(m_ASCII_UNDERSCORE1) {
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr
			return 0
		}
		goto _4
	_4:
		;
		ptr += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	}
	*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	return int32(1)
}

var _KW_version = [8]uint8{
	0: uint8(m_ASCII_v1),
	1: uint8(m_ASCII_e),
	2: uint8(m_ASCII_r),
	3: uint8(m_ASCII_s),
	4: uint8(m_ASCII_i1),
	5: uint8(m_ASCII_o),
	6: uint8(m_ASCII_n),
}

var _KW_encoding = [9]uint8{
	0: uint8(m_ASCII_e),
	1: uint8(m_ASCII_n),
	2: uint8(m_ASCII_c),
	3: uint8(m_ASCII_o),
	4: uint8(m_ASCII_d1),
	5: uint8(m_ASCII_i1),
	6: uint8(m_ASCII_n),
	7: uint8(m_ASCII_g),
}

var _KW_standalone = [11]uint8{
	0: uint8(m_ASCII_s),
	1: uint8(m_ASCII_t),
	2: uint8(m_ASCII_a),
	3: uint8(m_ASCII_n),
	4: uint8(m_ASCII_d1),
	5: uint8(m_ASCII_a),
	6: uint8(m_ASCII_l),
	7: uint8(m_ASCII_o),
	8: uint8(m_ASCII_n),
	9: uint8(m_ASCII_e),
}

var _KW_yes = [4]uint8{
	0: uint8(m_ASCII_y1),
	1: uint8(m_ASCII_e),
	2: uint8(m_ASCII_s),
}

var _KW_no = [3]uint8{
	0: uint8(m_ASCII_n),
	1: uint8(m_ASCII_o),
}

func _doParseXmlDecl(tls *libc.TLS, encodingFinder uintptr, isGeneralTextEntity int32, enc uintptr, _ptr uintptr, end uintptr, badPtr uintptr, versionPtr uintptr, versionEndPtr uintptr, encodingName uintptr, encoding uintptr, standalone uintptr) (r int32) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var c int32
	var _ /* name at bp+16 */ uintptr
	var _ /* nameEnd at bp+24 */ uintptr
	var _ /* val at bp+8 */ uintptr
	_ = c
	*(*uintptr)(unsafe.Pointer(bp + 8)) = libc.UintptrFromInt32(0)
	*(*uintptr)(unsafe.Pointer(bp + 16)) = libc.UintptrFromInt32(0)
	*(*uintptr)(unsafe.Pointer(bp + 24)) = libc.UintptrFromInt32(0)
	*(*uintptr)(unsafe.Pointer(bp)) += uintptr(int32(5) * (*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	end -= uintptr(int32(2) * (*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	if !(_parsePseudoAttribute(tls, enc, *(*uintptr)(unsafe.Pointer(bp)), end, bp+16, bp+24, bp+8, bp) != 0) || !(*(*uintptr)(unsafe.Pointer(bp + 16)) != 0) {
		*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return 0
	}
	if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 16)), *(*uintptr)(unsafe.Pointer(bp + 24)), uintptr(unsafe.Pointer(&_KW_version))) != 0) {
		if !(isGeneralTextEntity != 0) {
			*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp + 16))
			return 0
		}
	} else {
		if versionPtr != 0 {
			*(*uintptr)(unsafe.Pointer(versionPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
		}
		if versionEndPtr != 0 {
			*(*uintptr)(unsafe.Pointer(versionEndPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		}
		if !(_parsePseudoAttribute(tls, enc, *(*uintptr)(unsafe.Pointer(bp)), end, bp+16, bp+24, bp+8, bp) != 0) {
			*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return 0
		}
		if !(*(*uintptr)(unsafe.Pointer(bp + 16)) != 0) {
			if isGeneralTextEntity != 0 {
				/* a TextDecl must have an EncodingDecl */
				*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp))
				return 0
			}
			return int32(1)
		}
	}
	if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 16)), *(*uintptr)(unsafe.Pointer(bp + 24)), uintptr(unsafe.Pointer(&_KW_encoding))) != 0 {
		c = _toAscii(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 8)), end)
		if !(int32(m_ASCII_a) <= c && c <= int32(m_ASCII_z1)) && !(int32(m_ASCII_A) <= c && c <= int32(m_ASCII_Z1)) {
			*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return 0
		}
		if encodingName != 0 {
			*(*uintptr)(unsafe.Pointer(encodingName)) = *(*uintptr)(unsafe.Pointer(bp + 8))
		}
		if encoding != 0 {
			*(*uintptr)(unsafe.Pointer(encoding)) = (*(*func(*libc.TLS, uintptr, uintptr, uintptr) uintptr)(unsafe.Pointer(&struct{ uintptr }{encodingFinder})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 8)), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar))
		}
		if !(_parsePseudoAttribute(tls, enc, *(*uintptr)(unsafe.Pointer(bp)), end, bp+16, bp+24, bp+8, bp) != 0) {
			*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp))
			return 0
		}
		if !(*(*uintptr)(unsafe.Pointer(bp + 16)) != 0) {
			return int32(1)
		}
	}
	if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 16)), *(*uintptr)(unsafe.Pointer(bp + 24)), uintptr(unsafe.Pointer(&_KW_standalone))) != 0) || isGeneralTextEntity != 0 {
		*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp + 16))
		return 0
	}
	if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 8)), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), uintptr(unsafe.Pointer(&_KW_yes))) != 0 {
		if standalone != 0 {
			*(*int32)(unsafe.Pointer(standalone)) = int32(1)
		}
	} else {
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, *(*uintptr)(unsafe.Pointer(bp + 8)), *(*uintptr)(unsafe.Pointer(bp))-uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), uintptr(unsafe.Pointer(&_KW_no))) != 0 {
			if standalone != 0 {
				*(*int32)(unsafe.Pointer(standalone)) = 0
			}
		} else {
			*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp + 8))
			return 0
		}
	}
	for _isSpace(tls, _toAscii(tls, enc, *(*uintptr)(unsafe.Pointer(bp)), end)) != 0 {
		*(*uintptr)(unsafe.Pointer(bp)) += uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar)
	}
	if *(*uintptr)(unsafe.Pointer(bp)) != end {
		*(*uintptr)(unsafe.Pointer(badPtr)) = *(*uintptr)(unsafe.Pointer(bp))
		return 0
	}
	return int32(1)
}

func _checkCharRefNumber(tls *libc.TLS, result int32) (r int32) {
	switch result >> libc.Int32FromInt32(8) {
	case int32(0xD8):
		fallthrough
	case int32(0xD9):
		fallthrough
	case int32(0xDA):
		fallthrough
	case int32(0xDB):
		fallthrough
	case int32(0xDC):
		fallthrough
	case int32(0xDD):
		fallthrough
	case int32(0xDE):
		fallthrough
	case int32(0xDF):
		return -int32(1)
	case 0:
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_latin1_encoding)) + 136 + uintptr(result)))) == int32(_BT_NONXML) {
			return -int32(1)
		}
	case int32(0xFF):
		if result == int32(0xFFFE) || result == int32(0xFFFF) {
			return -int32(1)
		}
		break
	}
	return result
}

func XXmlUtf8Encode(tls *libc.TLS, c int32, buf uintptr) (r int32) {
	if c < 0 {
		return 0
	} /* LCOV_EXCL_LINE: this case is always eliminated beforehand */
	if c < 128 {
		*(*uint8)(unsafe.Pointer(buf)) = libc.Uint8FromInt32(c | int32(_UTF8_cval1))
		return int32(1)
	}
	if c < 2048 {
		*(*uint8)(unsafe.Pointer(buf)) = libc.Uint8FromInt32(c>>libc.Int32FromInt32(6) | int32(_UTF8_cval2))
		*(*uint8)(unsafe.Pointer(buf + 1)) = libc.Uint8FromInt32(c&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		return int32(2)
	}
	if c < 65536 {
		*(*uint8)(unsafe.Pointer(buf)) = libc.Uint8FromInt32(c>>libc.Int32FromInt32(12) | int32(_UTF8_cval3))
		*(*uint8)(unsafe.Pointer(buf + 1)) = libc.Uint8FromInt32(c>>libc.Int32FromInt32(6)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		*(*uint8)(unsafe.Pointer(buf + 2)) = libc.Uint8FromInt32(c&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		return int32(3)
	}
	if c < int32(0x110000) {
		*(*uint8)(unsafe.Pointer(buf)) = libc.Uint8FromInt32(c>>libc.Int32FromInt32(18) | int32(_UTF8_cval4))
		*(*uint8)(unsafe.Pointer(buf + 1)) = libc.Uint8FromInt32(c>>libc.Int32FromInt32(12)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		*(*uint8)(unsafe.Pointer(buf + 2)) = libc.Uint8FromInt32(c>>libc.Int32FromInt32(6)&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		*(*uint8)(unsafe.Pointer(buf + 3)) = libc.Uint8FromInt32(c&libc.Int32FromInt32(0x3f) | libc.Int32FromInt32(0x80))
		return int32(4)
	}
	return 0 /* LCOV_EXCL_LINE: this case too is eliminated before calling */
}

func XXmlUtf16Encode(tls *libc.TLS, charNum int32, buf uintptr) (r int32) {
	if charNum < 0 {
		return 0
	}
	if charNum < int32(0x10000) {
		*(*uint16)(unsafe.Pointer(buf)) = libc.Uint16FromInt32(charNum)
		return int32(1)
	}
	if charNum < int32(0x110000) {
		charNum -= int32(0x10000)
		*(*uint16)(unsafe.Pointer(buf)) = libc.Uint16FromInt32(charNum>>libc.Int32FromInt32(10) + libc.Int32FromInt32(0xD800))
		*(*uint16)(unsafe.Pointer(buf + 1*2)) = libc.Uint16FromInt32(charNum&libc.Int32FromInt32(0x3FF) + libc.Int32FromInt32(0xDC00))
		return int32(2)
	}
	return 0
}

type Tunknown_encoding = struct {
	Fnormal   Tnormal_encoding
	Fconvert  TCONVERTER
	FuserData uintptr
	Futf16    [256]uint16
	Futf8     [256][4]uint8
}

func XXmlSizeOfUnknownEncoding(tls *libc.TLS) (r int32) {
	return int32(2016)
}

func _unknown_isName(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	var c int32
	var uenc uintptr
	_, _ = c, uenc
	uenc = enc
	c = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tunknown_encoding)(unsafe.Pointer(uenc)).Fconvert})))(tls, (*Tunknown_encoding)(unsafe.Pointer(uenc)).FuserData, p)
	if c & ^libc.Int32FromInt32(0xFFFF) != 0 {
		return 0
	}
	return libc.Int32FromUint32(_namingBitmap[libc.Int32FromUint8(_namePages[c>>int32(8)])<<int32(3)+c&int32(0xFF)>>int32(5)] & (libc.Uint32FromUint32(1) << (c & libc.Int32FromInt32(0xFF) & libc.Int32FromInt32(0x1F))))
}

func _unknown_isNmstrt(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	var c int32
	var uenc uintptr
	_, _ = c, uenc
	uenc = enc
	c = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tunknown_encoding)(unsafe.Pointer(uenc)).Fconvert})))(tls, (*Tunknown_encoding)(unsafe.Pointer(uenc)).FuserData, p)
	if c & ^libc.Int32FromInt32(0xFFFF) != 0 {
		return 0
	}
	return libc.Int32FromUint32(_namingBitmap[libc.Int32FromUint8(_nmstrtPages[c>>int32(8)])<<int32(3)+c&int32(0xFF)>>int32(5)] & (libc.Uint32FromUint32(1) << (c & libc.Int32FromInt32(0xFF) & libc.Int32FromInt32(0x1F))))
}

func _unknown_isInvalid(tls *libc.TLS, enc uintptr, p uintptr) (r int32) {
	var c int32
	var uenc uintptr
	_, _ = c, uenc
	uenc = enc
	c = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tunknown_encoding)(unsafe.Pointer(uenc)).Fconvert})))(tls, (*Tunknown_encoding)(unsafe.Pointer(uenc)).FuserData, p)
	return libc.BoolInt32(c & ^libc.Int32FromInt32(0xFFFF) != 0 || _checkCharRefNumber(tls, c) < 0)
}

func _unknown_toUtf8(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var c, n int32
	var uenc, utf8, v2 uintptr
	var _ /* buf at bp+0 */ [4]uint8
	_, _, _, _, _ = c, n, uenc, utf8, v2
	uenc = enc
	for {
		if *(*uintptr)(unsafe.Pointer(fromP)) == fromLim {
			return int32(_XML_CONVERT_COMPLETED)
		}
		utf8 = uenc + 992 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)))))*4
		v2 = utf8
		utf8++
		n = libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(v2)))
		if n == 0 {
			c = (*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tunknown_encoding)(unsafe.Pointer(uenc)).Fconvert})))(tls, (*Tunknown_encoding)(unsafe.Pointer(uenc)).FuserData, *(*uintptr)(unsafe.Pointer(fromP)))
			n = XXmlUtf8Encode(tls, c, bp)
			if int64(n) > int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) {
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			utf8 = bp
			*(*uintptr)(unsafe.Pointer(fromP)) += uintptr(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)))))))) - (int32(_BT_LEAD2) - libc.Int32FromInt32(2)))
		} else {
			if int64(n) > int64(toLim)-int64(*(*uintptr)(unsafe.Pointer(toP))) {
				return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
			}
			*(*uintptr)(unsafe.Pointer(fromP))++
		}
		libc.Xmemcpy(tls, *(*uintptr)(unsafe.Pointer(toP)), utf8, libc.Uint64FromInt32(n))
		*(*uintptr)(unsafe.Pointer(toP)) += uintptr(n)
		goto _1
	_1:
	}
	return r
}

func _unknown_toUtf16(tls *libc.TLS, enc uintptr, fromP uintptr, fromLim uintptr, toP uintptr, toLim uintptr) (r _XML_Convert_Result) {
	var c uint16
	var uenc, v1, v2 uintptr
	_, _, _, _ = c, uenc, v1, v2
	uenc = enc
	for *(*uintptr)(unsafe.Pointer(fromP)) < fromLim && *(*uintptr)(unsafe.Pointer(toP)) < toLim {
		c = *(*uint16)(unsafe.Pointer(uenc + 480 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)))))*2))
		if libc.Int32FromUint16(c) == 0 {
			c = libc.Uint16FromInt32((*(*func(*libc.TLS, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*Tunknown_encoding)(unsafe.Pointer(uenc)).Fconvert})))(tls, (*Tunknown_encoding)(unsafe.Pointer(uenc)).FuserData, *(*uintptr)(unsafe.Pointer(fromP))))
			*(*uintptr)(unsafe.Pointer(fromP)) += uintptr(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(enc + 136 + uintptr(*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(fromP)))))))) - (int32(_BT_LEAD2) - libc.Int32FromInt32(2)))
		} else {
			*(*uintptr)(unsafe.Pointer(fromP))++
		}
		v2 = toP
		v1 = *(*uintptr)(unsafe.Pointer(v2))
		*(*uintptr)(unsafe.Pointer(v2)) += 2
		*(*uint16)(unsafe.Pointer(v1)) = c
	}
	if *(*uintptr)(unsafe.Pointer(toP)) == toLim && *(*uintptr)(unsafe.Pointer(fromP)) < fromLim {
		return int32(_XML_CONVERT_OUTPUT_EXHAUSTED)
	} else {
		return int32(_XML_CONVERT_COMPLETED)
	}
	return r
}

func XXmlInitUnknownEncoding(tls *libc.TLS, mem uintptr, table uintptr, convert TCONVERTER, userData uintptr) (r uintptr) {
	var c, i, v3 int32
	var e uintptr
	_, _, _, _ = c, e, i, v3
	e = mem
	libc.Xmemcpy(tls, mem, uintptr(unsafe.Pointer(&_latin1_encoding)), uint64(464))
	i = 0
	for {
		if !(i < int32(128)) {
			break
		}
		if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_latin1_encoding)) + 136 + uintptr(i)))) != int32(_BT_OTHER) && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_latin1_encoding)) + 136 + uintptr(i)))) != int32(_BT_NONXML) && *(*int32)(unsafe.Pointer(table + uintptr(i)*4)) != i {
			return uintptr(0)
		}
		goto _1
	_1:
		;
		i++
	}
	i = 0
	for {
		if !(i < int32(256)) {
			break
		}
		c = *(*int32)(unsafe.Pointer(table + uintptr(i)*4))
		if c == -int32(1) {
			*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = uint8(_BT_MALFORM)
			/* This shouldn't really get used. */
			*(*uint16)(unsafe.Pointer(e + 480 + uintptr(i)*2)) = uint16(0xFFFF)
			*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4)) = uint8(1)
			*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4 + 1)) = uint8(0)
		} else {
			if c < 0 {
				if c < -int32(4) {
					return uintptr(0)
				}
				/* Multi-byte sequences need a converter function */
				if !(convert != 0) {
					return uintptr(0)
				}
				*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = libc.Uint8FromInt32(int32(_BT_LEAD2) - (c + libc.Int32FromInt32(2)))
				*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4)) = uint8(0)
				*(*uint16)(unsafe.Pointer(e + 480 + uintptr(i)*2)) = uint16(0)
			} else {
				if c < int32(0x80) {
					if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_latin1_encoding)) + 136 + uintptr(c)))) != int32(_BT_OTHER) && libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_latin1_encoding)) + 136 + uintptr(c)))) != int32(_BT_NONXML) && c != i {
						return uintptr(0)
					}
					*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = *(*uint8)(unsafe.Pointer(uintptr(unsafe.Pointer(&_latin1_encoding)) + 136 + uintptr(c)))
					*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4)) = uint8(1)
					*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4 + 1)) = libc.Uint8FromInt32(c)
					if c == 0 {
						v3 = int32(0xFFFF)
					} else {
						v3 = c
					}
					*(*uint16)(unsafe.Pointer(e + 480 + uintptr(i)*2)) = libc.Uint16FromInt32(v3)
				} else {
					if _checkCharRefNumber(tls, c) < 0 {
						*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = uint8(_BT_NONXML)
						/* This shouldn't really get used. */
						*(*uint16)(unsafe.Pointer(e + 480 + uintptr(i)*2)) = uint16(0xFFFF)
						*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4)) = uint8(1)
						*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4 + 1)) = uint8(0)
					} else {
						if c > int32(0xFFFF) {
							return uintptr(0)
						}
						if _namingBitmap[libc.Int32FromUint8(_nmstrtPages[c>>int32(8)])<<int32(3)+c&int32(0xff)>>int32(5)]&(uint32(1)<<(c&int32(0xff)&int32(0x1F))) != 0 {
							*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = uint8(_BT_NMSTRT)
						} else {
							if _namingBitmap[libc.Int32FromUint8(_namePages[c>>int32(8)])<<int32(3)+c&int32(0xff)>>int32(5)]&(uint32(1)<<(c&int32(0xff)&int32(0x1F))) != 0 {
								*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = uint8(_BT_NAME)
							} else {
								*(*uint8)(unsafe.Pointer(e + 136 + uintptr(i))) = uint8(_BT_OTHER)
							}
						}
						*(*uint8)(unsafe.Pointer(e + 992 + uintptr(i)*4)) = libc.Uint8FromInt32(XXmlUtf8Encode(tls, c, e+992+uintptr(i)*4+uintptr(1)))
						*(*uint16)(unsafe.Pointer(e + 480 + uintptr(i)*2)) = libc.Uint16FromInt32(c)
					}
				}
			}
		}
		goto _2
	_2:
		;
		i++
	}
	(*Tunknown_encoding)(unsafe.Pointer(e)).FuserData = userData
	(*Tunknown_encoding)(unsafe.Pointer(e)).Fconvert = convert
	if convert != 0 {
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisName2 = __ccgo_fp(_unknown_isName)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisName3 = __ccgo_fp(_unknown_isName)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisName4 = __ccgo_fp(_unknown_isName)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisNmstrt2 = __ccgo_fp(_unknown_isNmstrt)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisNmstrt3 = __ccgo_fp(_unknown_isNmstrt)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisNmstrt4 = __ccgo_fp(_unknown_isNmstrt)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisInvalid2 = __ccgo_fp(_unknown_isInvalid)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisInvalid3 = __ccgo_fp(_unknown_isInvalid)
		(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.FisInvalid4 = __ccgo_fp(_unknown_isInvalid)
	}
	(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.Fenc.Futf8Convert = __ccgo_fp(_unknown_toUtf8)
	(*Tunknown_encoding)(unsafe.Pointer(e)).Fnormal.Fenc.Futf16Convert = __ccgo_fp(_unknown_toUtf16)
	return e
}

const _UNKNOWN_ENC = -1
const _ISO_8859_1_ENC = 0
const _US_ASCII_ENC = 1
const _UTF_8_ENC = 2
const _UTF_16_ENC = 3
const _UTF_16BE_ENC = 4
const _UTF_16LE_ENC = 5
const
/* must match encodingNames up to here */
_NO_ENC = 6

var _KW_ISO_8859_1 = [11]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_S),
	2: uint8(m_ASCII_O),
	3: uint8(m_ASCII_MINUS1),
	4: uint8(m_ASCII_8),
	5: uint8(m_ASCII_8),
	6: uint8(m_ASCII_51),
	7: uint8(m_ASCII_9),
	8: uint8(m_ASCII_MINUS1),
	9: uint8(m_ASCII_1),
}
var _KW_US_ASCII = [9]uint8{
	0: uint8(m_ASCII_U1),
	1: uint8(m_ASCII_S),
	2: uint8(m_ASCII_MINUS1),
	3: uint8(m_ASCII_A),
	4: uint8(m_ASCII_S),
	5: uint8(m_ASCII_C),
	6: uint8(m_ASCII_I),
	7: uint8(m_ASCII_I),
}
var _KW_UTF_8 = [6]uint8{
	0: uint8(m_ASCII_U1),
	1: uint8(m_ASCII_T),
	2: uint8(m_ASCII_F),
	3: uint8(m_ASCII_MINUS1),
	4: uint8(m_ASCII_8),
}
var _KW_UTF_16 = [7]uint8{
	0: uint8(m_ASCII_U1),
	1: uint8(m_ASCII_T),
	2: uint8(m_ASCII_F),
	3: uint8(m_ASCII_MINUS1),
	4: uint8(m_ASCII_1),
	5: uint8(m_ASCII_61),
}
var _KW_UTF_16BE = [9]uint8{
	0: uint8(m_ASCII_U1),
	1: uint8(m_ASCII_T),
	2: uint8(m_ASCII_F),
	3: uint8(m_ASCII_MINUS1),
	4: uint8(m_ASCII_1),
	5: uint8(m_ASCII_61),
	6: uint8(m_ASCII_B1),
	7: uint8(m_ASCII_E),
}
var _KW_UTF_16LE = [9]uint8{
	0: uint8(m_ASCII_U1),
	1: uint8(m_ASCII_T),
	2: uint8(m_ASCII_F),
	3: uint8(m_ASCII_MINUS1),
	4: uint8(m_ASCII_1),
	5: uint8(m_ASCII_61),
	6: uint8(m_ASCII_L),
	7: uint8(m_ASCII_E),
}

func _getEncodingIndex(tls *libc.TLS, name uintptr) (r int32) {
	var i int32
	_ = i
	if name == libc.UintptrFromInt32(0) {
		return int32(_NO_ENC)
	}
	i = 0
	for {
		if !(i < libc.Int32FromUint64(libc.Uint64FromInt64(48)/libc.Uint64FromInt64(8))) {
			break
		}
		if _streqci(tls, name, _encodingNames[i]) != 0 {
			return i
		}
		goto _1
	_1:
		;
		i++
	}
	return int32(_UNKNOWN_ENC)
}

var _encodingNames = [6]uintptr{
	0: uintptr(unsafe.Pointer(&_KW_ISO_8859_1)),
	1: uintptr(unsafe.Pointer(&_KW_US_ASCII)),
	2: uintptr(unsafe.Pointer(&_KW_UTF_8)),
	3: uintptr(unsafe.Pointer(&_KW_UTF_16)),
	4: uintptr(unsafe.Pointer(&_KW_UTF_16BE)),
	5: uintptr(unsafe.Pointer(&_KW_UTF_16LE)),
}

/* For binary compatibility, we store the index of the encoding
   specified at initialization in the isUtf16 member.
*/

/* This is what detects the encoding.  encodingTable maps from
   encoding indices to encodings; INIT_ENC_INDEX(enc) is the index of
   the external (protocol) specified encoding; state is
   XML_CONTENT_STATE if we're parsing an external text entity, and
   XML_PROLOG_STATE otherwise.
*/

func _initScan(tls *libc.TLS, encodingTable uintptr, enc uintptr, state int32, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	var e int32
	var encPtr uintptr
	_, _ = e, encPtr
	if ptr >= end {
		return -int32(4)
	}
	encPtr = (*TINIT_ENCODING)(unsafe.Pointer(enc)).FencPtr
	if ptr+uintptr(1) == end {
		/* only a single byte available for auto-detection */
		/* so we're parsing an external text entity... */
		/* if UTF-16 was externally specified, then we need at least 2 bytes */
		switch libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) {
		case int32(_UTF_16_ENC):
			fallthrough
		case int32(_UTF_16LE_ENC):
			fallthrough
		case int32(_UTF_16BE_ENC):
			return -int32(1)
		}
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) {
		case int32(0xFE):
			fallthrough
		case int32(0xFF):
			fallthrough
		case int32(0xEF): /* possibly first byte of UTF-8 BOM */
			if libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) == int32(_ISO_8859_1_ENC) && state == int32(m_XML_CONTENT_STATE) {
				break
			}
			/* fall through */
			fallthrough
		case 0x00:
			fallthrough
		case int32(0x3C):
			return -int32(1)
		}
	} else {
		switch libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr)))<<libc.Int32FromInt32(8) | libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) {
		case int32(0xFEFF):
			if libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) == int32(_ISO_8859_1_ENC) && state == int32(m_XML_CONTENT_STATE) {
				break
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(_UTF_16BE_ENC)*8))
			return int32(m_XML_TOK_BOM)
			/* 00 3C is handled in the default case */
			fallthrough
		case int32(0x3C00):
			if (libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) == int32(_UTF_16BE_ENC) || libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) == int32(_UTF_16_ENC)) && state == int32(m_XML_CONTENT_STATE) {
				break
			}
			*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(_UTF_16LE_ENC)*8))
			return (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(encPtr)) + uintptr(state)*8))})))(tls, *(*uintptr)(unsafe.Pointer(encPtr)), ptr, end, nextTokPtr)
		case int32(0xFFFE):
			if libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) == int32(_ISO_8859_1_ENC) && state == int32(m_XML_CONTENT_STATE) {
				break
			}
			*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(2)
			*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(_UTF_16LE_ENC)*8))
			return int32(m_XML_TOK_BOM)
		case int32(0xEFBB):
			/* Maybe a UTF-8 BOM (EF BB BF) */
			/* If there's an explicitly specified (external) encoding
			   of ISO-8859-1 or some flavour of UTF-16
			   and this is an external text entity,
			   don't look for the BOM,
			   because it might be a legal data.
			*/
			if state == int32(m_XML_CONTENT_STATE) {
				e = libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16)
				if e == int32(_ISO_8859_1_ENC) || e == int32(_UTF_16BE_ENC) || e == int32(_UTF_16LE_ENC) || e == int32(_UTF_16_ENC) {
					break
				}
			}
			if ptr+uintptr(2) == end {
				return -int32(1)
			}
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 2))) == int32(0xBF) {
				*(*uintptr)(unsafe.Pointer(nextTokPtr)) = ptr + uintptr(3)
				*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(_UTF_8_ENC)*8))
				return int32(m_XML_TOK_BOM)
			}
		default:
			if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr))) == int32('\000') {
				/* 0 isn't a legal data character. Furthermore a document
				   entity can only start with ASCII characters.  So the only
				   way this can fail to be big-endian UTF-16 if it it's an
				   external parsed general entity that's labelled as
				   UTF-16LE.
				*/
				if state == int32(m_XML_CONTENT_STATE) && libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16) == int32(_UTF_16LE_ENC) {
					break
				}
				*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(_UTF_16BE_ENC)*8))
				return (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(encPtr)) + uintptr(state)*8))})))(tls, *(*uintptr)(unsafe.Pointer(encPtr)), ptr, end, nextTokPtr)
			} else {
				if libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(ptr + 1))) == int32('\000') {
					/* We could recover here in the case:
					    - parsing an external entity
					    - second byte is 0
					    - no externally specified encoding
					    - no encoding declaration
					   by assuming UTF-16LE.  But we don't, because this would mean when
					   presented just with a single byte, we couldn't reliably determine
					   whether we needed further bytes.
					*/
					if state == int32(m_XML_CONTENT_STATE) {
						break
					}
					*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(_UTF_16LE_ENC)*8))
					return (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(encPtr)) + uintptr(state)*8))})))(tls, *(*uintptr)(unsafe.Pointer(encPtr)), ptr, end, nextTokPtr)
				}
			}
			break
		}
	}
	*(*uintptr)(unsafe.Pointer(encPtr)) = *(*uintptr)(unsafe.Pointer(encodingTable + uintptr(libc.Int32FromUint8((*TINIT_ENCODING)(unsafe.Pointer(enc)).FinitEnc.FisUtf16))*8))
	return (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{*(*TSCANNER)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(encPtr)) + uintptr(state)*8))})))(tls, *(*uintptr)(unsafe.Pointer(encPtr)), ptr, end, nextTokPtr)
}

/* This file is included!
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

func XXmlGetUtf8InternalEncoding(tls *libc.TLS) (r uintptr) {
	return uintptr(unsafe.Pointer(&_internal_utf8_encoding))
}

func XXmlGetUtf16InternalEncoding(tls *libc.TLS) (r uintptr) {
	return uintptr(unsafe.Pointer(&_internal_big2_encoding))
}

var _encodings = [7]uintptr{
	0: uintptr(unsafe.Pointer(&_latin1_encoding)),
	1: uintptr(unsafe.Pointer(&_ascii_encoding)),
	2: uintptr(unsafe.Pointer(&_utf8_encoding)),
	3: uintptr(unsafe.Pointer(&_big2_encoding)),
	4: uintptr(unsafe.Pointer(&_big2_encoding)),
	5: uintptr(unsafe.Pointer(&_little2_encoding)),
	6: uintptr(unsafe.Pointer(&_utf8_encoding)),
}

func _initScanProlog(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	return _initScan(tls, uintptr(unsafe.Pointer(&_encodings)), enc, m_XML_PROLOG_STATE, ptr, end, nextTokPtr)
}

func _initScanContent(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	return _initScan(tls, uintptr(unsafe.Pointer(&_encodings)), enc, int32(m_XML_CONTENT_STATE), ptr, end, nextTokPtr)
}

func XXmlInitEncoding(tls *libc.TLS, p uintptr, encPtr uintptr, name uintptr) (r int32) {
	var i int32
	_ = i
	i = _getEncodingIndex(tls, name)
	if i == int32(_UNKNOWN_ENC) {
		return 0
	}
	(*TINIT_ENCODING)(unsafe.Pointer(p)).FinitEnc.FisUtf16 = libc.Uint8FromInt32(i)
	*(*TSCANNER)(unsafe.Pointer(p)) = __ccgo_fp(_initScanProlog)
	*(*TSCANNER)(unsafe.Pointer(p + 1*8)) = __ccgo_fp(_initScanContent)
	(*TINIT_ENCODING)(unsafe.Pointer(p)).FinitEnc.FupdatePosition = __ccgo_fp(_initUpdatePosition)
	(*TINIT_ENCODING)(unsafe.Pointer(p)).FencPtr = encPtr
	*(*uintptr)(unsafe.Pointer(encPtr)) = p
	return int32(1)
}

func _findEncoding(tls *libc.TLS, enc uintptr, _ptr uintptr, end uintptr) (r uintptr) {
	bp := tls.Alloc(144)
	defer tls.Free(144)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var i int32
	var _ /* buf at bp+8 */ [128]uint8
	var _ /* p at bp+136 */ uintptr
	_ = i
	*(*uintptr)(unsafe.Pointer(bp + 136)) = bp + 8
	(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, end, bp+136, *(*uintptr)(unsafe.Pointer(bp + 136))+uintptr(m_ENCODING_MAX)-uintptr(1))
	if *(*uintptr)(unsafe.Pointer(bp)) != end {
		return uintptr(0)
	}
	*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 136)))) = uint8(0)
	if _streqci(tls, bp+8, uintptr(unsafe.Pointer(&_KW_UTF_16))) != 0 && (*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar == int32(2) {
		return enc
	}
	i = _getEncodingIndex(tls, bp+8)
	if i == int32(_UNKNOWN_ENC) {
		return uintptr(0)
	}
	return _encodings[i]
}

func XXmlParseXmlDecl(tls *libc.TLS, isGeneralTextEntity int32, enc uintptr, ptr uintptr, end uintptr, badPtr uintptr, versionPtr uintptr, versionEndPtr uintptr, encodingName uintptr, encoding uintptr, standalone uintptr) (r int32) {
	return _doParseXmlDecl(tls, __ccgo_fp(_findEncoding), isGeneralTextEntity, enc, ptr, end, badPtr, versionPtr, versionEndPtr, encodingName, encoding, standalone)
}

/* This file is included!
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

func XXmlGetUtf8InternalEncodingNS(tls *libc.TLS) (r uintptr) {
	return uintptr(unsafe.Pointer(&_internal_utf8_encoding_ns))
}

func XXmlGetUtf16InternalEncodingNS(tls *libc.TLS) (r uintptr) {
	return uintptr(unsafe.Pointer(&_internal_big2_encoding_ns))
}

var _encodingsNS = [7]uintptr{
	0: uintptr(unsafe.Pointer(&_latin1_encoding_ns)),
	1: uintptr(unsafe.Pointer(&_ascii_encoding_ns)),
	2: uintptr(unsafe.Pointer(&_utf8_encoding_ns)),
	3: uintptr(unsafe.Pointer(&_big2_encoding_ns)),
	4: uintptr(unsafe.Pointer(&_big2_encoding_ns)),
	5: uintptr(unsafe.Pointer(&_little2_encoding_ns)),
	6: uintptr(unsafe.Pointer(&_utf8_encoding_ns)),
}

func _initScanPrologNS(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	return _initScan(tls, uintptr(unsafe.Pointer(&_encodingsNS)), enc, m_XML_PROLOG_STATE, ptr, end, nextTokPtr)
}

func _initScanContentNS(tls *libc.TLS, enc uintptr, ptr uintptr, end uintptr, nextTokPtr uintptr) (r int32) {
	return _initScan(tls, uintptr(unsafe.Pointer(&_encodingsNS)), enc, int32(m_XML_CONTENT_STATE), ptr, end, nextTokPtr)
}

func XXmlInitEncodingNS(tls *libc.TLS, p uintptr, encPtr uintptr, name uintptr) (r int32) {
	var i int32
	_ = i
	i = _getEncodingIndex(tls, name)
	if i == int32(_UNKNOWN_ENC) {
		return 0
	}
	(*TINIT_ENCODING)(unsafe.Pointer(p)).FinitEnc.FisUtf16 = libc.Uint8FromInt32(i)
	*(*TSCANNER)(unsafe.Pointer(p)) = __ccgo_fp(_initScanPrologNS)
	*(*TSCANNER)(unsafe.Pointer(p + 1*8)) = __ccgo_fp(_initScanContentNS)
	(*TINIT_ENCODING)(unsafe.Pointer(p)).FinitEnc.FupdatePosition = __ccgo_fp(_initUpdatePosition)
	(*TINIT_ENCODING)(unsafe.Pointer(p)).FencPtr = encPtr
	*(*uintptr)(unsafe.Pointer(encPtr)) = p
	return int32(1)
}

func _findEncodingNS(tls *libc.TLS, enc uintptr, _ptr uintptr, end uintptr) (r uintptr) {
	bp := tls.Alloc(144)
	defer tls.Free(144)
	*(*uintptr)(unsafe.Pointer(bp)) = _ptr
	var i int32
	var _ /* buf at bp+8 */ [128]uint8
	var _ /* p at bp+136 */ uintptr
	_ = i
	*(*uintptr)(unsafe.Pointer(bp + 136)) = bp + 8
	(*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr, uintptr) _XML_Convert_Result)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).Futf8Convert})))(tls, enc, bp, end, bp+136, *(*uintptr)(unsafe.Pointer(bp + 136))+uintptr(m_ENCODING_MAX)-uintptr(1))
	if *(*uintptr)(unsafe.Pointer(bp)) != end {
		return uintptr(0)
	}
	*(*uint8)(unsafe.Pointer(*(*uintptr)(unsafe.Pointer(bp + 136)))) = uint8(0)
	if _streqci(tls, bp+8, uintptr(unsafe.Pointer(&_KW_UTF_16))) != 0 && (*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar == int32(2) {
		return enc
	}
	i = _getEncodingIndex(tls, bp+8)
	if i == int32(_UNKNOWN_ENC) {
		return uintptr(0)
	}
	return _encodingsNS[i]
}

func XXmlParseXmlDeclNS(tls *libc.TLS, isGeneralTextEntity int32, enc uintptr, ptr uintptr, end uintptr, badPtr uintptr, versionPtr uintptr, versionEndPtr uintptr, encodingName uintptr, encoding uintptr, standalone uintptr) (r int32) {
	return _doParseXmlDecl(tls, __ccgo_fp(_findEncodingNS), isGeneralTextEntity, enc, ptr, end, badPtr, versionPtr, versionEndPtr, encodingName, encoding, standalone)
}

func XXmlInitUnknownEncodingNS(tls *libc.TLS, mem uintptr, table uintptr, convert TCONVERTER, userData uintptr) (r uintptr) {
	var enc uintptr
	_ = enc
	enc = XXmlInitUnknownEncoding(tls, mem, table, convert, userData)
	if enc != 0 {
		*(*uint8)(unsafe.Pointer(enc + 136 + 58)) = uint8(_BT_COLON)
	}
	return enc
}

const m_ASCII_01 = 0x30
const m_ASCII_11 = 0x31
const m_ASCII_21 = 0x32
const m_ASCII_31 = 0x33
const m_ASCII_42 = 0x34
const m_ASCII_52 = 0x35
const m_ASCII_62 = 0x36
const m_ASCII_72 = 0x37
const m_ASCII_81 = 0x38
const m_ASCII_91 = 0x39
const m_ASCII_AMP2 = 0x26
const m_ASCII_APOS2 = 0x27
const m_ASCII_COLON1 = 0x3A
const m_ASCII_EQUALS1 = 0x3D
const m_ASCII_EXCL1 = 0x21
const m_ASCII_G1 = 71
const m_ASCII_GT2 = 0x3E
const m_ASCII_K2 = 75
const m_ASCII_LSQB2 = 0x5B
const m_ASCII_LT2 = 0x3C
const m_ASCII_MINUS2 = 0x2D
const m_ASCII_N2 = 78
const m_ASCII_P1 = 80
const m_ASCII_PERIOD1 = 0x2E
const m_ASCII_Q1 = 81
const m_ASCII_QUOT2 = 0x22
const m_ASCII_R2 = 82
const m_ASCII_RSQB2 = 0x5D
const m_ASCII_SEMI2 = 0x3B
const m_ASCII_SPACE2 = 0x20
const m_ASCII_TAB2 = 0x09
const m_ASCII_UNDERSCORE2 = 0x5F
const m_ASCII_Y2 = 89
const m_ASCII_Z2 = 0x5A
const m_ASCII_a1 = 0x61
const m_ASCII_b2 = 0x62
const m_ASCII_c1 = 0x63
const m_ASCII_d2 = 0x64
const m_ASCII_e1 = 0x65
const m_ASCII_f2 = 0x66
const m_ASCII_g1 = 0x67
const m_ASCII_i2 = 0x69
const m_ASCII_l1 = 0x6C
const m_ASCII_m1 = 0x6D
const m_ASCII_n1 = 0x6E
const m_ASCII_o1 = 0x6F
const m_ASCII_p1 = 0x70
const m_ASCII_q2 = 0x71
const m_ASCII_r1 = 0x72
const m_ASCII_s1 = 0x73
const m_ASCII_t1 = 0x74
const m_ASCII_u2 = 0x75
const m_ASCII_v2 = 0x76
const m_ASCII_x1 = 0x78
const m_ASCII_y2 = 0x79
const m_ASCII_z2 = 0x7A

/*
                            __  __            _
                         ___\ \/ /_ __   __ _| |_
                        / _ \\  /| '_ \ / _` | __|
                       |  __//  \| |_) | (_| | |_
                        \___/_/\_\ .__/ \__,_|\__|
                                 |_| XML parser

   Copyright (c) 1997-2000 Thai Open Source Software Center Ltd
   Copyright (c) 2000-2017 Expat development team
   Licensed under the MIT license:

   Permission is  hereby granted,  free of charge,  to any  person obtaining
   a  copy  of  this  software   and  associated  documentation  files  (the
   "Software"),  to  deal in  the  Software  without restriction,  including
   without  limitation the  rights  to use,  copy,  modify, merge,  publish,
   distribute, sublicense, and/or sell copies of the Software, and to permit
   persons  to whom  the Software  is  furnished to  do so,  subject to  the
   following conditions:

   The above copyright  notice and this permission notice  shall be included
   in all copies or substantial portions of the Software.

   THE  SOFTWARE  IS  PROVIDED  "AS  IS",  WITHOUT  WARRANTY  OF  ANY  KIND,
   EXPRESS  OR IMPLIED,  INCLUDING  BUT  NOT LIMITED  TO  THE WARRANTIES  OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
   NO EVENT SHALL THE AUTHORS OR  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
   DAMAGES OR  OTHER LIABILITY, WHETHER  IN AN  ACTION OF CONTRACT,  TORT OR
   OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
   USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/* Doesn't check:

that ,| are not mixed in a model group
content of literals

*/

var _KW_ANY = [4]uint8{
	0: uint8(m_ASCII_A),
	1: uint8(m_ASCII_N2),
	2: uint8(m_ASCII_Y2),
}
var _KW_ATTLIST = [8]uint8{
	0: uint8(m_ASCII_A),
	1: uint8(m_ASCII_T),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_L),
	4: uint8(m_ASCII_I),
	5: uint8(m_ASCII_S),
	6: uint8(m_ASCII_T),
}
var _KW_CDATA = [6]uint8{
	0: uint8(m_ASCII_C),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_A),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_A),
}
var _KW_DOCTYPE = [8]uint8{
	0: uint8(m_ASCII_D),
	1: uint8(m_ASCII_O),
	2: uint8(m_ASCII_C),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_Y2),
	5: uint8(m_ASCII_P1),
	6: uint8(m_ASCII_E),
}
var _KW_ELEMENT = [8]uint8{
	0: uint8(m_ASCII_E),
	1: uint8(m_ASCII_L),
	2: uint8(m_ASCII_E),
	3: uint8(m_ASCII_M),
	4: uint8(m_ASCII_E),
	5: uint8(m_ASCII_N2),
	6: uint8(m_ASCII_T),
}
var _KW_EMPTY = [6]uint8{
	0: uint8(m_ASCII_E),
	1: uint8(m_ASCII_M),
	2: uint8(m_ASCII_P1),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_Y2),
}
var _KW_ENTITIES = [9]uint8{
	0: uint8(m_ASCII_E),
	1: uint8(m_ASCII_N2),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_I),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_I),
	6: uint8(m_ASCII_E),
	7: uint8(m_ASCII_S),
}
var _KW_ENTITY = [7]uint8{
	0: uint8(m_ASCII_E),
	1: uint8(m_ASCII_N2),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_I),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_Y2),
}
var _KW_FIXED = [6]uint8{
	0: uint8(m_ASCII_F),
	1: uint8(m_ASCII_I),
	2: uint8(m_ASCII_X),
	3: uint8(m_ASCII_E),
	4: uint8(m_ASCII_D),
}
var _KW_ID = [3]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_D),
}
var _KW_IDREF = [6]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_R2),
	3: uint8(m_ASCII_E),
	4: uint8(m_ASCII_F),
}
var _KW_IDREFS = [7]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_R2),
	3: uint8(m_ASCII_E),
	4: uint8(m_ASCII_F),
	5: uint8(m_ASCII_S),
}
var _KW_IGNORE = [7]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_G1),
	2: uint8(m_ASCII_N2),
	3: uint8(m_ASCII_O),
	4: uint8(m_ASCII_R2),
	5: uint8(m_ASCII_E),
}
var _KW_IMPLIED = [8]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_M),
	2: uint8(m_ASCII_P1),
	3: uint8(m_ASCII_L),
	4: uint8(m_ASCII_I),
	5: uint8(m_ASCII_E),
	6: uint8(m_ASCII_D),
}
var _KW_INCLUDE = [8]uint8{
	0: uint8(m_ASCII_I),
	1: uint8(m_ASCII_N2),
	2: uint8(m_ASCII_C),
	3: uint8(m_ASCII_L),
	4: uint8(m_ASCII_U1),
	5: uint8(m_ASCII_D),
	6: uint8(m_ASCII_E),
}
var _KW_NDATA = [6]uint8{
	0: uint8(m_ASCII_N2),
	1: uint8(m_ASCII_D),
	2: uint8(m_ASCII_A),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_A),
}
var _KW_NMTOKEN = [8]uint8{
	0: uint8(m_ASCII_N2),
	1: uint8(m_ASCII_M),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_O),
	4: uint8(m_ASCII_K2),
	5: uint8(m_ASCII_E),
	6: uint8(m_ASCII_N2),
}
var _KW_NMTOKENS = [9]uint8{
	0: uint8(m_ASCII_N2),
	1: uint8(m_ASCII_M),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_O),
	4: uint8(m_ASCII_K2),
	5: uint8(m_ASCII_E),
	6: uint8(m_ASCII_N2),
	7: uint8(m_ASCII_S),
}
var _KW_NOTATION = [9]uint8{
	0: uint8(m_ASCII_N2),
	1: uint8(m_ASCII_O),
	2: uint8(m_ASCII_T),
	3: uint8(m_ASCII_A),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_I),
	6: uint8(m_ASCII_O),
	7: uint8(m_ASCII_N2),
}
var _KW_PCDATA = [7]uint8{
	0: uint8(m_ASCII_P1),
	1: uint8(m_ASCII_C),
	2: uint8(m_ASCII_D),
	3: uint8(m_ASCII_A),
	4: uint8(m_ASCII_T),
	5: uint8(m_ASCII_A),
}
var _KW_PUBLIC = [7]uint8{
	0: uint8(m_ASCII_P1),
	1: uint8(m_ASCII_U1),
	2: uint8(m_ASCII_B1),
	3: uint8(m_ASCII_L),
	4: uint8(m_ASCII_I),
	5: uint8(m_ASCII_C),
}
var _KW_REQUIRED = [9]uint8{
	0: uint8(m_ASCII_R2),
	1: uint8(m_ASCII_E),
	2: uint8(m_ASCII_Q1),
	3: uint8(m_ASCII_U1),
	4: uint8(m_ASCII_I),
	5: uint8(m_ASCII_R2),
	6: uint8(m_ASCII_E),
	7: uint8(m_ASCII_D),
}
var _KW_SYSTEM = [7]uint8{
	0: uint8(m_ASCII_S),
	1: uint8(m_ASCII_Y2),
	2: uint8(m_ASCII_S),
	3: uint8(m_ASCII_T),
	4: uint8(m_ASCII_E),
	5: uint8(m_ASCII_M),
}

func _prolog0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog1)
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_XML_DECL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog1)
		return int32(_XML_ROLE_XML_DECL)
	case int32(m_XML_TOK_PI):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog1)
		return int32(_XML_ROLE_PI)
	case int32(m_XML_TOK_COMMENT):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog1)
		return int32(_XML_ROLE_COMMENT)
	case int32(m_XML_TOK_BOM):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_DECL_OPEN):
		if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr(int32(2)*(*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_DOCTYPE))) != 0) {
			break
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype0)
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_INSTANCE_START):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_error)
		return int32(_XML_ROLE_INSTANCE_START)
	}
	return _common(tls, state, tok)
}

func _prolog1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_PI):
		return int32(_XML_ROLE_PI)
	case int32(m_XML_TOK_COMMENT):
		return int32(_XML_ROLE_COMMENT)
	case int32(m_XML_TOK_BOM):
		/* This case can never arise.  To reach this role function, the
		 * parse must have passed through prolog0 and therefore have had
		 * some form of input, even if only a space.  At that point, a
		 * byte order mark is no longer a valid character (though
		 * technically it should be interpreted as a non-breaking space),
		 * so will be rejected by the tokenizing stages.
		 */
		return int32(_XML_ROLE_NONE) /* LCOV_EXCL_LINE */
	case int32(m_XML_TOK_DECL_OPEN):
		if !((*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr(int32(2)*(*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_DOCTYPE))) != 0) {
			break
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype0)
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_INSTANCE_START):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_error)
		return int32(_XML_ROLE_INSTANCE_START)
	}
	return _common(tls, state, tok)
}

func _prolog2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_PI):
		return int32(_XML_ROLE_PI)
	case int32(m_XML_TOK_COMMENT):
		return int32(_XML_ROLE_COMMENT)
	case int32(m_XML_TOK_INSTANCE_START):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_error)
		return int32(_XML_ROLE_INSTANCE_START)
	}
	return _common(tls, state, tok)
}

func _doctype0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype1)
		return int32(_XML_ROLE_DOCTYPE_NAME)
	}
	return _common(tls, state, tok)
}

func _doctype1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_OPEN_BRACKET):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_internalSubset)
		return int32(_XML_ROLE_DOCTYPE_INTERNAL_SUBSET)
	case int32(m_XML_TOK_DECL_CLOSE):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog2)
		return int32(_XML_ROLE_DOCTYPE_CLOSE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_SYSTEM))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype3)
			return int32(_XML_ROLE_DOCTYPE_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_PUBLIC))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype2)
			return int32(_XML_ROLE_DOCTYPE_NONE)
		}
		break
	}
	return _common(tls, state, tok)
}

func _doctype2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype3)
		return int32(_XML_ROLE_DOCTYPE_PUBLIC_ID)
	}
	return _common(tls, state, tok)
}

func _doctype3(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype4)
		return int32(_XML_ROLE_DOCTYPE_SYSTEM_ID)
	}
	return _common(tls, state, tok)
}

func _doctype4(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_OPEN_BRACKET):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_internalSubset)
		return int32(_XML_ROLE_DOCTYPE_INTERNAL_SUBSET)
	case int32(m_XML_TOK_DECL_CLOSE):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog2)
		return int32(_XML_ROLE_DOCTYPE_CLOSE)
	}
	return _common(tls, state, tok)
}

func _doctype5(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case int32(m_XML_TOK_DECL_CLOSE):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog2)
		return int32(_XML_ROLE_DOCTYPE_CLOSE)
	}
	return _common(tls, state, tok)
}

func _internalSubset(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_DECL_OPEN):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr(int32(2)*(*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_ENTITY))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity0)
			return int32(_XML_ROLE_ENTITY_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr(int32(2)*(*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_ATTLIST))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist0)
			return int32(_XML_ROLE_ATTLIST_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr(int32(2)*(*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_ELEMENT))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element0)
			return int32(_XML_ROLE_ELEMENT_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr(int32(2)*(*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_NOTATION))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_notation0)
			return int32(_XML_ROLE_NOTATION_NONE)
		}
	case int32(m_XML_TOK_PI):
		return int32(_XML_ROLE_PI)
	case int32(m_XML_TOK_COMMENT):
		return int32(_XML_ROLE_COMMENT)
	case int32(m_XML_TOK_PARAM_ENTITY_REF):
		return int32(_XML_ROLE_PARAM_ENTITY_REF)
	case int32(m_XML_TOK_CLOSE_BRACKET):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_doctype5)
		return int32(_XML_ROLE_DOCTYPE_NONE)
	case -int32(4):
		return int32(_XML_ROLE_NONE)
	}
	return _common(tls, state, tok)
}

func _externalSubset0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_externalSubset1)
	if tok == int32(m_XML_TOK_XML_DECL) {
		return int32(_XML_ROLE_TEXT_DECL)
	}
	return _externalSubset1(tls, state, tok, ptr, end, enc)
}

func _externalSubset1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_COND_SECT_OPEN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_condSect0)
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_COND_SECT_CLOSE):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FincludeLevel == uint32(0) {
			break
		}
		*(*uint32)(unsafe.Pointer(state + 16)) -= uint32(1)
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_CLOSE_BRACKET):
	case -int32(4):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FincludeLevel != 0 {
			break
		}
		return int32(_XML_ROLE_NONE)
	default:
		return _internalSubset(tls, state, tok, ptr, end, enc)
	}
	return _common(tls, state, tok)
}

func _entity0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_PERCENT):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity1)
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity2)
		return int32(_XML_ROLE_GENERAL_ENTITY_NAME)
	}
	return _common(tls, state, tok)
}

func _entity1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity7)
		return int32(_XML_ROLE_PARAM_ENTITY_NAME)
	}
	return _common(tls, state, tok)
}

func _entity2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_SYSTEM))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity4)
			return int32(_XML_ROLE_ENTITY_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_PUBLIC))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity3)
			return int32(_XML_ROLE_ENTITY_NONE)
		}
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ENTITY_NONE)
		return int32(_XML_ROLE_ENTITY_VALUE)
	}
	return _common(tls, state, tok)
}

func _entity3(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity4)
		return int32(_XML_ROLE_ENTITY_PUBLIC_ID)
	}
	return _common(tls, state, tok)
}

func _entity4(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity5)
		return int32(_XML_ROLE_ENTITY_SYSTEM_ID)
	}
	return _common(tls, state, tok)
}

func _entity5(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	var v1 uintptr
	_ = v1
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_DECL_CLOSE):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity != 0 {
			v1 = __ccgo_fp(_internalSubset)
		} else {
			v1 = __ccgo_fp(_externalSubset1)
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = v1
		return int32(_XML_ROLE_ENTITY_COMPLETE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_NDATA))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity6)
			return int32(_XML_ROLE_ENTITY_NONE)
		}
		break
	}
	return _common(tls, state, tok)
}

func _entity6(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ENTITY_NONE)
		return int32(_XML_ROLE_ENTITY_NOTATION_NAME)
	}
	return _common(tls, state, tok)
}

func _entity7(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_SYSTEM))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity9)
			return int32(_XML_ROLE_ENTITY_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_PUBLIC))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity8)
			return int32(_XML_ROLE_ENTITY_NONE)
		}
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ENTITY_NONE)
		return int32(_XML_ROLE_ENTITY_VALUE)
	}
	return _common(tls, state, tok)
}

func _entity8(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity9)
		return int32(_XML_ROLE_ENTITY_PUBLIC_ID)
	}
	return _common(tls, state, tok)
}

func _entity9(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_entity10)
		return int32(_XML_ROLE_ENTITY_SYSTEM_ID)
	}
	return _common(tls, state, tok)
}

func _entity10(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	var v1 uintptr
	_ = v1
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ENTITY_NONE)
	case int32(m_XML_TOK_DECL_CLOSE):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity != 0 {
			v1 = __ccgo_fp(_internalSubset)
		} else {
			v1 = __ccgo_fp(_externalSubset1)
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = v1
		return int32(_XML_ROLE_ENTITY_COMPLETE)
	}
	return _common(tls, state, tok)
}

func _notation0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NOTATION_NONE)
	case int32(m_XML_TOK_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_notation1)
		return int32(_XML_ROLE_NOTATION_NAME)
	}
	return _common(tls, state, tok)
}

func _notation1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NOTATION_NONE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_SYSTEM))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_notation3)
			return int32(_XML_ROLE_NOTATION_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_PUBLIC))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_notation2)
			return int32(_XML_ROLE_NOTATION_NONE)
		}
		break
	}
	return _common(tls, state, tok)
}

func _notation2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NOTATION_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_notation4)
		return int32(_XML_ROLE_NOTATION_PUBLIC_ID)
	}
	return _common(tls, state, tok)
}

func _notation3(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NOTATION_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_NOTATION_NONE)
		return int32(_XML_ROLE_NOTATION_SYSTEM_ID)
	}
	return _common(tls, state, tok)
}

func _notation4(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	var v1 uintptr
	_ = v1
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NOTATION_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_NOTATION_NONE)
		return int32(_XML_ROLE_NOTATION_SYSTEM_ID)
	case int32(m_XML_TOK_DECL_CLOSE):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity != 0 {
			v1 = __ccgo_fp(_internalSubset)
		} else {
			v1 = __ccgo_fp(_externalSubset1)
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = v1
		return int32(_XML_ROLE_NOTATION_NO_SYSTEM_ID)
	}
	return _common(tls, state, tok)
}

func _attlist0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist1)
		return int32(_XML_ROLE_ATTLIST_ELEMENT_NAME)
	}
	return _common(tls, state, tok)
}

func _attlist1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	var v1 uintptr
	_ = v1
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_DECL_CLOSE):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity != 0 {
			v1 = __ccgo_fp(_internalSubset)
		} else {
			v1 = __ccgo_fp(_externalSubset1)
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = v1
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist2)
		return int32(_XML_ROLE_ATTRIBUTE_NAME)
	}
	return _common(tls, state, tok)
}

func _attlist2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	var i int32
	_ = i
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_NAME):
		i = 0
		for {
			if !(i < libc.Int32FromUint64(libc.Uint64FromInt64(64)/libc.Uint64FromInt64(8))) {
				break
			}
			if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, _types[i]) != 0 {
				(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist8)
				return int32(_XML_ROLE_ATTRIBUTE_TYPE_CDATA) + i
			}
			goto _1
		_1:
			;
			i++
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_NOTATION))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist5)
			return int32(_XML_ROLE_ATTLIST_NONE)
		}
	case int32(m_XML_TOK_OPEN_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist3)
		return int32(_XML_ROLE_ATTLIST_NONE)
	}
	return _common(tls, state, tok)
}

var _types = [8]uintptr{
	0: uintptr(unsafe.Pointer(&_KW_CDATA)),
	1: uintptr(unsafe.Pointer(&_KW_ID)),
	2: uintptr(unsafe.Pointer(&_KW_IDREF)),
	3: uintptr(unsafe.Pointer(&_KW_IDREFS)),
	4: uintptr(unsafe.Pointer(&_KW_ENTITY)),
	5: uintptr(unsafe.Pointer(&_KW_ENTITIES)),
	6: uintptr(unsafe.Pointer(&_KW_NMTOKEN)),
	7: uintptr(unsafe.Pointer(&_KW_NMTOKENS)),
}

func _attlist3(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_NMTOKEN):
		fallthrough
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist4)
		return int32(_XML_ROLE_ATTRIBUTE_ENUM_VALUE)
	}
	return _common(tls, state, tok)
}

func _attlist4(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_CLOSE_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist8)
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_OR):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist3)
		return int32(_XML_ROLE_ATTLIST_NONE)
	}
	return _common(tls, state, tok)
}

func _attlist5(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_OPEN_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist6)
		return int32(_XML_ROLE_ATTLIST_NONE)
	}
	return _common(tls, state, tok)
}

func _attlist6(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist7)
		return int32(_XML_ROLE_ATTRIBUTE_NOTATION_VALUE)
	}
	return _common(tls, state, tok)
}

func _attlist7(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_CLOSE_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist8)
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_OR):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist6)
		return int32(_XML_ROLE_ATTLIST_NONE)
	}
	return _common(tls, state, tok)
}

// C documentation
//
//	/* default value */
func _attlist8(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_POUND_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_IMPLIED))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist1)
			return int32(_XML_ROLE_IMPLIED_ATTRIBUTE_VALUE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_REQUIRED))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist1)
			return int32(_XML_ROLE_REQUIRED_ATTRIBUTE_VALUE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_FIXED))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist9)
			return int32(_XML_ROLE_ATTLIST_NONE)
		}
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist1)
		return int32(_XML_ROLE_DEFAULT_ATTRIBUTE_VALUE)
	}
	return _common(tls, state, tok)
}

func _attlist9(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ATTLIST_NONE)
	case int32(m_XML_TOK_LITERAL):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_attlist1)
		return int32(_XML_ROLE_FIXED_ATTRIBUTE_VALUE)
	}
	return _common(tls, state, tok)
}

func _element0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element1)
		return int32(_XML_ROLE_ELEMENT_NAME)
	}
	return _common(tls, state, tok)
}

func _element1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_EMPTY))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
			return int32(_XML_ROLE_CONTENT_EMPTY)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_ANY))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
			return int32(_XML_ROLE_CONTENT_ANY)
		}
	case int32(m_XML_TOK_OPEN_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element2)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Flevel = uint32(1)
		return int32(_XML_ROLE_GROUP_OPEN)
	}
	return _common(tls, state, tok)
}

func _element2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_POUND_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr+uintptr((*TENCODING)(unsafe.Pointer(enc)).FminBytesPerChar), end, uintptr(unsafe.Pointer(&_KW_PCDATA))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element3)
			return int32(_XML_ROLE_CONTENT_PCDATA)
		}
	case int32(m_XML_TOK_OPEN_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Flevel = uint32(2)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element6)
		return int32(_XML_ROLE_GROUP_OPEN)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT)
	case int32(m_XML_TOK_NAME_QUESTION):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT_OPT)
	case int32(m_XML_TOK_NAME_ASTERISK):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT_REP)
	case int32(m_XML_TOK_NAME_PLUS):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT_PLUS)
	}
	return _common(tls, state, tok)
}

func _element3(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_CLOSE_PAREN):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		return int32(_XML_ROLE_GROUP_CLOSE)
	case int32(m_XML_TOK_CLOSE_PAREN_ASTERISK):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		return int32(_XML_ROLE_GROUP_CLOSE_REP)
	case int32(m_XML_TOK_OR):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element4)
		return int32(_XML_ROLE_ELEMENT_NONE)
	}
	return _common(tls, state, tok)
}

func _element4(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element5)
		return int32(_XML_ROLE_CONTENT_ELEMENT)
	}
	return _common(tls, state, tok)
}

func _element5(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_CLOSE_PAREN_ASTERISK):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		return int32(_XML_ROLE_GROUP_CLOSE_REP)
	case int32(m_XML_TOK_OR):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element4)
		return int32(_XML_ROLE_ELEMENT_NONE)
	}
	return _common(tls, state, tok)
}

func _element6(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_OPEN_PAREN):
		*(*uint32)(unsafe.Pointer(state + 8)) += uint32(1)
		return int32(_XML_ROLE_GROUP_OPEN)
	case int32(m_XML_TOK_NAME):
		fallthrough
	case int32(m_XML_TOK_PREFIXED_NAME):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT)
	case int32(m_XML_TOK_NAME_QUESTION):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT_OPT)
	case int32(m_XML_TOK_NAME_ASTERISK):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT_REP)
	case int32(m_XML_TOK_NAME_PLUS):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element7)
		return int32(_XML_ROLE_CONTENT_ELEMENT_PLUS)
	}
	return _common(tls, state, tok)
}

func _element7(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_ELEMENT_NONE)
	case int32(m_XML_TOK_CLOSE_PAREN):
		*(*uint32)(unsafe.Pointer(state + 8)) -= uint32(1)
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).Flevel == uint32(0) {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		}
		return int32(_XML_ROLE_GROUP_CLOSE)
	case int32(m_XML_TOK_CLOSE_PAREN_ASTERISK):
		*(*uint32)(unsafe.Pointer(state + 8)) -= uint32(1)
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).Flevel == uint32(0) {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		}
		return int32(_XML_ROLE_GROUP_CLOSE_REP)
	case int32(m_XML_TOK_CLOSE_PAREN_QUESTION):
		*(*uint32)(unsafe.Pointer(state + 8)) -= uint32(1)
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).Flevel == uint32(0) {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		}
		return int32(_XML_ROLE_GROUP_CLOSE_OPT)
	case int32(m_XML_TOK_CLOSE_PAREN_PLUS):
		*(*uint32)(unsafe.Pointer(state + 8)) -= uint32(1)
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).Flevel == uint32(0) {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_declClose)
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none = int32(_XML_ROLE_ELEMENT_NONE)
		}
		return int32(_XML_ROLE_GROUP_CLOSE_PLUS)
	case int32(m_XML_TOK_COMMA):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element6)
		return int32(_XML_ROLE_GROUP_SEQUENCE)
	case int32(m_XML_TOK_OR):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_element6)
		return int32(_XML_ROLE_GROUP_CHOICE)
	}
	return _common(tls, state, tok)
}

func _condSect0(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_NAME):
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_INCLUDE))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_condSect1)
			return int32(_XML_ROLE_NONE)
		}
		if (*(*func(*libc.TLS, uintptr, uintptr, uintptr, uintptr) int32)(unsafe.Pointer(&struct{ uintptr }{(*TENCODING)(unsafe.Pointer(enc)).FnameMatchesAscii})))(tls, enc, ptr, end, uintptr(unsafe.Pointer(&_KW_IGNORE))) != 0 {
			(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_condSect2)
			return int32(_XML_ROLE_NONE)
		}
		break
	}
	return _common(tls, state, tok)
}

func _condSect1(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_OPEN_BRACKET):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_externalSubset1)
		*(*uint32)(unsafe.Pointer(state + 16)) += uint32(1)
		return int32(_XML_ROLE_NONE)
	}
	return _common(tls, state, tok)
}

func _condSect2(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return int32(_XML_ROLE_NONE)
	case int32(m_XML_TOK_OPEN_BRACKET):
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_externalSubset1)
		return int32(_XML_ROLE_IGNORE_SECT)
	}
	return _common(tls, state, tok)
}

func _declClose(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	var v1 uintptr
	_ = v1
	_ = ptr
	_ = end
	_ = enc
	switch tok {
	case int32(m_XML_TOK_PROLOG_S):
		return (*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none
	case int32(m_XML_TOK_DECL_CLOSE):
		if (*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity != 0 {
			v1 = __ccgo_fp(_internalSubset)
		} else {
			v1 = __ccgo_fp(_externalSubset1)
		}
		(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = v1
		return (*TPROLOG_STATE)(unsafe.Pointer(state)).Frole_none
	}
	return _common(tls, state, tok)
}

// C documentation
//
//	/* This function will only be invoked if the internal logic of the
//	 * parser has broken down.  It is used in two cases:
//	 *
//	 * 1: When the XML prolog has been finished.  At this point the
//	 * processor (the parser level above these role handlers) should
//	 * switch from prologProcessor to contentProcessor and reinitialise
//	 * the handler function.
//	 *
//	 * 2: When an error has been detected (via common() below).  At this
//	 * point again the processor should be switched to errorProcessor,
//	 * which will never call a handler.
//	 *
//	 * The result of this is that error() can only be called if the
//	 * processor switch failed to happen, which is an internal error and
//	 * therefore we shouldn't be able to provoke it simply by using the
//	 * library.  It is a necessary backstop, however, so we merely exclude
//	 * it from the coverage statistics.
//	 *
//	 * LCOV_EXCL_START
//	 */
func _error(tls *libc.TLS, state uintptr, tok int32, ptr uintptr, end uintptr, enc uintptr) (r int32) {
	_ = state
	_ = tok
	_ = ptr
	_ = end
	_ = enc
	return int32(_XML_ROLE_NONE)
}

/* LCOV_EXCL_STOP */

func _common(tls *libc.TLS, state uintptr, tok int32) (r int32) {
	if !((*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity != 0) && tok == int32(m_XML_TOK_PARAM_ENTITY_REF) {
		return int32(_XML_ROLE_INNER_PARAM_ENTITY_REF)
	}
	(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_error)
	return int32(_XML_ROLE_ERROR)
}

func XXmlPrologStateInit(tls *libc.TLS, state uintptr) {
	(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_prolog0)
	(*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity = int32(1)
	(*TPROLOG_STATE)(unsafe.Pointer(state)).FincludeLevel = uint32(0)
	(*TPROLOG_STATE)(unsafe.Pointer(state)).FinEntityValue = 0
}

func XXmlPrologStateInitExternalEntity(tls *libc.TLS, state uintptr) {
	(*TPROLOG_STATE)(unsafe.Pointer(state)).Fhandler = __ccgo_fp(_externalSubset0)
	(*TPROLOG_STATE)(unsafe.Pointer(state)).FdocumentEntity = 0
	(*TPROLOG_STATE)(unsafe.Pointer(state)).FincludeLevel = uint32(0)
}

func __ccgo_fp(f interface{}) uintptr {
	type iface [2]uintptr
	return (*iface)(unsafe.Pointer(&f))[1]
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\x00\x01\x02\x03\x04\x05\x06\a\b\t\n\v\f\r\x0e\x0f\x00/dev/urandom\x00EXPAT_ENTROPY_DEBUG\x001\x00Entropy: %s --> 0x%0*lx (%lu bytes)\n\x00getrandom\x00fallback(4)\x00fallback(8)\x00out of memory\x00syntax error\x00no element found\x00not well-formed (invalid token)\x00unclosed token\x00partial character\x00mismatched tag\x00duplicate attribute\x00junk after document element\x00illegal parameter entity reference\x00undefined entity\x00recursive entity reference\x00asynchronous entity\x00reference to invalid character number\x00reference to binary entity\x00reference to external entity in attribute\x00XML or text declaration not at start of entity\x00unknown encoding\x00encoding specified in XML declaration is incorrect\x00unclosed CDATA section\x00error in processing external entity reference\x00document is not standalone\x00unexpected parser state - please send a bug report\x00entity declared in parameter entity\x00requested feature requires XML_DTD support in Expat\x00cannot change setting once parsing has begun\x00unbound prefix\x00must not undeclare prefix\x00incomplete markup in parameter entity\x00XML declaration not well-formed\x00text declaration not well-formed\x00illegal character(s) in public id\x00parser suspended\x00parser not suspended\x00parsing aborted\x00parsing finished\x00cannot suspend in external parameter entity\x00reserved prefix (xml) must not be undeclared or bound to another namespace name\x00reserved prefix (xmlns) must not be declared or undeclared\x00prefix must not be bound to one of the reserved namespace names\x00invalid argument\x00expat_2.2.10\x00sizeof(XML_Char)\x00sizeof(XML_LChar)\x00XML_DTD\x00XML_CONTEXT_BYTES\x00XML_NS\x00"
